#version 150

in vec4 vertexColor;
in vec2 texCoord0;
in vec4 normal;
in vec4 snowDirection;
in vec4 lightMapColor;
in vec4 vertexPos;

out vec4 fragColor;

uniform vec4 ColorModulator;
uniform sampler2D Sampler0;
uniform vec4 SnowColor;
uniform mat3 IViewRotMat;

void main(){
    vec4 color = texture(Sampler0, texCoord0) * vertexColor * ColorModulator;
    if(color.a < 0.1){
        discard;
    }

    if(dot(snowDirection, normal) > 0.7){
        color = SnowColor;
    }
    color *= lightMapColor;

    fragColor = color;
}