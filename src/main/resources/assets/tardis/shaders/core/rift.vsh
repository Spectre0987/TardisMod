#version 150

#moj_import <projection.glsl>

in vec3 Position;
in vec2 UV;

uniform mat4 ModelViewMat;
uniform mat4 ProjMat;
uniform mat3 IViewRotMat;

out vec2 tex0;
out vec4 projTex;


void main(){

    gl_Position = ProjMat * ModelViewMat * vec4(Position, 1);
    tex0 = UV;
    projTex = projection_from_position(gl_Position);

}