#version 150

uniform float GameTime;
uniform sampler2D sampler0;

in float alpha;

out vec4 fragColor;


void main(){

    fragColor = vec4(1.0, 1.0, 1.0, alpha);

}