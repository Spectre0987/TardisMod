package net.tardis.mod.tags;

import net.minecraft.core.registries.Registries;
import net.minecraft.tags.TagKey;
import net.minecraft.world.level.biome.Biome;
import net.tardis.mod.helpers.Helper;

public class BiomeTags {

    public static final TagKey<Biome> TARDIS_GEN_BLACKLIST = create("tardis_gen_blacklist");

    public static TagKey<Biome> create(String name){
        return TagKey.create(Registries.BIOME, Helper.createRL(name));
    }

}
