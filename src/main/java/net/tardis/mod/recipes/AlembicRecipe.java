package net.tardis.mod.recipes;

import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import net.minecraft.advancements.CriterionTriggerInstance;
import net.minecraft.core.RegistryAccess;
import net.minecraft.data.recipes.FinishedRecipe;
import net.minecraft.network.FriendlyByteBuf;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.tags.TagKey;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.Items;
import net.minecraft.world.item.crafting.*;
import net.minecraft.world.level.ItemLike;
import net.minecraft.world.level.Level;
import net.minecraftforge.fluids.FluidStack;
import net.minecraftforge.fluids.capability.IFluidHandler;
import net.minecraftforge.fluids.capability.templates.FluidTank;
import net.minecraftforge.registries.ForgeRegistries;
import net.tardis.mod.Tardis;
import net.tardis.mod.blockentities.crafting.AlembicBlockEntity;
import org.jetbrains.annotations.Nullable;

import java.util.function.Consumer;

public class AlembicRecipe implements Recipe<ItemStackHandlerContinerWrapper<AlembicBlockEntity>> {

    private final ResourceLocation id;
    private final FluidStack result;
    private final Ingredient ingredient;
    private final FluidStack fluid;
    private final int craftingTime;

    public AlembicRecipe(ResourceLocation id, FluidStack result, Ingredient ingredient, FluidStack fluidIngredient, int craftingTime){
        this.id = id;
        this.result = result;
        this.ingredient = ingredient;
        this.fluid = fluidIngredient;
        this.craftingTime = craftingTime;
    }


    public Ingredient getIngredient(){
        return this.ingredient;
    }

    public FluidStack getResult(){
        return this.result;
    }

    public FluidStack getCraftingFluid(){
        return this.fluid;
    }

    public int timeToCraft(){
        return this.craftingTime;
    }

    @Override
    public boolean matches(ItemStackHandlerContinerWrapper<AlembicBlockEntity> container, Level level) {
        if(this.ingredient.test(container.getItem(2))){ //If item is correct
            //If we have enough fluid in the crafting tank to create this
            FluidTank craftingTank = container.getObject().getCraftingTank();
            FluidTank resultTank = container.getObject().getResultTank();
            if(this.fluid.containsFluid(craftingTank.drain(this.fluid, IFluidHandler.FluidAction.SIMULATE))){
                //If there is room in the result tank to make some amount of fluid
                if(resultTank.fill(this.result, IFluidHandler.FluidAction.SIMULATE) > 0){
                    return true;
                }
            }
        }
        return false;
    }

    @Override
    public ItemStack assemble(ItemStackHandlerContinerWrapper<AlembicBlockEntity> container, RegistryAccess registryAccess) {

        container.getObject().getCraftingTank().drain(this.fluid, IFluidHandler.FluidAction.EXECUTE);
        container.getObject().getResultTank().fill(this.result, IFluidHandler.FluidAction.EXECUTE);
        container.getObject().inventory.extractItem(2, 1, false); //Shrink the item ingredient

        return ItemStack.EMPTY;
    }

    @Override
    public boolean canCraftInDimensions(int i, int i1) {
        return true;
    }

    @Override
    public ItemStack getResultItem(RegistryAccess registryAccess) {
        return new ItemStack(Items.GLASS_BOTTLE);
    }

    @Override
    public ResourceLocation getId() {
        return this.id;
    }

    @Override
    public RecipeSerializer<?> getSerializer() {
        return RecipeRegistry.ALEMBIC_SERIALIZER.get();
    }

    @Override
    public RecipeType<?> getType() {
        return RecipeRegistry.ALEMBIC_TYPE;
    }

    public static class Serializer implements RecipeSerializer<AlembicRecipe>{

        @Override
        public AlembicRecipe fromJson(ResourceLocation id, JsonObject json) {
            try{
                JsonObject resultJson = json.getAsJsonObject("result");
                JsonObject fluidJson = json.getAsJsonObject("required_fluid");

                FluidStack resultStack = readStackFromJson(resultJson);
                FluidStack requiredStack = readStackFromJson(fluidJson);
                Ingredient ing = Ingredient.fromJson(json.get("ingredient"));

                return new AlembicRecipe(id, resultStack, ing, requiredStack, json.get("crafting_time").getAsInt());

            }
            catch(Exception e){}
            return null;
        }

        public FluidStack readStackFromJson(JsonObject object){
            if(object.has("fluid") && object.has("amount")){
                final ResourceLocation fluidKey = new ResourceLocation(object.get("fluid").getAsString());
                return new FluidStack(ForgeRegistries.FLUIDS.getValue(fluidKey), object.get("amount").getAsInt());
            }
            return FluidStack.EMPTY;
        }

        @Override
        public @Nullable AlembicRecipe fromNetwork(ResourceLocation id, FriendlyByteBuf buf) {
            FluidStack result = buf.readFluidStack();
            FluidStack required = buf.readFluidStack();
            Ingredient ing = Ingredient.fromNetwork(buf);
            int time = buf.readInt();
            return new AlembicRecipe(id, result, ing, required, time);
        }

        @Override
        public void toNetwork(FriendlyByteBuf buf, AlembicRecipe recipe) {
            buf.writeFluidStack(recipe.result);
            buf.writeFluidStack(recipe.fluid);
            recipe.ingredient.toNetwork(buf);
            buf.writeInt(recipe.craftingTime);
        }
    }

    public static class Builder extends BasicRecipeBuilder{

        FluidStack result = FluidStack.EMPTY;
        FluidStack required = FluidStack.EMPTY;
        Ingredient ingredient = Ingredient.EMPTY;
        int craftingTime = 200;

        private Builder(ResourceLocation id) {
            super(id);
        }

        @Override
        public void save(Consumer<FinishedRecipe> pFinishedRecipeConsumer, RecipeSerializer<?> serializer) {
            pFinishedRecipeConsumer.accept(new BasicFinishedRecipe<>(this, serializer));
        }

        @Override
        public JsonObject createJson(JsonObject root) {
            root.add("result", createFluidStackJson(this.result));
            root.add("required_fluid", createFluidStackJson(this.required));
            root.add("ingredient", this.ingredient.toJson());
            root.add("crafting_time", new JsonPrimitive(this.craftingTime));
            return root;
        }

        public static Builder create(ResourceLocation id) {
            return new Builder(id);
        }

        public Builder unlockedBy(String pCriterionName, CriterionTriggerInstance pCriterionTrigger) {
            this.advancement.addCriterion(pCriterionName, pCriterionTrigger);
            return this;
        }

        public Builder withResult(FluidStack stack) {
            this.result = stack;
            return this;
        }

        public Builder withRequiredFluid(FluidStack stack) {
            this.required = stack;
            return this;
        }

        public Builder ingredient(Ingredient ing) {

            if (this.ingredient != null && !ingredient.isEmpty()) {
                Tardis.LOGGER.log(org.apache.logging.log4j.Level.ERROR, "Error in recipe %s: Recipe can only have one ItemLike ingredient!".formatted(this.id));
            }

            this.ingredient = ing;
            return this;
        }

        public Builder ingredient(ItemStack... ing) {
            return ingredient(Ingredient.of(ing));
        }

        public Builder ingredient(ItemLike... ing) {
            return ingredient(Ingredient.of(ing));
        }

        public Builder ingredient(TagKey<Item> ing) {
            return ingredient(Ingredient.of(ing));
        }

        public Builder time(int time){
            this.craftingTime = time;
            return this;
        }

        private JsonObject createFluidStackJson(FluidStack stack) {
            final JsonObject obj = new JsonObject();
            obj.add("fluid", new JsonPrimitive(ForgeRegistries.FLUIDS.getKey(stack.getFluid()).toString()));
            obj.add("amount", new JsonPrimitive(stack.getAmount()));
            return obj;
        }
    }
}
