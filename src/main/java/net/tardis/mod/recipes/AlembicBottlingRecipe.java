package net.tardis.mod.recipes;

import com.google.gson.JsonObject;
import com.mojang.serialization.Codec;
import com.mojang.serialization.codecs.RecordCodecBuilder;
import net.minecraft.advancements.CriterionTriggerInstance;
import net.minecraft.core.RegistryAccess;
import net.minecraft.data.recipes.FinishedRecipe;
import net.minecraft.network.FriendlyByteBuf;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.tags.TagKey;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.Items;
import net.minecraft.world.item.crafting.Ingredient;
import net.minecraft.world.item.crafting.Recipe;
import net.minecraft.world.item.crafting.RecipeSerializer;
import net.minecraft.world.item.crafting.RecipeType;
import net.minecraft.world.level.ItemLike;
import net.minecraft.world.level.Level;
import net.minecraftforge.fluids.FluidStack;
import net.minecraftforge.fluids.capability.IFluidHandler;
import net.tardis.mod.blockentities.crafting.AlembicBlockEntity;
import net.tardis.mod.helpers.CodecHelper;
import net.tardis.mod.helpers.JsonHelper;
import org.jetbrains.annotations.Nullable;

import java.util.function.Consumer;

public record AlembicBottlingRecipe(ResourceLocation id, FluidStack ingredient, Ingredient container, ItemStack result) implements Recipe<ItemStackHandlerContinerWrapper<AlembicBlockEntity>> {

    @Override
    public boolean matches(ItemStackHandlerContinerWrapper<AlembicBlockEntity> pContainer, Level pLevel) {

        //If the alembic has this fluid
        if(pContainer.getObject().getResultTank().getFluid().containsFluid(this.ingredient)){
            //If we have a valid container
            if(this.container.test(pContainer.getItem(4))) {
                //If we can stack the result
                if(pContainer.getObject().inventory.insertItem(5, this.result, true) == ItemStack.EMPTY) {
                    return true;
                }
            }
        }
        return false;
    }

    @Override
    public ItemStack assemble(ItemStackHandlerContinerWrapper<AlembicBlockEntity> alembic, RegistryAccess reg) {
        alembic.getObject().getResultTank().drain(this.ingredient, IFluidHandler.FluidAction.EXECUTE);
        alembic.getObject().getInventory().extractItem(4, 1, false);
        return this.result.copy();
    }

    @Override
    public boolean canCraftInDimensions(int pWidth, int pHeight) {
        return true;
    }

    @Override
    public ItemStack getResultItem(RegistryAccess p_267052_) {
        return this.result;
    }

    @Override
    public ResourceLocation getId() {
        return this.id;
    }

    @Override
    public RecipeSerializer<?> getSerializer() {
        return RecipeRegistry.ALEMBIC_BOTTLE_RECIPE.get();
    }

    @Override
    public RecipeType<?> getType() {
        return RecipeRegistry.ALEMBIC_BOTTLING_TYPE;
    }

    public static class Serializer implements RecipeSerializer<AlembicBottlingRecipe>{

        @Override
        public AlembicBottlingRecipe fromJson(ResourceLocation id, JsonObject json) {

            return new AlembicBottlingRecipe(id, JsonHelper.readFluidStack(json.getAsJsonObject("ingredient")),
                    Ingredient.fromJson(json.getAsJsonObject("container")),
                    JsonHelper.readItemStack(json.get("result").getAsJsonObject()));
        }

        @Override
        public @Nullable AlembicBottlingRecipe fromNetwork(ResourceLocation id, FriendlyByteBuf buf) {
            return new AlembicBottlingRecipe(id, buf.readFluidStack(), Ingredient.fromNetwork(buf), buf.readItem());
        }

        @Override
        public void toNetwork(FriendlyByteBuf buf, AlembicBottlingRecipe recipe) {
            buf.writeFluidStack(recipe.ingredient);
            recipe.container.toNetwork(buf);
            buf.writeItem(recipe.result);
        }
    }

    public static class Builder extends BasicRecipeBuilder{

        private final ItemStack result;
        private FluidStack stack = FluidStack.EMPTY;
        private Ingredient bottle = Ingredient.of(Items.GLASS_BOTTLE);

        public Builder(ResourceLocation id, ItemStack result) {
            super(id);
            this.result = result;
        }

        public static Builder create(ResourceLocation id, ItemStack item){
            return new Builder(id, item);
        }

        public static Builder create(ResourceLocation id, ItemLike item){
            return create(id, new ItemStack(item));
        }

        public Builder fluid(FluidStack stack){
            this.stack = stack;
            return this;
        }
        public Builder container(Ingredient stack){
            this.bottle = stack;
            return this;
        }

        public Builder container(TagKey<Item> ing){
            return container(Ingredient.of(ing));
        }

        public Builder container(ItemLike ing){
            return container(Ingredient.of(ing));
        }

        public Builder unlockedBy(String name, CriterionTriggerInstance instance){
            this.advancement.addCriterion(name, instance);
            return this;
        }

        @Override
        public void save(Consumer<FinishedRecipe> pFinishedRecipeConsumer, RecipeSerializer<?> serializer) {
            pFinishedRecipeConsumer.accept(new BasicFinishedRecipe<>(this, serializer));
        }

        public void save(Consumer<FinishedRecipe> recipe){
            save(recipe, RecipeRegistry.ALEMBIC_BOTTLE_RECIPE.get());
        }

        @Override
        public JsonObject createJson(JsonObject root) {
            root.add("ingredient", JsonHelper.writeFluidStack(this.stack));
            root.add("container", this.bottle.toJson());
            root.add("result", JsonHelper.writeItemStack(this.result));
            return root;
        }
    }
}
