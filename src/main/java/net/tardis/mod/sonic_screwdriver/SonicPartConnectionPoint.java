package net.tardis.mod.sonic_screwdriver;

import com.mojang.serialization.Codec;
import com.mojang.serialization.codecs.RecordCodecBuilder;
import net.minecraft.resources.ResourceLocation;
import net.tardis.mod.helpers.CodecHelper;
import org.joml.Vector3f;

import java.util.Map;
import java.util.Optional;

public record SonicPartConnectionPoint(SonicPartSlot partSlot, ResourceLocation model, Map<SonicPartSlot, Vector3f> connectionPoints) {

    public static final Codec<Map<SonicPartSlot, Vector3f>> CONNECTION_CODEC = Codec.unboundedMap(SonicPartSlot.CODEC, CodecHelper.VEC3F_CODEC);

    public static final Codec<SonicPartConnectionPoint> CODEC = RecordCodecBuilder.create(instance ->
                instance.group(
                        SonicPartSlot.CODEC.fieldOf("part_slot").forGetter(SonicPartConnectionPoint::partSlot),
                        ResourceLocation.CODEC.fieldOf("model").forGetter(SonicPartConnectionPoint::model),
                        CONNECTION_CODEC.fieldOf("connection_points").forGetter(SonicPartConnectionPoint::connectionPoints)
                ).apply(instance, SonicPartConnectionPoint::new)
            );



    public Optional<Vector3f> getAttachPointFor(SonicPartSlot other){
        if(this.connectionPoints().containsKey(other)){
            return Optional.of(this.connectionPoints.get(other));
        }
        return Optional.empty();
    }

}
