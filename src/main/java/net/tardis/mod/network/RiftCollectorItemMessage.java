package net.tardis.mod.network;

import net.minecraft.core.BlockPos;
import net.minecraft.network.FriendlyByteBuf;
import net.minecraftforge.items.ItemStackHandler;
import net.minecraftforge.network.NetworkEvent;
import net.tardis.mod.Tardis;
import net.tardis.mod.blockentities.machines.RiftCollectorTile;

import java.util.function.Supplier;

public record RiftCollectorItemMessage(BlockPos pos, ItemStackHandler inv) {

    public static void encode(RiftCollectorItemMessage mes, FriendlyByteBuf buf){
        buf.writeBlockPos(mes.pos());
        buf.writeInt(mes.inv().getSlots());
        for(int i = 0; i < mes.inv().getSlots(); ++i){
            buf.writeItem(mes.inv().getStackInSlot(i));
        }
    }

    public static RiftCollectorItemMessage decode(FriendlyByteBuf buf){
        final BlockPos pos = buf.readBlockPos();
        final int size = buf.readInt();
        final ItemStackHandler inv = new ItemStackHandler(size);
        for(int i = 0; i < size; ++i){
            inv.setStackInSlot(i, buf.readItem());
        }
        return new RiftCollectorItemMessage(pos, inv);
    }

    public static void handle(RiftCollectorItemMessage mes, Supplier<NetworkEvent.Context> context){
        context.get().setPacketHandled(true);
        context.get().enqueueWork(() -> {
            Tardis.SIDE.getClientLevel().ifPresent(level -> {
                if(level.getBlockEntity(mes.pos()) instanceof RiftCollectorTile col){
                    for(int i = 0; i < col.getInventory().getSlots(); ++i){
                        col.getInventory().setStackInSlot(i, mes.inv().getStackInSlot(i));
                    }
                }
            });
        });
    }

}
