package net.tardis.mod.network;

import net.minecraft.core.Vec3i;
import net.minecraft.network.FriendlyByteBuf;
import net.minecraft.resources.ResourceKey;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.server.level.ServerPlayer;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.chunk.LevelChunk;
import net.minecraftforge.network.NetworkEvent;
import net.minecraftforge.network.NetworkRegistry;
import net.minecraftforge.network.PacketDistributor;
import net.minecraftforge.network.simple.SimpleChannel;
import net.tardis.mod.helpers.Helper;
import net.tardis.mod.network.packets.*;

import java.util.function.BiConsumer;
import java.util.function.Function;
import java.util.function.Supplier;

public class Network {

    public static final String NET_VERSION = "1.0";
    public static final SimpleChannel INSTANCE = NetworkRegistry.newSimpleChannel(Helper.createRL("main"), () -> NET_VERSION, NET_VERSION::equals, NET_VERSION::equals);

    public static int ID = 0;

    public static void registerPackets(){
        register(SyncDimensionListMessage.class, SyncDimensionListMessage::encode, SyncDimensionListMessage::decode, SyncDimensionListMessage::handle);
        register(ControlDataMessage.class, ControlDataMessage::encode, ControlDataMessage::decode, ControlDataMessage::handle);
        register(ControlEntitySizeMessage.class, ControlEntitySizeMessage::encode, ControlEntitySizeMessage::decode, ControlEntitySizeMessage::handle);
        register(TardisFlightStateMessage.class, TardisFlightStateMessage::encode, TardisFlightStateMessage::decode, TardisFlightStateMessage::handle);
        register(UpdateDoorStateMessage.class, UpdateDoorStateMessage::encode, UpdateDoorStateMessage::decode, UpdateDoorStateMessage::handle);
        register(TardisLoadMessage.class, TardisLoadMessage::encode, TardisLoadMessage::decode, TardisLoadMessage::handle);
        register(TardisUpdateMessage.class, TardisUpdateMessage::encode, TardisUpdateMessage::decode, TardisUpdateMessage::handle);
        register(MonitorActionMessage.class, MonitorActionMessage::encode, MonitorActionMessage::decode, MonitorActionMessage::handle);
        register(ToggleEngineSlotMessage.class, ToggleEngineSlotMessage::encode, ToggleEngineSlotMessage::decode, ToggleEngineSlotMessage::handle);
        register(OpenGuiDataMessage.class, OpenGuiDataMessage::encode, OpenGuiDataMessage::decode, OpenGuiDataMessage::handle);
        register(ChangeExteriorMessage.class, ChangeExteriorMessage::encode, ChangeExteriorMessage::decode, ChangeExteriorMessage::handle);
        register(UpdateTardisEngineContents.class, UpdateTardisEngineContents::encode, UpdateTardisEngineContents::decode, UpdateTardisEngineContents::handle);
        register(ItemFunctionMessage.class, ItemFunctionMessage::encode, ItemFunctionMessage::decode, ItemFunctionMessage::handle);
        register(StartShakeMessage.class, StartShakeMessage::encode, StartShakeMessage::decode, StartShakeMessage::handle);
        register(SetTardisNameMessage.class, SetTardisNameMessage::encode, SetTardisNameMessage::decode, SetTardisNameMessage::handle);
        register(ExteriorMatterStateMessage.class, ExteriorMatterStateMessage::encode, ExteriorMatterStateMessage::decode, ExteriorMatterStateMessage::handle);
        register(SetDematAnimMessage.class, SetDematAnimMessage::encode, SetDematAnimMessage::decode, SetDematAnimMessage::handle);
        register(PlayMovingSoundMessage.class, PlayMovingSoundMessage::encode, PlayMovingSoundMessage::decode, PlayMovingSoundMessage::handle);
        register(TelepathicMessage.class, TelepathicMessage::encode, TelepathicMessage::decode, TelepathicMessage::handle);
        register(SonicSetFunctionMessage.class, SonicSetFunctionMessage::encode, SonicSetFunctionMessage::decode, SonicSetFunctionMessage::handle);
        register(PlayParticleMessage.class, PlayParticleMessage::encode, PlayParticleMessage::decode, PlayParticleMessage::handle);
        register(LoadChunkCapabilityMessage.class, LoadChunkCapabilityMessage::encode, LoadChunkCapabilityMessage::decode, LoadChunkCapabilityMessage::handle);
        register(DimenionNameMessage.class, DimenionNameMessage::encode, DimenionNameMessage::decode, DimenionNameMessage::handle);
        register(ItemTrackingCoordMessage.class, ItemTrackingCoordMessage::encode, ItemTrackingCoordMessage::decode, ItemTrackingCoordMessage::handle);
        register(TardisLightMessage.class, TardisLightMessage::encode, TardisLightMessage::decode, TardisLightMessage::handle);
        register(UpdateTARDISNotifMessage.class, UpdateTARDISNotifMessage::encode, UpdateTARDISNotifMessage::decode, UpdateTARDISNotifMessage::handle);
        register(ARSSpawnItemMessage.class, ARSSpawnItemMessage::encode, ARSSpawnItemMessage::decode, ARSSpawnItemMessage::handle);
        register(RiftMessage.class, RiftMessage::encode, RiftMessage::decode, RiftMessage::handle);
        register(SoundSchemeMessage.class, SoundSchemeMessage::encode, SoundSchemeMessage::decode, SoundSchemeMessage::handle);
        register(BrokenExteriorTraitsMessage.class, BrokenExteriorTraitsMessage::encode, BrokenExteriorTraitsMessage::decode, BrokenExteriorTraitsMessage::handle);
        register(QuantiscopeModeChangeMessage.class, QuantiscopeModeChangeMessage::encode, QuantiscopeModeChangeMessage::decode, QuantiscopeModeChangeMessage::handle);
        register(SonicUpgradeQuantiscopeMessage.class, SonicUpgradeQuantiscopeMessage::encode, SonicUpgradeQuantiscopeMessage::decode, SonicUpgradeQuantiscopeMessage::handle);
        register(SonicChangeModelMessage.class, SonicChangeModelMessage::encode, SonicChangeModelMessage::decode, SonicChangeModelMessage::handle);
        register(AlembicFluidChangeMessage.class, AlembicFluidChangeMessage::encode, AlembicFluidChangeMessage::decode, AlembicFluidChangeMessage::handle);
        register(RiftArtronMessage.class, RiftArtronMessage::encode, RiftArtronMessage::decode, RiftArtronMessage::handle);
        register(SaveWaypointMessage.class, SaveWaypointMessage::encode, SaveWaypointMessage::decode, SaveWaypointMessage::handle);
        register(LoadWaypointMessage.class, LoadWaypointMessage::encode, LoadWaypointMessage::decode, LoadWaypointMessage::handle);
        register(UpdateChameleonDisguseMessage.class, UpdateChameleonDisguseMessage::encode, UpdateChameleonDisguseMessage::decode, UpdateChameleonDisguseMessage::handle);
        register(DriveMessage.class, DriveMessage::encode, DriveMessage::decode, DriveMessage::handle);
        register(DoorHandlerUpdateEntityMessage.class, DoorHandlerUpdateEntityMessage::encode, DoorHandlerUpdateEntityMessage::decode, DoorHandlerUpdateEntityMessage::handle);
        register(EntityMatterStateMessage.class, EntityMatterStateMessage::encode, EntityMatterStateMessage::decode, EntityMatterStateMessage::handle);
        register(UpdateShellMessage.class, UpdateShellMessage::encode, UpdateShellMessage::decode, UpdateShellMessage::handle);
        register(POSItemClearMessage.class, POSItemClearMessage::encode, POSItemClearMessage::decode, POSItemClearMessage::handle);
        register(MatterBufferUpdateInvMessage.class, MatterBufferUpdateInvMessage::encode, MatterBufferUpdateInvMessage::decode, MatterBufferUpdateInvMessage::handle);
        register(ChangeARSRoomMessage.class, ChangeARSRoomMessage::encode, ChangeARSRoomMessage::decode, ChangeARSRoomMessage::handle);
        register(SonicBlockNamingMessage.class, SonicBlockNamingMessage::encode, SonicBlockNamingMessage::decode, SonicBlockNamingMessage::handle);
        register(SetMainAtriumMessage.class, SetMainAtriumMessage::encode, SetMainAtriumMessage::decode, SetMainAtriumMessage::handle);
        register(UpdateDynamicMonitorBounds.class, UpdateDynamicMonitorBounds::encode, UpdateDynamicMonitorBounds::decode, UpdateDynamicMonitorBounds::handle);
        register(FabricatorSetCraftingItemMessage.class, FabricatorSetCraftingItemMessage::encode, FabricatorSetCraftingItemMessage::decode, FabricatorSetCraftingItemMessage::handle);
        register(RiftCollectorItemMessage.class, RiftCollectorItemMessage::encode, RiftCollectorItemMessage::decode, RiftCollectorItemMessage::handle);
        register(TeleportTubeEntityMessage.class, TeleportTubeEntityMessage::encode, TeleportTubeEntityMessage::decode, TeleportTubeEntityMessage::handle);
        register(UpdateTextureVariantMessage.class, UpdateTextureVariantMessage::encode, UpdateTextureVariantMessage::decode, UpdateTextureVariantMessage::handle);
        register(DeleteWaypointMessage.class, DeleteWaypointMessage::encode, DeleteWaypointMessage::decode, DeleteWaypointMessage::handle);
        register(AttumentProgressMessage.class, AttumentProgressMessage::encode, AttumentProgressMessage::decode, AttumentProgressMessage::handle);
    }

    public static <T> void register(Class<T> clazz, BiConsumer<T, FriendlyByteBuf> encoder, Function<FriendlyByteBuf, T> decoder, BiConsumer<T, Supplier<NetworkEvent.Context>> handler){
        INSTANCE.registerMessage(id(), clazz, encoder, decoder, handler);
    }

    public static void sendNear(ResourceKey<Level> level, Vec3i pos, double range, Object message){
        INSTANCE.send(PacketDistributor.NEAR.with(() -> new PacketDistributor.TargetPoint(
                pos.getX(), pos.getY(), pos.getZ(), range,  level
        )), message);
    }

    public static void sendPacketToAll(Object message){
        Network.INSTANCE.send(PacketDistributor.ALL.noArg(), message);
    }

    public static void sendPacketToDimension(ResourceKey<Level> level, Object mes){
        INSTANCE.send(PacketDistributor.DIMENSION.with(() -> level), mes);
    }

    public static void sendToTracking(Entity e, Object mes) {
        if(e.getLevel() instanceof ServerLevel)
            INSTANCE.send(PacketDistributor.TRACKING_ENTITY_AND_SELF.with(() -> e), mes);
    }

    public static void sendToTracking(BlockEntity tile, Object mes){
        if(tile.getLevel() instanceof ServerLevel)
            INSTANCE.send(PacketDistributor.TRACKING_CHUNK.with(() -> tile.getLevel().getChunkAt(tile.getBlockPos())), mes);
    }

    public static void sendToTracking(LevelChunk chunk, Object mes){
        if(chunk.getLevel() instanceof ServerLevel)
            INSTANCE.send(PacketDistributor.TRACKING_CHUNK.with(() -> chunk), mes);
    }

    public static void sendTo(ServerPlayer player, Object mes){
        INSTANCE.send(PacketDistributor.PLAYER.with(() -> player), mes);
    }

    public static void sendToServer(Object mes) {
        INSTANCE.sendToServer(mes);
    }

    public static int id(){
        return ++ID;
    }
}
