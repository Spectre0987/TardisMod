package net.tardis.mod.network.packets.tardis;

import net.minecraft.network.FriendlyByteBuf;
import net.tardis.mod.cap.level.ITardisLevel;
import net.tardis.mod.cap.level.TardisCap;
import net.tardis.mod.helpers.Helper;
import net.tardis.mod.misc.SpaceTimeCoord;
import net.tardis.mod.misc.tardis.FlightCourse;

import java.util.Optional;

public class UpdateDataLocation extends TardisData {

    private SpaceTimeCoord location;
    private SpaceTimeCoord destination;
    private Optional<FlightCourse> course = Optional.empty();



    public UpdateDataLocation(int id){
        super(id);
    }

    @Override
    public void serialize(FriendlyByteBuf buf) {
        this.location.encode(buf);
        this.destination.encode(buf);
        Helper.encodeOptional(buf, this.course, FlightCourse::encode);
    }

    @Override
    public void deserialize(FriendlyByteBuf buf) {
        this.location = SpaceTimeCoord.decode(buf);
        this.destination = SpaceTimeCoord.decode(buf);
        this.course = Helper.decodeOptional(buf, FlightCourse::new);
    }

    @Override
    public void apply(ITardisLevel tardis) {
        tardis.setLocation(this.location);
        tardis.setDestination(this.destination);
        tardis.setFlightCourse(this.course.isPresent() ? this.course.get() : null);
    }

    @Override
    public void createFromTardis(ITardisLevel tardis) {
        this.location = tardis.getLocation();
        this.destination = tardis.getDestination();
        this.course = tardis.getCurrentCourse();
    }
}
