package net.tardis.mod.network.packets;

import net.minecraft.network.FriendlyByteBuf;
import net.minecraft.world.InteractionHand;
import net.minecraftforge.network.NetworkEvent;
import net.tardis.mod.cap.Capabilities;
import net.tardis.mod.cap.items.functions.ItemFunctionRegistry;
import net.tardis.mod.cap.items.functions.sonic.SonicFunctionType;

import java.util.function.Supplier;

public record SonicSetFunctionMessage(InteractionHand hand, SonicFunctionType function) {

    public static void encode(SonicSetFunctionMessage mes, FriendlyByteBuf buf){
        buf.writeEnum(mes.hand());
        buf.writeRegistryId(ItemFunctionRegistry.REGISTRY.get(), mes.function());
    }

    public static SonicSetFunctionMessage decode(FriendlyByteBuf buf){
        return new SonicSetFunctionMessage(buf.readEnum(InteractionHand.class), buf.readRegistryId());
    }

    public static void handle(SonicSetFunctionMessage mes, Supplier<NetworkEvent.Context> context){
        context.get().setPacketHandled(true);
        context.get().enqueueWork(() -> {
            context.get().getSender().getItemInHand(mes.hand()).getCapability(Capabilities.SONIC).ifPresent(sonic -> {
                sonic.setFunction(mes.function());
            });
        });
    }

}
