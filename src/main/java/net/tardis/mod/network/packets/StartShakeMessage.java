package net.tardis.mod.network.packets;

import net.minecraft.network.FriendlyByteBuf;
import net.minecraftforge.network.NetworkEvent;
import net.tardis.mod.network.ClientPacketHandler;

import java.util.function.Supplier;

public record StartShakeMessage(int shakePower, int shakeTicks) {

    public static void encode(StartShakeMessage mes, FriendlyByteBuf buf){
        buf.writeInt(mes.shakePower());
        buf.writeInt(mes.shakeTicks());
    }
    public static StartShakeMessage decode(FriendlyByteBuf buf){
        return new StartShakeMessage(buf.readInt(), buf.readInt());
    }

    public static void handle(StartShakeMessage mes, Supplier<NetworkEvent.Context> context){
        context.get().setPacketHandled(true);
        context.get().enqueueWork(() -> {
            ClientPacketHandler.handleShakeMessage(mes);
        });
    }

}
