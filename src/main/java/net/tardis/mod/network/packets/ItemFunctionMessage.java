package net.tardis.mod.network.packets;

import net.minecraft.nbt.CompoundTag;
import net.minecraft.network.FriendlyByteBuf;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.item.ItemStack;
import net.minecraftforge.network.NetworkEvent;
import net.tardis.mod.cap.Capabilities;
import net.tardis.mod.cap.items.functions.ItemFunctionRegistry;
import net.tardis.mod.cap.items.functions.ItemFunctionType;
import net.tardis.mod.cap.items.functions.cfl.CFLFunctionType;

import java.util.function.Supplier;

public record ItemFunctionMessage(InteractionHand hand, ItemFunctionType<?> function, CompoundTag functionData) {

    public static void encode(ItemFunctionMessage mes, FriendlyByteBuf buf){
        buf.writeEnum(mes.hand());
        buf.writeBoolean(mes.function != null);

        if(mes.function != null) {
            buf.writeRegistryId(ItemFunctionRegistry.REGISTRY.get(), mes.function());
            buf.writeNbt(mes.functionData());
        }
    }
    public static ItemFunctionMessage decode(FriendlyByteBuf buf){
        final InteractionHand hand = buf.readEnum(InteractionHand.class);
        final boolean hasFunction = buf.readBoolean();
        if(hasFunction){
            return new ItemFunctionMessage(hand, buf.readRegistryId(), buf.readNbt());
        }

        return new ItemFunctionMessage(hand, null, null);
    }

    public static void handle(ItemFunctionMessage mes, Supplier<NetworkEvent.Context> context){
        context.get().setPacketHandled(true);
        context.get().enqueueWork(() -> {
            ItemStack stack = context.get().getSender().getItemInHand(mes.hand());
            stack.getCapability(Capabilities.CFL).ifPresent(tool -> {
                if(mes.function != null){
                    tool.setFunction((CFLFunctionType) mes.function());
                    tool.getCurrentFunction().ifPresent(f -> {
                        f.deserializeNBT(mes.functionData());
                        f.onSelected(context.get().getSender(), mes.hand());
                    });
                }
                else tool.setFunction(null);
            });
        });
    }

}
