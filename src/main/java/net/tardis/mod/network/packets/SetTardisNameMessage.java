package net.tardis.mod.network.packets;

import net.minecraft.core.registries.Registries;
import net.minecraft.network.FriendlyByteBuf;
import net.minecraft.resources.ResourceKey;
import net.minecraft.world.level.Level;
import net.minecraftforge.network.NetworkEvent;
import net.tardis.mod.resource_listener.server.TardisNames;

import java.nio.charset.Charset;
import java.util.function.Supplier;

public record SetTardisNameMessage(ResourceKey<Level> tardis, String name) {

    public static final Charset CHARSET = Charset.defaultCharset();

    public static void encode(SetTardisNameMessage mes, FriendlyByteBuf buf){
        buf.writeResourceKey(mes.tardis());
        buf.writeInt(mes.name().length());
        buf.writeCharSequence(mes.name(), CHARSET);
    }

    public static SetTardisNameMessage decode(FriendlyByteBuf buf){
        final ResourceKey<Level> tardis = buf.readResourceKey(Registries.DIMENSION);
        final int length = buf.readInt();
        String name = buf.readCharSequence(length, CHARSET).toString();
        return new SetTardisNameMessage(tardis, name);
    }

    public static void handle(SetTardisNameMessage mes, Supplier<NetworkEvent.Context> context){
        context.get().setPacketHandled(true);
        context.get().enqueueWork(() -> {
            TardisNames.CLIENT_NAMES.put(mes.tardis(), mes.name());
        });
    }

}
