package net.tardis.mod.network.packets;

import net.minecraft.network.FriendlyByteBuf;
import net.minecraftforge.network.NetworkEvent;
import net.tardis.mod.cap.Capabilities;

import java.util.function.Supplier;

public record TardisLightMessage(float percent) {


    public static void encode(TardisLightMessage mes, FriendlyByteBuf buf){
        buf.writeFloat(mes.percent());
    }

    public static TardisLightMessage decode(FriendlyByteBuf buf){
        return new TardisLightMessage(buf.readFloat());
    }

    public static void handle(TardisLightMessage mes, Supplier<NetworkEvent.Context> context){
        context.get().setPacketHandled(true);
        context.get().enqueueWork(() -> {
            context.get().getSender().level.getCapability(Capabilities.TARDIS).ifPresent(tardis -> {
                tardis.getInteriorManager().setLightLevel(mes.percent());
            });
        });
    }

}
