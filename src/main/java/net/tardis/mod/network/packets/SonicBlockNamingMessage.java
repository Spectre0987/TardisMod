package net.tardis.mod.network.packets;

import net.minecraft.core.BlockPos;
import net.minecraft.network.FriendlyByteBuf;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraftforge.network.NetworkEvent;
import net.tardis.mod.block.ISonicNamable;
import net.tardis.mod.helpers.Helper;

import java.util.Optional;
import java.util.function.Supplier;

public record SonicBlockNamingMessage(BlockPos pos, Optional<String> name) {


    public static void encode(SonicBlockNamingMessage mes, FriendlyByteBuf buf){
        buf.writeBlockPos(mes.pos);
        Helper.encodeOptional(buf, mes.name(), (s, b) -> b.writeUtf(s));
    }

    public static SonicBlockNamingMessage decode(FriendlyByteBuf buf){
        return new SonicBlockNamingMessage(buf.readBlockPos(), Helper.decodeOptional(buf, b -> b.readUtf()));
    }

    public static void handle(SonicBlockNamingMessage mes, Supplier<NetworkEvent.Context> context){
        context.get().setPacketHandled(true);
        context.get().enqueueWork(() -> {
            final BlockState state = context.get().getSender().getLevel().getBlockState(mes.pos());
            if(state.getBlock() instanceof ISonicNamable name){
                name.setName(context.get().getSender().getLevel(), mes.pos(), mes.name());
            }
        });
    }

}
