package net.tardis.mod.network.packets;

import net.minecraft.core.registries.Registries;
import net.minecraft.network.FriendlyByteBuf;
import net.minecraft.resources.ResourceKey;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.dimension.DimensionType;
import net.minecraftforge.network.NetworkDirection;
import net.minecraftforge.network.NetworkEvent;
import net.tardis.mod.helpers.DimensionNameHelper;
import net.tardis.mod.misc.TardisDimensionInfo;
import net.tardis.mod.network.Network;
import net.tardis.mod.registry.JsonRegistries;
import org.jetbrains.annotations.Nullable;

import java.nio.charset.Charset;
import java.util.Optional;
import java.util.function.Supplier;

public class DimenionNameMessage {

    final ResourceKey<Level> level;
    @Nullable
    public final String customName;


    public DimenionNameMessage(ResourceKey<Level> level, @Nullable String customName){
        this.level = level;
        this.customName = customName;
    }

    public static void encode(DimenionNameMessage mes, FriendlyByteBuf buf){
        buf.writeResourceKey(mes.level);
        buf.writeInt(mes.customName == null ? 0 : mes.customName.length());
        if(mes.customName != null){
            buf.writeCharSequence(mes.customName, Charset.defaultCharset());
        }
    }

    public static DimenionNameMessage decode(FriendlyByteBuf buf){
        ResourceKey<Level> level = buf.readResourceKey(Registries.DIMENSION);
        int nameLength = buf.readInt();
        if(nameLength > 0){
            return new DimenionNameMessage(level, buf.readCharSequence(nameLength, Charset.defaultCharset()).toString());
        }
        return new DimenionNameMessage(level, null);
    }


    public static void handle(DimenionNameMessage mes, Supplier<NetworkEvent.Context> context){
        context.get().setPacketHandled(true);
        context.get().enqueueWork(() -> {

            //If coming from client, gather name and send it back
            if(context.get().getDirection() == NetworkDirection.PLAY_TO_SERVER){
                DimensionNameHelper.getServerCustomName(context.get().getSender().getServer(), mes.level).ifPresent(name -> {
                    Network.sendTo(context.get().getSender(), new DimenionNameMessage(mes.level, name));
                });
            }
            else {
                DimensionNameHelper.CLIENT_NAMES.put(mes.level, mes.customName == null ? Optional.empty() : Optional.of(mes.customName));
            }

        });
    }


}
