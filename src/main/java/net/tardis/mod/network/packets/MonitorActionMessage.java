package net.tardis.mod.network.packets;

import net.minecraft.network.FriendlyByteBuf;
import net.minecraft.world.entity.player.Player;
import net.minecraftforge.network.NetworkEvent;
import net.tardis.mod.cap.Capabilities;
import net.tardis.mod.misc.tardis.montior.MonitorFunction;
import net.tardis.mod.registry.MonitorFunctionRegistry;

import java.util.function.Supplier;

public class MonitorActionMessage {

    final MonitorFunction function;
    final int player;

    public MonitorActionMessage(int player, MonitorFunction function){
        this.function = function;
        this.player = player;
    }

    public static void encode(MonitorActionMessage mes, FriendlyByteBuf buf){
        buf.writeInt(mes.player);
        buf.writeRegistryId(MonitorFunctionRegistry.REGISTRY.get(), mes.function);
    }

    public static MonitorActionMessage decode(FriendlyByteBuf buf){
        return new MonitorActionMessage(buf.readInt(), buf.readRegistryIdSafe(MonitorFunction.class));
    }

    public static void handle(MonitorActionMessage mes, Supplier<NetworkEvent.Context> event){
        if(event.get().getSender().getLevel().getEntity(mes.player) instanceof Player player){
            event.get().enqueueWork(() -> {
                event.get().getSender().level.getCapability(Capabilities.TARDIS).ifPresent(cap -> {
                    mes.function.doServerAction(cap, player);
                });
            });
        }
        event.get().setPacketHandled(true);
    }

}
