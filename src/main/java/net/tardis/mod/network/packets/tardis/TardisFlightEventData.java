package net.tardis.mod.network.packets.tardis;

import net.minecraft.network.FriendlyByteBuf;
import net.tardis.mod.cap.level.ITardisLevel;
import net.tardis.mod.flight_event.FlightEvent;
import net.tardis.mod.flight_event.FlightEventType;
import net.tardis.mod.registry.FlightEventRegistry;

public class TardisFlightEventData extends TardisData{

    FlightEventType event;
    boolean complete = false;

    public TardisFlightEventData(int id) {
        super(id);
    }

    @Override
    public void serialize(FriendlyByteBuf buf) {
        buf.writeByte(this.event != null ? 1 : 0);
        if(event != null){
            buf.writeRegistryId(FlightEventRegistry.REGISTRY.get(), this.event);
            buf.writeBoolean(complete);
        }
    }

    @Override
    public void deserialize(FriendlyByteBuf buf) {
        boolean hasEvent = buf.readByte() == 1;
        if(hasEvent){
            this.event = buf.readRegistryId();
            this.complete = buf.readBoolean();
        }
    }

    @Override
    public void apply(ITardisLevel tardis) {
        tardis.setCurrentFlightEvent(this.event);
        if(this.complete){
            tardis.getCurrentFlightEvent().ifPresent(FlightEvent::complete);
        }
    }

    @Override
    public void createFromTardis(ITardisLevel tardis) {
        tardis.getCurrentFlightEvent().ifPresent(event -> {
            this.event = event.getType();
            this.complete = event.isComplete();
        });
    }
}
