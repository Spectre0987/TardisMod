package net.tardis.mod.network.packets;

import net.minecraft.network.FriendlyByteBuf;
import net.minecraftforge.network.NetworkEvent;
import net.tardis.mod.cap.Capabilities;
import net.tardis.mod.network.ClientPacketHandler;
import net.tardis.mod.registry.DematAnimationRegistry;

import java.util.function.Supplier;

public record SetDematAnimMessage(DematAnimationRegistry.DematAnimationType type) {

    public static void encode(SetDematAnimMessage mes, FriendlyByteBuf buf){
        buf.writeRegistryId(DematAnimationRegistry.REGISTRY.get(), mes.type());
    }

    public static SetDematAnimMessage decode(FriendlyByteBuf buf){
        return new SetDematAnimMessage(buf.readRegistryId());
    }

    public static void handle(SetDematAnimMessage mes, Supplier<NetworkEvent.Context> context){
        context.get().setPacketHandled(true);
        context.get().enqueueWork(() -> {
            context.get().getSender().getLevel().getCapability(Capabilities.TARDIS).ifPresent(tardis -> {
                tardis.getInteriorManager().setDematAnimation(mes.type());
            });
        });
    }


}
