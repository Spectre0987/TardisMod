package net.tardis.mod.network.packets.tardis;

import net.minecraft.network.FriendlyByteBuf;
import net.tardis.mod.cap.level.ITardisLevel;

public abstract class TardisData {

    final int id;

    public TardisData(int id){
        this.id = id;
    }
    public int getId(){
        return this.id;
    }

    public abstract void serialize(FriendlyByteBuf buf);
    public abstract void deserialize(FriendlyByteBuf buf);
    public abstract void apply(ITardisLevel tardis);
    public abstract void createFromTardis(ITardisLevel tardis);

}
