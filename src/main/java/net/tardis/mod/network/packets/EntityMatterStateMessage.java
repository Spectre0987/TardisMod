package net.tardis.mod.network.packets;

import net.minecraft.network.FriendlyByteBuf;
import net.minecraftforge.network.NetworkEvent;
import net.tardis.mod.Tardis;
import net.tardis.mod.misc.IHaveMatterState;
import net.tardis.mod.misc.MatterStateHandler;

import java.util.function.Supplier;

public record EntityMatterStateMessage(int entityId, MatterStateHandler handler) {

    public static void encode(EntityMatterStateMessage mes, FriendlyByteBuf buf){
        buf.writeInt(mes.entityId());
        mes.handler().encode(buf);
    }

    public static EntityMatterStateMessage decode(FriendlyByteBuf buf){
        return new EntityMatterStateMessage(buf.readInt(), MatterStateHandler.decode(buf));
    }

    public static void handle(EntityMatterStateMessage mes, Supplier<NetworkEvent.Context> context){
        context.get().setPacketHandled(true);
        context.get().enqueueWork(() -> {
            Tardis.SIDE.getClientLevel().ifPresent(level -> {
                if(level.getEntity(mes.entityId()) instanceof IHaveMatterState state){
                    state.getMatterStateHandler().copyFrom(mes.handler());
                }
            });
        });
    }

}
