package net.tardis.mod.network.packets.tardis;

import net.minecraft.network.FriendlyByteBuf;
import net.tardis.mod.cap.level.ITardisLevel;

public class TardisFuelData extends TardisData{

    public float artron;
    public float maxArtron;
    public float chargeRate;

    public TardisFuelData(int id) {
        super(id);
    }

    @Override
    public void serialize(FriendlyByteBuf buf) {
        buf.writeFloat(this.artron);
        buf.writeFloat(this.maxArtron);
        buf.writeFloat(this.chargeRate);
    }

    @Override
    public void deserialize(FriendlyByteBuf buf) {
        this.artron = buf.readFloat();
        this.maxArtron = buf.readFloat();
        this.chargeRate = buf.readFloat();
    }

    @Override
    public void apply(ITardisLevel tardis) {
        tardis.getFuelHandler().updateFromPacket(this);
    }

    @Override
    public void createFromTardis(ITardisLevel tardis) {
        this.artron = tardis.getFuelHandler().getStoredArtron();
        this.maxArtron = tardis.getFuelHandler().getMaxArtron();
        this.chargeRate = tardis.getFuelHandler().calcArtronRechargeMult();
    }
}
