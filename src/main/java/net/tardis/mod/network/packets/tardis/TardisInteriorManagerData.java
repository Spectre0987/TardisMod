package net.tardis.mod.network.packets.tardis;

import net.minecraft.network.FriendlyByteBuf;
import net.tardis.mod.cap.level.ITardisLevel;
import net.tardis.mod.misc.tardis.InteriorManager;

public class TardisInteriorManagerData extends TardisData{

    private boolean hasNeverFlown;
    private int matterBuffer;
    private float lightLevel;

    public TardisInteriorManagerData(int id) {
        super(id);
    }

    @Override
    public void serialize(FriendlyByteBuf buf) {
        buf.writeBoolean(this.hasNeverFlown);
        buf.writeInt(this.matterBuffer);
        buf.writeFloat(this.lightLevel);
    }

    @Override
    public void deserialize(FriendlyByteBuf buf) {
        this.hasNeverFlown = buf.readBoolean();
        this.matterBuffer = buf.readInt();
        this.lightLevel = buf.readFloat();
    }

    @Override
    public void apply(ITardisLevel tardis) {
        if(!this.hasNeverFlown){
            tardis.getInteriorManager().setHasFlown();
        }
        tardis.getInteriorManager().setMatterBufferDirect(this.matterBuffer);
        tardis.getInteriorManager().setLightLevel(this.lightLevel);
        tardis.getInteriorManager().updateLighting();
    }

    @Override
    public void createFromTardis(ITardisLevel tardis) {
        InteriorManager man = tardis.getInteriorManager();
        this.hasNeverFlown = man.hasNeverFlown();
        this.matterBuffer = man.getMatterBuffer();
        this.lightLevel = man.getTardisLightLevel();
    }
}
