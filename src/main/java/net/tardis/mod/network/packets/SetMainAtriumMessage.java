package net.tardis.mod.network.packets;

import net.minecraft.core.BlockPos;
import net.minecraft.network.FriendlyByteBuf;
import net.minecraftforge.network.NetworkEvent;
import net.tardis.mod.cap.Capabilities;
import net.tardis.mod.registry.TardisWorldStructureRegistry;

import java.util.function.Supplier;

public record SetMainAtriumMessage(BlockPos pos) {


    public static void encode(SetMainAtriumMessage mes, FriendlyByteBuf buf){
        buf.writeBlockPos(mes.pos());
    }

    public static SetMainAtriumMessage decode(FriendlyByteBuf buf){
        return new SetMainAtriumMessage(buf.readBlockPos());
    }

    public static void handle(SetMainAtriumMessage mes, Supplier<NetworkEvent.Context> context){
        context.get().setPacketHandled(true);
        context.get().enqueueWork(() -> {
            Capabilities.getCap(Capabilities.TARDIS, context.get().getSender().getLevel()).ifPresent(tardis -> {
                if(tardis.getInteriorManager().getWorldStructure(mes.pos(), TardisWorldStructureRegistry.ATRIUM.get()).isPresent()){
                    tardis.getInteriorManager().setMainAtrium(mes.pos());
                }
            });
        });
    }

}
