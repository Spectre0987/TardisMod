package net.tardis.mod.network.packets;

import net.minecraft.network.FriendlyByteBuf;
import net.minecraftforge.network.NetworkEvent;
import net.tardis.mod.helpers.Helper;
import net.tardis.mod.misc.tardis.FlightCourse;
import net.tardis.mod.misc.enums.FlightState;
import net.tardis.mod.network.ClientPacketHandler;

import java.util.Optional;
import java.util.function.Supplier;

//SHould be sent anytime a journey changes in any way. Taking off, altering course, landing, etc
public class TardisFlightStateMessage {

    public int flightTicks;
    public int landTick;
    public float distanceTraveled;
    public FlightState state;
    public Optional<FlightCourse> course;

    public TardisFlightStateMessage(int flightTicks, int landTick, float distanceTraveled, FlightState state, Optional<FlightCourse> course){
        this.flightTicks = flightTicks;
        this.landTick = landTick;
        this.distanceTraveled = distanceTraveled;
        this.state = state;
        this.course = course;
    }

    public static void encode(TardisFlightStateMessage mes, FriendlyByteBuf buf){
        buf.writeInt(mes.flightTicks);
        buf.writeInt(mes.landTick);
        buf.writeFloat(mes.distanceTraveled);
        buf.writeEnum(mes.state);
        Helper.encodeOptional(buf, mes.course, FlightCourse::encode);
    }

    public static TardisFlightStateMessage decode(FriendlyByteBuf buf){
        final int flightTick = buf.readInt();
        final int landTick = buf.readInt();
        final float distanceTraveled = buf.readFloat();
        final FlightState state = buf.readEnum(FlightState.class);
        final Optional<FlightCourse> course = Helper.decodeOptional(buf, FlightCourse::new);

        return new TardisFlightStateMessage(flightTick, landTick, distanceTraveled, state, course);
    }

    public static void handle(final TardisFlightStateMessage mes, Supplier<NetworkEvent.Context> context){
        context.get().enqueueWork(() -> {
            ClientPacketHandler.handleTardisFlightState(mes);
        });
        context.get().setPacketHandled(true);
    }

}
