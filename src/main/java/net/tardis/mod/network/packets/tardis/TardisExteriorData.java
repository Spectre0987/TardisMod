package net.tardis.mod.network.packets.tardis;

import net.minecraft.network.FriendlyByteBuf;
import net.tardis.mod.cap.level.ITardisLevel;
import net.tardis.mod.exterior.ExteriorType;
import net.tardis.mod.registry.ExteriorRegistry;

public class TardisExteriorData extends TardisData{

    ExteriorType type;

    public TardisExteriorData(int id) {
        super(id);
    }

    @Override
    public void serialize(FriendlyByteBuf buf) {
        buf.writeRegistryId(ExteriorRegistry.REGISTRY.get(), this.type);
    }

    @Override
    public void deserialize(FriendlyByteBuf buf) {
        this.type = buf.readRegistryId();
    }

    @Override
    public void apply(ITardisLevel tardis) {
        tardis.setExterior(this.type, false);
    }

    @Override
    public void createFromTardis(ITardisLevel tardis) {
        this.type = tardis.getExterior().getType();
    }
}
