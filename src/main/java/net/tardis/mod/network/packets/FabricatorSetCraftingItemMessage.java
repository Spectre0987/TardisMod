package net.tardis.mod.network.packets;

import net.minecraft.core.BlockPos;
import net.minecraft.network.FriendlyByteBuf;
import net.minecraft.world.item.Item;
import net.minecraftforge.network.NetworkEvent;
import net.minecraftforge.registries.ForgeRegistries;
import net.tardis.mod.helpers.Helper;
import net.tardis.mod.network.ClientPacketHandler;

import java.util.Optional;
import java.util.function.Supplier;

public record FabricatorSetCraftingItemMessage(BlockPos pos, Optional<Item> item) {

    public static void encode(FabricatorSetCraftingItemMessage mes, FriendlyByteBuf buf){
        buf.writeBlockPos(mes.pos());
        Helper.encodeOptional(buf, mes.item(), (i, b) -> b.writeRegistryId(ForgeRegistries.ITEMS, i));
    }

    public static FabricatorSetCraftingItemMessage decode(FriendlyByteBuf buf){
        return new FabricatorSetCraftingItemMessage(buf.readBlockPos(), Helper.decodeOptional(buf, b -> b.readRegistryId()));
    }

    public static void handle(FabricatorSetCraftingItemMessage mes, Supplier<NetworkEvent.Context> context){
        context.get().setPacketHandled(true);
        context.get().enqueueWork(() -> {
            ClientPacketHandler.handleFabricateMessage(mes);
        });
    }

}
