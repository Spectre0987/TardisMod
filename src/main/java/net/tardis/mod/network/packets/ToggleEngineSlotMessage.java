package net.tardis.mod.network.packets;

import net.minecraft.network.FriendlyByteBuf;
import net.minecraft.network.protocol.PacketFlow;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.Level;
import net.minecraftforge.network.NetworkDirection;
import net.minecraftforge.network.NetworkEvent;
import net.tardis.mod.cap.Capabilities;
import net.tardis.mod.item.IEngineToggleable;
import net.tardis.mod.item.components.SubsystemItem;
import net.tardis.mod.misc.tardis.TardisEngine;
import net.tardis.mod.network.ClientPacketHandler;
import net.tardis.mod.network.Network;

import java.util.function.Supplier;

/**
 *  Bi- directional packet for toggling engine subsystems
 * @param engineSide
 * @param slotID
 * @param activated
 */
public record ToggleEngineSlotMessage(TardisEngine.EngineSide engineSide, int slotID, boolean activated) {

    public static void encode(ToggleEngineSlotMessage mes, FriendlyByteBuf buf){
        buf.writeEnum(mes.engineSide());
        buf.writeInt(mes.slotID());
        buf.writeBoolean(mes.activated());
    }

    public static ToggleEngineSlotMessage decode(FriendlyByteBuf buf){
        return new ToggleEngineSlotMessage(
                buf.readEnum(TardisEngine.EngineSide.class),
                buf.readInt(),
                buf.readBoolean()
        );
    }

    public static void handle(ToggleEngineSlotMessage mes, Supplier<NetworkEvent.Context> context){
        context.get().enqueueWork(() -> {

            //If this is coming from the client to the server, set it and send it out to all players in the dimension
            if(context.get().getNetworkManager().getDirection() == PacketFlow.SERVERBOUND){

                final Level tardisLevel = context.get().getSender().getLevel();
                tardisLevel.getCapability(Capabilities.TARDIS).ifPresent(tardis -> {
                    final ItemStack item = tardis.getEngine().getInventoryFor(mes.engineSide()).getStackInSlot(mes.slotID());

                    if(item.getItem() instanceof IEngineToggleable sys){
                        sys.onToggle(tardis, mes.activated());

                        Network.sendPacketToDimension(tardisLevel.dimension(), new ToggleEngineSlotMessage(mes.engineSide(), mes.slotID(), mes.activated()));
                    }

                });

            }
            //If we are receiving this packet as a client
            else ClientPacketHandler.handleEngineToggleMessage(mes);
        });
        context.get().setPacketHandled(true);
    }
}
