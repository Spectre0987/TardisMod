package net.tardis.mod.network.packets;

import net.minecraft.nbt.CompoundTag;
import net.minecraft.network.FriendlyByteBuf;
import net.minecraft.world.level.ChunkPos;
import net.minecraft.world.level.chunk.LevelChunk;
import net.minecraftforge.event.level.ChunkDataEvent;
import net.minecraftforge.event.level.LevelEvent;
import net.minecraftforge.network.NetworkDirection;
import net.minecraftforge.network.NetworkEvent;
import net.tardis.mod.Tardis;
import net.tardis.mod.cap.Capabilities;
import net.tardis.mod.network.Network;

import java.util.function.Supplier;

public record LoadChunkCapabilityMessage(ChunkPos pos, CompoundTag tag) {


    public static void encode(LoadChunkCapabilityMessage mes, FriendlyByteBuf buf){
        buf.writeChunkPos(mes.pos());
        buf.writeNbt(mes.tag());
    }

    public static LoadChunkCapabilityMessage decode(FriendlyByteBuf buf){
        return new LoadChunkCapabilityMessage(buf.readChunkPos(), buf.readNbt());
    }

    public static void handle(LoadChunkCapabilityMessage mes, Supplier<NetworkEvent.Context> context){

        context.get().setPacketHandled(true);

        //If sent from the client, it's requesting an update
        if(context.get().getDirection() == NetworkDirection.PLAY_TO_SERVER){
            LevelChunk chunk = context.get().getSender().level.getChunk(mes.pos.x, mes.pos.z);
            if(chunk != null){
                chunk.getCapability(Capabilities.CHUNK).ifPresent(cap -> {
                    Network.sendTo(context.get().getSender(), new LoadChunkCapabilityMessage(chunk.getPos(), cap.serializeNBT()));
                });
            }
            return;
        }

        //If it comes from the server, update the client

        context.get().enqueueWork(() -> {
            Tardis.SIDE.getClientLevel().ifPresent(level -> {
                LevelChunk chunk = level.getChunk(mes.pos.x, mes.pos.z);
                if(chunk != null){
                    chunk.getCapability(Capabilities.CHUNK).ifPresent(cap -> cap.deserializeNBT(mes.tag));
                }
            });
        });
    }

}
