package net.tardis.mod.upgrade;

import net.minecraft.nbt.CompoundTag;
import net.minecraft.world.damagesource.DamageSource;
import net.minecraftforge.common.util.INBTSerializable;
import net.tardis.mod.helpers.Helper;
import net.tardis.mod.misc.TypeHolder;
import net.tardis.mod.registry.UpgradeRegistry;
import net.tardis.mod.upgrade.types.UpgradeType;

public abstract class Upgrade<C> extends TypeHolder<UpgradeType<C, ?>> implements INBTSerializable<CompoundTag> {

    private final C instance;

    public Upgrade(UpgradeType<C, ?> type, C instance) {
        super(type);
        this.instance = instance;
    }


    /**
     *
     * @param damage
     * @return true if this upgrade broke due to damage
     */
    public abstract boolean damage(DamageSource damage);

    public abstract void onBreak();
    public abstract void onTick();

    public abstract boolean canBeUsed();

    public C getInstance(){
        return this.instance;
    }

    @Override
    public CompoundTag serializeNBT() {
        final CompoundTag tag = new CompoundTag();
        Helper.writeRegistryToNBT(tag, UpgradeRegistry.REGISTRY.get(), this.getType(), "type");
        return tag;
    }

    @Override
    public void deserializeNBT(CompoundTag tag) {

    }
}
