package net.tardis.mod.upgrade.tardis;

import net.minecraft.world.damagesource.DamageSource;
import net.tardis.mod.cap.level.ITardisLevel;
import net.tardis.mod.subsystem.Subsystem;
import net.tardis.mod.subsystem.SubsystemType;
import net.tardis.mod.upgrade.types.UpgradeType;

import java.util.function.Supplier;

public abstract class BaseSubsytemTardisUpgrade extends BaseTardisUpgrade {

    final Supplier<SubsystemType<? extends Subsystem>> requiredSubsystem;

    public BaseSubsytemTardisUpgrade(UpgradeType<ITardisLevel, ?> type, ITardisLevel instance, Supplier<SubsystemType<?>> requiredSystem) {
        super(type, instance);
        this.requiredSubsystem = requiredSystem;
    }

    @Override
    public boolean damage(DamageSource damage) {
        return false;
    }

    @Override
    public boolean canBeUsed() {
        return requiredSubsystem.get().canBeUsed(this.getInstance());
    }
}
