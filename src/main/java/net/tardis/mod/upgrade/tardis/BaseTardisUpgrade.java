package net.tardis.mod.upgrade.tardis;

import net.minecraft.nbt.CompoundTag;
import net.minecraft.world.item.ItemStack;
import net.minecraftforge.items.ItemStackHandler;
import net.tardis.mod.cap.level.ITardisLevel;
import net.tardis.mod.misc.tardis.TardisEngine;
import net.tardis.mod.upgrade.Upgrade;
import net.tardis.mod.upgrade.types.UpgradeType;

public abstract class BaseTardisUpgrade extends Upgrade<ITardisLevel> {

    private boolean active = true;

    public BaseTardisUpgrade(UpgradeType<ITardisLevel, ?> type, ITardisLevel instance) {
        super(type, instance);
    }

    /**
     * Multiplied by the TARDISes current speed
     * @return
     */
    public float speedMod(){
        return 1.0F;
    }

    public void onTakeoff(){}

    public void onLandAttempt(){}

    public void onLandComplete(){}

    /**
     *
     * @return If this can be used. Checks stuff like if the item exists, if the upgrade is enabled, if the required subsystem can be used, etc
     */
    @Override
    public boolean canBeUsed() {
        return !getItem().isEmpty() && this.isActive();
    }

    /**
     *
     * @return The item in the engine this upgrade is attached to
     */
    public ItemStack getItem(){
        final ItemStackHandler inv = this.getInstance().getEngine().getInventoryFor(TardisEngine.EngineSide.UPGRADES);
        for(int i = 0; i < inv.getSlots(); ++i){
            if(this.getType().isValid(inv.getStackInSlot(i))){
                return inv.getStackInSlot(i);
            }
        }
        return ItemStack.EMPTY;
    }

    /**
     *
     * @return If this Upgrade is turned on
     */
    public boolean isActive(){
        return this.active;
    }

    /**
     * See {@link BaseTardisUpgrade#isActive()}
     * @param active
     */
    public void setActive(boolean active){
        this.active = active;
    }

    @Override
    public CompoundTag serializeNBT() {
        CompoundTag tag = super.serializeNBT();
        tag.putBoolean("active", this.active);
        return tag;
    }

    @Override
    public void deserializeNBT(CompoundTag tag) {
        super.deserializeNBT(tag);
        this.active = tag.getBoolean("active");
    }
}
