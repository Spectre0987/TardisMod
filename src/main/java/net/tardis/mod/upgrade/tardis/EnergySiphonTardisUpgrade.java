package net.tardis.mod.upgrade.tardis;

import net.minecraft.core.BlockPos;
import net.minecraft.core.Vec3i;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.world.damagesource.DamageSource;
import net.minecraft.world.level.block.Blocks;
import net.tardis.mod.cap.level.ITardisLevel;
import net.tardis.mod.upgrade.types.UpgradeType;
import org.jetbrains.annotations.Nullable;

public class EnergySiphonTardisUpgrade extends BaseTardisUpgrade{

    ServerLevel cachedOutside;

    public EnergySiphonTardisUpgrade(UpgradeType<ITardisLevel, ?> type, ITardisLevel instance) {
        super(type, instance);
    }

    @Override
    public boolean damage(DamageSource damage) {
        return false;
    }

    @Override
    public void onBreak() {

    }

    @Override
    public void onTick() {
        if(this.getInstance().isInVortex())
            return;

        if(getInstance().getLevel().getGameTime() % 200 == 0){
            ServerLevel level = getOutside();
            if(level != null){
                final BlockPos start = getInstance().getLocation().getPos();
                final Vec3i radius = new Vec3i(8, 8, 8);
                BlockPos.betweenClosed(start.subtract(radius), start.offset(radius)).forEach(p -> {
                    if(level.getBlockState(p).getBlock() == Blocks.WATER){
                        level.setBlock(p, Blocks.ICE.defaultBlockState(), 3);
                    }

                    if(level.getBlockState(p).getBlock() == Blocks.LAVA)
                        level.setBlock(p, Blocks.OBSIDIAN.defaultBlockState(), 3);

                });
            }
        }

    }

    public @Nullable ServerLevel getOutside(){

        if(getInstance().isClient())
            return null;

        if(this.cachedOutside == null){
            this.cachedOutside = getInstance().getLevel().getServer().getLevel(getInstance().getLocation().getLevel());
        }

        if(cachedOutside != null){
            //If the TARDIS is no longer in this world, invalidate the cache
            if(!cachedOutside.dimension().equals(getInstance().getLocation().getLevel())){
                this.cachedOutside = null;
            }
        }

        return this.cachedOutside;
    }
}
