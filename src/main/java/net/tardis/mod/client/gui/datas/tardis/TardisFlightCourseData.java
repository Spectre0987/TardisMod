package net.tardis.mod.client.gui.datas.tardis;

import net.minecraft.network.FriendlyByteBuf;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.world.level.ChunkPos;
import net.tardis.mod.cap.level.ITardisLevel;
import net.tardis.mod.client.gui.datas.GuiData;
import net.tardis.mod.helpers.Helper;
import net.tardis.mod.misc.SpaceTimeCoord;
import net.tardis.mod.misc.tardis.FlightCourse;
import net.tardis.mod.world.data.TardisLevelData;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

public class TardisFlightCourseData extends GuiData {

    public Optional<FlightCourse> course;
    public float distTardisTraveled;
    public Map<ChunkPos, TardisLevelData.VortexPhenomenaDTO> vortexPhenomenaNear = new HashMap<>();
    public SpaceTimeCoord currentLocation;

    public TardisFlightCourseData(int id) {
        super(id);
    }

    public TardisFlightCourseData from(ITardisLevel tardis){
        this.course = tardis.getCurrentCourse();
        this.currentLocation = tardis.getLocation();

        if(this.course.isEmpty()){
            this.course = Optional.of(new FlightCourse().addPoint(tardis, tardis.getDestination()));
        }

        this.distTardisTraveled = tardis.getDistanceTraveled();

        TardisLevelData.get((ServerLevel) tardis.getLevel()).getTypesNear(tardis, 30).entrySet().forEach(entry -> {
            this.vortexPhenomenaNear.put(entry.getKey(), entry.getValue());
        });

        return this;
    }

    @Override
    public void encode(FriendlyByteBuf buf) {
        Helper.encodeOptional(buf, this.course, FlightCourse::encode);
        this.currentLocation.encode(buf);
        buf.writeFloat(this.distTardisTraveled);
        buf.writeInt(this.vortexPhenomenaNear.size());
        for(Map.Entry<ChunkPos, TardisLevelData.VortexPhenomenaDTO> entry : this.vortexPhenomenaNear.entrySet()){
            buf.writeChunkPos(entry.getKey());
            entry.getValue().encode(buf);
        }
    }

    @Override
    public void decode(FriendlyByteBuf buf) {
        this.course = Helper.decodeOptional(buf, FlightCourse::new);
        this.currentLocation = SpaceTimeCoord.decode(buf);
        this.distTardisTraveled = buf.readFloat();
        final int vpSize = buf.readInt();
        for(int i = 0; i < vpSize; ++i){
            this.vortexPhenomenaNear.put(buf.readChunkPos(), TardisLevelData.VortexPhenomenaDTO.fromNetwork(buf));
        }
    }
}
