package net.tardis.mod.client.gui.monitor;

import net.minecraft.client.Minecraft;
import net.minecraft.network.chat.Component;
import net.tardis.mod.block.monitors.MonitorData;
import net.tardis.mod.client.gui.datas.GuiDatas;
import net.tardis.mod.client.gui.datas.MonitorWaypointData;
import net.tardis.mod.client.gui.datas.WaypointEditGuiData;
import net.tardis.mod.network.Network;
import net.tardis.mod.network.packets.LoadWaypointMessage;
import net.tardis.mod.network.packets.SaveWaypointMessage;

import java.util.ArrayList;
import java.util.List;

public class MontiorWaypointScreen extends MonitorScreen{

    public final List<MonitorWaypointData.WaypointDTO> waypoints = new ArrayList<>();

    public MontiorWaypointScreen(MonitorData data, MonitorWaypointData waypointData) {
        super(data);
        waypoints.addAll(waypointData.waypoints);
    }

    @Override
    public void setup() {
        for(MonitorWaypointData.WaypointDTO dto : this.waypoints){
            this.addTextOption(Component.literal(dto.name()), b -> {
                this.openMenu(d -> new MonitorWaypointEditScreen(d, dto));
            });
        }
        this.addTextOption(Component.literal("Save Current Location"), b -> {
            this.openMenu(MonitorWaypointSaveScreen::new);
        });
    }
}
