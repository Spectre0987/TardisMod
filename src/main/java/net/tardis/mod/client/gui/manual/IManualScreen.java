package net.tardis.mod.client.gui.manual;

import java.util.List;

public interface IManualScreen {

    int getNumberOfPages();
    List<Page> getPages();

    //int turnPage(int startingIndex);

}
