package net.tardis.mod.client.gui.diagnostic_tool;

import com.mojang.blaze3d.vertex.PoseStack;
import net.minecraft.client.Minecraft;
import net.minecraft.network.chat.Component;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.crafting.Ingredient;
import net.tardis.mod.exterior.ExteriorType;
import net.tardis.mod.helpers.GuiHelper;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class DiagnosticScreenBrokenTardisInfo extends DiagnosticToolBaseScreen{

    final ExteriorType type;
    final List<Ingredient> unlockItems = new ArrayList<>();

    long guiTicks = 0;
    int ingredientIndex = 0;

    public DiagnosticScreenBrokenTardisInfo(ExteriorType type, List<Ingredient> unlock){
        this.type = type;
        this.unlockItems.addAll(unlock);
    }

    @Override
    public void renderAxilliaryData(PoseStack stack, int mouseX, int mouseY, float partialTicks) {

        final int midX = width / 2, midY = height / 2;

        GuiHelper.renderExteriorToGui(stack, this.type, midX - 70, midY - 20);


        drawCenteredString(stack, this.font, Component.literal("Projected Likes:"), midX + 60, midY - 60 - font.lineHeight - 2, 0xFFFFFF);
        for(int i = 0; i < this.unlockItems.size(); ++i){
            final int index = i;
            getComponentFromIng(this.unlockItems.get(i)).ifPresent(ing -> {
                drawCenteredString(stack, Minecraft.getInstance().font, ing, midX + 60, midY - 60 + (index * this.font.lineHeight + 3), 0x000000);
            });
        }

    }

    public Optional<Component> getComponentFromIng(Ingredient ing){
        ItemStack[] stacks = ing.getItems();
        if(stacks == null || stacks.length == 0){
            return Optional.empty();
        }
        int trueIndex = this.ingredientIndex % stacks.length;
        return Optional.of(stacks[trueIndex].getDisplayName());
    }

    @Override
    public void tick() {
        super.tick();
        ++this.guiTicks;
        if(this.guiTicks % 40 == 0){
            ++this.ingredientIndex;
        }
    }
}
