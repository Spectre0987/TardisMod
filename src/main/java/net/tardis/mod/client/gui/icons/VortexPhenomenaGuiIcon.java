package net.tardis.mod.client.gui.icons;

import com.mojang.blaze3d.vertex.PoseStack;
import net.minecraft.client.gui.screens.Screen;
import net.minecraft.resources.ResourceLocation;
import net.tardis.mod.helpers.Helper;

public class VortexPhenomenaGuiIcon extends GuiIconRenderer{

    public static final ResourceLocation TEXTURE = Helper.createRL("textures/screens/monitors/flight_course/vortex_phenomena_icon_sheet.png");
    public static final int MAX_U = 58, MAX_V = 52;

    public VortexPhenomenaGuiIcon(Screen screen, int u, int v) {
        super(screen, TEXTURE);
    }

    @Override
    public void render(PoseStack pose, int mouseX, int mouseY) {

        //TODO: Hook this up

    }
}
