package net.tardis.mod.client.gui.monitor.vortex_phenomena;

import com.mojang.blaze3d.systems.RenderSystem;
import com.mojang.blaze3d.vertex.PoseStack;
import com.mojang.datafixers.util.Pair;
import net.minecraft.client.gui.Gui;
import net.minecraft.client.renderer.GameRenderer;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.util.Mth;
import net.tardis.mod.helpers.Helper;
import net.tardis.mod.misc.tardis.vortex.VortexPhenomenaType;
import net.tardis.mod.registry.VortexPhenomenaRegistry;

import java.util.HashMap;

public class DefaultVortexPhenomenaRenderer implements VortexPhenomenaRenderer{

    public static final ResourceLocation TEXTURE = Helper.createRL("textures/screens/monitors/flight_course/vortex_phenomena_icon_sheet.png");
    private static final HashMap<VortexPhenomenaType<?>, Icon> ICONS = new HashMap<>();

    @Override
    public boolean isValid(VortexPhenomenaType<?> type) {
        return ICONS.containsKey(type);
    }

    @Override
    public void render(PoseStack stack, VortexPhenomenaType<?> type, int x, int y, int radius) {

        Icon icon = ICONS.get(type);
        if(icon == null){
            //TODO: Missing texture
            return;
        }

        final float aspect = icon.height() / (float)icon.width();
        final int width = radius * 2;

        Gui.blit(stack, x - radius, y - radius, width, Mth.floor(width * aspect), icon.u(), icon.v(), icon.width(), icon.height(), 256, 256);

    }

    @Override
    public void setupRenderer(PoseStack stack) {
        RenderSystem.setShader(GameRenderer::getPositionTexShader);
        RenderSystem.setShaderTexture(0, TEXTURE);
    }

    @Override
    public void endRenderer(PoseStack stack) {

    }

    public record Icon(int u, int v, int width, int height){}

    public static void registerDefault(){
        ICONS.put(VortexPhenomenaRegistry.SPACE_BATTLE.get(), new Icon(60, 54, 58, 52));
        ICONS.put(VortexPhenomenaRegistry.WORMHOLE.get(), new Icon(178, 1, 58, 52));
        ICONS.put(VortexPhenomenaRegistry.ANCIENT_DEBRIS.get(), new Icon(1, 107, 58, 56));
        ICONS.put(VortexPhenomenaRegistry.ASTROID.get(), new Icon(178, 54, 58, 52));
    }
}
