package net.tardis.mod.client.gui.containers.quantiscope;

import com.mojang.blaze3d.vertex.PoseStack;
import net.minecraft.client.gui.components.ImageButton;
import net.minecraft.client.gui.screens.inventory.AbstractContainerScreen;
import net.minecraft.network.chat.Component;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.entity.player.Inventory;
import net.tardis.mod.menu.quantiscope.BaseQuantiscopeMenu;

public abstract class BaseQuantiscopeMenuScreen<T extends BaseQuantiscopeMenu<?>> extends AbstractContainerScreen<T> {

    int changeModeButtonX = 95,
            changeModeButtonY = 62,
            changeModeXDiff = 64;

    public BaseQuantiscopeMenuScreen(T pMenu, Inventory pPlayerInventory, Component pTitle) {
        super(pMenu, pPlayerInventory, pTitle);
    }

    @Override
    protected void init() {
        super.init();

        //Prev
        this.addRenderableWidget(new ImageButton(this.leftPos + changeModeButtonX, this.topPos + changeModeButtonY, 4, 8, 188, 17, 9, getTexture(), but -> {
            this.getMenu().setting.getParent().changeMode(-1);
        }));

        //Next
        this.addRenderableWidget(new ImageButton(this.leftPos + changeModeButtonX + changeModeXDiff, this.topPos + changeModeButtonY, 4, 8, 193, 17, 9, getTexture(), but -> {
            this.getMenu().setting.getParent().changeMode(1);
        }));

    }

    public abstract ResourceLocation getTexture();

    @Override
    public void render(PoseStack pPoseStack, int pMouseX, int pMouseY, float pPartialTick) {
        super.render(pPoseStack, pMouseX, pMouseY, pPartialTick);
        this.renderTooltip(pPoseStack, pMouseX, pMouseY);
        this.font.draw(pPoseStack, this.getMenu().setting.getTitle(), this.leftPos + 101, this.topPos + 61, 0x000000);
    }
}
