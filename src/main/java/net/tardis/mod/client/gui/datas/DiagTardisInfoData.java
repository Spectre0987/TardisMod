package net.tardis.mod.client.gui.datas;

import net.minecraft.network.FriendlyByteBuf;
import net.tardis.mod.cap.level.ITardisLevel;
import net.tardis.mod.misc.SpaceTimeCoord;

public class DiagTardisInfoData extends GuiData{

    public SpaceTimeCoord location;
    public SpaceTimeCoord destination;
    public boolean isInFlight;
    public float currentArtron;


    public DiagTardisInfoData(int id) {
        super(id);
    }

    public DiagTardisInfoData set(ITardisLevel tardis){
        this.location = tardis.getLocation();
        this.destination = tardis.getDestination();
        this.isInFlight = tardis.isInVortex();
        this.currentArtron = tardis.getFuelHandler().getStoredArtron();
        return this;
    }

    @Override
    public void encode(FriendlyByteBuf buf) {
        this.location.encode(buf);
        this.destination.encode(buf);
        buf.writeBoolean(this.isInFlight);
        buf.writeFloat(this.currentArtron);
    }

    @Override
    public void decode(FriendlyByteBuf buf) {
        this.location = SpaceTimeCoord.decode(buf);
        this.destination = SpaceTimeCoord.decode(buf);
        this.isInFlight = buf.readBoolean();
        this.currentArtron = buf.readFloat();
    }
}
