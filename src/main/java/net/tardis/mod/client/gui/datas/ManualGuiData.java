package net.tardis.mod.client.gui.datas;

import net.minecraft.network.FriendlyByteBuf;
import net.minecraft.world.item.Item;
import net.minecraft.world.level.ItemLike;
import net.minecraftforge.registries.ForgeRegistries;
import net.tardis.mod.client.gui.manual.entries.ManualEntry;
import net.tardis.mod.helpers.Helper;

import java.util.Optional;

public class ManualGuiData extends GuiData{

    public Optional<Item> item = Optional.empty();

    public ManualGuiData(int id) {
        super(id);
    }

    public ManualGuiData fromItem(Item like){
        item = Optional.of(like);
        return this;
    }

    @Override
    public void encode(FriendlyByteBuf buf) {
        Helper.encodeOptional(buf, this.item, (item, b) -> buf.writeRegistryId(ForgeRegistries.ITEMS, item));
    }

    @Override
    public void decode(FriendlyByteBuf buf) {
        this.item = Helper.decodeOptional(buf, b -> b.readRegistryIdSafe(Item.class));
    }
}
