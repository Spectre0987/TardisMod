package net.tardis.mod.client.gui.manual.entries;

import com.mojang.blaze3d.vertex.PoseStack;
import com.mojang.serialization.Codec;
import com.mojang.serialization.codecs.RecordCodecBuilder;
import net.minecraft.client.gui.Font;
import net.minecraft.network.chat.Component;
import net.minecraft.network.chat.Style;
import net.tardis.mod.client.gui.manual.ManualChapter;
import net.tardis.mod.helpers.Helper;

public class TitleEntry extends ManualEntry {

    public static final Codec<TitleEntry> CODEC = RecordCodecBuilder.create(instance ->
                instance.group(
                        EntryType.CODEC.fieldOf("type").forGetter(TitleEntry::getEntryType),
                        ManualChapter.CODEC.fieldOf("chapter").forGetter(TitleEntry::getChapter),
                        Codec.STRING.fieldOf("title").forGetter(t -> t.title)
                ).apply(instance, TitleEntry::new)
            );
    public final String title;

    public TitleEntry(EntryType type, ManualChapter chapter, String title) {
        super(type, chapter);
        this.title = title;
    }

    @Override
    public boolean isFullPage() {
        return true;
    }

    @Override
    public void render(PoseStack pose, Font font, int x, int y, int pageWidth, int pageHeight, int mouseX, int mouseY) {
        final Component t = Component.literal(this.title).withStyle(Style.EMPTY.withFont(Helper.createRL("fancy")));
        int textWidth = font.width(t);

        int middleX = x;

        if(textWidth < pageWidth){
            middleX = x + (pageWidth / 2 - textWidth / 2);
        }

        font.drawWordWrap(pose, t, middleX, y, pageWidth, 0x000000);
    }

    @Override
    public int getHeightUsed(Font font, int width) {
        return 0;
    }
}
