package net.tardis.mod.client.gui.diagnostic_tool;

import com.mojang.blaze3d.vertex.PoseStack;
import net.minecraft.network.chat.Component;
import net.minecraft.util.Mth;
import net.tardis.mod.client.gui.datas.CFLSubsystemGuiInfo;

import java.util.ArrayList;
import java.util.List;

public class CFLSubsytemInfoScreen extends DiagnosticToolBaseScreen{

    final List<CFLSubsystemGuiInfo.SubsystemInfo> infos = new ArrayList<>();

    public CFLSubsytemInfoScreen(CFLSubsystemGuiInfo info) {
        this.infos.addAll(info.infos);
    }

    @Override
    protected void init() {}

    @Override
    public void renderAxilliaryData(PoseStack pose, int mouseX, int mouseY, float partialTicks) {

        final int x = this.width / 2 - 85,
                y = this.height / 2 - 75;
        int usedHeight = 0;

        for(CFLSubsystemGuiInfo.SubsystemInfo info : this.infos){
            this.font.draw(pose, Component.translatable(info.type().getTranslationKey())
                    .append(" : ")
                            .append("%d%%".formatted(Mth.floor(info.health() * 100.0))),
                    x, y + usedHeight, 0x000000);
            usedHeight += this.font.lineHeight;
        }


    }
}
