package net.tardis.mod.client.gui.sonic;

import com.mojang.blaze3d.systems.RenderSystem;
import com.mojang.blaze3d.vertex.PoseStack;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.components.Button;
import net.minecraft.client.gui.components.EditBox;
import net.minecraft.client.gui.screens.Screen;
import net.minecraft.client.renderer.GameRenderer;
import net.minecraft.core.BlockPos;
import net.minecraft.network.chat.Component;
import net.minecraft.network.chat.MutableComponent;
import net.minecraft.resources.ResourceLocation;
import net.tardis.mod.Tardis;
import net.tardis.mod.client.gui.widgets.SonicTextWidget;
import net.tardis.mod.helpers.GuiHelper;
import net.tardis.mod.helpers.Helper;
import net.tardis.mod.network.Network;
import net.tardis.mod.network.packets.SonicBlockNamingMessage;

import javax.annotation.Nullable;

public class SonicNamingGui extends Screen {

    public static final MutableComponent TITLE = Component.translatable("screen.tardis.title.sonic_name");
    public static final Component SAVE_TRANS = Component.translatable("screen." + Tardis.MODID + ".sonic_name.save");
    public static final ResourceLocation TEXTURE = Helper.createRL("textures/screens/sonic/sonic_nametag_gui.png");

    final BlockPos editingBlock;
    public EditBox nameBox;
    public String existingName;

    int guiLeft;
    int guiTop;

    public SonicNamingGui(BlockPos pos, @Nullable String existingName) {
        super(TITLE);
        this.editingBlock = pos;
        this.existingName = existingName;
    }

    @Override
    protected void init() {
        super.init();
        this.guiLeft = (width - 159) / 2;
        this.guiTop = (height - 158) / 2;

        this.addRenderableWidget(this.nameBox = new SonicTextWidget(this.font, TEXTURE, this.guiLeft + 35, this.guiTop + 64, this.existingName == null ? Component.empty() : Component.literal(this.existingName)));
        this.addRenderableWidget(new Button.Builder(SAVE_TRANS, but -> {
            Network.sendToServer(new SonicBlockNamingMessage(this.editingBlock, Helper.nullableToOptional(this.nameBox.getValue())));
            Minecraft.getInstance().setScreen(null);
        })
                .pos((width - 25)  / 2, height - 22)
                .size(50, 20)
                .build());

        if(this.existingName != null)
            this.nameBox.setValue(this.existingName);
    }

    @Override
    public void renderBackground(PoseStack pose) {
        super.renderBackground(pose);
        RenderSystem.setShader(GameRenderer::getPositionTexShader);
        RenderSystem.setShaderTexture(0, TEXTURE);
        blit(pose, this.guiLeft, this.guiTop, 0, 0, 159, 158);
    }

    @Override
    public void render(PoseStack pose, int pMouseX, int pMouseY, float pPartialTick) {
        this.renderBackground(pose);
        super.render(pose, pMouseX, pMouseY, pPartialTick);
        GuiHelper.drawCenteredText(pose, font, TITLE, width / 2, 20, 0xFFFFFF);
    }
}
