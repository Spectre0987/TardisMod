package net.tardis.mod.client.gui.datas;

import net.minecraft.network.FriendlyByteBuf;
import net.tardis.mod.cap.level.ITardisLevel;

public class MonitorDematGuiData extends GuiData{
    public MonitorDematGuiData(int id) {
        super(id);
    }

    public MonitorDematGuiData fromTardis(ITardisLevel tardis){ // TODO: Make these unlockable
        return this;
    }

    @Override
    public void encode(FriendlyByteBuf buf) {

    }

    @Override
    public void decode(FriendlyByteBuf buf) {

    }
}
