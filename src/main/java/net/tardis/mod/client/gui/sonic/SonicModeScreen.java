package net.tardis.mod.client.gui.sonic;

import com.mojang.blaze3d.systems.RenderSystem;
import com.mojang.blaze3d.vertex.PoseStack;
import com.mojang.math.Axis;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.screens.Screen;
import net.minecraft.client.renderer.GameRenderer;
import net.minecraft.network.chat.Component;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.InteractionHand;
import net.tardis.mod.cap.items.ISonicCapability;
import net.tardis.mod.cap.items.functions.ItemFunctionType;
import net.tardis.mod.cap.items.functions.sonic.SonicFunctionType;
import net.tardis.mod.client.gui.widgets.SonicItemButton;
import net.tardis.mod.helpers.Helper;
import net.tardis.mod.network.Network;
import net.tardis.mod.network.packets.SonicSetFunctionMessage;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class SonicModeScreen extends Screen {

    public long ticks = 0;
    final ISonicCapability cap;
    final InteractionHand hand;
    final List<SonicFunctionType> activeTypes = new ArrayList<>();

    public static final ResourceLocation TEXTURE = Helper.createRL("textures/screens/sonic.png");

    public SonicModeScreen(ISonicCapability cap, InteractionHand hand, Collection<? extends ItemFunctionType<ISonicCapability>> types) {
        super(Component.empty());
        this.cap = cap;
        this.hand = hand;
        for(ItemFunctionType<ISonicCapability> t : types){
            if(t instanceof SonicFunctionType st)
                this.activeTypes.add(st);
        }
    }

    @Override
    protected void init() {
        super.init();

        int index = 0;
        final float angle = 360.0F / (float)this.activeTypes.size();
        final float distance = 55;

        for(SonicFunctionType type : this.activeTypes){

            int xOffset = (int)(Math.sin(Math.toRadians(angle * index)) * distance);
            int yOffset = (int)-(Math.cos(Math.toRadians(angle * index)) * distance);

            int x = width / 2 - 8 + xOffset,
                    y = height / 2 - 8 + yOffset;

            ++index;

            this.addRenderableWidget(new SonicItemButton(x, y, type.getDisplayItem(), TEXTURE, (but) -> {
                this.cap.setFunction(type);
                Network.sendToServer(new SonicSetFunctionMessage(this.hand, type));
                Minecraft.getInstance().setScreen(null);
            }));
        }

    }

    @Override
    public void render(PoseStack pose, int pMouseX, int pMouseY, float pPartialTick) {

        RenderSystem.setShader(GameRenderer::getPositionTexShader);
        RenderSystem.setShaderTexture(0, TEXTURE);

        blit(pose, width / 2 - 143 / 2, height / 2 - 143 / 2, 0, 0, 143, 143);

        pose.pushPose();
        pose.translate(width / 2, height / 2, 0);
        pose.mulPose(Axis.ZP.rotationDegrees(ticks % 360.0F));
        pose.translate(-width / 2, -height / 2, 0);
        blit(pose, width / 2 - 112 / 2, (height / 2 - 112 / 2) + 1, 143, 0, 112, 112);
        pose.popPose();

        super.render(pose, pMouseX, pMouseY, pPartialTick);

    }

    @Override
    public void tick() {
        super.tick();
        ++ticks;
    }
}
