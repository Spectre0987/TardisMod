package net.tardis.mod.client.gui.containers;

import com.mojang.blaze3d.systems.RenderSystem;
import com.mojang.blaze3d.vertex.PoseStack;
import net.minecraft.client.gui.screens.inventory.AbstractContainerScreen;
import net.minecraft.client.renderer.GameRenderer;
import net.minecraft.network.chat.Component;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.util.Mth;
import net.minecraft.world.entity.player.Inventory;
import net.tardis.mod.helpers.Helper;
import net.tardis.mod.registry.ItemRegistry;
import net.tardis.mod.menu.ARSMenu;

public class ARSPanelScreen extends AbstractContainerScreen<ARSMenu> {

    public static final ResourceLocation TEXTURE = Helper.createRL("textures/screens/containers/ars_panel.png");

    public int renderTicks;

    public ARSPanelScreen(ARSMenu pMenu, Inventory pPlayerInventory, Component pTitle) {
        super(pMenu, pPlayerInventory, pTitle);
        this.imageWidth = 176;
        this.imageHeight = 166;
    }

    @Override
    protected void init() {
        super.init();
    }

    @Override
    protected void renderBg(PoseStack pPoseStack, float pPartialTick, int pMouseX, int pMouseY) {
        RenderSystem.setShader(GameRenderer::getPositionTexShader);
        RenderSystem.setShaderTexture(0, TEXTURE);
        blit(pPoseStack, (this.width - this.imageWidth) / 2, (this.height - this.imageHeight) / 2, 0, 0, this.imageWidth, this.imageHeight);

        //Blinking lights
        if(renderTicks % 40 < 20) {
            //Circuit fault light
            if(!this.areCircuitsComplete())
                blit(pPoseStack, this.leftPos + 119, this.topPos + 26, 177, 1, 9, 9);
            //Fluid link fault light
            if(!isFluidLinkReady())
                blit(pPoseStack, this.leftPos + 139, this.topPos + 26, 177, 1, 9, 9);
        }
        //Progress bar
        float barPercent = 1.0F;
        blit(pPoseStack, this.leftPos + 115, this.topPos + 52, 177, 28, Mth.floor(18 * barPercent), 9);

        //Big blue
        if(1 == 1) {
            blit(pPoseStack, this.leftPos + 135, this.topPos + 49, 177, 11, 16, 16);
        }
    }

    public boolean areCircuitsComplete(){
        for(int i = 0; i < 3; ++i){
            if(this.getMenu().getSlot(i).getItem().getItem() != ItemRegistry.DATA_CRYSTAL.get()){
                return false;
            }
        }
        return true;
    }
    public boolean isFluidLinkReady(){
        return this.menu.getSlot(3).getItem().getItem() == ItemRegistry.FLUID_LINKS.get();
    }

    @Override
    protected void containerTick() {
        super.containerTick();
        ++this.renderTicks;
    }

    @Override
    public void render(PoseStack pPoseStack, int pMouseX, int pMouseY, float pPartialTick) {
        super.render(pPoseStack, pMouseX, pMouseY, pPartialTick);

        this.renderTooltip(pPoseStack, pMouseX, pMouseY);
    }
}
