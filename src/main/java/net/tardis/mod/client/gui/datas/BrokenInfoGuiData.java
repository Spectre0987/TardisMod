package net.tardis.mod.client.gui.datas;

import com.mojang.datafixers.util.Pair;
import net.minecraft.network.FriendlyByteBuf;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.crafting.Ingredient;
import net.minecraft.world.level.ItemLike;
import net.tardis.mod.blockentities.BrokenExteriorTile;
import net.tardis.mod.emotional.Trait;
import net.tardis.mod.exterior.ExteriorType;
import net.tardis.mod.misc.ObjectOrTagCodec;
import net.tardis.mod.registry.ExteriorRegistry;

import java.util.ArrayList;
import java.util.List;

public class BrokenInfoGuiData extends GuiData{

    public ExteriorType exteriorType;
    public List<Ingredient> unlockables = new ArrayList<>();

    public BrokenInfoGuiData(int id) {
        super(id);
    }

    public BrokenInfoGuiData(int id, BrokenExteriorTile tile){
        this(id);
        this.setFromTile(tile);
    }

    public BrokenInfoGuiData setFromTile(BrokenExteriorTile tile){
        tile.getExterior().ifPresent(ext -> this.exteriorType = ext);
        for(Trait t : tile.getTraits()){
            t.getLikedItems().stream().map(Pair::getFirst)
                    .forEach(e -> {
                        if(e.isTag())
                            this.unlockables.add(Ingredient.of(e.getTag()));
                        else this.unlockables.add(Ingredient.of(e.getObject()));
                    });
        }
        return this;
    }

    @Override
    public void encode(FriendlyByteBuf buf) {
        buf.writeRegistryId(ExteriorRegistry.REGISTRY.get(), this.exteriorType);
        buf.writeInt(this.unlockables.size());
        for(Ingredient ing : this.unlockables){
            ing.toNetwork(buf);
        }
    }

    @Override
    public void decode(FriendlyByteBuf buf) {

        this.unlockables.clear();

        this.exteriorType = buf.readRegistryId();
        final int unlockSize = buf.readInt();
        for(int i = 0; i < unlockSize; ++i){
            this.unlockables.add(Ingredient.fromNetwork(buf));
        }

    }
}
