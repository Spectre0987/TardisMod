package net.tardis.mod.client.gui.diagnostic_tool;

import com.mojang.blaze3d.vertex.PoseStack;
import net.minecraft.network.chat.Component;
import net.tardis.mod.client.gui.datas.DiagTardisInfoData;
import net.tardis.mod.client.gui.widgets.TextGroup;
import net.tardis.mod.client.monitor_world_text.FuelMonitorEntry;
import net.tardis.mod.client.monitor_world_text.LocationalMonitorEntry;

public class DiagTardisInfoScreen extends DiagnosticToolBaseScreen{

    public static final String IN_FLIGHT_TRANS = "cfl.tardis.in_flight";

    final DiagTardisInfoData data;

    TextGroup infoGroup;

    public DiagTardisInfoScreen(DiagTardisInfoData data){
        this.data = data;
    }

    @Override
    protected void init() {
        this.infoGroup = new TextGroup(width / 2, height / 2 - 60, 100 , 0x000000, this.font)
                .addText(Component.translatable(LocationalMonitorEntry.LOCATION_KEY,
                        data.location.getPos().getX(),
                        data.location.getPos().getY(),
                        data.location.getPos().getZ()))
                .addText(Component.translatable(LocationalMonitorEntry.DEST_KEY,
                        data.destination.getPos().getX(),
                        data.destination.getPos().getY(),
                        data.destination.getPos().getZ()
                        ))
                .addText(Component.translatable(FuelMonitorEntry.TRANS_KEY, data.currentArtron))
                .addText(Component.translatable(IN_FLIGHT_TRANS, this.data.isInFlight))
                .centered()
        ;
    }

    @Override
    public void renderAxilliaryData(PoseStack pose, int mouseX, int mouseY, float partialTicks) {

        infoGroup.render(pose, mouseX, mouseY, partialTicks);

    }
}
