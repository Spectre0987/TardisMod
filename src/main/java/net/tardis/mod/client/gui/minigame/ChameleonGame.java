package net.tardis.mod.client.gui.minigame;

import com.mojang.blaze3d.vertex.PoseStack;
import net.minecraft.client.gui.screens.Screen;
import net.minecraft.network.chat.Component;
import net.tardis.mod.Constants;
import net.tardis.mod.Tardis;
import net.tardis.mod.client.gui.minigame.misc.GameGrid;

public class ChameleonGame extends Screen {

    public static Component TITLE = Component.translatable(Constants.Translation.makeGuiTitleTranslation("chameleon"));
    final GameGrid grid = new GameGrid(0, 0, 0, 0, 0, 0);

    public ChameleonGame() {
        super(TITLE);
    }

    @Override
    public void render(PoseStack pPoseStack, int pMouseX, int pMouseY, float pPartialTick) {
        super.render(pPoseStack, pMouseX, pMouseY, pPartialTick);
    }
}
