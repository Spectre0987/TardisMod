package net.tardis.mod.client.gui.datas;

import net.minecraft.network.FriendlyByteBuf;

public class EmptyGuiData extends GuiData{
    public EmptyGuiData(int id) {
        super(id);
    }

    @Override
    public void encode(FriendlyByteBuf buf) {

    }

    @Override
    public void decode(FriendlyByteBuf buf) {

    }
}
