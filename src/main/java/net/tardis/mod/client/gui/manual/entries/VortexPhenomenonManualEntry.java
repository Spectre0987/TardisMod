package net.tardis.mod.client.gui.manual.entries;

import com.mojang.blaze3d.vertex.PoseStack;
import com.mojang.serialization.Codec;
import com.mojang.serialization.DataResult;
import com.mojang.serialization.codecs.RecordCodecBuilder;
import net.minecraft.client.gui.Font;
import net.minecraft.client.gui.screens.Screen;
import net.minecraft.resources.ResourceLocation;
import net.tardis.mod.client.gui.manual.ManualChapter;
import net.tardis.mod.client.gui.monitor.MonitorFlightCourseScreen;
import net.tardis.mod.client.gui.monitor.vortex_phenomena.VortexPhenomenaRenderer;
import net.tardis.mod.misc.tardis.vortex.VortexPhenomenaType;
import net.tardis.mod.registry.VortexPhenomenaRegistry;

public class VortexPhenomenonManualEntry extends TextEntry{

    public static final Codec<VortexPhenomenonManualEntry> CODEC = RecordCodecBuilder.create(instance ->
            instance.group(
                    EntryType.CODEC.fieldOf("type").forGetter(ManualEntry::getEntryType),
                    ManualChapter.CODEC.fieldOf("chapter").forGetter(ManualEntry::getChapter),
                    ResourceLocation.CODEC.comapFlatMap(
                        rl -> DataResult.success(VortexPhenomenaRegistry.REGISTRY.get().getValue(rl)),
                            vp -> VortexPhenomenaRegistry.REGISTRY.get().getKey(vp)
                    ).fieldOf("vortex_phenomenon").forGetter(v -> v.vpType),
                    Codec.STRING.fieldOf("text").forGetter(v -> v.text)
            ).apply(instance, VortexPhenomenonManualEntry::new)
    );

    public final VortexPhenomenaType vpType;

    public VortexPhenomenonManualEntry(EntryType<?> type, ManualChapter chapter, VortexPhenomenaType<?> vpType, String text) {
        super(type, chapter, text);
        this.vpType = vpType;
    }

    @Override
    public void render(PoseStack pose, Font font, int x, int y, int pageWidth, int pageHeight, int mouseX, int mouseY) {

        for(VortexPhenomenaRenderer renderer : MonitorFlightCourseScreen.RENDERERS){
            if(renderer.isValid(this.vpType)){
                pose.pushPose();

                final int vpSize = 18,
                    backgroundX = x + (pageWidth - vpSize) / 2,
                    backgroundY = y + 4;
                Screen.fill(pose, backgroundX, backgroundY, backgroundX + vpSize, backgroundY + vpSize, 0xFF000000);

                renderer.setupRenderer(pose);
                renderer.render(pose, this.vpType, x + pageWidth / 2, y + 14, 8);
                renderer.endRenderer(pose);
                pose.popPose();
                break;
            }
        }

        super.render(pose, font, x, y, pageWidth, pageHeight, mouseX, mouseY);
    }

    @Override
    public int nonTextSpace() {
        return 35;
    }

    @Override
    public boolean isFullPage() {
        return true;
    }

    @Override
    public int getHeightUsed(Font font, int width) {
        return super.getHeightUsed(font, width);
    }
}
