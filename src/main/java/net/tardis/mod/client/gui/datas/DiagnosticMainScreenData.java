package net.tardis.mod.client.gui.datas;

import net.minecraft.core.registries.Registries;
import net.minecraft.network.FriendlyByteBuf;
import net.minecraft.resources.ResourceKey;
import net.minecraft.server.MinecraftServer;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.level.Level;
import net.tardis.mod.helpers.Helper;

import java.util.Optional;

public class DiagnosticMainScreenData extends GuiData{

    public InteractionHand hand;
    public Optional<ResourceKey<Level>> tardis;

    public DiagnosticMainScreenData(int id) {
        super(id);
    }

    public DiagnosticMainScreenData setHand(InteractionHand hand){
        this.hand = hand;
        return this;
    }

    @Override
    public void encode(FriendlyByteBuf buf) {
        buf.writeEnum(this.hand);
        Helper.encodeOptional(buf, this.tardis, (item, b) -> b.writeResourceKey(item));
    }

    @Override
    public void decode(FriendlyByteBuf buf) {
        this.hand = buf.readEnum(InteractionHand.class);
        this.tardis = Helper.decodeOptional(buf, b -> b.readResourceKey(Registries.DIMENSION));
    }

    public DiagnosticMainScreenData setTardis(Optional<ResourceKey<Level>> boundTardis) {
        this.tardis = boundTardis;
        return this;
    }
}
