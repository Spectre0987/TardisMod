package net.tardis.mod.client.gui.datas;

import net.minecraft.core.Vec3i;
import net.minecraft.network.FriendlyByteBuf;
import net.tardis.mod.blockentities.dev.TardisStructureTile;

import java.util.Optional;

public class StructureBlockData extends GuiData{

    public Optional<Vec3i> size = Optional.empty();

    public StructureBlockData(int id) {
        super(id);
    }

    public StructureBlockData from(TardisStructureTile tile){
        this.size = tile.getSize();
        return this;
    }

    @Override
    public void encode(FriendlyByteBuf buf) {
        buf.writeBoolean(this.size.isPresent());
        this.size.ifPresent(s -> {
            buf.writeInt(s.getX());
            buf.writeInt(s.getY());
            buf.writeInt(s.getZ());
        });
    }

    @Override
    public void decode(FriendlyByteBuf buf) {
        boolean has = buf.readBoolean();
        if(!has){
            this.size = Optional.empty();
            return;
        }
        final int x = buf.readInt(),
                y = buf.readInt(),
                z = buf.readInt();
        this.size = Optional.of(new Vec3i(x, y, z));
    }
}
