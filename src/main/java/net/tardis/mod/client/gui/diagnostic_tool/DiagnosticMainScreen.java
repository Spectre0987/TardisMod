package net.tardis.mod.client.gui.diagnostic_tool;

import net.minecraft.client.Minecraft;
import net.minecraft.network.chat.Component;
import net.minecraft.world.InteractionHand;
import net.tardis.mod.cap.items.ICFLTool;
import net.tardis.mod.cap.items.functions.cfl.CFLFunctionType;
import net.tardis.mod.client.gui.widgets.TextOption;
import net.tardis.mod.misc.ObjectHolder;
import net.tardis.mod.network.Network;
import net.tardis.mod.network.packets.ItemFunctionMessage;

public class DiagnosticMainScreen extends DiagnosticToolBaseScreen {


    public final InteractionHand hand;
    public final ICFLTool tool;


    public DiagnosticMainScreen(InteractionHand hand, ICFLTool tool){
        this.hand = hand;
        this.tool = tool;
    }

    @Override
    protected void init() {
        super.init();

        final int x = this.width / 2 - 100, y = this.height / 2 - 75;
        final ObjectHolder<Integer> currentY = new ObjectHolder<>(y);

        for(CFLFunctionType type : tool.getAllValidTypes()){

            tool.getFunctionByType(type).ifPresent(function -> {
                if(function.shouldDisplay(tool)) {
                    this.addRenderableWidget(new TextOption(x, currentY.get(), this.font, tool.getFunctionByType(type).get().getDisplayName(), b -> {
                        tool.setFunction(type);
                        function.onSelected(Minecraft.getInstance().player, this.hand);
                        Network.sendToServer(new ItemFunctionMessage(this.hand, type, function.serializeNBT()));
                        Minecraft.getInstance().setScreen(null);
                    }));

                    currentY.set(currentY.get() + font.lineHeight + 3);
                }
            });

        }
        this.addRenderableWidget(new TextOption(x, currentY.get(), this.font, Component.literal("Cancel"), b -> {
            tool.setFunction(null);
            Network.sendToServer(new ItemFunctionMessage(this.hand, null, null));
        }));
    }
}
