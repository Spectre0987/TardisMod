package net.tardis.mod.client.gui;

import com.mojang.blaze3d.vertex.PoseStack;
import net.minecraft.client.gui.components.EditBox;
import net.minecraft.client.gui.components.Renderable;
import net.minecraft.client.gui.components.events.GuiEventListener;
import net.minecraft.client.gui.screens.Screen;
import net.minecraft.client.gui.screens.inventory.StructureBlockEditScreen;
import net.minecraft.core.Vec3i;
import net.minecraft.network.chat.Component;
import org.jline.reader.Widget;

import java.util.Optional;

public class StructureScreen extends Screen {

    public final Optional<Vec3i> size;

    public StructureScreen(Optional<Vec3i> size) {
        super(Component.empty());
        this.size = size;
    }

    @Override
    protected void init() {
        super.init();
        this.children().clear();

        int middleX = width / 2,
                middleY = height / 2 - 100;

        this.addRenderableWidget(new EditBox(this.font, middleX - 40, middleY, 30, 20, Component.empty()));
        this.addRenderableWidget(new EditBox(this.font, middleX, middleY, 30, 20, Component.empty()));
        this.addRenderableWidget(new EditBox(this.font, middleX + 40, middleY, 30, 20, Component.empty()));

        this.addRenderableWidget(new EditBox(this.font, middleX - 40, middleY + 30, 110, 20, Component.empty()));

    }

    @Override
    public void render(PoseStack pPoseStack, int pMouseX, int pMouseY, float pPartialTick) {
        super.render(pPoseStack, pMouseX, pMouseY, pPartialTick);
        this.renderBackground(pPoseStack);

        for(Renderable listener : this.renderables){
            listener.render(pPoseStack, pMouseX, pMouseY, pPartialTick);
        }

    }
}
