package net.tardis.mod.client.renderers.entities;

import com.mojang.blaze3d.vertex.PoseStack;
import com.mojang.math.Axis;
import net.minecraft.client.model.HierarchicalModel;
import net.minecraft.client.renderer.MultiBufferSource;
import net.minecraft.client.renderer.entity.EntityRenderer;
import net.minecraft.client.renderer.entity.EntityRendererProvider;
import net.minecraft.client.renderer.texture.OverlayTexture;
import net.minecraft.resources.ResourceLocation;
import net.tardis.mod.client.animations.AnimationHelper;
import net.tardis.mod.client.animations.demat.DematAnimation;
import net.tardis.mod.client.models.exteriors.entities.ImpalaExteriorModel;
import net.tardis.mod.client.renderers.exteriors.ExteriorRenderer;
import net.tardis.mod.entity.CarExteriorEntity;
import net.tardis.mod.helpers.Helper;
import net.tardis.mod.misc.enums.MatterState;

import java.util.function.Function;

public class ImpalaExteriorRenderer<T extends CarExteriorEntity, M extends HierarchicalModel<T>> extends EntityRenderer<T> {

    public static final ResourceLocation TEXTURE = Helper.createRL("textures/entities/impala.png");
    private final M model;

    public ImpalaExteriorRenderer(EntityRendererProvider.Context pContext, M model) {
        super(pContext);
        this.model = model;
    }

    @Override
    public ResourceLocation getTextureLocation(T pEntity) {
        return TEXTURE;
    }

    @Override
    public void render(T pEntity, float pEntityYaw, float pPartialTick, PoseStack pose, MultiBufferSource pBuffer, int pPackedLight) {
        pose.pushPose();
        pose.mulPose(Axis.XP.rotationDegrees(180));
        pose.mulPose(Axis.YP.rotationDegrees(pEntityYaw));
        pose.translate(0, -1.5, -1.5);

        final DematAnimation dematAnim = ExteriorRenderer.getAnimationType(pEntity.getMatterStateHandler().getDematType());
        final float[] colors = dematAnim.getColors(pEntity.getMatterStateHandler(), pPartialTick);

        if(pEntity.getMatterStateHandler().getMatterState() != MatterState.SOLID){
            dematAnim.modifyPose(pose, pEntity.getMatterStateHandler(), pPartialTick);
            dematAnim.renderExtra(pose, pEntity.getMatterStateHandler(), null, pBuffer, pPartialTick, pPackedLight, OverlayTexture.NO_OVERLAY);
        }


        this.model.setupAnim(pEntity, 0, 0, pPartialTick, 0, 0);
        model.renderToBuffer(pose, pBuffer.getBuffer(this.model.renderType(getTextureLocation(pEntity))), pPackedLight, OverlayTexture.NO_OVERLAY,
                colors[0], colors[1], colors[2], colors[3]);
        super.render(pEntity, pEntityYaw, pPartialTick, pose, pBuffer, pPackedLight);

        pose.popPose();
    }
}
