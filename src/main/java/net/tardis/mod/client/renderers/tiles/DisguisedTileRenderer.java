package net.tardis.mod.client.renderers.tiles;

import com.mojang.blaze3d.vertex.PoseStack;
import com.mojang.blaze3d.vertex.VertexConsumer;
import com.mojang.math.Axis;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.MultiBufferSource;
import net.minecraft.client.renderer.RenderType;
import net.minecraft.client.renderer.blockentity.BlockEntityRenderer;
import net.minecraft.client.renderer.blockentity.BlockEntityRendererProvider;
import net.minecraft.client.resources.model.BakedModel;
import net.minecraft.util.RandomSource;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraftforge.client.model.data.ModelData;
import net.tardis.mod.blockentities.DisguisedBlockTile;
import net.tardis.mod.blockentities.IDisguisedBlock;
import net.tardis.mod.client.TardisRenderTypes;
import net.tardis.mod.client.animations.AnimationHelper;
import net.tardis.mod.client.animations.demat.DematAnimation;
import net.tardis.mod.client.renderers.exteriors.ExteriorRenderer;
import net.tardis.mod.misc.IHaveMatterState;
import net.tardis.mod.misc.MatterStateHandler;
import net.tardis.mod.misc.enums.MatterState;

public class DisguisedTileRenderer<T extends BlockEntity & IDisguisedBlock> implements BlockEntityRenderer<T> {

    public DisguisedTileRenderer(BlockEntityRendererProvider.Context context){}

    @Override
    public void render(T entity, float pPartialTick, PoseStack pose, MultiBufferSource source, int pPackedLight, int pPackedOverlay) {

        pose.pushPose();
        final MatterStateHandler matterHandler = entity instanceof IHaveMatterState matter ? matter.getMatterStateHandler() : null;
        final DematAnimation animation = matterHandler != null ? ExteriorRenderer.getAnimationType(matterHandler.getDematType()) : null;
        final float[] colors = animation != null ? animation.getColors(matterHandler, pPartialTick) : DematAnimation.DEFAULT_COLORS;

        if(matterHandler != null && matterHandler.getMatterState() != MatterState.SOLID && animation != null){
            pose.mulPose(Axis.XP.rotationDegrees(180));
            animation.modifyPose(pose, matterHandler, pPartialTick);
            pose.mulPose(Axis.XN.rotationDegrees(180));
        }

        if(entity.getDisguisedState() != null){
            final BakedModel model = Minecraft.getInstance().getBlockRenderer().getBlockModel(entity.getDisguisedState());
            final RandomSource random = RandomSource.create(42l);
            if(model != null){

                final boolean isDematting = matterHandler != null && matterHandler.getMatterState() != MatterState.SOLID;

                ModelData data = model.getModelData(entity.getLevel(), entity.getBlockPos(), entity.getDisguisedState(), entity.getModelData());
                for(RenderType type : model.getRenderTypes(entity.getDisguisedState(), random, data)) {
                    Minecraft.getInstance().getBlockRenderer().getModelRenderer().tesselateWithAO(
                            entity.getLevel(), model, entity.getDisguisedState(), entity.getBlockPos(), pose, source.getBuffer(isDematting ? TardisRenderTypes.DEMAT_BLOCK_TYPE.apply(colors[3]) : type),
                            true, random, 42l, pPackedOverlay
                    );
                }
            }
            if(entity.getDisguisedTile() != null){
                BlockEntityRenderer<BlockEntity> renderer = Minecraft.getInstance().getBlockEntityRenderDispatcher().getRenderer(entity.getDisguisedTile());
                if(renderer != null){
                    try{
                        renderer.render(entity.getDisguisedTile(), pPartialTick, pose, source, pPackedLight, pPackedOverlay);
                    }
                    catch (Exception e){

                    }
                }
            }
        }

        pose.popPose();
    }
}
