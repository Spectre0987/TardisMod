package net.tardis.mod.client.renderers;

import com.mojang.blaze3d.vertex.PoseStack;
import com.mojang.math.Axis;
import net.minecraft.client.Minecraft;
import net.minecraft.client.model.geom.EntityModelSet;
import net.minecraft.client.renderer.BlockEntityWithoutLevelRenderer;
import net.minecraft.client.renderer.MultiBufferSource;
import net.minecraft.client.renderer.RenderType;
import net.minecraft.client.renderer.blockentity.BlockEntityRenderDispatcher;
import net.minecraft.client.renderer.texture.TextureAtlas;
import net.minecraft.client.resources.model.BakedModel;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.server.packs.resources.ResourceManager;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemDisplayContext;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.block.Block;
import net.minecraftforge.registries.ForgeRegistries;
import net.minecraftforge.registries.RegistryObject;
import net.tardis.mod.Constants;
import net.tardis.mod.cap.Capabilities;
import net.tardis.mod.cap.items.ISonicCapability;
import net.tardis.mod.client.ModelHolder;
import net.tardis.mod.client.SonicPartModelLoader;
import net.tardis.mod.registry.ItemRegistry;
import net.tardis.mod.registry.SonicPartRegistry;
import net.tardis.mod.sonic_screwdriver.SonicPartConnectionPoint;
import net.tardis.mod.sonic_screwdriver.SonicPartSlot;
import org.joml.Vector3f;

import java.util.*;
import java.util.function.Predicate;

public class SpecialItemRenderer extends BlockEntityWithoutLevelRenderer {

    private static final List<ModelHolder<ItemStack, ?>> MODELS = new ArrayList<>();

    public SpecialItemRenderer(BlockEntityRenderDispatcher pBlockEntityRenderDispatcher, EntityModelSet set) {
        super(pBlockEntityRenderDispatcher, set);
        this.bakeModels(set);
    }

    @Override
    public void onResourceManagerReload(ResourceManager pResourceManager) {
        super.onResourceManagerReload(pResourceManager);
        this.bakeModels(Minecraft.getInstance().getEntityModels());
    }

    public void bakeModels(EntityModelSet set){

        for(ModelHolder<ItemStack, ?> holder : MODELS){
            holder.bake(set);
        }
    }

    public static void register(ModelHolder<ItemStack, ?> holder){
        MODELS.add(holder);
    }

    public static Item getBlockItemOf(RegistryObject<? extends Block> block){
        return block.get().asItem();
    }

    public static Predicate<ItemStack> isBlock(RegistryObject<? extends Block> block){
        return Constants.Predicates.isItem(getBlockItemOf(block));
    }

    @Override
    public void renderByItem(ItemStack stack, ItemDisplayContext context, PoseStack pPoseStack, MultiBufferSource buffer, int packedLight, int packedOverlay) {
        pPoseStack.pushPose();

        for(ModelHolder<ItemStack, ?> holder : MODELS){
            if(holder.shouldRenderFor(stack) && holder.getModel().isPresent()){
                pPoseStack.translate(0.5F, 0.9F, 0.5F);
                pPoseStack.mulPose(Axis.XP.rotationDegrees(22.5F));
                pPoseStack.mulPose(Axis.ZP.rotationDegrees(180));
                pPoseStack.mulPose(Axis.YP.rotationDegrees(22.5F));
                pPoseStack.scale(0.45F, 0.45F, 0.45F);
                holder.applyTranslations(pPoseStack);
                holder.getModel().get().renderToBuffer(pPoseStack, buffer.getBuffer(holder.getRenderType()), packedLight, packedOverlay, 1.0F, 1.0F, 1.0F, 1.0F);
                break;
            }
        }

        //Sonics
        if(stack.getItem() == ItemRegistry.SONIC.get()){
            stack.getCapability(Capabilities.SONIC).ifPresent(sonic -> {
                    pPoseStack.pushPose();
                    pPoseStack.translate(4 / 16.0 ,10 / 16.0 ,7 / 16.0);
                    pPoseStack.mulPose(Axis.XP.rotationDegrees(90));
                    pPoseStack.scale(0.5F, 0.5F, 0.5F);

                    getConnectionPointFor(sonic, SonicPartSlot.HANDLE).ifPresent(handle -> {

                        //BakedModel handleModel = Minecraft.getInstance().getModelManager().getModel(handle.model());
                        //handleModel.applyTransform(context, pPoseStack, false);

                        renderConnectionPoint(pPoseStack, stack, sonic, handle, new ArrayList<>(), packedLight, packedOverlay, buffer);
                    });
                    pPoseStack.popPose();
                });
        }

        pPoseStack.popPose();
    }

    public static void renderConnectionPoint(PoseStack pose, ItemStack stack, ISonicCapability cap, SonicPartConnectionPoint point, List<SonicPartConnectionPoint> previouslyRenderered, int packedLight, int packedOverlay, MultiBufferSource source){
        pose.pushPose();

        pose.pushPose();
        BakedModel model = Minecraft.getInstance().getModelManager().getModel(point.model());
        Minecraft.getInstance().getItemRenderer().renderModelLists(model, stack, packedLight, packedOverlay,
                pose, source.getBuffer(RenderType.entityCutout(TextureAtlas.LOCATION_BLOCKS))
        );
        pose.popPose();

        previouslyRenderered.add(point);
        //render linked points
        for(Map.Entry<SonicPartSlot, Vector3f> entry : point.connectionPoints().entrySet()){
            pose.pushPose();
            final SonicPartConnectionPoint newPoint = getConnectionPointFor(cap, entry.getKey()).get();
            if(!previouslyRenderered.contains(newPoint)){
                pose.translate(entry.getValue().x, entry.getValue().y, entry.getValue().z);
                newPoint.getAttachPointFor(point.partSlot()).ifPresent(pos -> {
                    pose.translate(-pos.x, -pos.y, -pos.z);
                });
                renderConnectionPoint(pose, stack, cap, newPoint, previouslyRenderered, packedLight, packedOverlay, source);
            }
            pose.popPose();
        }
        pose.popPose();
    }

    public static Optional<SonicPartConnectionPoint> getConnectionPointFor(ISonicCapability cap, SonicPartSlot slot){
        final ResourceLocation key = SonicPartRegistry.REGISTRY.get().getKey(cap.getSonicPart(slot))
                .withPrefix("sonic_parts/");
        final SonicPartConnectionPoint point = SonicPartModelLoader.INSTANCE.points.get(key);
        return point != null ? Optional.of(point) : Optional.empty();
    }

}
