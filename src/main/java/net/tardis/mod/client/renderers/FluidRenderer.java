package net.tardis.mod.client.renderers;

import com.mojang.blaze3d.vertex.Tesselator;
import com.mojang.blaze3d.vertex.VertexConsumer;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.MultiBufferSource;
import net.minecraft.client.renderer.texture.TextureAtlas;
import net.minecraft.client.renderer.texture.TextureAtlasSprite;
import net.minecraft.util.Mth;
import net.minecraft.world.level.material.Fluid;
import net.minecraft.world.level.material.FluidState;
import net.minecraft.world.level.material.Fluids;
import net.minecraftforge.client.extensions.common.IClientFluidTypeExtensions;
import net.minecraftforge.fluids.FluidStack;
import net.minecraftforge.fluids.FluidType;
import net.tardis.mod.client.TardisRenderTypes;

import java.awt.*;

public class FluidRenderer {


    public static void renderIntoGui(FluidStack stack, int startX, int startY, int width, int height, float percentFull){
        if(stack.isEmpty() || stack.getFluid().getFluidType() == null)
            return;
        renderIntoGui(stack.getFluid().getFluidType(), startX, startY, width, height, percentFull);
    }

    public static void renderIntoGui(FluidType fluid, int startX, int startY, int width, int height, float percentFull){

        if (fluid.getRenderPropertiesInternal() instanceof IClientFluidTypeExtensions client){
            TextureAtlasSprite sprite = Minecraft.getInstance().getTextureAtlas(TextureAtlas.LOCATION_BLOCKS)
                    .apply(client.getStillTexture());

            MultiBufferSource.BufferSource source = MultiBufferSource.immediate(Tesselator.getInstance().getBuilder());
            VertexConsumer buffer = source.getBuffer(TardisRenderTypes.GUI_FLUID.apply(TextureAtlas.LOCATION_BLOCKS));

            float aspectRatio = width / (float)height;
            float uSize = sprite.getU1() - sprite.getU0();
            float vSize = sprite.getV1() - sprite.getV0();
            float centerU = sprite.getU0() + uSize / 2.0F, centerV = sprite.getV0() + vSize / 2.0F;

            float startU = centerU - (uSize * aspectRatio / 2.0F), startV = centerV - (vSize / 2.0F);
            float endU = centerU + ((uSize * aspectRatio / 2.0F) * aspectRatio);
            float endV = centerV + ((vSize / 2.0F) * percentFull);


            final int color = client.getTintColor();

            final int pixelSize = (int)Math.floor(height * (1.0 - Mth.clamp(percentFull, 0, 1F)));

            buffer.vertex(startX, startY + pixelSize, 0).color(color).uv(startU, startV).endVertex();
            buffer.vertex(startX, startY + height, 0).color(color).uv(startU, endV).endVertex();
            buffer.vertex(startX + width, startY + height, 0).color(color).uv(endU, endV).endVertex();
            buffer.vertex(startX + width, startY + pixelSize, 0).color(color).uv(endU, startV).endVertex();

            source.endBatch();

        }
    }


}
