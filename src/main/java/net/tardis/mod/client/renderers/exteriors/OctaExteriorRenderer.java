package net.tardis.mod.client.renderers.exteriors;

import net.minecraft.client.model.geom.EntityModelSet;
import net.minecraft.client.renderer.blockentity.BlockEntityRendererProvider;
import net.minecraft.resources.ResourceLocation;
import net.tardis.mod.blockentities.exteriors.OctaExteriorTile;
import net.tardis.mod.client.models.exteriors.OctaExteriorModel;
import net.tardis.mod.helpers.Helper;

public class OctaExteriorRenderer extends ExteriorRenderer<OctaExteriorTile, OctaExteriorModel<OctaExteriorTile>>{

    public static final ResourceLocation TEXTURES = Helper.createRL("textures/tiles/exteriors/octacapsule.png");

    public OctaExteriorRenderer(BlockEntityRendererProvider.Context context) {
        super(context);
    }

    @Override
    public OctaExteriorModel<OctaExteriorTile> bakeModel(EntityModelSet set) {
        return new OctaExteriorModel<>(set.bakeLayer(OctaExteriorModel.LAYER_LOCATION));
    }

    @Override
    public ResourceLocation getTexture(OctaExteriorTile exterior) {
        return TEXTURES;
    }
}
