package net.tardis.mod.client.renderers.exteriors;

import com.mojang.blaze3d.vertex.PoseStack;
import com.mojang.blaze3d.vertex.VertexConsumer;
import com.mojang.math.Axis;
import net.minecraft.client.renderer.MultiBufferSource;
import net.minecraft.client.renderer.RenderType;
import net.minecraft.client.renderer.blockentity.BlockEntityRendererProvider;
import net.tardis.mod.blockentities.exteriors.ChameleonExteriorTile;
import net.tardis.mod.client.TardisRenderTypes;
import net.tardis.mod.client.renderers.tiles.DisguisedTileRenderer;
import net.tardis.mod.helpers.WorldHelper;

public class ChameleonExteriorRenderer extends DisguisedTileRenderer<ChameleonExteriorTile> {
    public ChameleonExteriorRenderer(BlockEntityRendererProvider.Context context) {
        super(context);
    }

    @Override
    public void render(ChameleonExteriorTile entity, float pPartialTick, PoseStack pose, MultiBufferSource source, int pPackedLight, int pPackedOverlay) {
        super.render(entity, pPartialTick, pose, source, pPackedLight, pPackedOverlay);
        if(entity.getDoorHandler().getDoorState().isOpen()){
            final VertexConsumer consumer = source.getBuffer(TardisRenderTypes.COLOR);
            final int color = 0xFF000000;
            pose.pushPose();
            pose.translate(0.5, 0, 0.5);
            pose.mulPose(Axis.YP.rotationDegrees(WorldHelper.getFacingAngle(entity.getBlockState())));
            pose.translate(-0.5, 0, -0.5);
            consumer.vertex(pose.last().pose(), 0.1F, 0, -0.001F).color(color).endVertex();
            consumer.vertex(pose.last().pose(), 0.1F, 2, -0.001F).color(color).endVertex();
            consumer.vertex(pose.last().pose(), 0.9F, 2, -0.001F).color(color).endVertex();
            consumer.vertex(pose.last().pose(), 0.9F, 0, -0.001F).color(color).endVertex();
            pose.popPose();
        }
    }
}
