package net.tardis.mod.client.renderers.tiles;

import com.mojang.blaze3d.vertex.PoseStack;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.MultiBufferSource;
import net.minecraft.client.renderer.blockentity.BlockEntityRenderer;
import net.minecraft.client.renderer.blockentity.BlockEntityRendererProvider;
import net.minecraft.world.item.ItemDisplayContext;
import net.tardis.mod.blockentities.machines.MatterBufferTile;

public class MatterBufferRenderer implements BlockEntityRenderer<MatterBufferTile> {

    public MatterBufferRenderer(BlockEntityRendererProvider.Context context){}

    @Override
    public void render(MatterBufferTile buffer, float pPartialTick, PoseStack pose, MultiBufferSource source, int pPackedLight, int pPackedOverlay) {

        pose.pushPose();
        pose.translate(0.5, 0.1, 0.5);
        if(!buffer.getInventory().getStackInSlot(0).isEmpty()){
            Minecraft.getInstance().getItemRenderer().renderStatic(buffer.getInventory().getStackInSlot(0), ItemDisplayContext.FIXED, pPackedLight, pPackedOverlay, pose, source, Minecraft.getInstance().level, 0);
        }
        pose.popPose();

    }
}
