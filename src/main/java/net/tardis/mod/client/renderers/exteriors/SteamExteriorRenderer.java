package net.tardis.mod.client.renderers.exteriors;

import net.minecraft.client.model.geom.EntityModelSet;
import net.minecraft.client.renderer.blockentity.BlockEntityRendererProvider;
import net.minecraft.resources.ResourceLocation;
import net.tardis.mod.blockentities.exteriors.SteamExteriorTile;
import net.tardis.mod.client.models.exteriors.SteamExteriorModel;
import net.tardis.mod.helpers.Helper;

public class SteamExteriorRenderer extends ExteriorRenderer<SteamExteriorTile, SteamExteriorModel>{

    public static final ResourceLocation TEXTURE = Helper.createRL("textures/tiles/exteriors/steam.png");

    public SteamExteriorRenderer(BlockEntityRendererProvider.Context context) {
        super(context);
    }

    @Override
    public SteamExteriorModel bakeModel(EntityModelSet set) {
        return new SteamExteriorModel(set.bakeLayer(SteamExteriorModel.LAYER_LOCATION));
    }

    @Override
    public ResourceLocation getTexture(SteamExteriorTile exterior) {
        return TEXTURE;
    }
}
