package net.tardis.mod.client.renderers.tiles;

import com.mojang.blaze3d.vertex.PoseStack;
import com.mojang.math.Axis;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.MultiBufferSource;
import net.minecraft.client.renderer.blockentity.BlockEntityRenderer;
import net.minecraft.client.renderer.blockentity.BlockEntityRendererProvider;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.item.ItemDisplayContext;
import net.minecraft.world.item.ItemStack;
import net.tardis.mod.blockentities.machines.FabricatorTile;
import net.tardis.mod.client.models.machines.FabricatorModel;
import net.tardis.mod.helpers.Helper;
import net.tardis.mod.helpers.WorldHelper;

public class FabricatorRenderer implements BlockEntityRenderer<FabricatorTile> {

    public static final ResourceLocation TEXTURE = Helper.createRL("textures/tiles/machines/fab.png");
    final FabricatorModel model;

    public FabricatorRenderer(BlockEntityRendererProvider.Context context){
        this.model = new FabricatorModel(context.bakeLayer(FabricatorModel.LAYER_LOCATION));
    }

    @Override
    public void render(FabricatorTile fab, float pPartialTick, PoseStack pose, MultiBufferSource pBufferSource, int pPackedLight, int pPackedOverlay) {

        fab.craftingAnimState.startIfStopped((int)Minecraft.getInstance().level.getGameTime());

        pose.pushPose();
        pose.translate(0.5, 1.5, 0.5);
        pose.mulPose(Axis.YP.rotationDegrees(WorldHelper.getFacingAngle(fab.getBlockState())));
        pose.mulPose(Axis.ZP.rotationDegrees(180));

        this.model.setupAnimations(fab, Minecraft.getInstance().level.getGameTime() + pPartialTick);
        this.model.renderToBuffer(pose, pBufferSource.getBuffer(model.renderType(TEXTURE)), pPackedLight, pPackedOverlay, 1.0F, 1.0F, 1.0F, 1.0F);

        fab.getCraftingItem().ifPresent(item -> {
            pose.pushPose();
            pose.translate(0, 0.5, -0.6);
            pose.mulPose(Axis.XN.rotationDegrees(32.5F));
            pose.mulPose(Axis.ZN.rotationDegrees(180));
            pose.scale(0.25F, 0.25F, 0.25F);
            Minecraft.getInstance().getItemRenderer().renderStatic(new ItemStack(item), ItemDisplayContext.FIXED, pPackedLight, pPackedOverlay, pose, pBufferSource, Minecraft.getInstance().level, 0);
            pose.popPose();
        });

        pose.popPose();

    }
}
