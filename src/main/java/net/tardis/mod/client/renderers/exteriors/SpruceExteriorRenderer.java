package net.tardis.mod.client.renderers.exteriors;

import net.minecraft.client.model.geom.EntityModelSet;
import net.minecraft.client.renderer.blockentity.BlockEntityRendererProvider;
import net.minecraft.resources.ResourceLocation;
import net.tardis.mod.blockentities.exteriors.SpruceExteriorTile;
import net.tardis.mod.client.models.exteriors.SpruceDoorExteriorModel;
import net.tardis.mod.client.models.exteriors.interior_door.SpruceDoorInteriorModel;
import net.tardis.mod.helpers.Helper;

public class SpruceExteriorRenderer extends ExteriorRenderer<SpruceExteriorTile, SpruceDoorExteriorModel<SpruceExteriorTile>>{

    public static final ResourceLocation TEXTURE = Helper.createRL("textures/tiles/exteriors/mcdoor_exterior_spruce.png");

    public SpruceExteriorRenderer(BlockEntityRendererProvider.Context context) {
        super(context);
    }

    @Override
    public SpruceDoorExteriorModel<SpruceExteriorTile> bakeModel(EntityModelSet set) {
        return new SpruceDoorExteriorModel<>(set.bakeLayer(SpruceDoorInteriorModel.LAYER_LOCATION));
    }

    @Override
    public ResourceLocation getTexture(SpruceExteriorTile exterior) {
        return TEXTURE;
    }

}
