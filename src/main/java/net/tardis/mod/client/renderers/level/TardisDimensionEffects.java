package net.tardis.mod.client.renderers.level;

import com.mojang.blaze3d.systems.RenderSystem;
import com.mojang.blaze3d.vertex.PoseStack;
import com.mojang.blaze3d.vertex.Tesselator;
import com.mojang.blaze3d.vertex.VertexConsumer;
import net.minecraft.client.Camera;
import net.minecraft.client.multiplayer.ClientLevel;
import net.minecraft.client.renderer.*;
import net.minecraft.util.Mth;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.chunk.LevelChunk;
import net.minecraft.world.level.lighting.BlockLightEngine;
import net.minecraft.world.phys.Vec3;
import net.minecraftforge.client.event.RenderLevelStageEvent;
import net.minecraftforge.common.util.LazyOptional;
import net.tardis.mod.cap.Capabilities;
import net.tardis.mod.cap.level.ITardisLevel;
import net.tardis.mod.client.TardisRenderTypes;
import net.tardis.mod.helpers.Helper;
import org.jetbrains.annotations.Nullable;
import org.joml.Matrix4f;
import org.joml.Vector3f;

public class TardisDimensionEffects extends DimensionSpecialEffects {

    public static Vec3 COLOR = new Vec3(0, 0, 0);

    public TardisDimensionEffects() {
        super(-128, false, SkyType.NONE, false, false);
    }

    @Override
    public Vec3 getBrightnessDependentFogColor(Vec3 pFogColor, float pBrightness) {
        return COLOR;
    }

    @Override
    public boolean isFoggyAt(int pX, int pY) {
        return false;
    }

    @Override
    public boolean renderClouds(ClientLevel level, int ticks, float partialTick, PoseStack poseStack, double camX, double camY, double camZ, Matrix4f projectionMatrix) {
        return true;
    }

    @Override
    public boolean renderSky(ClientLevel level, int ticks, float partialTick, PoseStack poseStack, Camera camera, Matrix4f projectionMatrix, boolean isFoggy, Runnable setupFog) {

        LazyOptional<ITardisLevel> tardis = Capabilities.getCap(Capabilities.TARDIS, level);
        if(!tardis.isPresent() || !tardis.orElseThrow(NullPointerException::new).isInVortex()){
            return true;
        }

        final float size = 10;

        MultiBufferSource.BufferSource source = MultiBufferSource.immediate(Tesselator.getInstance().getBuilder());
        VertexConsumer buffer = source.getBuffer(TardisRenderTypes.VORTEX);

        RenderSystem.setShaderGameTime(ticks, partialTick);

        poseStack.pushPose();

        for(int i = -100; i < 100; ++i){
            renderVortexBox(poseStack, buffer, 0, Mth.floor(i * size * 2), size);
        }

        poseStack.popPose();
        source.endBatch();

        return true;
    }

    public void renderVortexBox(PoseStack poseStack, VertexConsumer buffer, int x, int z, float size){
        final int length = 100;
        final float texV = length / size;
        //top
        buffer.vertex(poseStack.last().pose(), -size + x, size, -length + z).uv(0, 0).normal(0, 1, 0).endVertex();
        buffer.vertex(poseStack.last().pose(), size + x, size, -length + z).uv(1, 0).normal(0, 1, 0).endVertex();
        buffer.vertex(poseStack.last().pose(), size + x, size, length + z).uv(1, texV).normal(0, 1, 0).endVertex();
        buffer.vertex(poseStack.last().pose(), -size + x, size, length + z).uv(0, texV).normal(0, 1, 0).endVertex();


        //-X (west)
        buffer.vertex(poseStack.last().pose(), -size + x, -size, -length + z).uv(0, 0).normal(1, 0, 0).endVertex();
        buffer.vertex(poseStack.last().pose(), -size + x, size, -length + z).uv(1, 0).normal(1, 0, 0).endVertex();
        buffer.vertex(poseStack.last().pose(), -size + x, size, length + z).uv(1, texV).normal(1, 0, 0).endVertex();
        buffer.vertex(poseStack.last().pose(), -size + x, -size, length + z).uv(0, texV).normal(1, 0, 0).endVertex();

        //X (East)
        buffer.vertex(poseStack.last().pose(), size + x, -size, -length + z).uv(0, 0).normal(1, 0, 0).endVertex();
        buffer.vertex(poseStack.last().pose(), size + x, -size, length + z).uv(1, 0).normal(1, 0, 0).endVertex();
        buffer.vertex(poseStack.last().pose(), size + x, size, length + z).uv(1, texV).normal(1, 0, 0).endVertex();
        buffer.vertex(poseStack.last().pose(), size + x, size, -length + z).uv(1, 0).normal(1, 0, 0).endVertex();

        //bottom
        buffer.vertex(poseStack.last().pose(), -size + x, -size, -length + z).uv(0, 0).normal(0, 1, 0).endVertex();
        buffer.vertex(poseStack.last().pose(), -size + x, -size, length + z).uv(0, texV).normal(0, 1, 0).endVertex();
        buffer.vertex(poseStack.last().pose(), size + x, -size, length + z).uv(1, texV).normal(0, 1, 0).endVertex();
        buffer.vertex(poseStack.last().pose(), size + x, -size, -length + z).uv(1, 0).normal(0, 1, 0).endVertex();
    }

    @Override
    public boolean renderSnowAndRain(ClientLevel level, int ticks, float partialTick, LightTexture lightTexture, double camX, double camY, double camZ) {
        return true;
    }

    @Override
    public boolean tickRain(ClientLevel level, int ticks, Camera camera) {
        return true;
    }

    @Nullable
    @Override
    public float[] getSunriseColor(float pTimeOfDay, float pPartialTicks) {
        return new float[]{0.0F, 0.0F, 0.0F, 0.0F};
    }

    @Override
    public void adjustLightmapColors(ClientLevel level, float partialTicks, float skyDarken, float blockLightRedFlicker, float skyLight, int pixelX, int pixelY, Vector3f colors) {
        super.adjustLightmapColors(level, partialTicks, skyDarken, blockLightRedFlicker, skyLight, pixelX, pixelY, colors);
        /*
        colors.x = Math.max((float)Math.sin(level.getGameTime() * 0.1F + partialTicks) * 1.0F, 0.1F);
        colors.y = 0.01F;
        colors.z = 0.01F;

         */
    }
}
