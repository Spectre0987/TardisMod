package net.tardis.mod.client.monitor_world_text;

import net.minecraft.network.chat.Component;
import net.tardis.mod.cap.level.ITardisLevel;

import java.util.List;

public class ExteriorExtraMonitorEntry extends MonitorEntry{

    public static final Component IS_IN_RIFT = Component.translatable(BASE_KEY + "exterior_extra.in_rift");

    @Override
    public List<Component> getText(ITardisLevel tardis) {
        return List.of(IS_IN_RIFT);
    }

    @Override
    public boolean shouldDisplay(ITardisLevel tardis) {
        return tardis.getExteriorExtraData().IsInRift();
    }
}
