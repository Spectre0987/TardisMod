package net.tardis.mod.client.monitor_world_text;

import com.google.common.collect.Lists;
import net.minecraft.network.chat.Component;
import net.tardis.mod.cap.level.ITardisLevel;

import java.util.List;

public class FirstEnterTARDISMonitorEntry extends MonitorEntry{

    public static final Component TRANS = Component.translatable(BASE_KEY + "first_tardis");
    public static final List<Component> LIST = Lists.newArrayList(TRANS);

    @Override
    public List<Component> getText(ITardisLevel tardis) {
        return LIST;
    }

    @Override
    public boolean shouldDisplay(ITardisLevel tardis) {
        return tardis.getInteriorManager().hasNeverFlown();
    }

    @Override
    public int overridePriority() {
        return 9999999;
    }

    @Override
    public OverrideType getOverrideType() {
        return OverrideType.ALL;
    }
}
