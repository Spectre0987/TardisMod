package net.tardis.mod.client.animations.consoles;

import net.minecraft.client.animation.AnimationChannel;
import net.minecraft.client.animation.AnimationDefinition;
import net.minecraft.client.animation.Keyframe;
import net.minecraft.client.animation.KeyframeAnimations;
import net.minecraft.core.Direction;
import net.tardis.mod.cap.level.ITardisLevel;
import net.tardis.mod.client.animations.AnimationHelper;
import net.tardis.mod.client.models.consoles.SteamConsoleModel;
import net.tardis.mod.control.IncrementControl;
import net.tardis.mod.control.datas.ControlDataBool;
import net.tardis.mod.control.datas.ControlDataEnum;
import net.tardis.mod.control.datas.ControlDataInt;
import net.tardis.mod.misc.enums.LandingType;
import net.tardis.mod.registry.ControlRegistry;
import net.tardis.mod.control.datas.ControlData;
import org.joml.Vector3f;

import java.util.function.Function;

public class SteamConsoleAnimation {

    public static final AnimationDefinition MODEL_STEAMROTOR = AnimationDefinition.Builder.withLength(4.000000F).looping().addAnimation("spinner_slide_y", new AnimationChannel(AnimationChannel.Targets.POSITION, new Keyframe(0.000000F, KeyframeAnimations.posVec(0.000000F, 0.000000F, 0.000000F), AnimationChannel.Interpolations.CATMULLROM), new Keyframe(0.916700F, KeyframeAnimations.posVec(0.000000F, 6.000000F, 0.000000F), AnimationChannel.Interpolations.CATMULLROM), new Keyframe(1.083300F, KeyframeAnimations.posVec(0.000000F, 6.000000F, 0.000000F), AnimationChannel.Interpolations.CATMULLROM), new Keyframe(1.916700F, KeyframeAnimations.posVec(0.000000F, 0.000000F, 0.000000F), AnimationChannel.Interpolations.CATMULLROM), new Keyframe(2.083300F, KeyframeAnimations.posVec(0.000000F, 0.000000F, 0.000000F), AnimationChannel.Interpolations.CATMULLROM), new Keyframe(2.916700F, KeyframeAnimations.posVec(0.000000F, 6.000000F, 0.000000F), AnimationChannel.Interpolations.CATMULLROM), new Keyframe(3.083300F, KeyframeAnimations.posVec(0.000000F, 6.000000F, 0.000000F), AnimationChannel.Interpolations.CATMULLROM), new Keyframe(4.000000F, KeyframeAnimations.posVec(0.000000F, 0.000000F, 0.000000F), AnimationChannel.Interpolations.CATMULLROM))).addAnimation("hourflip_rotate_x", new AnimationChannel(AnimationChannel.Targets.ROTATION, new Keyframe(0.000000F, KeyframeAnimations.degreeVec(0.000000F, 0.000000F, 0.000000F), AnimationChannel.Interpolations.LINEAR), new Keyframe(0.875000F, KeyframeAnimations.degreeVec(0.000000F, 0.000000F, 0.000000F), AnimationChannel.Interpolations.CATMULLROM), new Keyframe(1.041700F, KeyframeAnimations.degreeVec(-180.000000F, 0.000000F, 0.000000F), AnimationChannel.Interpolations.LINEAR), new Keyframe(2.791700F, KeyframeAnimations.degreeVec(-180.000000F, 0.000000F, 0.000000F), AnimationChannel.Interpolations.CATMULLROM), new Keyframe(3.000000F, KeyframeAnimations.degreeVec(-360.000000F, 0.000000F, 0.000000F), AnimationChannel.Interpolations.LINEAR), new Keyframe(4.000000F, KeyframeAnimations.degreeVec(-360.000000F, 0.000000F, 0.000000F), AnimationChannel.Interpolations.LINEAR))).build();
    public static final AnimationDefinition RADIO = AnimationDefinition.Builder.withLength(2.000000F).addAnimation("radio_dial_rotate_y", new AnimationChannel(AnimationChannel.Targets.ROTATION, new Keyframe(0.000000F, KeyframeAnimations.degreeVec(0.000000F, 0.000000F, 0.000000F), AnimationChannel.Interpolations.LINEAR), new Keyframe(0.625000F, KeyframeAnimations.degreeVec(0.000000F, 60.000000F, 0.000000F), AnimationChannel.Interpolations.LINEAR), new Keyframe(1.000000F, KeyframeAnimations.degreeVec(0.000000F, 60.000000F, 0.000000F), AnimationChannel.Interpolations.LINEAR), new Keyframe(1.500000F, KeyframeAnimations.degreeVec(0.000000F, 20.000000F, 0.000000F), AnimationChannel.Interpolations.LINEAR), new Keyframe(1.625000F, KeyframeAnimations.degreeVec(0.000000F, 20.000000F, 0.000000F), AnimationChannel.Interpolations.LINEAR), new Keyframe(2.000000F, KeyframeAnimations.degreeVec(0.000000F, 0.000000F, 0.000000F), AnimationChannel.Interpolations.LINEAR))).addAnimation("radio_needle_rotate_y", new AnimationChannel(AnimationChannel.Targets.POSITION, new Keyframe(0.000000F, KeyframeAnimations.posVec(0.000000F, 0.000000F, 0.000000F), AnimationChannel.Interpolations.LINEAR), new Keyframe(0.500000F, KeyframeAnimations.posVec(0.000000F, 0.000000F, 1.000000F), AnimationChannel.Interpolations.LINEAR), new Keyframe(0.541700F, KeyframeAnimations.posVec(0.000000F, 0.000000F, 1.000000F), AnimationChannel.Interpolations.LINEAR), new Keyframe(0.833300F, KeyframeAnimations.posVec(0.000000F, 0.000000F, 0.500000F), AnimationChannel.Interpolations.LINEAR), new Keyframe(0.875000F, KeyframeAnimations.posVec(0.000000F, 0.000000F, 0.500000F), AnimationChannel.Interpolations.LINEAR), new Keyframe(1.291700F, KeyframeAnimations.posVec(0.000000F, 0.000000F, 2.000000F), AnimationChannel.Interpolations.LINEAR), new Keyframe(1.333300F, KeyframeAnimations.posVec(0.000000F, 0.000000F, 2.000000F), AnimationChannel.Interpolations.LINEAR), new Keyframe(2.000000F, KeyframeAnimations.posVec(0.000000F, 0.000000F, 0.000000F), AnimationChannel.Interpolations.LINEAR))).build();
    public static final AnimationDefinition RANDOMIZER = AnimationDefinition.Builder.withLength(1.333330F).addAnimation("globe_rotate_y", new AnimationChannel(AnimationChannel.Targets.ROTATION, new Keyframe(0.000000F, KeyframeAnimations.degreeVec(0.000000F, 0.000000F, 0.000000F), AnimationChannel.Interpolations.LINEAR), new Keyframe(0.125000F, KeyframeAnimations.degreeVec(0.000000F, -17.000000F, 0.000000F), AnimationChannel.Interpolations.LINEAR), new Keyframe(0.250000F, KeyframeAnimations.degreeVec(0.000000F, 0.000000F, 0.000000F), AnimationChannel.Interpolations.LINEAR), new Keyframe(1.291700F, KeyframeAnimations.degreeVec(0.000000F, 720.000000F, 0.000000F), AnimationChannel.Interpolations.LINEAR))).build();
    public static final AnimationDefinition FACING = AnimationDefinition.Builder.withLength(1.041670F).addAnimation("rotation_crank_rotate_y", new AnimationChannel(AnimationChannel.Targets.ROTATION, new Keyframe(0.000000F, KeyframeAnimations.degreeVec(0.000000F, 0.000000F, 0.000000F), AnimationChannel.Interpolations.LINEAR), new Keyframe(1.041700F, KeyframeAnimations.degreeVec(0.000000F, 360.000000F, 0.000000F), AnimationChannel.Interpolations.LINEAR))).build();
    public static final AnimationDefinition TELEPATHICS = AnimationDefinition.Builder.withLength(2.000000F).addAnimation("scrying_glass_rotate_y", new AnimationChannel(AnimationChannel.Targets.POSITION, new Keyframe(0.000000F, KeyframeAnimations.posVec(0.000000F, 0.000000F, 0.000000F), AnimationChannel.Interpolations.LINEAR), new Keyframe(0.541700F, KeyframeAnimations.posVec(-1.000000F, 0.000000F, 3.000000F), AnimationChannel.Interpolations.LINEAR), new Keyframe(0.583300F, KeyframeAnimations.posVec(-1.000000F, 0.000000F, 3.000000F), AnimationChannel.Interpolations.LINEAR), new Keyframe(1.041700F, KeyframeAnimations.posVec(1.000000F, 0.000000F, 3.000000F), AnimationChannel.Interpolations.LINEAR), new Keyframe(1.125000F, KeyframeAnimations.posVec(1.000000F, 0.000000F, 3.000000F), AnimationChannel.Interpolations.LINEAR), new Keyframe(1.500000F, KeyframeAnimations.posVec(-1.000000F, 0.000000F, 1.000000F), AnimationChannel.Interpolations.LINEAR), new Keyframe(1.541700F, KeyframeAnimations.posVec(-1.000000F, 0.000000F, 1.000000F), AnimationChannel.Interpolations.LINEAR), new Keyframe(2.000000F, KeyframeAnimations.posVec(0.000000F, 0.000000F, 0.000000F), AnimationChannel.Interpolations.LINEAR))).build();
    public static final AnimationDefinition FAST_RETURN = AnimationDefinition.Builder.withLength(1.500000F).addAnimation("fast_return_rotate_x", new AnimationChannel(AnimationChannel.Targets.ROTATION, new Keyframe(0.000000F, KeyframeAnimations.degreeVec(0.000000F, 0.000000F, 0.000000F), AnimationChannel.Interpolations.LINEAR), new Keyframe(0.750000F, KeyframeAnimations.degreeVec(0.000000F, 0.000000F, -27.500000F), AnimationChannel.Interpolations.LINEAR), new Keyframe(1.000000F, KeyframeAnimations.degreeVec(0.000000F, 0.000000F, -27.500000F), AnimationChannel.Interpolations.LINEAR), new Keyframe(1.500000F, KeyframeAnimations.degreeVec(0.000000F, 0.000000F, 0.000000F), AnimationChannel.Interpolations.LINEAR))).build();
    public static final AnimationDefinition DOOR = AnimationDefinition.Builder.withLength(0.75F)
            .addAnimation("rotation_crank_rotate_y", new AnimationChannel(AnimationChannel.Targets.ROTATION,
                    new Keyframe(0.0F, KeyframeAnimations.degreeVec(0.0F, 0.0F, 0.0F), AnimationChannel.Interpolations.LINEAR),
                    new Keyframe(0.75F, KeyframeAnimations.degreeVec(0.0F, 360.0F, 0.0F), AnimationChannel.Interpolations.LINEAR)
            ))
            .build();


    //Manage data-based animations

    public static void animateConditional(ITardisLevel tardis, SteamConsoleModel model, float ageInTicks) {
        ControlData<Boolean> brake = tardis.getControlDataOrCreate(ControlRegistry.HANDBRAKE.get());
        ControlData<Float> throttle = tardis.getControlDataOrCreate(ControlRegistry.THROTTLE.get());
        ControlDataInt coord_inc = tardis.getControlDataOrCreate(ControlRegistry.INCREMENT.get());
        ControlDataEnum<Direction> facing = tardis.getControlDataOrCreate(ControlRegistry.FACING.get());
        ControlDataEnum<LandingType> land_state = tardis.getControlDataOrCreate(ControlRegistry.LANDING_TYPE.get());
        ControlDataBool stabilizer = tardis.getControlDataOrCreate(ControlRegistry.STABILIZERS.get());

        model.getAnyDescendantWithName("lever_f1_rotate_z").ifPresent(part -> {
            part.offsetRotation(new Vector3f(0, 0, (float)Math.toRadians(brake.get() ? -200 : 0)));
        });


        model.getAnyDescendantWithName("leaver_b1_rotate_z").ifPresent(control ->{
            control.offsetRotation(new Vector3f(0, 0, AnimationHelper
                    .getRotationBaseOnState(throttle, -135 * throttle.getPrevious(), -120 * (throttle.get() - throttle.getPrevious()), ageInTicks, 5, true)));
        });

        model.getAnyDescendantWithName("cord_slider_rotate_z").ifPresent(control -> {

            float prePercent = coord_inc.getPrevious() / (float) IncrementControl.VALUES.length;
            float percent = coord_inc.get() / (float)IncrementControl.VALUES.length;

            control.offsetRotation(AnimationHelper.getSteppedRotation(
                    coord_inc, 9F, prePercent, percent, ageInTicks, 5,
                    new Vector3f(0, 0, -1)
            ));
        });

        model.getAnyDescendantWithName("door_crank_rotate_y").ifPresent(control -> {

            float prevPercent = facing.getPrevious().toYRot() / 360.0F;
            float percent = facing.get().toYRot() / 360.0F;

            control.offsetRotation(
                    AnimationHelper.getSteppedRotation(
                            facing,
                            360,
                            prevPercent,
                            percent,
                            ageInTicks,
                            10,
                            new Vector3f(0, 1, 0)

                    )
            );
        });

        //
        final float land_type_sides = AnimationHelper.getRotationBaseOnState(land_state,-10, ageInTicks, 20, land_state.get() == LandingType.UP);
        model.getAnyDescendantWithName("sliderknob_c1_rotate_z").ifPresent(control -> {
            control.offsetRotation(new Vector3f(0, 0, land_type_sides));
        });
        model.getAnyDescendantWithName("sliderknob_c1_rotate_z3").ifPresent(control -> {
            control.offsetRotation(new Vector3f(0, 0, land_type_sides));
        });
        model.getAnyDescendantWithName("sliderknob_c1_rotate_z2").ifPresent(control -> {
            control.offsetRotation(new Vector3f(0, 0, AnimationHelper.getRotationBaseOnState(land_state, 10, ageInTicks, 20, land_state.get() == LandingType.UP)));
        });

        model.getAnyDescendantWithName("stabilizer_button_rotate_z").ifPresent(control -> {
            Function<Boolean, Float> percent = b -> b ? 1.0F : 0.0F;
            control.offsetPos(AnimationHelper.slideBasedOnState(
                    stabilizer, percent.apply(stabilizer.getPrevious()), percent.apply(stabilizer.get()),
                    ageInTicks,
                    0.25F,
                    10,
                    new Vector3f(0, 1, 0)

            ));
        });
    }
}
