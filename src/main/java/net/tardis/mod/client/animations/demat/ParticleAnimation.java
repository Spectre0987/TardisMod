package net.tardis.mod.client.animations.demat;

import com.mojang.blaze3d.vertex.PoseStack;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.MultiBufferSource;
import net.minecraft.core.particles.ParticleTypes;
import net.minecraft.util.Mth;
import net.minecraft.world.phys.Vec3;
import net.tardis.mod.Tardis;
import net.tardis.mod.blockentities.exteriors.ExteriorTile;
import net.tardis.mod.client.models.BasicTileHierarchicalModel;
import net.tardis.mod.helpers.WorldHelper;
import net.tardis.mod.misc.MatterStateHandler;
import net.tardis.mod.misc.enums.MatterState;
import org.joml.Vector3f;

public class ParticleAnimation extends DematAnimation{

    static final float distToMove = 4.0F;
    static final int ticksToTranslate = 40;

    @Override
    public void modifyPose(PoseStack pose, MatterStateHandler tile, float partialTicks) {

        float ticksLeft = this.animLength(Minecraft.getInstance().level, tile) - this.animTicks(Minecraft.getInstance().level, tile, partialTicks);

        float rematPercent = Mth.clamp(ticksLeft / (float) ticksToTranslate, 0.0F, 1.0F);

        float YPos = rematPercent * distToMove;

        if(tile.getMatterState() == MatterState.DEMAT){
            YPos = (1.0F - rematPercent) * distToMove;
        }

        pose.translate(0, YPos, 0);

    }

    public boolean isTranslating(MatterStateHandler tile, float partialTicks){
        float ticksLeft = this.animTicks(Minecraft.getInstance().level, tile, partialTicks);
        return ticksLeft > this.animLength(Minecraft.getInstance().level, tile) - ticksToTranslate;
    }

    @Override
    public float[] getColors(MatterStateHandler tile, float partialTicks) {

        if(!isTranslating(tile, partialTicks)){
            if(tile.getMatterState() == MatterState.REMAT)
                return new float[]{0, 0, 0, 0}; // Render invis
        }

        return super.getColors(tile, partialTicks);
    }

    @Override
    public void renderExtra(PoseStack pose, MatterStateHandler tile, BasicTileHierarchicalModel<?> exteriorModel, MultiBufferSource source, float partialTicks, int packedLight, int packedOverlay) {

        final int particles = Mth.floor(50 * this.animPercent(Minecraft.getInstance().level, tile, partialTicks));
        final Vec3 pos = tile.getPosition();
        final float animPercent = animPercent(Minecraft.getInstance().level, tile, partialTicks);
        float rad = tile.getRadius();

        for(int i = 0; i < particles; ++i){
            final float angle = (float)Math.toRadians(((i / (float)particles) * animPercent) * 360.0F);
            Minecraft.getInstance().level.addParticle(ParticleTypes.WITCH,
                        pos.x + Math.sin(angle) * tile.getRadius(), pos.y, pos.z + Math.cos(angle) * tile.getRadius(),
                        0, 0.01, 0
                    );
        }


        float angle = (float)Math.toRadians(this.animTicks(Minecraft.getInstance().level, tile, partialTicks) * 4.0F % 360.0F);
        double x = Math.sin(angle) * tile.getRadius(),
                y = Math.cos(angle) * tile.getRadius();

        Minecraft.getInstance().level.addParticle(ParticleTypes.WITCH, pos.x + x, pos.y + ((animPercent * 80 % 80) / 80.0F) * 3.0F, pos.z + y,
                0, 0.25, 0
        );

    }
}
