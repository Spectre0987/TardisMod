package net.tardis.mod.client.animations.demat;

import com.mojang.blaze3d.vertex.PoseStack;
import net.minecraft.client.renderer.MultiBufferSource;
import net.minecraft.world.level.Level;
import net.tardis.mod.blockentities.exteriors.ExteriorTile;
import net.tardis.mod.client.models.BasicTileHierarchicalModel;
import net.tardis.mod.misc.MatterStateHandler;

public abstract class DematAnimation {

    public static final float[] DEFAULT_COLORS = {1.0F, 1.0F, 1.0F, 1.0F};

    /**
     *
     * @param tile
     * @return MUST return with a length of > 4
     */
    public float[] getColors(MatterStateHandler tile, float partialTicks){

        return new float[]{1.0F, 1.0F, 1.0F ,1.0F};
    }

    public void renderExtra(PoseStack pose, MatterStateHandler tile, BasicTileHierarchicalModel<?> exteriorModel, MultiBufferSource source, float partialTicks, int packedLight, int packedOverlay){}

    public void modifyPose(PoseStack pose, MatterStateHandler tile, float partialTicks){}


    public float animTicks(Level level, MatterStateHandler tile, float partialTicks){
        return (level.getGameTime() + partialTicks) - tile.getStartTick();
    }

    public float animPercent(Level level, MatterStateHandler tile, float partialTicks){
        return animTicks(level, tile, partialTicks) / (float) animLength(level, tile);
    }

    public int animLength(Level level, MatterStateHandler tile){
        return (int)(tile.getEndTick() - tile.getStartTick());
    }
}
