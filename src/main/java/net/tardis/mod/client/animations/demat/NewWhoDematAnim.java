package net.tardis.mod.client.animations.demat;

import net.minecraft.client.Minecraft;
import net.minecraft.util.Mth;
import net.tardis.mod.blockentities.exteriors.ExteriorTile;
import net.tardis.mod.misc.MatterStateHandler;
import net.tardis.mod.misc.enums.MatterState;

public class NewWhoDematAnim extends DematAnimation{


    @Override
    public float[] getColors(MatterStateHandler tile, float partialTicks) {

        final int steps = 3;
        final float alphaPerStep = 1.0F / (float)steps;

        float percent = this.animPercent(Minecraft.getInstance().level, tile, partialTicks);
        if(tile.getMatterState() == MatterState.DEMAT){
            percent = 1.0F - percent;
        }

        float currentStep = Mth.floor(percent * steps);


        return new float[]{1.0F, 1.0F, 1.0F, (float)Mth.clamp(
                (alphaPerStep * currentStep) + (Math.sin(this.animTicks(Minecraft.getInstance().level, tile, partialTicks) * 0.1) * alphaPerStep), 0, 1)
        };
    }
}
