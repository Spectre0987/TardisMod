package net.tardis.mod.client.animations.consoles;

import net.minecraft.client.animation.AnimationChannel;
import net.minecraft.client.animation.AnimationDefinition;
import net.minecraft.client.animation.Keyframe;
import net.minecraft.client.animation.KeyframeAnimations;
import net.minecraft.core.Direction;
import net.tardis.mod.cap.level.ITardisLevel;
import net.tardis.mod.client.animations.AnimationHelper;
import net.tardis.mod.client.models.consoles.NemoConsoleModel;
import net.tardis.mod.control.datas.ControlDataBool;
import net.tardis.mod.control.datas.ControlDataEnum;
import net.tardis.mod.control.datas.ControlDataFloat;
import net.tardis.mod.misc.enums.LandingType;
import net.tardis.mod.registry.ControlRegistry;
import org.joml.Vector3f;

import java.util.function.Function;

public class NemoConsoleAnimations {

    public static final AnimationDefinition TIME_ROTOR = AnimationDefinition.Builder.withLength(2.0F).looping()
            .addAnimation("time_rotor_slide_y", new AnimationChannel(AnimationChannel.Targets.POSITION,
                    new Keyframe(0.0F, KeyframeAnimations.posVec(0.0F, 0.0F, 0.0F), AnimationChannel.Interpolations.LINEAR),
                    new Keyframe(1.0F, KeyframeAnimations.posVec(0.0F, -9.0F, 0.0F), AnimationChannel.Interpolations.LINEAR),
                    new Keyframe(2.0F, KeyframeAnimations.posVec(0.0F, 0.0F, 0.0F), AnimationChannel.Interpolations.LINEAR)
            ))
            .build();

    public static final AnimationDefinition INCREMENT = AnimationDefinition.Builder.withLength(1.75F)
            .addAnimation("xyz_increment_rotate_z", new AnimationChannel(AnimationChannel.Targets.ROTATION,
                    new Keyframe(0.0F, KeyframeAnimations.degreeVec(0.0F, 0.0F, 0.0F), AnimationChannel.Interpolations.LINEAR),
                    new Keyframe(0.25F, KeyframeAnimations.degreeVec(0.0F, 0.0F, -22.5F), AnimationChannel.Interpolations.LINEAR),
                    new Keyframe(1.4167F, KeyframeAnimations.degreeVec(0.0F, 0.0F, 720.0F), AnimationChannel.Interpolations.CATMULLROM)
            ))
            .build();

    public static final AnimationDefinition Bell = AnimationDefinition.Builder.withLength(2.5833F)
            .addAnimation("bell_rotate_x", new AnimationChannel(AnimationChannel.Targets.ROTATION,
                    new Keyframe(0.0F, KeyframeAnimations.degreeVec(0.0F, 0.0F, 0.0F), AnimationChannel.Interpolations.LINEAR),
                    new Keyframe(0.4583F, KeyframeAnimations.degreeVec(27.5F, 0.0F, 0.0F), AnimationChannel.Interpolations.LINEAR),
                    new Keyframe(1.0F, KeyframeAnimations.degreeVec(-27.5F, 0.0F, 0.0F), AnimationChannel.Interpolations.LINEAR),
                    new Keyframe(1.5F, KeyframeAnimations.degreeVec(27.5F, 0.0F, 0.0F), AnimationChannel.Interpolations.LINEAR),
                    new Keyframe(2.0F, KeyframeAnimations.degreeVec(0.0F, 0.0F, 0.0F), AnimationChannel.Interpolations.LINEAR)
            ))
            .build();

    public static final AnimationDefinition RANDOMIZER = AnimationDefinition.Builder.withLength(1.5F)
            .addAnimation("rotate_y", new AnimationChannel(AnimationChannel.Targets.ROTATION,
                    new Keyframe(0.0F, KeyframeAnimations.degreeVec(0.0F, 0.0F, 0.0F), AnimationChannel.Interpolations.LINEAR),
                    new Keyframe(0.25F, KeyframeAnimations.degreeVec(0.0F, 10.0F, 0.0F), AnimationChannel.Interpolations.LINEAR),
                    new Keyframe(1.5F, KeyframeAnimations.degreeVec(0.0F, -720.0F, 0.0F), AnimationChannel.Interpolations.LINEAR)
            ))
            .build();
    public static final AnimationDefinition DOOR = AnimationDefinition.Builder.withLength(2.0F)
            .addAnimation("door_knob_rotate_z", new AnimationChannel(AnimationChannel.Targets.ROTATION,
                    new Keyframe(0.0F, KeyframeAnimations.degreeVec(0.0F, 0.0F, 0.0F), AnimationChannel.Interpolations.LINEAR),
                    new Keyframe(0.5F, KeyframeAnimations.degreeVec(0.0F, 0.0F, -62.5F), AnimationChannel.Interpolations.LINEAR),
                    new Keyframe(1.0F, KeyframeAnimations.degreeVec(0.0F, 0.0F, 0.0F), AnimationChannel.Interpolations.LINEAR),
                    new Keyframe(1.5F, KeyframeAnimations.degreeVec(0.0F, 0.0F, 52.5F), AnimationChannel.Interpolations.LINEAR),
                    new Keyframe(2.0F, KeyframeAnimations.degreeVec(0.0F, 0.0F, 0.0F), AnimationChannel.Interpolations.LINEAR)
            ))
            .build();

    public static void animateOthers(ITardisLevel tardis, NemoConsoleModel<?> model, float ageInTicks){
        final ControlDataBool refuel = tardis.getControlDataOrCreate(ControlRegistry.REFUELER.get());
        final ControlDataFloat throttle = tardis.getControlDataOrCreate(ControlRegistry.THROTTLE.get());
        final ControlDataBool handbrake = tardis.getControlDataOrCreate(ControlRegistry.HANDBRAKE.get());
        final ControlDataEnum<Direction> facing = tardis.getControlDataOrCreate(ControlRegistry.FACING.get());
        final ControlDataEnum<LandingType> landingType = tardis.getControlDataOrCreate(ControlRegistry.LANDING_TYPE.get());
        final ControlDataBool stabilizers = tardis.getControlDataOrCreate(ControlRegistry.STABILIZERS.get());

        model.getAnyDescendantWithName("refuel_needle_rotate_z").ifPresent(control -> {
            float percent = tardis.getFuelHandler().getStoredArtron() / tardis.getFuelHandler().getMaxArtron();
            control.offsetRotation(AnimationHelper.getSteppedRotation(refuel, -165, percent, percent, ageInTicks, 20, new Vector3f(0, 0, -1)));
        });

        model.getAnyDescendantWithName("throttle").ifPresent(control -> {
            control.offsetRotation(AnimationHelper.getSteppedRotation(throttle, -165, throttle.getPrevious(), throttle.get(), ageInTicks, 10, new Vector3f(1, 0, 0)));
        });

        model.getAnyDescendantWithName("handbreak").ifPresent(control -> {
            Function<Boolean, Float> conversion = b -> b ? 1.0F : 0.0F;
            control.offsetRotation(new Vector3f(
                    AnimationHelper.getRotationBaseOnState(handbrake, -165, ageInTicks, 30, handbrake.get()),
                        0, 0
                    ));
        });

        model.getAnyDescendantWithName("faceing_needle_rotate_z").ifPresent(control -> {
            control.offsetRotation(AnimationHelper.getSteppedRotation(facing, -360, facing.getPrevious().toYRot() / 360.0F, facing.get().toYRot() / 360.0F, ageInTicks, 60, new Vector3f(0, 0, 1)));
        });

        model.getAnyDescendantWithName("rotate_bar_x").ifPresent(control -> {
            final Function<LandingType, Float> percentFunc = type -> type == LandingType.DOWN ? 0.0F : 1.0F;
            control.offsetPos(AnimationHelper.slideBasedOnState(landingType, percentFunc.apply(landingType.getPrevious()), percentFunc.apply(landingType.get()), ageInTicks, -1.1F, 20, new Vector3f(0, -1, 0)));
        });

        model.getAnyDescendantWithName("stablizers_button_rotate_x").ifPresent(control -> {
            final Function<Boolean, Float> percentFunc = b -> b ? 1.0F : 0.0F;
            control.offsetPos(AnimationHelper.slideBasedOnState(stabilizers, percentFunc.apply(stabilizers.getPrevious()), percentFunc.apply(stabilizers.get()), ageInTicks, 0.5F, 20, new Vector3f(0, 0, 1)));
        });
    }

}
