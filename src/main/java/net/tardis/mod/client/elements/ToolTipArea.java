package net.tardis.mod.client.elements;

import com.mojang.blaze3d.vertex.PoseStack;
import net.minecraft.client.gui.components.AbstractWidget;
import net.minecraft.client.gui.components.Tooltip;
import net.minecraft.client.gui.narration.NarrationElementOutput;
import net.minecraft.client.gui.screens.inventory.tooltip.ClientTooltipPositioner;
import net.minecraft.network.chat.Component;
import org.jetbrains.annotations.Nullable;

import java.awt.*;
import java.util.function.Supplier;

public class ToolTipArea extends AbstractWidget {

    final Supplier<Tooltip> tooltips;

    public ToolTipArea(int x, int y, int width, int height, Supplier<Tooltip> tip){
        super(x, y, width, height, Component.empty());
        this.setTooltip(tip.get());
        this.tooltips = tip;
    }

    @Override
    public void render(PoseStack pPoseStack, int pMouseX, int pMouseY, float pPartialTick) {
        super.render(pPoseStack, pMouseX, pMouseY, pPartialTick);
        if(this.isMouseOver(pMouseX, pMouseY)){
            this.setTooltip(this.tooltips.get());
        }
    }

    @Override
    protected ClientTooltipPositioner createTooltipPositioner() {
        return super.createTooltipPositioner();
    }

    @Override
    public void renderWidget(PoseStack pose, int mouseX, int mouseY, float partialTicks) {

    }
    @Override
    protected void updateWidgetNarration(NarrationElementOutput p_259858_) {

    }
}
