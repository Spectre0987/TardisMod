package net.tardis.mod.client.particle;

import com.mojang.blaze3d.vertex.VertexConsumer;
import com.mojang.math.Axis;
import net.minecraft.client.Camera;
import net.minecraft.client.multiplayer.ClientLevel;
import net.minecraft.client.particle.ParticleRenderType;
import net.minecraft.client.particle.SingleQuadParticle;
import net.minecraft.core.particles.ParticleType;
import net.minecraft.util.Mth;
import net.tardis.mod.client.TardisRenderTypes;
import org.joml.Vector3f;

public class RiftParticle extends SingleQuadParticle {

    final ParticleType<?> type;

    public RiftParticle(ParticleType<?> type, ClientLevel pLevel, double pX, double pY, double pZ, double pXSpeed, double pYSpeed, double pZSpeed) {
        super(pLevel, pX, pY, pZ, pXSpeed, pYSpeed, pZSpeed);
        this.type = type;
        this.setPos(pX, pY, pZ);
        this.setParticleSpeed(pXSpeed, pYSpeed, pZSpeed);
        this.setLifetime(7 * 20);
        this.gravity = 0;
        this.scale((pLevel.random.nextFloat() * 1.25F) + 0.25F);
    }

    @Override
    public void render(VertexConsumer buffer, Camera info, float pPartialTicks) {
        //super.render(buffer, pRenderInfo, pPartialTicks);

        float x = (float)(Mth.lerp(pPartialTicks, this.xo, this.x) - info.getPosition().x),
                y = (float)(Mth.lerp(pPartialTicks, this.yo, this.y) - info.getPosition().y),
                z = (float)(Mth.lerp(pPartialTicks, this.zo, this.z) - info.getPosition().z);

        final Vector3f[] mat = new Vector3f[]{
                new Vector3f(-1, -1, 0),
                new Vector3f(-1, 1.0F, 0),
                new Vector3f(1.0F, 1.0F, 0),
                new Vector3f(1.0F, -1, 0),
        };

        for(int i = 0; i < 4; ++i){
            Vector3f vec = mat[i];
            vec.rotate(Axis.XP.rotationDegrees(info.getXRot()));
            vec.rotate(info.rotation());
            vec.mul(this.quadSize);
            vec.add(x, y, z);
        }


        buffer.vertex(mat[0].x, mat[0].y, mat[0].z).uv(0, 0).endVertex();
        buffer.vertex(mat[1].x, mat[1].y, mat[1].z).uv(0, getV1()).endVertex();
        buffer.vertex(mat[2].x, mat[2].y, mat[2].z).uv(getU1(), getV1()).endVertex();
        buffer.vertex(mat[3].x, mat[3].y, mat[3].z).uv(getU1(), 0).endVertex();
    }

    @Override
    public void tick() {
        super.tick();
        if(onGround)
            this.remove();
    }

    @Override
    protected float getU0() {
        return 0;
    }

    @Override
    protected float getU1() {
        return 1.0F;
    }

    @Override
    protected float getV0() {
        return 0;
    }

    @Override
    protected float getV1() {
        return 1.0F;
    }

    @Override
    public ParticleRenderType getRenderType() {
        return TardisRenderTypes.RIFT_PARTICLE;
    }

    @Override
    public boolean shouldCull() {
        return super.shouldCull();
    }
}
