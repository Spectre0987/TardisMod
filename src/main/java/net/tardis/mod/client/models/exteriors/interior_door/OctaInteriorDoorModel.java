package net.tardis.mod.client.models.exteriors.interior_door;// Made with Blockbench 4.12.2
// Exported for Minecraft version 1.17 or later with Mojang mappings
// Paste this class into your mod and generate all required imports


import com.mojang.blaze3d.vertex.PoseStack;
import com.mojang.blaze3d.vertex.VertexConsumer;
import net.minecraft.client.model.geom.ModelLayerLocation;
import net.minecraft.client.model.geom.ModelPart;
import net.minecraft.client.model.geom.PartPose;
import net.minecraft.client.model.geom.builders.*;
import net.tardis.mod.blockentities.InteriorDoorTile;
import net.tardis.mod.client.animations.AnimationHelper;
import net.tardis.mod.client.models.BasicTileHierarchicalModel;
import net.tardis.mod.helpers.Helper;

public class OctaInteriorDoorModel extends BasicTileHierarchicalModel<InteriorDoorTile> {
	// This layer location should be baked with EntityRendererProvider.Context in the entity renderer and passed into this model's constructor
	public static final ModelLayerLocation LAYER_LOCATION = new ModelLayerLocation(Helper.createRL("exteriors/inteiror/octa"), "main");
	private final ModelPart octa_capsule_interior_door;
	private final ModelPart frame;
	private final ModelPart doors;
	private final ModelPart door_left_rotate_y;
	private final ModelPart north;
	private final ModelPart ne;
	private final ModelPart door_right_rotate_y;
	private final ModelPart nw;
	private final ModelPart north2;
	private final ModelPart base;
	private final ModelPart boti;

	public OctaInteriorDoorModel(ModelPart root) {
		super(root);
		this.octa_capsule_interior_door = root.getChild("octa_capsule_interior_door");
		this.frame = this.octa_capsule_interior_door.getChild("frame");
		this.doors = this.octa_capsule_interior_door.getChild("doors");
		this.door_left_rotate_y = this.doors.getChild("door_left_rotate_y");
		this.north = this.door_left_rotate_y.getChild("north");
		this.ne = this.door_left_rotate_y.getChild("ne");
		this.door_right_rotate_y = this.doors.getChild("door_right_rotate_y");
		this.nw = this.door_right_rotate_y.getChild("nw");
		this.north2 = this.door_right_rotate_y.getChild("north2");
		this.base = this.octa_capsule_interior_door.getChild("base");
		this.boti = this.octa_capsule_interior_door.getChild("boti");
	}

	public static LayerDefinition createBodyLayer() {
		MeshDefinition meshdefinition = new MeshDefinition();
		PartDefinition partdefinition = meshdefinition.getRoot();

		PartDefinition octa_capsule_interior_door = partdefinition.addOrReplaceChild("octa_capsule_interior_door", CubeListBuilder.create(), PartPose.offsetAndRotation(0.0F, 24.0F, 0.0F, 0.0F, 3.1416F, 0.0F));

		PartDefinition frame = octa_capsule_interior_door.addOrReplaceChild("frame", CubeListBuilder.create().texOffs(68, 1).addBox(11.239F, -39.0F, -5.2218F, 5.0F, 39.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(80, 1).addBox(-16.264F, -39.0F, -5.2218F, 5.0F, 39.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, -2.0F, 5.2F));

		PartDefinition cube_r1 = frame.addOrReplaceChild("cube_r1", CubeListBuilder.create().texOffs(50, 61).addBox(-15.0F, -5.0F, -1.0F, 19.0F, 10.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(15.364F, -24.0F, -8.2218F, -1.5708F, 0.0F, 1.5708F));

		PartDefinition cube_r2 = frame.addOrReplaceChild("cube_r2", CubeListBuilder.create().texOffs(49, 61).addBox(-16.0F, -5.0F, -1.0F, 20.0F, 10.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(15.464F, -4.0F, -8.2218F, -1.5708F, 0.0F, 1.5708F));

		PartDefinition cube_r3 = frame.addOrReplaceChild("cube_r3", CubeListBuilder.create().texOffs(49, 61).addBox(-16.0F, -5.0F, -1.0F, 20.0F, 10.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(-15.536F, -4.0F, -8.2218F, -1.5708F, 0.0F, 1.5708F));

		PartDefinition cube_r4 = frame.addOrReplaceChild("cube_r4", CubeListBuilder.create().texOffs(50, 61).addBox(-15.0F, -5.0F, -1.0F, 19.0F, 10.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(-15.536F, -24.0F, -8.2218F, -1.5708F, 0.0F, 1.5708F));

		PartDefinition cube_r5 = frame.addOrReplaceChild("cube_r5", CubeListBuilder.create().texOffs(46, 55).addBox(-5.0F, 1.5F, -0.5F, 10.0F, 15.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(17.864F, -39.5F, -8.2218F, 0.0F, -1.5708F, 1.5708F));

		PartDefinition cube_r6 = frame.addOrReplaceChild("cube_r6", CubeListBuilder.create().texOffs(46, 55).addBox(-5.0F, 8.5F, -0.5F, 10.0F, 3.0F, 2.0F, new CubeDeformation(0.01F)), PartPose.offsetAndRotation(9.986F, -39.5F, -8.2218F, 0.0F, -1.5708F, 1.5708F));

		PartDefinition cube_r7 = frame.addOrReplaceChild("cube_r7", CubeListBuilder.create().texOffs(46, 55).addBox(-5.0F, 1.5F, -0.5F, 10.0F, 15.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(-0.036F, -39.5F, -8.2218F, 0.0F, -1.5708F, 1.5708F));

		PartDefinition doors = octa_capsule_interior_door.addOrReplaceChild("doors", CubeListBuilder.create(), PartPose.offset(0.0F, -2.0F, 4.2F));

		PartDefinition door_left_rotate_y = doors.addOrReplaceChild("door_left_rotate_y", CubeListBuilder.create(), PartPose.offset(-12.0625F, 0.0F, -4.9375F));

		PartDefinition north = door_left_rotate_y.addOrReplaceChild("north", CubeListBuilder.create().texOffs(80, 1).addBox(-5.0F, -39.0F, -12.0F, 5.0F, 39.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(107, 3).addBox(-1.8125F, -20.75F, -12.5F, 1.0F, 4.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(107, 3).addBox(-1.8125F, -20.75F, -11.25F, 1.0F, 4.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offset(12.0625F, 0.0F, 4.9375F));

		PartDefinition ne = door_left_rotate_y.addOrReplaceChild("ne", CubeListBuilder.create().texOffs(1, 1).addBox(-5.0503F, -39.0F, -12.0208F, 10.0F, 39.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(1, 1).addBox(-5.0503F, -39.0F, -12.0208F, 10.0F, 39.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(12.0625F, 0.0F, 4.9375F, 0.0F, 0.7854F, 0.0F));

		PartDefinition door_right_rotate_y = doors.addOrReplaceChild("door_right_rotate_y", CubeListBuilder.create(), PartPose.offset(12.0F, 0.0F, -4.9063F));

		PartDefinition nw = door_right_rotate_y.addOrReplaceChild("nw", CubeListBuilder.create().texOffs(12, 1).addBox(-4.9497F, -39.0F, -12.0208F, 10.0F, 39.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(-12.0F, 0.0F, 4.9063F, 0.0F, -0.7854F, 0.0F));

		PartDefinition north2 = door_right_rotate_y.addOrReplaceChild("north2", CubeListBuilder.create().texOffs(68, 1).addBox(0.0F, -39.0F, -12.0F, 5.0F, 39.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(107, 3).addBox(0.7625F, -20.75F, -12.5F, 1.0F, 4.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(107, 3).addBox(0.7625F, -20.75F, -11.25F, 1.0F, 4.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offset(-12.0F, 0.0F, 4.9063F));

		PartDefinition base = octa_capsule_interior_door.addOrReplaceChild("base", CubeListBuilder.create(), PartPose.offset(0.0F, 0.0F, 3.7F));

		PartDefinition cube_r8 = base.addOrReplaceChild("cube_r8", CubeListBuilder.create().texOffs(71, 61).addBox(-5.0F, -5.0F, -1.0F, 9.0F, 11.0F, 2.0F, new CubeDeformation(0.0F))
		.texOffs(68, 61).addBox(-17.0F, -5.0F, -1.0F, 12.0F, 11.0F, 2.0F, new CubeDeformation(0.0F))
		.texOffs(47, 61).addBox(4.0F, -5.0F, -1.0F, 12.0F, 11.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.464F, -1.0F, -5.7218F, -1.5708F, 0.0F, 0.0F));

		PartDefinition boti = octa_capsule_interior_door.addOrReplaceChild("boti", CubeListBuilder.create().texOffs(0, 53).addBox(-10.9289F, -42.0F, -7.9289F, 22.0F, 41.0F, 0.0F, new CubeDeformation(0.0F))
		.texOffs(10, 54).addBox(-14.9289F, -41.0F, -7.9289F, 4.0F, 40.0F, 0.0F, new CubeDeformation(0.0F))
		.texOffs(10, 54).addBox(11.0711F, -41.0F, -7.9289F, 4.0F, 40.0F, 0.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 0.0F, 0.0F));

		return LayerDefinition.create(meshdefinition, 256, 256);
	}

	@Override
	public void renderToBuffer(PoseStack poseStack, VertexConsumer vertexConsumer, int packedLight, int packedOverlay, float red, float green, float blue, float alpha) {
		octa_capsule_interior_door.render(poseStack, vertexConsumer, packedLight, packedOverlay, red, green, blue, alpha);
	}

	@Override
	public void setupAnimations(InteriorDoorTile tile, float ageInTicks) {
		this.root().getAllParts().forEach(ModelPart::resetPose);
		if(tile.getDoorHandler().getDoorState().isOpen()) {
			this.door_left_rotate_y.offsetRotation(AnimationHelper.degrees(45, AnimationHelper.Axis.Y));
			this.door_right_rotate_y.offsetRotation(AnimationHelper.degrees(-45, AnimationHelper.Axis.Y));
		}
	}
}