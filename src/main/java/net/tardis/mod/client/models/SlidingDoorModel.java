package net.tardis.mod.client.models;// Made with Blockbench 4.11.1
// Exported for Minecraft version 1.17 or later with Mojang mappings
// Paste this class into your mod and generate all required imports


import com.mojang.blaze3d.vertex.PoseStack;
import com.mojang.blaze3d.vertex.VertexConsumer;
import net.minecraft.client.model.geom.ModelLayerLocation;
import net.minecraft.client.model.geom.ModelPart;
import net.minecraft.client.model.geom.PartPose;
import net.minecraft.client.model.geom.builders.*;
import net.minecraft.client.renderer.RenderType;
import net.tardis.mod.block.BigDoorBlock;
import net.tardis.mod.blockentities.BigDoorTile;
import net.tardis.mod.client.animations.SlidingDoorAnimations;
import net.tardis.mod.helpers.Helper;

public class SlidingDoorModel<T extends BigDoorTile> extends BasicTileHierarchicalModel<T> {
	// This layer location should be baked with EntityRendererProvider.Context in the entity renderer and passed into this model's constructor
	public static final ModelLayerLocation LAYER_LOCATION = new ModelLayerLocation(Helper.createRL("tiles/big_doors"), "main");
	private final ModelPart east;
	private final ModelPart west;

	public SlidingDoorModel(ModelPart root) {
		super(root, RenderType::entityCutoutNoCull);
		this.east = root.getChild("east");
		this.west = root.getChild("west");
	}

	public static LayerDefinition createBodyLayer() {
		MeshDefinition meshdefinition = new MeshDefinition();
		PartDefinition partdefinition = meshdefinition.getRoot();

		PartDefinition east = partdefinition.addOrReplaceChild("east", CubeListBuilder.create().texOffs(0, 0).addBox(-31.0F, -64.0F, -1.0F, 32.0F, 64.0F, 4.0F, new CubeDeformation(0.0F)), PartPose.offset(-1.0F, 24.0F, -1.0F));

		PartDefinition west = partdefinition.addOrReplaceChild("west", CubeListBuilder.create(), PartPose.offset(16.0F, -8.0F, 0.0F));

		PartDefinition cube_r1 = west.addOrReplaceChild("cube_r1", CubeListBuilder.create().texOffs(0, 0).addBox(-16.0F, -32.0F, -2.0F, 32.0F, 64.0F, 4.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.0F, 3.1416F, 0.0F));

		return LayerDefinition.create(meshdefinition, 128, 128);
	}

	@Override
	public void renderToBuffer(PoseStack poseStack, VertexConsumer vertexConsumer, int packedLight, int packedOverlay, float red, float green, float blue, float alpha) {
		east.render(poseStack, vertexConsumer, packedLight, packedOverlay, red, green, blue, alpha);
		west.render(poseStack, vertexConsumer, packedLight, packedOverlay, red, green, blue, alpha);
	}

	@Override
	public void setupAnimations(T tile, float ageInTicks) {
		this.root().getAllParts().forEach(ModelPart::resetPose);
		boolean isOpen = tile.getBlockState().getValue(BigDoorBlock.OPEN);
		if(isOpen)
			this.animate(tile.openingState, SlidingDoorAnimations.OPEN, ageInTicks);
		else this.animate(tile.closingState, SlidingDoorAnimations.CLOSE, ageInTicks);
	}
}