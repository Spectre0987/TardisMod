package net.tardis.mod.client.models;

import net.minecraft.client.model.geom.ModelPart;

public interface IHaveBotiPortal {

    ModelPart getBotiWindow();

}
