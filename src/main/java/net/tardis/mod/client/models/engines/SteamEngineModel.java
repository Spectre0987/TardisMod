package net.tardis.mod.client.models.engines;
// Made with Blockbench 4.11.2
// Exported for Minecraft version 1.17 or later with Mojang mappings
// Paste this class into your mod and generate all required imports


import com.mojang.blaze3d.vertex.PoseStack;
import com.mojang.blaze3d.vertex.VertexConsumer;
import net.minecraft.client.model.EntityModel;
import net.minecraft.client.model.geom.ModelLayerLocation;
import net.minecraft.client.model.geom.ModelPart;
import net.minecraft.client.model.geom.PartPose;
import net.minecraft.client.model.geom.builders.*;
import net.minecraft.world.entity.Entity;
import net.tardis.mod.helpers.Helper;

public class SteamEngineModel extends EntityModel<Entity> {
	// This layer location should be baked with EntityRendererProvider.Context in the entity renderer and passed into this model's constructor
	public static final ModelLayerLocation LAYER_LOCATION = new ModelLayerLocation(Helper.createRL("engines/steam"), "main");
	private final ModelPart head;
	private final ModelPart body;
	private final ModelPart arm1;
	private final ModelPart arm2;
	private final ModelPart arm3;
	private final ModelPart doors;
	private final ModelPart north;
	private final ModelPart east;
	private final ModelPart west;
	private final ModelPart south;
	private final ModelPart foot;

	public SteamEngineModel(ModelPart root) {
		this.head = root.getChild("head");
		this.body = root.getChild("body");
		this.arm1 = root.getChild("arm1");
		this.arm2 = root.getChild("arm2");
		this.arm3 = root.getChild("arm3");
		this.doors = root.getChild("doors");
		this.north = this.doors.getChild("north");
		this.east = this.doors.getChild("east");
		this.west = this.doors.getChild("west");
		this.south = this.doors.getChild("south");
		this.foot = root.getChild("foot");
	}

	public static LayerDefinition createBodyLayer() {
		MeshDefinition meshdefinition = new MeshDefinition();
		PartDefinition partdefinition = meshdefinition.getRoot();

		PartDefinition head = partdefinition.addOrReplaceChild("head", CubeListBuilder.create().texOffs(58, 1).addBox(-13.0F, -22.0F, 3.0F, 10.0F, 2.0F, 10.0F, new CubeDeformation(0.0F))
		.texOffs(1, 32).addBox(-16.0F, -32.0F, 0.0F, 16.0F, 4.0F, 16.0F, new CubeDeformation(0.0F))
		.texOffs(101, 62).addBox(-11.0F, -26.0F, 5.0F, 6.0F, 4.0F, 6.0F, new CubeDeformation(0.0F))
		.texOffs(1, 53).addBox(-13.0F, -28.0F, 3.0F, 10.0F, 2.0F, 10.0F, new CubeDeformation(0.0F)), PartPose.offset(8.0F, 8.0F, -8.0F));

		PartDefinition body = partdefinition.addOrReplaceChild("body", CubeListBuilder.create().texOffs(66, 14).addBox(-14.0F, -3.0F, 2.0F, 12.0F, 3.0F, 12.0F, new CubeDeformation(0.0F))
		.texOffs(1, 1).addBox(-16.0F, -17.0F, 0.0F, 16.0F, 14.0F, 16.0F, new CubeDeformation(0.0F))
		.texOffs(66, 14).addBox(-14.0F, -20.0F, 2.0F, 12.0F, 3.0F, 12.0F, new CubeDeformation(0.0F)), PartPose.offset(8.0F, 8.0F, -8.0F));

		PartDefinition arm1 = partdefinition.addOrReplaceChild("arm1", CubeListBuilder.create().texOffs(89, 1).addBox(-3.0F, -22.0714F, 11.2857F, 6.0F, 2.0F, 6.0F, new CubeDeformation(0.0F))
		.texOffs(33, 53).addBox(-2.5F, -20.0714F, 11.7857F, 5.0F, 1.0F, 5.0F, new CubeDeformation(0.0F))
		.texOffs(84, 53).addBox(-2.0F, -19.0714F, 12.2857F, 4.0F, 15.0F, 4.0F, new CubeDeformation(0.0F))
		.texOffs(33, 53).addBox(-2.5F, -4.0714F, 11.7857F, 5.0F, 1.0F, 5.0F, new CubeDeformation(0.0F))
		.texOffs(52, 34).addBox(-3.0F, -3.0714F, 11.2857F, 6.0F, 6.0F, 6.0F, new CubeDeformation(0.0F))
		.texOffs(84, 73).addBox(-2.0F, -2.0714F, 7.2857F, 4.0F, 4.0F, 3.0F, new CubeDeformation(0.0F))
		.texOffs(3, 40).addBox(-2.5F, -2.5714F, 10.2857F, 5.0F, 5.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, -1.9286F, 0.0476F));

		PartDefinition arm2 = partdefinition.addOrReplaceChild("arm2", CubeListBuilder.create().texOffs(89, 1).addBox(-3.0F, -22.0714F, 11.2857F, 6.0F, 2.0F, 6.0F, new CubeDeformation(0.0F))
		.texOffs(33, 53).addBox(-2.5F, -20.0714F, 11.7857F, 5.0F, 1.0F, 5.0F, new CubeDeformation(0.0F))
		.texOffs(84, 53).addBox(-2.0F, -19.0714F, 12.2857F, 4.0F, 15.0F, 4.0F, new CubeDeformation(0.0F))
		.texOffs(33, 53).addBox(-2.5F, -4.0714F, 11.7857F, 5.0F, 1.0F, 5.0F, new CubeDeformation(0.0F))
		.texOffs(52, 34).addBox(-3.0F, -3.0714F, 11.2857F, 6.0F, 6.0F, 6.0F, new CubeDeformation(0.0F))
		.texOffs(84, 73).addBox(-2.0F, -2.0714F, 7.2857F, 4.0F, 4.0F, 3.0F, new CubeDeformation(0.0F))
		.texOffs(3, 40).addBox(-2.5F, -2.5714F, 10.2857F, 5.0F, 5.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, -1.9286F, 0.0476F, 0.0F, -1.5708F, 0.0F));

		PartDefinition arm3 = partdefinition.addOrReplaceChild("arm3", CubeListBuilder.create().texOffs(89, 1).addBox(-3.0F, -22.0714F, 11.2857F, 6.0F, 2.0F, 6.0F, new CubeDeformation(0.0F))
		.texOffs(33, 53).addBox(-2.5F, -20.0714F, 11.7857F, 5.0F, 1.0F, 5.0F, new CubeDeformation(0.0F))
		.texOffs(84, 53).addBox(-2.0F, -19.0714F, 12.2857F, 4.0F, 15.0F, 4.0F, new CubeDeformation(0.0F))
		.texOffs(33, 53).addBox(-2.5F, -4.0714F, 11.7857F, 5.0F, 1.0F, 5.0F, new CubeDeformation(0.0F))
		.texOffs(52, 34).addBox(-3.0F, -3.0714F, 11.2857F, 6.0F, 6.0F, 6.0F, new CubeDeformation(0.0F))
		.texOffs(84, 73).addBox(-2.0F, -2.0714F, 7.2857F, 4.0F, 4.0F, 3.0F, new CubeDeformation(0.0F))
		.texOffs(3, 40).addBox(-2.5F, -2.5714F, 10.2857F, 5.0F, 5.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, -1.9286F, 0.0476F, 0.0F, 1.5708F, 0.0F));

		PartDefinition doors = partdefinition.addOrReplaceChild("doors", CubeListBuilder.create(), PartPose.offset(8.0F, 8.0F, -8.0F));

		PartDefinition north = doors.addOrReplaceChild("north", CubeListBuilder.create().texOffs(50, 72).addBox(0.5F, -4.0F, -0.5F, 7.0F, 8.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offset(-12.0F, 5.0F, 1.5F));

		PartDefinition east = doors.addOrReplaceChild("east", CubeListBuilder.create().texOffs(67, 56).addBox(-0.5F, -4.0F, -7.75F, 1.0F, 8.0F, 7.0F, new CubeDeformation(0.0F)), PartPose.offset(-14.5F, 5.0F, 12.25F));

		PartDefinition west = doors.addOrReplaceChild("west", CubeListBuilder.create().texOffs(67, 72).addBox(-0.5F, -4.0F, 0.25F, 1.0F, 8.0F, 7.0F, new CubeDeformation(0.0F)), PartPose.offset(-1.5F, 5.0F, 4.25F));

		PartDefinition south = doors.addOrReplaceChild("south", CubeListBuilder.create().texOffs(50, 62).addBox(-7.5F, -4.0F, -0.5F, 7.0F, 8.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offset(-4.0F, 5.0F, 14.5F));

		PartDefinition foot = partdefinition.addOrReplaceChild("foot", CubeListBuilder.create().texOffs(1, 66).addBox(-16.0F, 10.0F, 0.0F, 16.0F, 6.0F, 16.0F, new CubeDeformation(0.0F))
		.texOffs(66, 37).addBox(-15.0F, 9.0F, 1.0F, 14.0F, 1.0F, 14.0F, new CubeDeformation(0.0F))
		.texOffs(4, 89).addBox(-14.5F, 0.0F, 1.5F, 13.0F, 9.0F, 13.0F, new CubeDeformation(0.0F)), PartPose.offset(8.0F, 8.0F, -8.0F));

		return LayerDefinition.create(meshdefinition, 128, 128);
	}

	@Override
	public void setupAnim(Entity entity, float limbSwing, float limbSwingAmount, float ageInTicks, float netHeadYaw, float headPitch) {

	}

	@Override
	public void renderToBuffer(PoseStack poseStack, VertexConsumer vertexConsumer, int packedLight, int packedOverlay, float red, float green, float blue, float alpha) {
		head.render(poseStack, vertexConsumer, packedLight, packedOverlay, red, green, blue, alpha);
		body.render(poseStack, vertexConsumer, packedLight, packedOverlay, red, green, blue, alpha);
		arm1.render(poseStack, vertexConsumer, packedLight, packedOverlay, red, green, blue, alpha);
		arm2.render(poseStack, vertexConsumer, packedLight, packedOverlay, red, green, blue, alpha);
		arm3.render(poseStack, vertexConsumer, packedLight, packedOverlay, red, green, blue, alpha);
		doors.render(poseStack, vertexConsumer, packedLight, packedOverlay, red, green, blue, alpha);
		foot.render(poseStack, vertexConsumer, packedLight, packedOverlay, red, green, blue, alpha);
	}
}