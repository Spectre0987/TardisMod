package net.tardis.mod.client.models.consoles;


import com.mojang.blaze3d.vertex.PoseStack;
import com.mojang.blaze3d.vertex.VertexConsumer;
import net.minecraft.client.Minecraft;
import net.minecraft.client.model.geom.ModelLayerLocation;
import net.minecraft.client.model.geom.ModelPart;
import net.minecraft.client.model.geom.PartPose;
import net.minecraft.client.model.geom.builders.*;
import net.tardis.mod.blockentities.consoles.GalvanicTile;
import net.tardis.mod.cap.Capabilities;
import net.tardis.mod.client.animations.consoles.GalvanicConsoleAnimations;
import net.tardis.mod.client.models.BasicTileHierarchicalModel;
import net.tardis.mod.control.ControlType;
import net.tardis.mod.helpers.Helper;
import net.tardis.mod.registry.ControlRegistry;

import java.util.Optional;

public class GalvanicConsoleModel<T extends GalvanicTile> extends BasicTileHierarchicalModel<T> implements IAdditionalConsoleRenderData {
	// This layer location should be baked with EntityRendererProvider.Context in the entity renderer and passed into this model's constructor
	public static final ModelLayerLocation LAYER_LOCATION = new ModelLayerLocation(Helper.createRL("consoles/galvanic"), "main");
	private final ModelPart console;
	private final ModelPart time_scepter;
	private final ModelPart controls;
	private final ModelPart bb_main;

	public GalvanicConsoleModel(ModelPart root) {
		super(root);
		this.console = root.getChild("console");
		this.time_scepter = root.getChild("time_scepter");
		this.controls = root.getChild("controls");
		this.bb_main = root.getChild("bb_main");
	}

	public static LayerDefinition createBodyLayer() {
		MeshDefinition meshdefinition = new MeshDefinition();
		PartDefinition partdefinition = meshdefinition.getRoot();

		PartDefinition console = partdefinition.addOrReplaceChild("console", CubeListBuilder.create(), PartPose.offset(0.0F, 24.0F, 0.0F));

		PartDefinition bottom_plate = console.addOrReplaceChild("bottom_plate", CubeListBuilder.create().texOffs(50, 13).addBox(-5.8F, -0.6F, -2.0F, 11.0F, 1.0F, 4.0F, new CubeDeformation(-0.3F))
		.texOffs(50, 13).addBox(-4.8F, -0.6F, -4.4F, 9.0F, 1.0F, 3.0F, new CubeDeformation(-0.3F))
		.texOffs(50, 13).addBox(-4.8F, -0.6F, 1.425F, 9.0F, 1.0F, 3.0F, new CubeDeformation(-0.3F))
		.texOffs(50, 13).addBox(-2.8F, -0.6F, 3.825F, 6.0F, 1.0F, 1.0F, new CubeDeformation(-0.3F))
		.texOffs(50, 13).addBox(-2.8F, -0.6F, -4.8F, 6.0F, 1.0F, 1.0F, new CubeDeformation(-0.3F)), PartPose.offset(0.0F, -0.1F, 0.0F));

		PartDefinition stations = console.addOrReplaceChild("stations", CubeListBuilder.create(), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition station_0 = stations.addOrReplaceChild("station_0", CubeListBuilder.create(), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition edge_0 = station_0.addOrReplaceChild("edge_0", CubeListBuilder.create().texOffs(2, 4).addBox(-8.5F, -15.0F, -15.25F, 17.0F, 1.0F, 2.0F, new CubeDeformation(0.0F))
		.texOffs(3, 40).addBox(-2.5F, -19.3F, -5.25F, 5.0F, 7.0F, 3.0F, new CubeDeformation(0.0F))
		.texOffs(3, 40).addBox(-2.4797F, -20.925F, -4.3387F, 5.0F, 2.0F, 2.0F, new CubeDeformation(0.0F))
		.texOffs(3, 40).addBox(-2.4824F, -22.7F, -4.3169F, 5.0F, 1.0F, 2.0F, new CubeDeformation(0.0F))
		.texOffs(50, 13).addBox(-1.9664F, -21.75F, -3.4588F, 4.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(3, 40).addBox(-2.4824F, -33.7F, -4.3169F, 5.0F, 2.0F, 2.0F, new CubeDeformation(0.0F))
		.texOffs(3, 40).addBox(-2.4797F, -30.925F, -4.3387F, 5.0F, 1.0F, 2.0F, new CubeDeformation(0.0F))
		.texOffs(50, 13).addBox(-1.9664F, -31.75F, -3.4588F, 4.0F, 1.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition cooling_blades_0 = edge_0.addOrReplaceChild("cooling_blades_0", CubeListBuilder.create().texOffs(50, 13).addBox(-3.85F, -8.175F, -6.625F, 8.0F, 2.0F, 2.0F, new CubeDeformation(-0.6F))
		.texOffs(49, 13).addBox(-3.8F, -8.25F, -7.0F, 8.0F, 2.0F, 2.0F, new CubeDeformation(-0.8F)), PartPose.offset(0.0F, -0.25F, 0.0F));

		PartDefinition glow_plasma_coil_0 = edge_0.addOrReplaceChild("glow_plasma_coil_0", CubeListBuilder.create().texOffs(107, 12).addBox(-0.975F, -20.95F, -5.6F, 2.0F, 3.0F, 3.0F, new CubeDeformation(-0.7F))
		.texOffs(107, 12).addBox(-0.025F, -20.95F, -5.6F, 2.0F, 3.0F, 3.0F, new CubeDeformation(-0.7F))
		.texOffs(107, 12).addBox(-1.925F, -20.95F, -5.6F, 2.0F, 3.0F, 3.0F, new CubeDeformation(-0.7F))
		.texOffs(3, 40).addBox(-2.8F, -19.6F, -4.575F, 5.0F, 1.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition belly_0 = edge_0.addOrReplaceChild("belly_0", CubeListBuilder.create().texOffs(50, 13).addBox(-2.8F, -0.5F, -6.0F, 6.0F, 1.0F, 2.0F, new CubeDeformation(-0.2F))
		.texOffs(50, 13).addBox(-1.8F, -0.5F, -6.6F, 4.0F, 1.0F, 1.0F, new CubeDeformation(-0.2F))
		.texOffs(50, 13).addBox(-2.8F, -4.25F, -5.1F, 6.0F, 1.0F, 1.0F, new CubeDeformation(-0.2F))
		.texOffs(50, 13).addBox(-2.8F, -3.7F, -6.0F, 6.0F, 1.0F, 2.0F, new CubeDeformation(-0.2F))
		.texOffs(50, 13).addBox(2.2F, -3.1F, -6.0F, 1.0F, 3.0F, 2.0F, new CubeDeformation(-0.2F))
		.texOffs(50, 13).addBox(-2.8F, -3.1F, -6.0F, 1.0F, 3.0F, 2.0F, new CubeDeformation(-0.2F))
		.texOffs(50, 13).addBox(-2.7F, -3.6F, -6.3F, 2.0F, 4.0F, 2.0F, new CubeDeformation(-0.7F))
		.texOffs(50, 13).addBox(-1.45F, -3.6F, -6.3F, 2.0F, 4.0F, 2.0F, new CubeDeformation(-0.7F))
		.texOffs(50, 13).addBox(-0.15F, -3.6F, -6.3F, 2.0F, 4.0F, 2.0F, new CubeDeformation(-0.7F))
		.texOffs(50, 13).addBox(1.1F, -3.6F, -6.3F, 2.0F, 4.0F, 2.0F, new CubeDeformation(-0.7F)), PartPose.offset(0.0F, -0.25F, 0.0F));

		PartDefinition plane_0 = station_0.addOrReplaceChild("plane_0", CubeListBuilder.create().texOffs(3, 15).addBox(-7.9F, -20.2154F, -5.5748F, 16.0F, 3.0F, 3.0F, new CubeDeformation(-0.6F))
		.texOffs(3, 15).addBox(-1.6F, -20.2154F, -3.7748F, 9.0F, 2.0F, 3.0F, new CubeDeformation(-0.6F))
		.texOffs(3, 15).addBox(-7.4F, -20.2154F, -3.7748F, 7.0F, 2.0F, 3.0F, new CubeDeformation(-0.6F))
		.texOffs(3, 15).addBox(-6.6F, -20.2154F, -1.9748F, 13.0F, 2.0F, 2.0F, new CubeDeformation(-0.6F))
		.texOffs(3, 15).addBox(0.075F, -20.2154F, -1.1748F, 6.0F, 2.0F, 3.0F, new CubeDeformation(-0.6F))
		.texOffs(3, 15).addBox(-5.725F, -20.2154F, -1.1748F, 7.0F, 2.0F, 3.0F, new CubeDeformation(-0.6F))
		.texOffs(3, 15).addBox(-5.7F, -20.2154F, 0.6252F, 11.0F, 2.0F, 2.0F, new CubeDeformation(-0.6F))
		.texOffs(3, 15).addBox(-4.8F, -20.2154F, 1.4252F, 10.0F, 2.0F, 2.0F, new CubeDeformation(-0.6F))
		.texOffs(2, 15).addBox(-0.65F, -20.2154F, 2.2252F, 5.0F, 2.0F, 3.0F, new CubeDeformation(-0.6F))
		.texOffs(2, 15).addBox(-4.45F, -20.2154F, 2.2252F, 5.0F, 2.0F, 3.0F, new CubeDeformation(-0.6F))
		.texOffs(3, 15).addBox(-3.85F, -20.2154F, 4.0252F, 8.0F, 2.0F, 2.0F, new CubeDeformation(-0.6F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.5236F, 0.0F, 0.0F));

		PartDefinition underbox_0 = plane_0.addOrReplaceChild("underbox_0", CubeListBuilder.create().texOffs(50, 13).addBox(-3.25F, -17.25F, -2.75F, 6.0F, 1.0F, 6.0F, new CubeDeformation(0.0F))
		.texOffs(50, 13).addBox(-3.75F, -19.25F, -4.25F, 8.0F, 2.0F, 8.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition rib_o = station_0.addOrReplaceChild("rib_o", CubeListBuilder.create(), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.0F, -0.5236F, 0.0F));

		PartDefinition rib_tilt_0 = rib_o.addOrReplaceChild("rib_tilt_0", CubeListBuilder.create().texOffs(3, 40).addBox(-0.35F, -14.8F, -4.85F, 1.0F, 2.0F, 2.0F, new CubeDeformation(-0.2F))
		.texOffs(3, 40).addBox(-0.35F, -16.325F, -4.85F, 1.0F, 1.0F, 2.0F, new CubeDeformation(-0.2F))
		.texOffs(3, 40).addBox(-0.35F, -17.825F, -4.85F, 1.0F, 1.0F, 2.0F, new CubeDeformation(-0.2F))
		.texOffs(3, 40).addBox(-0.35F, -20.325F, -4.85F, 1.0F, 2.0F, 2.0F, new CubeDeformation(-0.2F))
		.texOffs(3, 40).addBox(-0.35F, -20.8F, -5.45F, 1.0F, 8.0F, 1.0F, new CubeDeformation(-0.2F))
		.texOffs(3, 40).addBox(-0.35F, -20.1F, -3.7F, 1.0F, 7.0F, 2.0F, new CubeDeformation(0.0F))
		.texOffs(3, 40).addBox(-0.375F, -20.75F, -9.0F, 1.0F, 3.0F, 12.0F, new CubeDeformation(0.1F))
		.texOffs(3, 40).addBox(-0.375F, -20.0F, -9.5F, 1.0F, 3.0F, 12.0F, new CubeDeformation(-0.1F))
		.texOffs(3, 40).addBox(-0.5F, -18.0F, 0.0F, 1.0F, 2.0F, 4.0F, new CubeDeformation(0.0F))
		.texOffs(3, 40).addBox(-0.35F, -5.4003F, -9.5594F, 1.0F, 1.0F, 3.0F, new CubeDeformation(-0.2F))
		.texOffs(3, 40).addBox(-0.35F, -6.0003F, -10.8094F, 1.0F, 1.0F, 4.0F, new CubeDeformation(-0.2F))
		.texOffs(3, 40).addBox(-0.35F, -7.1753F, -13.3094F, 1.0F, 1.0F, 7.0F, new CubeDeformation(-0.2F))
		.texOffs(3, 40).addBox(-0.35F, -6.6003F, -7.6094F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.2F))
		.texOffs(3, 40).addBox(-0.35F, -6.6003F, -8.8094F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.2F))
		.texOffs(3, 40).addBox(-0.35F, -6.6003F, -10.0094F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.2F))
		.texOffs(3, 40).addBox(-0.375F, -6.6003F, -12.2094F, 1.0F, 1.0F, 2.0F, new CubeDeformation(-0.2F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.4363F, 0.0F, 0.0F));

		PartDefinition rib_deco_0 = rib_tilt_0.addOrReplaceChild("rib_deco_0", CubeListBuilder.create().texOffs(50, 13).addBox(-0.85F, -21.25F, -10.0F, 2.0F, 2.0F, 2.0F, new CubeDeformation(0.0F))
		.texOffs(50, 13).addBox(-1.375F, -21.75F, 1.25F, 3.0F, 3.0F, 2.0F, new CubeDeformation(-0.8F))
		.texOffs(50, 13).addBox(-1.375F, -21.75F, -2.75F, 3.0F, 3.0F, 2.0F, new CubeDeformation(-0.8F))
		.texOffs(50, 13).addBox(-1.375F, -21.75F, -3.5F, 3.0F, 3.0F, 2.0F, new CubeDeformation(-0.8F))
		.texOffs(50, 13).addBox(-1.375F, -21.75F, -4.25F, 3.0F, 3.0F, 2.0F, new CubeDeformation(-0.8F))
		.texOffs(3, 40).addBox(-1.375F, -21.75F, 1.95F, 3.0F, 3.0F, 3.0F, new CubeDeformation(-0.8F)), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition clawfoot_0 = station_0.addOrReplaceChild("clawfoot_0", CubeListBuilder.create(), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.0F, -0.5236F, 0.0F));

		PartDefinition leg_0 = clawfoot_0.addOrReplaceChild("leg_0", CubeListBuilder.create().texOffs(3, 40).addBox(-0.35F, -1.0F, -13.0F, 1.0F, 1.0F, 8.0F, new CubeDeformation(0.0F))
		.texOffs(3, 40).addBox(-0.35F, -15.85F, -9.0F, 1.0F, 15.0F, 4.0F, new CubeDeformation(-0.1F))
		.texOffs(3, 40).addBox(-0.85F, -7.0F, -11.25F, 2.0F, 5.0F, 4.0F, new CubeDeformation(-0.75F))
		.texOffs(3, 40).addBox(-0.85F, -11.5F, -11.25F, 2.0F, 6.0F, 4.0F, new CubeDeformation(-0.75F))
		.texOffs(3, 40).addBox(-0.85F, -14.25F, -12.6F, 2.0F, 13.0F, 2.0F, new CubeDeformation(-0.8F))
		.texOffs(3, 40).addBox(-0.35F, -9.35F, -11.7F, 1.0F, 1.0F, 2.0F, new CubeDeformation(-0.3F))
		.texOffs(3, 40).addBox(-0.35F, -8.1F, -11.7F, 1.0F, 1.0F, 2.0F, new CubeDeformation(-0.3F))
		.texOffs(3, 40).addBox(-0.35F, -6.85F, -11.7F, 1.0F, 1.0F, 2.0F, new CubeDeformation(-0.3F))
		.texOffs(3, 40).addBox(-0.35F, -5.6F, -11.7F, 1.0F, 1.0F, 2.0F, new CubeDeformation(-0.3F))
		.texOffs(3, 40).addBox(-0.35F, -3.1F, -11.7F, 1.0F, 1.0F, 2.0F, new CubeDeformation(-0.3F))
		.texOffs(3, 40).addBox(-0.35F, -4.35F, -11.7F, 1.0F, 1.0F, 2.0F, new CubeDeformation(-0.3F))
		.texOffs(3, 40).addBox(-0.35F, -12.65F, -11.7F, 1.0F, 3.0F, 1.0F, new CubeDeformation(-0.3F))
		.texOffs(3, 40).addBox(-0.35F, -11.65F, -11.3F, 1.0F, 2.0F, 1.0F, new CubeDeformation(-0.3F))
		.texOffs(3, 40).addBox(-0.35F, -10.65F, -10.9F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.3F))
		.texOffs(3, 40).addBox(-0.85F, -1.4342F, -15.3467F, 2.0F, 2.0F, 3.0F, new CubeDeformation(-0.65F)), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition ball_0 = clawfoot_0.addOrReplaceChild("ball_0", CubeListBuilder.create().texOffs(50, 13).addBox(-0.35F, -1.8F, -15.8F, 1.0F, 2.0F, 2.0F, new CubeDeformation(-0.1F))
		.texOffs(50, 13).addBox(-0.85F, -1.8F, -15.8F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.2F))
		.texOffs(50, 13).addBox(-1.1F, -1.8F, -15.8F, 1.0F, 2.0F, 2.0F, new CubeDeformation(-0.3F))
		.texOffs(40, 87).addBox(-1.7F, -1.8F, -15.8F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.7F))
		.texOffs(40, 88).addBox(0.05F, -1.8F, -15.8F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.7F))
		.texOffs(50, 13).addBox(0.4F, -1.8F, -15.8F, 1.0F, 2.0F, 2.0F, new CubeDeformation(-0.3F)), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition station_1 = stations.addOrReplaceChild("station_1", CubeListBuilder.create(), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.0F, -1.0472F, 0.0F));

		PartDefinition edge_1 = station_1.addOrReplaceChild("edge_1", CubeListBuilder.create().texOffs(2, 4).addBox(-8.5F, -15.0F, -15.25F, 17.0F, 1.0F, 2.0F, new CubeDeformation(0.0F))
		.texOffs(3, 40).addBox(-2.5F, -19.3F, -5.25F, 5.0F, 7.0F, 3.0F, new CubeDeformation(0.0F))
		.texOffs(3, 40).addBox(-2.4797F, -20.9F, -4.3387F, 5.0F, 2.0F, 2.0F, new CubeDeformation(0.0F))
		.texOffs(3, 40).addBox(-2.4824F, -22.675F, -4.3169F, 5.0F, 1.0F, 2.0F, new CubeDeformation(0.0F))
		.texOffs(50, 13).addBox(-1.9664F, -21.75F, -3.4588F, 4.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(3, 40).addBox(-2.4824F, -33.675F, -4.3169F, 5.0F, 2.0F, 2.0F, new CubeDeformation(0.0F))
		.texOffs(3, 40).addBox(-2.4797F, -30.9F, -4.3387F, 5.0F, 1.0F, 2.0F, new CubeDeformation(0.0F))
		.texOffs(50, 13).addBox(-1.9664F, -31.75F, -3.4588F, 4.0F, 1.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition cooling_blades_1 = edge_1.addOrReplaceChild("cooling_blades_1", CubeListBuilder.create().texOffs(50, 13).addBox(-3.85F, -8.175F, -6.625F, 8.0F, 2.0F, 2.0F, new CubeDeformation(-0.6F))
		.texOffs(49, 13).addBox(-3.8F, -8.25F, -7.0F, 8.0F, 2.0F, 2.0F, new CubeDeformation(-0.8F)), PartPose.offset(0.0F, -0.25F, 0.0F));

		PartDefinition glow_plasma_coil_1 = edge_1.addOrReplaceChild("glow_plasma_coil_1", CubeListBuilder.create().texOffs(107, 12).addBox(-0.975F, -20.95F, -5.6F, 2.0F, 3.0F, 3.0F, new CubeDeformation(-0.7F))
		.texOffs(107, 12).addBox(-0.025F, -20.95F, -5.6F, 2.0F, 3.0F, 3.0F, new CubeDeformation(-0.7F))
		.texOffs(107, 12).addBox(-1.925F, -20.95F, -5.6F, 2.0F, 3.0F, 3.0F, new CubeDeformation(-0.7F))
		.texOffs(3, 40).addBox(-2.8F, -19.6F, -4.575F, 5.0F, 1.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition belly_1 = edge_1.addOrReplaceChild("belly_1", CubeListBuilder.create().texOffs(50, 13).addBox(-2.8F, -0.5F, -6.0F, 6.0F, 1.0F, 2.0F, new CubeDeformation(-0.2F))
		.texOffs(50, 13).addBox(-1.8F, -0.5F, -6.6F, 4.0F, 1.0F, 1.0F, new CubeDeformation(-0.2F))
		.texOffs(50, 13).addBox(-2.8F, -4.25F, -5.1F, 6.0F, 1.0F, 1.0F, new CubeDeformation(-0.2F))
		.texOffs(50, 13).addBox(-2.8F, -3.7F, -6.0F, 6.0F, 1.0F, 2.0F, new CubeDeformation(-0.2F))
		.texOffs(50, 13).addBox(2.2F, -3.1F, -6.0F, 1.0F, 3.0F, 2.0F, new CubeDeformation(-0.2F))
		.texOffs(50, 13).addBox(-2.8F, -3.1F, -6.0F, 1.0F, 3.0F, 2.0F, new CubeDeformation(-0.2F))
		.texOffs(50, 13).addBox(-2.7F, -3.6F, -6.3F, 2.0F, 4.0F, 2.0F, new CubeDeformation(-0.7F))
		.texOffs(50, 13).addBox(-1.45F, -3.6F, -6.3F, 2.0F, 4.0F, 2.0F, new CubeDeformation(-0.7F))
		.texOffs(50, 13).addBox(-0.15F, -3.6F, -6.3F, 2.0F, 4.0F, 2.0F, new CubeDeformation(-0.7F))
		.texOffs(50, 13).addBox(1.1F, -3.6F, -6.3F, 2.0F, 4.0F, 2.0F, new CubeDeformation(-0.7F)), PartPose.offset(0.0F, -0.25F, 0.0F));

		PartDefinition plane_1 = station_1.addOrReplaceChild("plane_1", CubeListBuilder.create().texOffs(3, 15).addBox(-7.9F, -20.2154F, -5.5748F, 16.0F, 3.0F, 3.0F, new CubeDeformation(-0.6F))
		.texOffs(3, 15).addBox(-1.6F, -20.2154F, -3.7748F, 9.0F, 2.0F, 3.0F, new CubeDeformation(-0.6F))
		.texOffs(3, 15).addBox(-7.4F, -20.2154F, -3.7748F, 7.0F, 2.0F, 3.0F, new CubeDeformation(-0.6F))
		.texOffs(3, 15).addBox(-6.6F, -20.2154F, -1.9748F, 13.0F, 2.0F, 2.0F, new CubeDeformation(-0.6F))
		.texOffs(3, 15).addBox(0.075F, -20.2154F, -1.1748F, 6.0F, 2.0F, 3.0F, new CubeDeformation(-0.6F))
		.texOffs(3, 15).addBox(-5.725F, -20.2154F, -1.1748F, 7.0F, 2.0F, 3.0F, new CubeDeformation(-0.6F))
		.texOffs(3, 15).addBox(-5.7F, -20.2154F, 0.6252F, 11.0F, 2.0F, 2.0F, new CubeDeformation(-0.6F))
		.texOffs(3, 15).addBox(-4.8F, -20.2154F, 1.4252F, 10.0F, 2.0F, 2.0F, new CubeDeformation(-0.6F))
		.texOffs(2, 15).addBox(-0.65F, -20.2154F, 2.2252F, 5.0F, 2.0F, 3.0F, new CubeDeformation(-0.6F))
		.texOffs(2, 15).addBox(-4.45F, -20.2154F, 2.2252F, 5.0F, 2.0F, 3.0F, new CubeDeformation(-0.6F))
		.texOffs(3, 15).addBox(-3.85F, -20.2154F, 4.0252F, 8.0F, 2.0F, 2.0F, new CubeDeformation(-0.6F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.5236F, 0.0F, 0.0F));

		PartDefinition underbox_1 = plane_1.addOrReplaceChild("underbox_1", CubeListBuilder.create().texOffs(50, 13).addBox(-3.25F, -17.25F, -2.75F, 6.0F, 1.0F, 6.0F, new CubeDeformation(0.0F))
		.texOffs(50, 13).addBox(-3.75F, -19.25F, -4.25F, 8.0F, 2.0F, 8.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition rib_o2 = station_1.addOrReplaceChild("rib_o2", CubeListBuilder.create(), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.0F, -0.5236F, 0.0F));

		PartDefinition rib_tilt_1 = rib_o2.addOrReplaceChild("rib_tilt_1", CubeListBuilder.create().texOffs(3, 40).addBox(-0.35F, -14.8F, -4.85F, 1.0F, 2.0F, 2.0F, new CubeDeformation(-0.2F))
		.texOffs(3, 40).addBox(-0.35F, -16.325F, -4.85F, 1.0F, 1.0F, 2.0F, new CubeDeformation(-0.2F))
		.texOffs(3, 40).addBox(-0.35F, -17.825F, -4.85F, 1.0F, 1.0F, 2.0F, new CubeDeformation(-0.2F))
		.texOffs(3, 40).addBox(-0.35F, -20.325F, -4.85F, 1.0F, 2.0F, 2.0F, new CubeDeformation(-0.2F))
		.texOffs(3, 40).addBox(-0.35F, -20.8F, -5.45F, 1.0F, 8.0F, 1.0F, new CubeDeformation(-0.2F))
		.texOffs(3, 40).addBox(-0.35F, -20.1F, -3.7F, 1.0F, 7.0F, 2.0F, new CubeDeformation(0.0F))
		.texOffs(3, 40).addBox(-0.375F, -20.75F, -9.0F, 1.0F, 3.0F, 12.0F, new CubeDeformation(0.1F))
		.texOffs(3, 40).addBox(-0.375F, -20.0F, -9.5F, 1.0F, 3.0F, 12.0F, new CubeDeformation(-0.1F))
		.texOffs(3, 40).addBox(-0.5F, -18.0F, 0.0F, 1.0F, 2.0F, 4.0F, new CubeDeformation(0.0F))
		.texOffs(3, 40).addBox(-0.35F, -5.4003F, -9.5594F, 1.0F, 1.0F, 3.0F, new CubeDeformation(-0.2F))
		.texOffs(3, 40).addBox(-0.35F, -6.0003F, -10.8094F, 1.0F, 1.0F, 4.0F, new CubeDeformation(-0.2F))
		.texOffs(3, 40).addBox(-0.35F, -7.1753F, -13.3094F, 1.0F, 1.0F, 7.0F, new CubeDeformation(-0.2F))
		.texOffs(3, 40).addBox(-0.35F, -6.6003F, -7.6094F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.2F))
		.texOffs(3, 40).addBox(-0.35F, -6.6003F, -8.8094F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.2F))
		.texOffs(3, 40).addBox(-0.35F, -6.6003F, -10.0094F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.2F))
		.texOffs(3, 40).addBox(-0.375F, -6.6003F, -12.2094F, 1.0F, 1.0F, 2.0F, new CubeDeformation(-0.2F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.4363F, 0.0F, 0.0F));

		PartDefinition rib_deco_1 = rib_tilt_1.addOrReplaceChild("rib_deco_1", CubeListBuilder.create().texOffs(50, 13).addBox(-0.85F, -21.25F, -10.0F, 2.0F, 2.0F, 2.0F, new CubeDeformation(0.0F))
		.texOffs(50, 13).addBox(-1.375F, -21.75F, 1.25F, 3.0F, 3.0F, 2.0F, new CubeDeformation(-0.8F))
		.texOffs(50, 13).addBox(-1.375F, -21.75F, -2.75F, 3.0F, 3.0F, 2.0F, new CubeDeformation(-0.8F))
		.texOffs(50, 13).addBox(-1.375F, -21.75F, -3.5F, 3.0F, 3.0F, 2.0F, new CubeDeformation(-0.8F))
		.texOffs(50, 13).addBox(-1.375F, -21.75F, -4.25F, 3.0F, 3.0F, 2.0F, new CubeDeformation(-0.8F))
		.texOffs(3, 40).addBox(-1.375F, -21.75F, 1.95F, 3.0F, 3.0F, 3.0F, new CubeDeformation(-0.8F)), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition clawfoot_1 = station_1.addOrReplaceChild("clawfoot_1", CubeListBuilder.create(), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.0F, -0.5236F, 0.0F));

		PartDefinition leg_1 = clawfoot_1.addOrReplaceChild("leg_1", CubeListBuilder.create().texOffs(3, 40).addBox(-0.35F, -1.0F, -13.0F, 1.0F, 1.0F, 8.0F, new CubeDeformation(0.0F))
		.texOffs(3, 40).addBox(-0.35F, -15.85F, -9.0F, 1.0F, 15.0F, 4.0F, new CubeDeformation(-0.1F))
		.texOffs(3, 40).addBox(-0.85F, -7.0F, -11.25F, 2.0F, 5.0F, 4.0F, new CubeDeformation(-0.75F))
		.texOffs(3, 40).addBox(-0.85F, -11.5F, -11.25F, 2.0F, 6.0F, 4.0F, new CubeDeformation(-0.75F))
		.texOffs(3, 40).addBox(-0.85F, -14.25F, -12.6F, 2.0F, 13.0F, 2.0F, new CubeDeformation(-0.8F))
		.texOffs(3, 40).addBox(-0.35F, -9.35F, -11.7F, 1.0F, 1.0F, 2.0F, new CubeDeformation(-0.3F))
		.texOffs(3, 40).addBox(-0.35F, -8.1F, -11.7F, 1.0F, 1.0F, 2.0F, new CubeDeformation(-0.3F))
		.texOffs(3, 40).addBox(-0.35F, -6.85F, -11.7F, 1.0F, 1.0F, 2.0F, new CubeDeformation(-0.3F))
		.texOffs(3, 40).addBox(-0.35F, -5.6F, -11.7F, 1.0F, 1.0F, 2.0F, new CubeDeformation(-0.3F))
		.texOffs(3, 40).addBox(-0.35F, -3.1F, -11.7F, 1.0F, 1.0F, 2.0F, new CubeDeformation(-0.3F))
		.texOffs(3, 40).addBox(-0.35F, -4.35F, -11.7F, 1.0F, 1.0F, 2.0F, new CubeDeformation(-0.3F))
		.texOffs(3, 40).addBox(-0.35F, -12.65F, -11.7F, 1.0F, 3.0F, 1.0F, new CubeDeformation(-0.3F))
		.texOffs(3, 40).addBox(-0.35F, -11.65F, -11.3F, 1.0F, 2.0F, 1.0F, new CubeDeformation(-0.3F))
		.texOffs(3, 40).addBox(-0.35F, -10.65F, -10.9F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.3F))
		.texOffs(3, 40).addBox(-0.85F, -1.4342F, -15.3467F, 2.0F, 2.0F, 3.0F, new CubeDeformation(-0.65F)), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition ball_1 = clawfoot_1.addOrReplaceChild("ball_1", CubeListBuilder.create().texOffs(50, 13).addBox(-0.35F, -1.8F, -15.8F, 1.0F, 2.0F, 2.0F, new CubeDeformation(-0.1F))
		.texOffs(50, 13).addBox(-0.85F, -1.8F, -15.8F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.2F))
		.texOffs(50, 13).addBox(-1.1F, -1.8F, -15.8F, 1.0F, 2.0F, 2.0F, new CubeDeformation(-0.3F))
		.texOffs(40, 87).addBox(-1.7F, -1.8F, -15.8F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.7F))
		.texOffs(40, 88).addBox(0.05F, -1.8F, -15.8F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.7F))
		.texOffs(50, 13).addBox(0.4F, -1.8F, -15.8F, 1.0F, 2.0F, 2.0F, new CubeDeformation(-0.3F)), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition station_2 = stations.addOrReplaceChild("station_2", CubeListBuilder.create(), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.0F, -2.0944F, 0.0F));

		PartDefinition edge_2 = station_2.addOrReplaceChild("edge_2", CubeListBuilder.create().texOffs(2, 4).addBox(-8.5F, -15.0F, -15.25F, 17.0F, 1.0F, 2.0F, new CubeDeformation(0.0F))
		.texOffs(3, 40).addBox(-2.5F, -19.3F, -5.25F, 5.0F, 7.0F, 3.0F, new CubeDeformation(0.0F))
		.texOffs(3, 40).addBox(-2.4797F, -20.925F, -4.3387F, 5.0F, 2.0F, 2.0F, new CubeDeformation(0.0F))
		.texOffs(3, 40).addBox(-2.4824F, -22.7F, -4.3169F, 5.0F, 1.0F, 2.0F, new CubeDeformation(0.0F))
		.texOffs(50, 13).addBox(-1.9664F, -21.75F, -3.4588F, 4.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(3, 40).addBox(-2.4824F, -33.7F, -4.3169F, 5.0F, 2.0F, 2.0F, new CubeDeformation(0.0F))
		.texOffs(3, 40).addBox(-2.4797F, -30.925F, -4.3387F, 5.0F, 1.0F, 2.0F, new CubeDeformation(0.0F))
		.texOffs(50, 13).addBox(-1.9664F, -31.75F, -3.4588F, 4.0F, 1.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition cooling_blades_2 = edge_2.addOrReplaceChild("cooling_blades_2", CubeListBuilder.create().texOffs(50, 13).addBox(-3.85F, -8.175F, -6.625F, 8.0F, 2.0F, 2.0F, new CubeDeformation(-0.6F))
		.texOffs(49, 13).addBox(-3.8F, -8.25F, -7.0F, 8.0F, 2.0F, 2.0F, new CubeDeformation(-0.8F)), PartPose.offset(0.0F, -0.25F, 0.0F));

		PartDefinition glow_plasma_coil_2 = edge_2.addOrReplaceChild("glow_plasma_coil_2", CubeListBuilder.create().texOffs(107, 12).addBox(-0.975F, -20.95F, -5.6F, 2.0F, 3.0F, 3.0F, new CubeDeformation(-0.7F))
		.texOffs(107, 12).addBox(-0.025F, -20.95F, -5.6F, 2.0F, 3.0F, 3.0F, new CubeDeformation(-0.7F))
		.texOffs(107, 12).addBox(-1.925F, -20.95F, -5.6F, 2.0F, 3.0F, 3.0F, new CubeDeformation(-0.7F))
		.texOffs(3, 40).addBox(-2.8F, -19.6F, -4.575F, 5.0F, 1.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition belly_2 = edge_2.addOrReplaceChild("belly_2", CubeListBuilder.create().texOffs(50, 13).addBox(-2.8F, -0.5F, -6.0F, 6.0F, 1.0F, 2.0F, new CubeDeformation(-0.2F))
		.texOffs(50, 13).addBox(-1.8F, -0.5F, -6.6F, 4.0F, 1.0F, 1.0F, new CubeDeformation(-0.2F))
		.texOffs(50, 13).addBox(-2.8F, -4.25F, -5.1F, 6.0F, 1.0F, 1.0F, new CubeDeformation(-0.2F))
		.texOffs(50, 13).addBox(-2.8F, -3.7F, -6.0F, 6.0F, 1.0F, 2.0F, new CubeDeformation(-0.2F))
		.texOffs(50, 13).addBox(2.2F, -3.1F, -6.0F, 1.0F, 3.0F, 2.0F, new CubeDeformation(-0.2F))
		.texOffs(50, 13).addBox(-2.8F, -3.1F, -6.0F, 1.0F, 3.0F, 2.0F, new CubeDeformation(-0.2F))
		.texOffs(50, 13).addBox(-2.7F, -3.6F, -6.3F, 2.0F, 4.0F, 2.0F, new CubeDeformation(-0.7F))
		.texOffs(50, 13).addBox(-1.45F, -3.6F, -6.3F, 2.0F, 4.0F, 2.0F, new CubeDeformation(-0.7F))
		.texOffs(50, 13).addBox(-0.15F, -3.6F, -6.3F, 2.0F, 4.0F, 2.0F, new CubeDeformation(-0.7F))
		.texOffs(50, 13).addBox(1.1F, -3.6F, -6.3F, 2.0F, 4.0F, 2.0F, new CubeDeformation(-0.7F)), PartPose.offset(0.0F, -0.25F, 0.0F));

		PartDefinition plane_2 = station_2.addOrReplaceChild("plane_2", CubeListBuilder.create().texOffs(3, 15).addBox(-7.9F, -20.2154F, -5.5748F, 16.0F, 3.0F, 3.0F, new CubeDeformation(-0.6F))
		.texOffs(3, 15).addBox(-1.6F, -20.2154F, -3.7748F, 9.0F, 2.0F, 3.0F, new CubeDeformation(-0.6F))
		.texOffs(3, 15).addBox(-7.4F, -20.2154F, -3.7748F, 7.0F, 2.0F, 3.0F, new CubeDeformation(-0.6F))
		.texOffs(3, 15).addBox(-6.6F, -20.2154F, -1.9748F, 13.0F, 2.0F, 2.0F, new CubeDeformation(-0.6F))
		.texOffs(3, 15).addBox(0.075F, -20.2154F, -1.1748F, 6.0F, 2.0F, 3.0F, new CubeDeformation(-0.6F))
		.texOffs(3, 15).addBox(-5.725F, -20.2154F, -1.1748F, 7.0F, 2.0F, 3.0F, new CubeDeformation(-0.6F))
		.texOffs(3, 15).addBox(-5.7F, -20.2154F, 0.6252F, 11.0F, 2.0F, 2.0F, new CubeDeformation(-0.6F))
		.texOffs(3, 15).addBox(-4.8F, -20.2154F, 1.4252F, 10.0F, 2.0F, 2.0F, new CubeDeformation(-0.6F))
		.texOffs(2, 15).addBox(-0.65F, -20.2154F, 2.2252F, 5.0F, 2.0F, 3.0F, new CubeDeformation(-0.6F))
		.texOffs(2, 15).addBox(-4.45F, -20.2154F, 2.2252F, 5.0F, 2.0F, 3.0F, new CubeDeformation(-0.6F))
		.texOffs(3, 15).addBox(-3.85F, -20.2154F, 4.0252F, 8.0F, 2.0F, 2.0F, new CubeDeformation(-0.6F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.5236F, 0.0F, 0.0F));

		PartDefinition underbox_2 = plane_2.addOrReplaceChild("underbox_2", CubeListBuilder.create().texOffs(50, 13).addBox(-3.25F, -17.25F, -2.75F, 6.0F, 1.0F, 6.0F, new CubeDeformation(0.0F))
		.texOffs(50, 13).addBox(-3.75F, -19.25F, -4.25F, 8.0F, 2.0F, 8.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition rib_o3 = station_2.addOrReplaceChild("rib_o3", CubeListBuilder.create(), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.0F, -0.5236F, 0.0F));

		PartDefinition rib_tilt_2 = rib_o3.addOrReplaceChild("rib_tilt_2", CubeListBuilder.create().texOffs(3, 40).addBox(-0.35F, -14.8F, -4.85F, 1.0F, 2.0F, 2.0F, new CubeDeformation(-0.2F))
		.texOffs(3, 40).addBox(-0.35F, -16.325F, -4.85F, 1.0F, 1.0F, 2.0F, new CubeDeformation(-0.2F))
		.texOffs(3, 40).addBox(-0.35F, -17.825F, -4.85F, 1.0F, 1.0F, 2.0F, new CubeDeformation(-0.2F))
		.texOffs(3, 40).addBox(-0.35F, -20.325F, -4.85F, 1.0F, 2.0F, 2.0F, new CubeDeformation(-0.2F))
		.texOffs(3, 40).addBox(-0.35F, -20.8F, -5.45F, 1.0F, 8.0F, 1.0F, new CubeDeformation(-0.2F))
		.texOffs(3, 40).addBox(-0.35F, -20.1F, -3.7F, 1.0F, 7.0F, 2.0F, new CubeDeformation(0.0F))
		.texOffs(3, 40).addBox(-0.375F, -20.75F, -9.0F, 1.0F, 3.0F, 12.0F, new CubeDeformation(0.1F))
		.texOffs(3, 40).addBox(-0.375F, -20.0F, -9.5F, 1.0F, 3.0F, 12.0F, new CubeDeformation(-0.1F))
		.texOffs(3, 40).addBox(-0.5F, -18.0F, 0.0F, 1.0F, 2.0F, 4.0F, new CubeDeformation(0.0F))
		.texOffs(3, 40).addBox(-0.35F, -5.4003F, -9.5594F, 1.0F, 1.0F, 3.0F, new CubeDeformation(-0.2F))
		.texOffs(3, 40).addBox(-0.35F, -6.0003F, -10.8094F, 1.0F, 1.0F, 4.0F, new CubeDeformation(-0.2F))
		.texOffs(3, 40).addBox(-0.35F, -7.1753F, -13.3094F, 1.0F, 1.0F, 7.0F, new CubeDeformation(-0.2F))
		.texOffs(3, 40).addBox(-0.35F, -6.6003F, -7.6094F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.2F))
		.texOffs(3, 40).addBox(-0.35F, -6.6003F, -8.8094F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.2F))
		.texOffs(3, 40).addBox(-0.35F, -6.6003F, -10.0094F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.2F))
		.texOffs(3, 40).addBox(-0.375F, -6.6003F, -12.2094F, 1.0F, 1.0F, 2.0F, new CubeDeformation(-0.2F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.4363F, 0.0F, 0.0F));

		PartDefinition rib_deco_2 = rib_tilt_2.addOrReplaceChild("rib_deco_2", CubeListBuilder.create().texOffs(50, 13).addBox(-0.85F, -21.25F, -10.0F, 2.0F, 2.0F, 2.0F, new CubeDeformation(0.0F))
		.texOffs(50, 13).addBox(-1.375F, -21.75F, 1.25F, 3.0F, 3.0F, 2.0F, new CubeDeformation(-0.8F))
		.texOffs(50, 13).addBox(-1.375F, -21.75F, -2.75F, 3.0F, 3.0F, 2.0F, new CubeDeformation(-0.8F))
		.texOffs(50, 13).addBox(-1.375F, -21.75F, -3.5F, 3.0F, 3.0F, 2.0F, new CubeDeformation(-0.8F))
		.texOffs(50, 13).addBox(-1.375F, -21.75F, -4.25F, 3.0F, 3.0F, 2.0F, new CubeDeformation(-0.8F))
		.texOffs(3, 40).addBox(-1.375F, -21.75F, 1.95F, 3.0F, 3.0F, 3.0F, new CubeDeformation(-0.8F)), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition clawfoot_2 = station_2.addOrReplaceChild("clawfoot_2", CubeListBuilder.create(), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.0F, -0.5236F, 0.0F));

		PartDefinition leg_2 = clawfoot_2.addOrReplaceChild("leg_2", CubeListBuilder.create().texOffs(3, 40).addBox(-0.35F, -1.0F, -13.0F, 1.0F, 1.0F, 8.0F, new CubeDeformation(0.0F))
		.texOffs(3, 40).addBox(-0.35F, -15.85F, -9.0F, 1.0F, 15.0F, 4.0F, new CubeDeformation(-0.1F))
		.texOffs(3, 40).addBox(-0.85F, -7.0F, -11.25F, 2.0F, 5.0F, 4.0F, new CubeDeformation(-0.75F))
		.texOffs(3, 40).addBox(-0.85F, -11.5F, -11.25F, 2.0F, 6.0F, 4.0F, new CubeDeformation(-0.75F))
		.texOffs(3, 40).addBox(-0.85F, -14.25F, -12.6F, 2.0F, 13.0F, 2.0F, new CubeDeformation(-0.8F))
		.texOffs(3, 40).addBox(-0.35F, -9.35F, -11.7F, 1.0F, 1.0F, 2.0F, new CubeDeformation(-0.3F))
		.texOffs(3, 40).addBox(-0.35F, -8.1F, -11.7F, 1.0F, 1.0F, 2.0F, new CubeDeformation(-0.3F))
		.texOffs(3, 40).addBox(-0.35F, -6.85F, -11.7F, 1.0F, 1.0F, 2.0F, new CubeDeformation(-0.3F))
		.texOffs(3, 40).addBox(-0.35F, -5.6F, -11.7F, 1.0F, 1.0F, 2.0F, new CubeDeformation(-0.3F))
		.texOffs(3, 40).addBox(-0.35F, -3.1F, -11.7F, 1.0F, 1.0F, 2.0F, new CubeDeformation(-0.3F))
		.texOffs(3, 40).addBox(-0.35F, -4.35F, -11.7F, 1.0F, 1.0F, 2.0F, new CubeDeformation(-0.3F))
		.texOffs(3, 40).addBox(-0.35F, -12.65F, -11.7F, 1.0F, 3.0F, 1.0F, new CubeDeformation(-0.3F))
		.texOffs(3, 40).addBox(-0.35F, -11.65F, -11.3F, 1.0F, 2.0F, 1.0F, new CubeDeformation(-0.3F))
		.texOffs(3, 40).addBox(-0.35F, -10.65F, -10.9F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.3F))
		.texOffs(3, 40).addBox(-0.85F, -1.4342F, -15.3467F, 2.0F, 2.0F, 3.0F, new CubeDeformation(-0.65F)), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition ball_2 = clawfoot_2.addOrReplaceChild("ball_2", CubeListBuilder.create().texOffs(50, 13).addBox(-0.35F, -1.8F, -15.8F, 1.0F, 2.0F, 2.0F, new CubeDeformation(-0.1F))
		.texOffs(50, 13).addBox(-0.85F, -1.8F, -15.8F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.2F))
		.texOffs(50, 13).addBox(-1.1F, -1.8F, -15.8F, 1.0F, 2.0F, 2.0F, new CubeDeformation(-0.3F))
		.texOffs(40, 87).addBox(-1.7F, -1.8F, -15.8F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.7F))
		.texOffs(40, 88).addBox(0.05F, -1.8F, -15.8F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.7F))
		.texOffs(50, 13).addBox(0.4F, -1.8F, -15.8F, 1.0F, 2.0F, 2.0F, new CubeDeformation(-0.3F)), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition station_3 = stations.addOrReplaceChild("station_3", CubeListBuilder.create(), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.0F, 3.1416F, 0.0F));

		PartDefinition edge_3 = station_3.addOrReplaceChild("edge_3", CubeListBuilder.create().texOffs(2, 4).addBox(-8.5F, -15.0F, -15.25F, 17.0F, 1.0F, 2.0F, new CubeDeformation(0.0F))
		.texOffs(3, 40).addBox(-2.5F, -19.3F, -5.25F, 5.0F, 7.0F, 3.0F, new CubeDeformation(0.0F))
		.texOffs(3, 40).addBox(-2.4797F, -20.875F, -4.3387F, 5.0F, 2.0F, 2.0F, new CubeDeformation(0.0F))
		.texOffs(3, 40).addBox(-2.4824F, -22.65F, -4.3169F, 5.0F, 1.0F, 2.0F, new CubeDeformation(0.0F))
		.texOffs(50, 13).addBox(-1.9664F, -21.75F, -3.4588F, 4.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(3, 40).addBox(-2.4824F, -33.65F, -4.3169F, 5.0F, 2.0F, 2.0F, new CubeDeformation(0.0F))
		.texOffs(3, 40).addBox(-2.4797F, -30.875F, -4.3387F, 5.0F, 1.0F, 2.0F, new CubeDeformation(0.0F))
		.texOffs(50, 13).addBox(-1.9664F, -31.75F, -3.4588F, 4.0F, 1.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition cooling_blades_3 = edge_3.addOrReplaceChild("cooling_blades_3", CubeListBuilder.create().texOffs(50, 13).addBox(-3.85F, -8.175F, -6.625F, 8.0F, 2.0F, 2.0F, new CubeDeformation(-0.6F))
		.texOffs(49, 13).addBox(-3.8F, -8.25F, -7.0F, 8.0F, 2.0F, 2.0F, new CubeDeformation(-0.8F)), PartPose.offset(0.0F, -0.25F, 0.0F));

		PartDefinition glow_plasma_coil_3 = edge_3.addOrReplaceChild("glow_plasma_coil_3", CubeListBuilder.create().texOffs(107, 12).addBox(-0.975F, -20.95F, -5.6F, 2.0F, 3.0F, 3.0F, new CubeDeformation(-0.7F))
		.texOffs(107, 12).addBox(-0.025F, -20.95F, -5.6F, 2.0F, 3.0F, 3.0F, new CubeDeformation(-0.7F))
		.texOffs(107, 12).addBox(-1.925F, -20.95F, -5.6F, 2.0F, 3.0F, 3.0F, new CubeDeformation(-0.7F))
		.texOffs(3, 40).addBox(-2.8F, -19.6F, -4.575F, 5.0F, 1.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition belly_3 = edge_3.addOrReplaceChild("belly_3", CubeListBuilder.create().texOffs(50, 13).addBox(-2.8F, -0.5F, -6.0F, 6.0F, 1.0F, 2.0F, new CubeDeformation(-0.2F))
		.texOffs(50, 13).addBox(-1.8F, -0.5F, -6.6F, 4.0F, 1.0F, 1.0F, new CubeDeformation(-0.2F))
		.texOffs(50, 13).addBox(-2.8F, -4.25F, -5.1F, 6.0F, 1.0F, 1.0F, new CubeDeformation(-0.2F))
		.texOffs(50, 13).addBox(-2.8F, -3.7F, -6.0F, 6.0F, 1.0F, 2.0F, new CubeDeformation(-0.2F))
		.texOffs(50, 13).addBox(2.2F, -3.1F, -6.0F, 1.0F, 3.0F, 2.0F, new CubeDeformation(-0.2F))
		.texOffs(50, 13).addBox(-2.8F, -3.1F, -6.0F, 1.0F, 3.0F, 2.0F, new CubeDeformation(-0.2F))
		.texOffs(50, 13).addBox(-2.7F, -3.6F, -6.3F, 2.0F, 4.0F, 2.0F, new CubeDeformation(-0.7F))
		.texOffs(50, 13).addBox(-1.45F, -3.6F, -6.3F, 2.0F, 4.0F, 2.0F, new CubeDeformation(-0.7F))
		.texOffs(50, 13).addBox(-0.15F, -3.6F, -6.3F, 2.0F, 4.0F, 2.0F, new CubeDeformation(-0.7F))
		.texOffs(50, 13).addBox(1.1F, -3.6F, -6.3F, 2.0F, 4.0F, 2.0F, new CubeDeformation(-0.7F)), PartPose.offset(0.0F, -0.25F, 0.0F));

		PartDefinition plane_3 = station_3.addOrReplaceChild("plane_3", CubeListBuilder.create().texOffs(3, 15).addBox(-7.9F, -20.2154F, -5.5748F, 16.0F, 3.0F, 3.0F, new CubeDeformation(-0.6F))
		.texOffs(3, 15).addBox(-1.6F, -20.2154F, -3.7748F, 9.0F, 2.0F, 3.0F, new CubeDeformation(-0.6F))
		.texOffs(3, 15).addBox(-7.4F, -20.2154F, -3.7748F, 7.0F, 2.0F, 3.0F, new CubeDeformation(-0.6F))
		.texOffs(3, 15).addBox(-6.6F, -20.2154F, -1.9748F, 13.0F, 2.0F, 2.0F, new CubeDeformation(-0.6F))
		.texOffs(3, 15).addBox(0.075F, -20.2154F, -1.1748F, 6.0F, 2.0F, 3.0F, new CubeDeformation(-0.6F))
		.texOffs(3, 15).addBox(-5.725F, -20.2154F, -1.1748F, 7.0F, 2.0F, 3.0F, new CubeDeformation(-0.6F))
		.texOffs(3, 15).addBox(-5.7F, -20.2154F, 0.6252F, 11.0F, 2.0F, 2.0F, new CubeDeformation(-0.6F))
		.texOffs(3, 15).addBox(-4.8F, -20.2154F, 1.4252F, 10.0F, 2.0F, 2.0F, new CubeDeformation(-0.6F))
		.texOffs(2, 15).addBox(-0.65F, -20.2154F, 2.2252F, 5.0F, 2.0F, 3.0F, new CubeDeformation(-0.6F))
		.texOffs(2, 15).addBox(-4.45F, -20.2154F, 2.2252F, 5.0F, 2.0F, 3.0F, new CubeDeformation(-0.6F))
		.texOffs(3, 15).addBox(-3.85F, -20.2154F, 4.0252F, 8.0F, 2.0F, 2.0F, new CubeDeformation(-0.6F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.5236F, 0.0F, 0.0F));

		PartDefinition underbox_3 = plane_3.addOrReplaceChild("underbox_3", CubeListBuilder.create().texOffs(50, 13).addBox(-3.25F, -17.25F, -2.75F, 6.0F, 1.0F, 6.0F, new CubeDeformation(0.0F))
		.texOffs(50, 13).addBox(-3.75F, -19.25F, -4.25F, 8.0F, 2.0F, 8.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition rib_o4 = station_3.addOrReplaceChild("rib_o4", CubeListBuilder.create(), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.0F, -0.5236F, 0.0F));

		PartDefinition rib_tilt_3 = rib_o4.addOrReplaceChild("rib_tilt_3", CubeListBuilder.create().texOffs(3, 40).addBox(-0.35F, -14.8F, -4.85F, 1.0F, 2.0F, 2.0F, new CubeDeformation(-0.2F))
		.texOffs(3, 40).addBox(-0.35F, -16.325F, -4.85F, 1.0F, 1.0F, 2.0F, new CubeDeformation(-0.2F))
		.texOffs(3, 40).addBox(-0.35F, -17.825F, -4.85F, 1.0F, 1.0F, 2.0F, new CubeDeformation(-0.2F))
		.texOffs(3, 40).addBox(-0.35F, -20.325F, -4.85F, 1.0F, 2.0F, 2.0F, new CubeDeformation(-0.2F))
		.texOffs(3, 40).addBox(-0.35F, -20.8F, -5.45F, 1.0F, 8.0F, 1.0F, new CubeDeformation(-0.2F))
		.texOffs(3, 40).addBox(-0.35F, -20.1F, -3.7F, 1.0F, 7.0F, 2.0F, new CubeDeformation(0.0F))
		.texOffs(3, 40).addBox(-0.375F, -20.75F, -9.0F, 1.0F, 3.0F, 12.0F, new CubeDeformation(0.1F))
		.texOffs(3, 40).addBox(-0.375F, -20.0F, -9.5F, 1.0F, 3.0F, 12.0F, new CubeDeformation(-0.1F))
		.texOffs(3, 40).addBox(-0.5F, -18.0F, 0.0F, 1.0F, 2.0F, 4.0F, new CubeDeformation(0.0F))
		.texOffs(3, 40).addBox(-0.35F, -5.4003F, -9.5594F, 1.0F, 1.0F, 3.0F, new CubeDeformation(-0.2F))
		.texOffs(3, 40).addBox(-0.35F, -6.0003F, -10.8094F, 1.0F, 1.0F, 4.0F, new CubeDeformation(-0.2F))
		.texOffs(3, 40).addBox(-0.35F, -7.1753F, -13.3094F, 1.0F, 1.0F, 7.0F, new CubeDeformation(-0.2F))
		.texOffs(3, 40).addBox(-0.35F, -6.6003F, -7.6094F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.2F))
		.texOffs(3, 40).addBox(-0.35F, -6.6003F, -8.8094F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.2F))
		.texOffs(3, 40).addBox(-0.35F, -6.6003F, -10.0094F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.2F))
		.texOffs(3, 40).addBox(-0.375F, -6.6003F, -12.2094F, 1.0F, 1.0F, 2.0F, new CubeDeformation(-0.2F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.4363F, 0.0F, 0.0F));

		PartDefinition rib_deco_3 = rib_tilt_3.addOrReplaceChild("rib_deco_3", CubeListBuilder.create().texOffs(50, 13).addBox(-0.85F, -21.25F, -10.0F, 2.0F, 2.0F, 2.0F, new CubeDeformation(0.0F))
		.texOffs(50, 13).addBox(-1.375F, -21.75F, 1.25F, 3.0F, 3.0F, 2.0F, new CubeDeformation(-0.8F))
		.texOffs(50, 13).addBox(-1.375F, -21.75F, -2.75F, 3.0F, 3.0F, 2.0F, new CubeDeformation(-0.8F))
		.texOffs(50, 13).addBox(-1.375F, -21.75F, -3.5F, 3.0F, 3.0F, 2.0F, new CubeDeformation(-0.8F))
		.texOffs(50, 13).addBox(-1.375F, -21.75F, -4.25F, 3.0F, 3.0F, 2.0F, new CubeDeformation(-0.8F))
		.texOffs(3, 40).addBox(-1.375F, -21.75F, 1.95F, 3.0F, 3.0F, 3.0F, new CubeDeformation(-0.8F)), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition clawfoot_3 = station_3.addOrReplaceChild("clawfoot_3", CubeListBuilder.create(), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.0F, -0.5236F, 0.0F));

		PartDefinition leg_3 = clawfoot_3.addOrReplaceChild("leg_3", CubeListBuilder.create().texOffs(3, 40).addBox(-0.35F, -1.0F, -13.0F, 1.0F, 1.0F, 8.0F, new CubeDeformation(0.0F))
		.texOffs(3, 40).addBox(-0.35F, -15.85F, -9.0F, 1.0F, 15.0F, 4.0F, new CubeDeformation(-0.1F))
		.texOffs(3, 40).addBox(-0.85F, -7.0F, -11.25F, 2.0F, 5.0F, 4.0F, new CubeDeformation(-0.75F))
		.texOffs(3, 40).addBox(-0.85F, -11.5F, -11.25F, 2.0F, 6.0F, 4.0F, new CubeDeformation(-0.75F))
		.texOffs(3, 40).addBox(-0.85F, -14.25F, -12.6F, 2.0F, 13.0F, 2.0F, new CubeDeformation(-0.8F))
		.texOffs(3, 40).addBox(-0.35F, -9.35F, -11.7F, 1.0F, 1.0F, 2.0F, new CubeDeformation(-0.3F))
		.texOffs(3, 40).addBox(-0.35F, -8.1F, -11.7F, 1.0F, 1.0F, 2.0F, new CubeDeformation(-0.3F))
		.texOffs(3, 40).addBox(-0.35F, -6.85F, -11.7F, 1.0F, 1.0F, 2.0F, new CubeDeformation(-0.3F))
		.texOffs(3, 40).addBox(-0.35F, -5.6F, -11.7F, 1.0F, 1.0F, 2.0F, new CubeDeformation(-0.3F))
		.texOffs(3, 40).addBox(-0.35F, -3.1F, -11.7F, 1.0F, 1.0F, 2.0F, new CubeDeformation(-0.3F))
		.texOffs(3, 40).addBox(-0.35F, -4.35F, -11.7F, 1.0F, 1.0F, 2.0F, new CubeDeformation(-0.3F))
		.texOffs(3, 40).addBox(-0.35F, -12.65F, -11.7F, 1.0F, 3.0F, 1.0F, new CubeDeformation(-0.3F))
		.texOffs(3, 40).addBox(-0.35F, -11.65F, -11.3F, 1.0F, 2.0F, 1.0F, new CubeDeformation(-0.3F))
		.texOffs(3, 40).addBox(-0.35F, -10.65F, -10.9F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.3F))
		.texOffs(3, 40).addBox(-0.85F, -1.4342F, -15.3467F, 2.0F, 2.0F, 3.0F, new CubeDeformation(-0.65F)), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition ball_3 = clawfoot_3.addOrReplaceChild("ball_3", CubeListBuilder.create().texOffs(50, 13).addBox(-0.35F, -1.8F, -15.8F, 1.0F, 2.0F, 2.0F, new CubeDeformation(-0.1F))
		.texOffs(50, 13).addBox(-0.85F, -1.8F, -15.8F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.2F))
		.texOffs(50, 13).addBox(-1.1F, -1.8F, -15.8F, 1.0F, 2.0F, 2.0F, new CubeDeformation(-0.3F))
		.texOffs(40, 87).addBox(-1.7F, -1.8F, -15.8F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.7F))
		.texOffs(40, 88).addBox(0.05F, -1.8F, -15.8F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.7F))
		.texOffs(50, 13).addBox(0.4F, -1.8F, -15.8F, 1.0F, 2.0F, 2.0F, new CubeDeformation(-0.3F)), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition station_4 = stations.addOrReplaceChild("station_4", CubeListBuilder.create(), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.0F, 2.0944F, 0.0F));

		PartDefinition edge_4 = station_4.addOrReplaceChild("edge_4", CubeListBuilder.create().texOffs(2, 4).addBox(-8.5F, -15.0F, -15.25F, 17.0F, 1.0F, 2.0F, new CubeDeformation(0.0F))
		.texOffs(3, 40).addBox(-2.5F, -19.3F, -5.25F, 5.0F, 7.0F, 3.0F, new CubeDeformation(0.0F))
		.texOffs(3, 40).addBox(-2.4797F, -20.925F, -4.3387F, 5.0F, 2.0F, 2.0F, new CubeDeformation(0.0F))
		.texOffs(3, 40).addBox(-2.4824F, -22.7F, -4.3169F, 5.0F, 1.0F, 2.0F, new CubeDeformation(0.0F))
		.texOffs(50, 13).addBox(-1.9664F, -21.75F, -3.4588F, 4.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(3, 40).addBox(-2.4824F, -33.7F, -4.3169F, 5.0F, 2.0F, 2.0F, new CubeDeformation(0.0F))
		.texOffs(3, 40).addBox(-2.4797F, -30.925F, -4.3387F, 5.0F, 1.0F, 2.0F, new CubeDeformation(0.0F))
		.texOffs(50, 13).addBox(-1.9664F, -31.75F, -3.4588F, 4.0F, 1.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition cooling_blades_4 = edge_4.addOrReplaceChild("cooling_blades_4", CubeListBuilder.create().texOffs(50, 13).addBox(-3.85F, -8.175F, -6.625F, 8.0F, 2.0F, 2.0F, new CubeDeformation(-0.6F))
		.texOffs(49, 13).addBox(-3.8F, -8.25F, -7.0F, 8.0F, 2.0F, 2.0F, new CubeDeformation(-0.8F)), PartPose.offset(0.0F, -0.25F, 0.0F));

		PartDefinition glow_plasma_coil_4 = edge_4.addOrReplaceChild("glow_plasma_coil_4", CubeListBuilder.create().texOffs(107, 12).addBox(-0.975F, -20.95F, -5.6F, 2.0F, 3.0F, 3.0F, new CubeDeformation(-0.7F))
		.texOffs(107, 12).addBox(-0.025F, -20.95F, -5.6F, 2.0F, 3.0F, 3.0F, new CubeDeformation(-0.7F))
		.texOffs(107, 12).addBox(-1.925F, -20.95F, -5.6F, 2.0F, 3.0F, 3.0F, new CubeDeformation(-0.7F))
		.texOffs(3, 40).addBox(-2.8F, -19.6F, -4.575F, 5.0F, 1.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition belly_4 = edge_4.addOrReplaceChild("belly_4", CubeListBuilder.create().texOffs(50, 13).addBox(-2.8F, -0.5F, -6.0F, 6.0F, 1.0F, 2.0F, new CubeDeformation(-0.2F))
		.texOffs(50, 13).addBox(-1.8F, -0.5F, -6.6F, 4.0F, 1.0F, 1.0F, new CubeDeformation(-0.2F))
		.texOffs(50, 13).addBox(-2.8F, -4.25F, -5.1F, 6.0F, 1.0F, 1.0F, new CubeDeformation(-0.2F))
		.texOffs(50, 13).addBox(-2.8F, -3.7F, -6.0F, 6.0F, 1.0F, 2.0F, new CubeDeformation(-0.2F))
		.texOffs(50, 13).addBox(2.2F, -3.1F, -6.0F, 1.0F, 3.0F, 2.0F, new CubeDeformation(-0.2F))
		.texOffs(50, 13).addBox(-2.8F, -3.1F, -6.0F, 1.0F, 3.0F, 2.0F, new CubeDeformation(-0.2F))
		.texOffs(50, 13).addBox(-2.7F, -3.6F, -6.3F, 2.0F, 4.0F, 2.0F, new CubeDeformation(-0.7F))
		.texOffs(50, 13).addBox(-1.45F, -3.6F, -6.3F, 2.0F, 4.0F, 2.0F, new CubeDeformation(-0.7F))
		.texOffs(50, 13).addBox(-0.15F, -3.6F, -6.3F, 2.0F, 4.0F, 2.0F, new CubeDeformation(-0.7F))
		.texOffs(50, 13).addBox(1.1F, -3.6F, -6.3F, 2.0F, 4.0F, 2.0F, new CubeDeformation(-0.7F)), PartPose.offset(0.0F, -0.25F, 0.0F));

		PartDefinition plane_4 = station_4.addOrReplaceChild("plane_4", CubeListBuilder.create().texOffs(3, 15).addBox(-7.9F, -20.2154F, -5.5748F, 16.0F, 3.0F, 3.0F, new CubeDeformation(-0.6F))
		.texOffs(3, 15).addBox(-1.6F, -20.2154F, -3.7748F, 9.0F, 2.0F, 3.0F, new CubeDeformation(-0.6F))
		.texOffs(3, 15).addBox(-7.4F, -20.2154F, -3.7748F, 7.0F, 2.0F, 3.0F, new CubeDeformation(-0.6F))
		.texOffs(3, 15).addBox(-6.6F, -20.2154F, -1.9748F, 13.0F, 2.0F, 2.0F, new CubeDeformation(-0.6F))
		.texOffs(3, 15).addBox(0.075F, -20.2154F, -1.1748F, 6.0F, 2.0F, 3.0F, new CubeDeformation(-0.6F))
		.texOffs(3, 15).addBox(-5.725F, -20.2154F, -1.1748F, 7.0F, 2.0F, 3.0F, new CubeDeformation(-0.6F))
		.texOffs(3, 15).addBox(-5.7F, -20.2154F, 0.6252F, 11.0F, 2.0F, 2.0F, new CubeDeformation(-0.6F))
		.texOffs(3, 15).addBox(-4.8F, -20.2154F, 1.4252F, 10.0F, 2.0F, 2.0F, new CubeDeformation(-0.6F))
		.texOffs(2, 15).addBox(-0.65F, -20.2154F, 2.2252F, 5.0F, 2.0F, 3.0F, new CubeDeformation(-0.6F))
		.texOffs(2, 15).addBox(-4.45F, -20.2154F, 2.2252F, 5.0F, 2.0F, 3.0F, new CubeDeformation(-0.6F))
		.texOffs(3, 15).addBox(-3.85F, -20.2154F, 4.0252F, 8.0F, 2.0F, 2.0F, new CubeDeformation(-0.6F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.5236F, 0.0F, 0.0F));

		PartDefinition underbox_4 = plane_4.addOrReplaceChild("underbox_4", CubeListBuilder.create().texOffs(50, 13).addBox(-3.25F, -17.25F, -2.75F, 6.0F, 1.0F, 6.0F, new CubeDeformation(0.0F))
		.texOffs(50, 13).addBox(-3.75F, -19.25F, -4.25F, 8.0F, 2.0F, 8.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition rib_o5 = station_4.addOrReplaceChild("rib_o5", CubeListBuilder.create(), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.0F, -0.5236F, 0.0F));

		PartDefinition rib_tilt_4 = rib_o5.addOrReplaceChild("rib_tilt_4", CubeListBuilder.create().texOffs(3, 40).addBox(-0.35F, -14.8F, -4.85F, 1.0F, 2.0F, 2.0F, new CubeDeformation(-0.2F))
		.texOffs(3, 40).addBox(-0.35F, -16.325F, -4.85F, 1.0F, 1.0F, 2.0F, new CubeDeformation(-0.2F))
		.texOffs(3, 40).addBox(-0.35F, -17.825F, -4.85F, 1.0F, 1.0F, 2.0F, new CubeDeformation(-0.2F))
		.texOffs(3, 40).addBox(-0.35F, -20.325F, -4.85F, 1.0F, 2.0F, 2.0F, new CubeDeformation(-0.2F))
		.texOffs(3, 40).addBox(-0.35F, -20.8F, -5.45F, 1.0F, 8.0F, 1.0F, new CubeDeformation(-0.2F))
		.texOffs(3, 40).addBox(-0.35F, -20.1F, -3.7F, 1.0F, 7.0F, 2.0F, new CubeDeformation(0.0F))
		.texOffs(3, 40).addBox(-0.375F, -20.75F, -9.0F, 1.0F, 3.0F, 12.0F, new CubeDeformation(0.1F))
		.texOffs(3, 40).addBox(-0.375F, -20.0F, -9.5F, 1.0F, 3.0F, 12.0F, new CubeDeformation(-0.1F))
		.texOffs(3, 40).addBox(-0.5F, -18.0F, 0.0F, 1.0F, 2.0F, 4.0F, new CubeDeformation(0.0F))
		.texOffs(3, 40).addBox(-0.35F, -5.4003F, -9.5594F, 1.0F, 1.0F, 3.0F, new CubeDeformation(-0.2F))
		.texOffs(3, 40).addBox(-0.35F, -6.0003F, -10.8094F, 1.0F, 1.0F, 4.0F, new CubeDeformation(-0.2F))
		.texOffs(3, 40).addBox(-0.35F, -7.1753F, -13.3094F, 1.0F, 1.0F, 7.0F, new CubeDeformation(-0.2F))
		.texOffs(3, 40).addBox(-0.35F, -6.6003F, -7.6094F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.2F))
		.texOffs(3, 40).addBox(-0.35F, -6.6003F, -8.8094F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.2F))
		.texOffs(3, 40).addBox(-0.35F, -6.6003F, -10.0094F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.2F))
		.texOffs(3, 40).addBox(-0.375F, -6.6003F, -12.2094F, 1.0F, 1.0F, 2.0F, new CubeDeformation(-0.2F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.4363F, 0.0F, 0.0F));

		PartDefinition rib_deco_4 = rib_tilt_4.addOrReplaceChild("rib_deco_4", CubeListBuilder.create().texOffs(50, 13).addBox(-0.85F, -21.25F, -10.0F, 2.0F, 2.0F, 2.0F, new CubeDeformation(0.0F))
		.texOffs(50, 13).addBox(-1.375F, -21.75F, 1.25F, 3.0F, 3.0F, 2.0F, new CubeDeformation(-0.8F))
		.texOffs(50, 13).addBox(-1.375F, -21.75F, -2.75F, 3.0F, 3.0F, 2.0F, new CubeDeformation(-0.8F))
		.texOffs(50, 13).addBox(-1.375F, -21.75F, -3.5F, 3.0F, 3.0F, 2.0F, new CubeDeformation(-0.8F))
		.texOffs(50, 13).addBox(-1.375F, -21.75F, -4.25F, 3.0F, 3.0F, 2.0F, new CubeDeformation(-0.8F))
		.texOffs(3, 40).addBox(-1.375F, -21.75F, 1.95F, 3.0F, 3.0F, 3.0F, new CubeDeformation(-0.8F)), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition clawfoot_4 = station_4.addOrReplaceChild("clawfoot_4", CubeListBuilder.create(), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.0F, -0.5236F, 0.0F));

		PartDefinition leg_4 = clawfoot_4.addOrReplaceChild("leg_4", CubeListBuilder.create().texOffs(3, 40).addBox(-0.35F, -1.0F, -13.0F, 1.0F, 1.0F, 8.0F, new CubeDeformation(0.0F))
		.texOffs(3, 40).addBox(-0.35F, -15.85F, -9.0F, 1.0F, 15.0F, 4.0F, new CubeDeformation(-0.1F))
		.texOffs(3, 40).addBox(-0.85F, -7.0F, -11.25F, 2.0F, 5.0F, 4.0F, new CubeDeformation(-0.75F))
		.texOffs(3, 40).addBox(-0.85F, -11.5F, -11.25F, 2.0F, 6.0F, 4.0F, new CubeDeformation(-0.75F))
		.texOffs(3, 40).addBox(-0.85F, -14.25F, -12.6F, 2.0F, 13.0F, 2.0F, new CubeDeformation(-0.8F))
		.texOffs(3, 40).addBox(-0.35F, -9.35F, -11.7F, 1.0F, 1.0F, 2.0F, new CubeDeformation(-0.3F))
		.texOffs(3, 40).addBox(-0.35F, -8.1F, -11.7F, 1.0F, 1.0F, 2.0F, new CubeDeformation(-0.3F))
		.texOffs(3, 40).addBox(-0.35F, -6.85F, -11.7F, 1.0F, 1.0F, 2.0F, new CubeDeformation(-0.3F))
		.texOffs(3, 40).addBox(-0.35F, -5.6F, -11.7F, 1.0F, 1.0F, 2.0F, new CubeDeformation(-0.3F))
		.texOffs(3, 40).addBox(-0.35F, -3.1F, -11.7F, 1.0F, 1.0F, 2.0F, new CubeDeformation(-0.3F))
		.texOffs(3, 40).addBox(-0.35F, -4.35F, -11.7F, 1.0F, 1.0F, 2.0F, new CubeDeformation(-0.3F))
		.texOffs(3, 40).addBox(-0.35F, -12.65F, -11.7F, 1.0F, 3.0F, 1.0F, new CubeDeformation(-0.3F))
		.texOffs(3, 40).addBox(-0.35F, -11.65F, -11.3F, 1.0F, 2.0F, 1.0F, new CubeDeformation(-0.3F))
		.texOffs(3, 40).addBox(-0.35F, -10.65F, -10.9F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.3F))
		.texOffs(3, 40).addBox(-0.85F, -1.4342F, -15.3467F, 2.0F, 2.0F, 3.0F, new CubeDeformation(-0.65F)), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition ball_4 = clawfoot_4.addOrReplaceChild("ball_4", CubeListBuilder.create().texOffs(50, 13).addBox(-0.35F, -1.8F, -15.8F, 1.0F, 2.0F, 2.0F, new CubeDeformation(-0.1F))
		.texOffs(50, 13).addBox(-0.85F, -1.8F, -15.8F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.2F))
		.texOffs(50, 13).addBox(-1.1F, -1.8F, -15.8F, 1.0F, 2.0F, 2.0F, new CubeDeformation(-0.3F))
		.texOffs(40, 87).addBox(-1.7F, -1.8F, -15.8F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.7F))
		.texOffs(40, 88).addBox(0.05F, -1.8F, -15.8F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.7F))
		.texOffs(50, 13).addBox(0.4F, -1.8F, -15.8F, 1.0F, 2.0F, 2.0F, new CubeDeformation(-0.3F)), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition station_5 = stations.addOrReplaceChild("station_5", CubeListBuilder.create(), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.0F, 1.0472F, 0.0F));

		PartDefinition edge_5 = station_5.addOrReplaceChild("edge_5", CubeListBuilder.create().texOffs(2, 4).addBox(-8.5F, -15.0F, -15.25F, 17.0F, 1.0F, 2.0F, new CubeDeformation(0.0F))
		.texOffs(3, 40).addBox(-2.5F, -19.3F, -5.25F, 5.0F, 7.0F, 3.0F, new CubeDeformation(0.0F))
		.texOffs(3, 40).addBox(-2.4797F, -20.9F, -4.3387F, 5.0F, 2.0F, 2.0F, new CubeDeformation(0.0F))
		.texOffs(3, 40).addBox(-2.4824F, -22.675F, -4.3169F, 5.0F, 1.0F, 2.0F, new CubeDeformation(0.0F))
		.texOffs(50, 13).addBox(-1.9664F, -21.75F, -3.4588F, 4.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(3, 40).addBox(-2.4824F, -33.675F, -4.3169F, 5.0F, 2.0F, 2.0F, new CubeDeformation(0.0F))
		.texOffs(3, 40).addBox(-2.4797F, -30.9F, -4.3387F, 5.0F, 1.0F, 2.0F, new CubeDeformation(0.0F))
		.texOffs(50, 13).addBox(-1.9664F, -31.75F, -3.4588F, 4.0F, 1.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition cooling_blades_5 = edge_5.addOrReplaceChild("cooling_blades_5", CubeListBuilder.create().texOffs(50, 13).addBox(-3.85F, -8.175F, -6.625F, 8.0F, 2.0F, 2.0F, new CubeDeformation(-0.6F))
		.texOffs(49, 13).addBox(-3.8F, -8.25F, -7.0F, 8.0F, 2.0F, 2.0F, new CubeDeformation(-0.8F)), PartPose.offset(0.0F, -0.25F, 0.0F));

		PartDefinition glow_plasma_coil_5 = edge_5.addOrReplaceChild("glow_plasma_coil_5", CubeListBuilder.create().texOffs(107, 12).addBox(-0.975F, -20.95F, -5.6F, 2.0F, 3.0F, 3.0F, new CubeDeformation(-0.7F))
		.texOffs(107, 12).addBox(-0.025F, -20.95F, -5.6F, 2.0F, 3.0F, 3.0F, new CubeDeformation(-0.7F))
		.texOffs(107, 12).addBox(-1.925F, -20.95F, -5.6F, 2.0F, 3.0F, 3.0F, new CubeDeformation(-0.7F))
		.texOffs(3, 40).addBox(-2.8F, -19.6F, -4.575F, 5.0F, 1.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition belly_5 = edge_5.addOrReplaceChild("belly_5", CubeListBuilder.create().texOffs(50, 13).addBox(-2.8F, -0.5F, -6.0F, 6.0F, 1.0F, 2.0F, new CubeDeformation(-0.2F))
		.texOffs(50, 13).addBox(-1.8F, -0.5F, -6.6F, 4.0F, 1.0F, 1.0F, new CubeDeformation(-0.2F))
		.texOffs(50, 13).addBox(-2.8F, -4.25F, -5.1F, 6.0F, 1.0F, 1.0F, new CubeDeformation(-0.2F))
		.texOffs(50, 13).addBox(-2.8F, -3.7F, -6.0F, 6.0F, 1.0F, 2.0F, new CubeDeformation(-0.2F))
		.texOffs(50, 13).addBox(2.2F, -3.1F, -6.0F, 1.0F, 3.0F, 2.0F, new CubeDeformation(-0.2F))
		.texOffs(50, 13).addBox(-2.8F, -3.1F, -6.0F, 1.0F, 3.0F, 2.0F, new CubeDeformation(-0.2F))
		.texOffs(50, 13).addBox(-2.7F, -3.6F, -6.3F, 2.0F, 4.0F, 2.0F, new CubeDeformation(-0.7F))
		.texOffs(50, 13).addBox(-1.45F, -3.6F, -6.3F, 2.0F, 4.0F, 2.0F, new CubeDeformation(-0.7F))
		.texOffs(50, 13).addBox(-0.15F, -3.6F, -6.3F, 2.0F, 4.0F, 2.0F, new CubeDeformation(-0.7F))
		.texOffs(50, 13).addBox(1.1F, -3.6F, -6.3F, 2.0F, 4.0F, 2.0F, new CubeDeformation(-0.7F)), PartPose.offset(0.0F, -0.25F, 0.0F));

		PartDefinition plane_5 = station_5.addOrReplaceChild("plane_5", CubeListBuilder.create().texOffs(3, 15).addBox(-7.9F, -20.2154F, -5.5748F, 16.0F, 3.0F, 3.0F, new CubeDeformation(-0.6F))
		.texOffs(3, 15).addBox(-1.6F, -20.2154F, -3.7748F, 9.0F, 2.0F, 3.0F, new CubeDeformation(-0.6F))
		.texOffs(3, 15).addBox(-7.4F, -20.2154F, -3.7748F, 7.0F, 2.0F, 3.0F, new CubeDeformation(-0.6F))
		.texOffs(3, 15).addBox(-6.6F, -20.2154F, -1.9748F, 13.0F, 2.0F, 2.0F, new CubeDeformation(-0.6F))
		.texOffs(3, 15).addBox(0.075F, -20.2154F, -1.1748F, 6.0F, 2.0F, 3.0F, new CubeDeformation(-0.6F))
		.texOffs(3, 15).addBox(-5.725F, -20.2154F, -1.1748F, 7.0F, 2.0F, 3.0F, new CubeDeformation(-0.6F))
		.texOffs(3, 15).addBox(-5.7F, -20.2154F, 0.6252F, 11.0F, 2.0F, 2.0F, new CubeDeformation(-0.6F))
		.texOffs(3, 15).addBox(-4.8F, -20.2154F, 1.4252F, 10.0F, 2.0F, 2.0F, new CubeDeformation(-0.6F))
		.texOffs(2, 15).addBox(-0.65F, -20.2154F, 2.2252F, 5.0F, 2.0F, 3.0F, new CubeDeformation(-0.6F))
		.texOffs(2, 15).addBox(-4.45F, -20.2154F, 2.2252F, 5.0F, 2.0F, 3.0F, new CubeDeformation(-0.6F))
		.texOffs(3, 15).addBox(-3.85F, -20.2154F, 4.0252F, 8.0F, 2.0F, 2.0F, new CubeDeformation(-0.6F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.5236F, 0.0F, 0.0F));

		PartDefinition underbox_5 = plane_5.addOrReplaceChild("underbox_5", CubeListBuilder.create().texOffs(50, 13).addBox(-3.25F, -17.25F, -2.75F, 6.0F, 1.0F, 6.0F, new CubeDeformation(0.0F))
		.texOffs(50, 13).addBox(-3.75F, -19.25F, -4.25F, 8.0F, 2.0F, 8.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition rib_o6 = station_5.addOrReplaceChild("rib_o6", CubeListBuilder.create(), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.0F, -0.5236F, 0.0F));

		PartDefinition rib_tilt_5 = rib_o6.addOrReplaceChild("rib_tilt_5", CubeListBuilder.create().texOffs(3, 40).addBox(-0.35F, -14.8F, -4.85F, 1.0F, 2.0F, 2.0F, new CubeDeformation(-0.2F))
		.texOffs(3, 40).addBox(-0.35F, -16.325F, -4.85F, 1.0F, 1.0F, 2.0F, new CubeDeformation(-0.2F))
		.texOffs(3, 40).addBox(-0.35F, -17.825F, -4.85F, 1.0F, 1.0F, 2.0F, new CubeDeformation(-0.2F))
		.texOffs(3, 40).addBox(-0.35F, -20.325F, -4.85F, 1.0F, 2.0F, 2.0F, new CubeDeformation(-0.2F))
		.texOffs(3, 40).addBox(-0.35F, -20.8F, -5.45F, 1.0F, 8.0F, 1.0F, new CubeDeformation(-0.2F))
		.texOffs(3, 40).addBox(-0.35F, -20.1F, -3.7F, 1.0F, 7.0F, 2.0F, new CubeDeformation(0.0F))
		.texOffs(3, 40).addBox(-0.375F, -20.75F, -9.0F, 1.0F, 3.0F, 12.0F, new CubeDeformation(0.1F))
		.texOffs(3, 40).addBox(-0.375F, -20.0F, -9.5F, 1.0F, 3.0F, 12.0F, new CubeDeformation(-0.1F))
		.texOffs(3, 40).addBox(-0.5F, -18.0F, 0.0F, 1.0F, 2.0F, 4.0F, new CubeDeformation(0.0F))
		.texOffs(3, 40).addBox(-0.35F, -5.4003F, -9.5594F, 1.0F, 1.0F, 3.0F, new CubeDeformation(-0.2F))
		.texOffs(3, 40).addBox(-0.35F, -6.0003F, -10.8094F, 1.0F, 1.0F, 4.0F, new CubeDeformation(-0.2F))
		.texOffs(3, 40).addBox(-0.35F, -7.1753F, -13.3094F, 1.0F, 1.0F, 7.0F, new CubeDeformation(-0.2F))
		.texOffs(3, 40).addBox(-0.35F, -6.6003F, -7.6094F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.2F))
		.texOffs(3, 40).addBox(-0.35F, -6.6003F, -8.8094F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.2F))
		.texOffs(3, 40).addBox(-0.35F, -6.6003F, -10.0094F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.2F))
		.texOffs(3, 40).addBox(-0.375F, -6.6003F, -12.2094F, 1.0F, 1.0F, 2.0F, new CubeDeformation(-0.2F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.4363F, 0.0F, 0.0F));

		PartDefinition rib_deco_5 = rib_tilt_5.addOrReplaceChild("rib_deco_5", CubeListBuilder.create().texOffs(50, 13).addBox(-0.85F, -21.25F, -10.0F, 2.0F, 2.0F, 2.0F, new CubeDeformation(0.0F))
		.texOffs(50, 13).addBox(-1.375F, -21.75F, 1.25F, 3.0F, 3.0F, 2.0F, new CubeDeformation(-0.8F))
		.texOffs(50, 13).addBox(-1.375F, -21.75F, -2.75F, 3.0F, 3.0F, 2.0F, new CubeDeformation(-0.8F))
		.texOffs(50, 13).addBox(-1.375F, -21.75F, -3.5F, 3.0F, 3.0F, 2.0F, new CubeDeformation(-0.8F))
		.texOffs(50, 13).addBox(-1.375F, -21.75F, -4.25F, 3.0F, 3.0F, 2.0F, new CubeDeformation(-0.8F))
		.texOffs(3, 40).addBox(-1.375F, -21.75F, 1.95F, 3.0F, 3.0F, 3.0F, new CubeDeformation(-0.8F)), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition clawfoot_5 = station_5.addOrReplaceChild("clawfoot_5", CubeListBuilder.create(), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.0F, -0.5236F, 0.0F));

		PartDefinition leg_5 = clawfoot_5.addOrReplaceChild("leg_5", CubeListBuilder.create().texOffs(3, 40).addBox(-0.35F, -1.0F, -13.0F, 1.0F, 1.0F, 8.0F, new CubeDeformation(0.0F))
		.texOffs(3, 40).addBox(-0.35F, -15.85F, -9.0F, 1.0F, 15.0F, 4.0F, new CubeDeformation(-0.1F))
		.texOffs(3, 40).addBox(-0.85F, -7.0F, -11.25F, 2.0F, 5.0F, 4.0F, new CubeDeformation(-0.75F))
		.texOffs(3, 40).addBox(-0.85F, -11.5F, -11.25F, 2.0F, 6.0F, 4.0F, new CubeDeformation(-0.75F))
		.texOffs(3, 40).addBox(-0.85F, -14.25F, -12.6F, 2.0F, 13.0F, 2.0F, new CubeDeformation(-0.8F))
		.texOffs(3, 40).addBox(-0.35F, -9.35F, -11.7F, 1.0F, 1.0F, 2.0F, new CubeDeformation(-0.3F))
		.texOffs(3, 40).addBox(-0.35F, -8.1F, -11.7F, 1.0F, 1.0F, 2.0F, new CubeDeformation(-0.3F))
		.texOffs(3, 40).addBox(-0.35F, -6.85F, -11.7F, 1.0F, 1.0F, 2.0F, new CubeDeformation(-0.3F))
		.texOffs(3, 40).addBox(-0.35F, -5.6F, -11.7F, 1.0F, 1.0F, 2.0F, new CubeDeformation(-0.3F))
		.texOffs(3, 40).addBox(-0.35F, -3.1F, -11.7F, 1.0F, 1.0F, 2.0F, new CubeDeformation(-0.3F))
		.texOffs(3, 40).addBox(-0.35F, -4.35F, -11.7F, 1.0F, 1.0F, 2.0F, new CubeDeformation(-0.3F))
		.texOffs(3, 40).addBox(-0.35F, -12.65F, -11.7F, 1.0F, 3.0F, 1.0F, new CubeDeformation(-0.3F))
		.texOffs(3, 40).addBox(-0.35F, -11.65F, -11.3F, 1.0F, 2.0F, 1.0F, new CubeDeformation(-0.3F))
		.texOffs(3, 40).addBox(-0.35F, -10.65F, -10.9F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.3F))
		.texOffs(3, 40).addBox(-0.85F, -1.4342F, -15.3467F, 2.0F, 2.0F, 3.0F, new CubeDeformation(-0.65F)), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition ball_5 = clawfoot_5.addOrReplaceChild("ball_5", CubeListBuilder.create().texOffs(50, 13).addBox(-0.35F, -1.8F, -15.8F, 1.0F, 2.0F, 2.0F, new CubeDeformation(-0.1F))
		.texOffs(50, 13).addBox(-0.85F, -1.8F, -15.8F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.2F))
		.texOffs(50, 13).addBox(-1.1F, -1.8F, -15.8F, 1.0F, 2.0F, 2.0F, new CubeDeformation(-0.3F))
		.texOffs(40, 87).addBox(-1.7F, -1.8F, -15.8F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.7F))
		.texOffs(40, 88).addBox(0.05F, -1.8F, -15.8F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.7F))
		.texOffs(50, 13).addBox(0.4F, -1.8F, -15.8F, 1.0F, 2.0F, 2.0F, new CubeDeformation(-0.3F)), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition time_scepter = partdefinition.addOrReplaceChild("time_scepter", CubeListBuilder.create(), PartPose.offset(-0.0095F, -2.3844F, 0.024F));

		PartDefinition upper_coil = time_scepter.addOrReplaceChild("upper_coil", CubeListBuilder.create(), PartPose.offset(-0.7875F, 48.1344F, -0.0937F));

		PartDefinition band_0 = upper_coil.addOrReplaceChild("band_0", CubeListBuilder.create(), PartPose.offset(0.0F, -0.175F, 0.0F));

		PartDefinition narrow_ring_0 = band_0.addOrReplaceChild("narrow_ring_0", CubeListBuilder.create().texOffs(82, 90).addBox(-2.0017F, -29.225F, -3.3033F, 4.0F, 1.0F, 1.0F, new CubeDeformation(-0.3F))
		.texOffs(82, 90).addBox(-2.0017F, -29.225F, 2.1856F, 4.0F, 1.0F, 1.0F, new CubeDeformation(-0.3F)), PartPose.offset(0.797F, -22.125F, 0.0697F));

		PartDefinition cube_r1 = narrow_ring_0.addOrReplaceChild("cube_r1", CubeListBuilder.create().texOffs(82, 90).addBox(-1.0518F, -19.975F, 1.4237F, 4.0F, 1.0F, 1.0F, new CubeDeformation(-0.3F)), PartPose.offsetAndRotation(-1.1866F, -9.25F, -0.4586F, 0.0F, -1.0472F, 0.0F));

		PartDefinition cube_r2 = narrow_ring_0.addOrReplaceChild("cube_r2", CubeListBuilder.create().texOffs(82, 90).addBox(-1.0518F, -19.975F, -4.0763F, 4.0F, 1.0F, 1.0F, new CubeDeformation(-0.3F)), PartPose.offsetAndRotation(-1.1866F, -9.25F, -0.4476F, 0.0F, -1.0472F, 0.0F));

		PartDefinition cube_r3 = narrow_ring_0.addOrReplaceChild("cube_r3", CubeListBuilder.create().texOffs(82, 90).addBox(-1.0518F, -19.975F, -4.0763F, 4.0F, 1.0F, 1.0F, new CubeDeformation(-0.3F)), PartPose.offsetAndRotation(-0.2384F, -9.25F, -1.2794F, 0.0F, -2.0944F, 0.0F));

		PartDefinition cube_r4 = narrow_ring_0.addOrReplaceChild("cube_r4", CubeListBuilder.create().texOffs(82, 90).addBox(-1.0518F, -19.975F, 1.4237F, 4.0F, 1.0F, 1.0F, new CubeDeformation(-0.3F)), PartPose.offsetAndRotation(-0.2384F, -9.25F, -1.2904F, 0.0F, -2.0944F, 0.0F));

		PartDefinition wide_ring_0 = band_0.addOrReplaceChild("wide_ring_0", CubeListBuilder.create().texOffs(82, 93).addBox(-2.0651F, -30.225F, -1.4615F, 4.0F, 2.0F, 1.0F, new CubeDeformation(-0.4F))
		.texOffs(82, 93).addBox(-2.0651F, -30.225F, 3.8811F, 4.0F, 2.0F, 1.0F, new CubeDeformation(-0.4F)), PartPose.offset(0.897F, -21.65F, -1.7053F));

		PartDefinition cube_r5 = wide_ring_0.addOrReplaceChild("cube_r5", CubeListBuilder.create().texOffs(82, 93).addBox(-1.0518F, -20.975F, -5.0763F, 4.0F, 2.0F, 1.0F, new CubeDeformation(-0.4F)), PartPose.offsetAndRotation(-1.2408F, -9.25F, -0.0639F, 0.0F, -2.0944F, 0.0F));

		PartDefinition cube_r6 = wide_ring_0.addOrReplaceChild("cube_r6", CubeListBuilder.create().texOffs(82, 93).addBox(-1.0518F, -20.975F, 1.4237F, 4.0F, 2.0F, 1.0F, new CubeDeformation(-0.4F)), PartPose.offsetAndRotation(-0.2384F, -9.25F, 0.5148F, 0.0F, -2.0944F, 0.0F));

		PartDefinition cube_r7 = wide_ring_0.addOrReplaceChild("cube_r7", CubeListBuilder.create().texOffs(82, 93).addBox(-1.0518F, -20.975F, 1.4237F, 4.0F, 2.0F, 1.0F, new CubeDeformation(-0.4F)), PartPose.offsetAndRotation(-1.1866F, -9.25F, 1.2624F, 0.0F, -1.0472F, 0.0F));

		PartDefinition cube_r8 = wide_ring_0.addOrReplaceChild("cube_r8", CubeListBuilder.create().texOffs(82, 93).addBox(-1.0518F, -20.975F, -5.0763F, 4.0F, 2.0F, 1.0F, new CubeDeformation(-0.4F)), PartPose.offsetAndRotation(-2.189F, -9.25F, 1.8412F, 0.0F, -1.0472F, 0.0F));

		PartDefinition band_1 = upper_coil.addOrReplaceChild("band_1", CubeListBuilder.create(), PartPose.offset(0.0F, 1.85F, 0.0F));

		PartDefinition narrow_ring_1 = band_1.addOrReplaceChild("narrow_ring_1", CubeListBuilder.create().texOffs(82, 90).addBox(-2.0017F, -29.225F, -3.3033F, 4.0F, 1.0F, 1.0F, new CubeDeformation(-0.3F))
		.texOffs(82, 90).addBox(-2.0017F, -29.225F, 2.1856F, 4.0F, 1.0F, 1.0F, new CubeDeformation(-0.3F)), PartPose.offset(0.797F, -22.125F, 0.0697F));

		PartDefinition cube_r9 = narrow_ring_1.addOrReplaceChild("cube_r9", CubeListBuilder.create().texOffs(82, 90).addBox(-1.0518F, -19.975F, 1.4237F, 4.0F, 1.0F, 1.0F, new CubeDeformation(-0.3F)), PartPose.offsetAndRotation(-1.1866F, -9.25F, -0.4586F, 0.0F, -1.0472F, 0.0F));

		PartDefinition cube_r10 = narrow_ring_1.addOrReplaceChild("cube_r10", CubeListBuilder.create().texOffs(82, 90).addBox(-1.0518F, -19.975F, -4.0763F, 4.0F, 1.0F, 1.0F, new CubeDeformation(-0.3F)), PartPose.offsetAndRotation(-1.1866F, -9.25F, -0.4476F, 0.0F, -1.0472F, 0.0F));

		PartDefinition cube_r11 = narrow_ring_1.addOrReplaceChild("cube_r11", CubeListBuilder.create().texOffs(82, 90).addBox(-1.0518F, -19.975F, -4.0763F, 4.0F, 1.0F, 1.0F, new CubeDeformation(-0.3F)), PartPose.offsetAndRotation(-0.2384F, -9.25F, -1.2794F, 0.0F, -2.0944F, 0.0F));

		PartDefinition cube_r12 = narrow_ring_1.addOrReplaceChild("cube_r12", CubeListBuilder.create().texOffs(82, 90).addBox(-1.0518F, -19.975F, 1.4237F, 4.0F, 1.0F, 1.0F, new CubeDeformation(-0.3F)), PartPose.offsetAndRotation(-0.2384F, -9.25F, -1.2904F, 0.0F, -2.0944F, 0.0F));

		PartDefinition wide_ring_1 = band_1.addOrReplaceChild("wide_ring_1", CubeListBuilder.create().texOffs(82, 93).addBox(-2.0651F, -30.225F, -1.4615F, 4.0F, 2.0F, 1.0F, new CubeDeformation(-0.4F))
		.texOffs(82, 93).addBox(-2.0651F, -30.225F, 3.8811F, 4.0F, 2.0F, 1.0F, new CubeDeformation(-0.4F)), PartPose.offset(0.897F, -21.65F, -1.7053F));

		PartDefinition cube_r13 = wide_ring_1.addOrReplaceChild("cube_r13", CubeListBuilder.create().texOffs(82, 93).addBox(-1.0518F, -20.975F, -5.0763F, 4.0F, 2.0F, 1.0F, new CubeDeformation(-0.4F)), PartPose.offsetAndRotation(-1.2408F, -9.25F, -0.0639F, 0.0F, -2.0944F, 0.0F));

		PartDefinition cube_r14 = wide_ring_1.addOrReplaceChild("cube_r14", CubeListBuilder.create().texOffs(82, 93).addBox(-1.0518F, -20.975F, 1.4237F, 4.0F, 2.0F, 1.0F, new CubeDeformation(-0.4F)), PartPose.offsetAndRotation(-0.2384F, -9.25F, 0.5148F, 0.0F, -2.0944F, 0.0F));

		PartDefinition cube_r15 = wide_ring_1.addOrReplaceChild("cube_r15", CubeListBuilder.create().texOffs(82, 93).addBox(-1.0518F, -20.975F, 1.4237F, 4.0F, 2.0F, 1.0F, new CubeDeformation(-0.4F)), PartPose.offsetAndRotation(-1.1866F, -9.25F, 1.2624F, 0.0F, -1.0472F, 0.0F));

		PartDefinition cube_r16 = wide_ring_1.addOrReplaceChild("cube_r16", CubeListBuilder.create().texOffs(82, 93).addBox(-1.0518F, -20.975F, -5.0763F, 4.0F, 2.0F, 1.0F, new CubeDeformation(-0.4F)), PartPose.offsetAndRotation(-2.189F, -9.25F, 1.8412F, 0.0F, -1.0472F, 0.0F));

		PartDefinition band_2 = upper_coil.addOrReplaceChild("band_2", CubeListBuilder.create(), PartPose.offset(0.0F, 3.925F, 0.0F));

		PartDefinition narrow_ring_2 = band_2.addOrReplaceChild("narrow_ring_2", CubeListBuilder.create().texOffs(82, 90).addBox(-2.0017F, -29.225F, -3.3033F, 4.0F, 1.0F, 1.0F, new CubeDeformation(-0.3F))
		.texOffs(82, 90).addBox(-2.0017F, -29.225F, 2.1856F, 4.0F, 1.0F, 1.0F, new CubeDeformation(-0.3F)), PartPose.offset(0.797F, -22.125F, 0.0697F));

		PartDefinition cube_r17 = narrow_ring_2.addOrReplaceChild("cube_r17", CubeListBuilder.create().texOffs(82, 90).addBox(-1.0518F, -19.975F, 1.4237F, 4.0F, 1.0F, 1.0F, new CubeDeformation(-0.3F)), PartPose.offsetAndRotation(-1.1866F, -9.25F, -0.4586F, 0.0F, -1.0472F, 0.0F));

		PartDefinition cube_r18 = narrow_ring_2.addOrReplaceChild("cube_r18", CubeListBuilder.create().texOffs(82, 90).addBox(-1.0518F, -19.975F, -4.0763F, 4.0F, 1.0F, 1.0F, new CubeDeformation(-0.3F)), PartPose.offsetAndRotation(-1.1866F, -9.25F, -0.4476F, 0.0F, -1.0472F, 0.0F));

		PartDefinition cube_r19 = narrow_ring_2.addOrReplaceChild("cube_r19", CubeListBuilder.create().texOffs(82, 90).addBox(-1.0518F, -19.975F, -4.0763F, 4.0F, 1.0F, 1.0F, new CubeDeformation(-0.3F)), PartPose.offsetAndRotation(-0.2384F, -9.25F, -1.2794F, 0.0F, -2.0944F, 0.0F));

		PartDefinition cube_r20 = narrow_ring_2.addOrReplaceChild("cube_r20", CubeListBuilder.create().texOffs(82, 90).addBox(-1.0518F, -19.975F, 1.4237F, 4.0F, 1.0F, 1.0F, new CubeDeformation(-0.3F)), PartPose.offsetAndRotation(-0.2384F, -9.25F, -1.2904F, 0.0F, -2.0944F, 0.0F));

		PartDefinition wide_ring_2 = band_2.addOrReplaceChild("wide_ring_2", CubeListBuilder.create().texOffs(82, 93).addBox(-2.0651F, -30.225F, -1.4615F, 4.0F, 2.0F, 1.0F, new CubeDeformation(-0.4F))
		.texOffs(82, 93).addBox(-2.0651F, -30.225F, 3.8811F, 4.0F, 2.0F, 1.0F, new CubeDeformation(-0.4F)), PartPose.offset(0.897F, -21.65F, -1.7053F));

		PartDefinition cube_r21 = wide_ring_2.addOrReplaceChild("cube_r21", CubeListBuilder.create().texOffs(82, 93).addBox(-1.0518F, -20.975F, -5.0763F, 4.0F, 2.0F, 1.0F, new CubeDeformation(-0.4F)), PartPose.offsetAndRotation(-1.2408F, -9.25F, -0.0639F, 0.0F, -2.0944F, 0.0F));

		PartDefinition cube_r22 = wide_ring_2.addOrReplaceChild("cube_r22", CubeListBuilder.create().texOffs(82, 93).addBox(-1.0518F, -20.975F, 1.4237F, 4.0F, 2.0F, 1.0F, new CubeDeformation(-0.4F)), PartPose.offsetAndRotation(-0.2384F, -9.25F, 0.5148F, 0.0F, -2.0944F, 0.0F));

		PartDefinition cube_r23 = wide_ring_2.addOrReplaceChild("cube_r23", CubeListBuilder.create().texOffs(82, 93).addBox(-1.0518F, -20.975F, 1.4237F, 4.0F, 2.0F, 1.0F, new CubeDeformation(-0.4F)), PartPose.offsetAndRotation(-1.1866F, -9.25F, 1.2624F, 0.0F, -1.0472F, 0.0F));

		PartDefinition cube_r24 = wide_ring_2.addOrReplaceChild("cube_r24", CubeListBuilder.create().texOffs(82, 93).addBox(-1.0518F, -20.975F, -5.0763F, 4.0F, 2.0F, 1.0F, new CubeDeformation(-0.4F)), PartPose.offsetAndRotation(-2.189F, -9.25F, 1.8412F, 0.0F, -1.0472F, 0.0F));

		PartDefinition band_3 = upper_coil.addOrReplaceChild("band_3", CubeListBuilder.create(), PartPose.offset(0.0F, 5.85F, 0.0F));

		PartDefinition narrow_ring_3 = band_3.addOrReplaceChild("narrow_ring_3", CubeListBuilder.create().texOffs(82, 90).addBox(-2.0017F, -29.225F, -3.3033F, 4.0F, 1.0F, 1.0F, new CubeDeformation(-0.3F))
		.texOffs(82, 90).addBox(-2.0017F, -29.225F, 2.1856F, 4.0F, 1.0F, 1.0F, new CubeDeformation(-0.3F)), PartPose.offset(0.797F, -22.125F, 0.0697F));

		PartDefinition cube_r25 = narrow_ring_3.addOrReplaceChild("cube_r25", CubeListBuilder.create().texOffs(82, 90).addBox(-1.0518F, -19.975F, 1.4237F, 4.0F, 1.0F, 1.0F, new CubeDeformation(-0.3F)), PartPose.offsetAndRotation(-1.1866F, -9.25F, -0.4586F, 0.0F, -1.0472F, 0.0F));

		PartDefinition cube_r26 = narrow_ring_3.addOrReplaceChild("cube_r26", CubeListBuilder.create().texOffs(82, 90).addBox(-1.0518F, -19.975F, -4.0763F, 4.0F, 1.0F, 1.0F, new CubeDeformation(-0.3F)), PartPose.offsetAndRotation(-1.1866F, -9.25F, -0.4476F, 0.0F, -1.0472F, 0.0F));

		PartDefinition cube_r27 = narrow_ring_3.addOrReplaceChild("cube_r27", CubeListBuilder.create().texOffs(82, 90).addBox(-1.0518F, -19.975F, -4.0763F, 4.0F, 1.0F, 1.0F, new CubeDeformation(-0.3F)), PartPose.offsetAndRotation(-0.2384F, -9.25F, -1.2794F, 0.0F, -2.0944F, 0.0F));

		PartDefinition cube_r28 = narrow_ring_3.addOrReplaceChild("cube_r28", CubeListBuilder.create().texOffs(82, 90).addBox(-1.0518F, -19.975F, 1.4237F, 4.0F, 1.0F, 1.0F, new CubeDeformation(-0.3F)), PartPose.offsetAndRotation(-0.2384F, -9.25F, -1.2904F, 0.0F, -2.0944F, 0.0F));

		PartDefinition wide_ring_3 = band_3.addOrReplaceChild("wide_ring_3", CubeListBuilder.create().texOffs(82, 93).addBox(-2.0651F, -30.225F, -1.4615F, 4.0F, 2.0F, 1.0F, new CubeDeformation(-0.4F))
		.texOffs(82, 93).addBox(-2.0651F, -30.225F, 3.8811F, 4.0F, 2.0F, 1.0F, new CubeDeformation(-0.4F)), PartPose.offset(0.897F, -21.65F, -1.7053F));

		PartDefinition cube_r29 = wide_ring_3.addOrReplaceChild("cube_r29", CubeListBuilder.create().texOffs(82, 93).addBox(-1.0518F, -20.975F, -5.0763F, 4.0F, 2.0F, 1.0F, new CubeDeformation(-0.4F)), PartPose.offsetAndRotation(-1.2408F, -9.25F, -0.0639F, 0.0F, -2.0944F, 0.0F));

		PartDefinition cube_r30 = wide_ring_3.addOrReplaceChild("cube_r30", CubeListBuilder.create().texOffs(82, 93).addBox(-1.0518F, -20.975F, 1.4237F, 4.0F, 2.0F, 1.0F, new CubeDeformation(-0.4F)), PartPose.offsetAndRotation(-0.2384F, -9.25F, 0.5148F, 0.0F, -2.0944F, 0.0F));

		PartDefinition cube_r31 = wide_ring_3.addOrReplaceChild("cube_r31", CubeListBuilder.create().texOffs(82, 93).addBox(-1.0518F, -20.975F, 1.4237F, 4.0F, 2.0F, 1.0F, new CubeDeformation(-0.4F)), PartPose.offsetAndRotation(-1.1866F, -9.25F, 1.2624F, 0.0F, -1.0472F, 0.0F));

		PartDefinition cube_r32 = wide_ring_3.addOrReplaceChild("cube_r32", CubeListBuilder.create().texOffs(82, 93).addBox(-1.0518F, -20.975F, -5.0763F, 4.0F, 2.0F, 1.0F, new CubeDeformation(-0.4F)), PartPose.offsetAndRotation(-2.189F, -9.25F, 1.8412F, 0.0F, -1.0472F, 0.0F));

		PartDefinition lower_coil = time_scepter.addOrReplaceChild("lower_coil", CubeListBuilder.create(), PartPose.offset(-0.7875F, 83.7344F, -0.0937F));

		PartDefinition band_4 = lower_coil.addOrReplaceChild("band_4", CubeListBuilder.create(), PartPose.offset(0.0F, -17.775F, 0.0F));

		PartDefinition narrow_ring_4 = band_4.addOrReplaceChild("narrow_ring_4", CubeListBuilder.create().texOffs(82, 90).addBox(-2.0017F, -29.225F, -3.3033F, 4.0F, 1.0F, 1.0F, new CubeDeformation(-0.3F))
		.texOffs(82, 90).addBox(-2.0017F, -29.225F, 2.1856F, 4.0F, 1.0F, 1.0F, new CubeDeformation(-0.3F)), PartPose.offset(0.797F, -22.125F, 0.0697F));

		PartDefinition cube_r33 = narrow_ring_4.addOrReplaceChild("cube_r33", CubeListBuilder.create().texOffs(82, 90).addBox(-1.0518F, -19.975F, 1.4237F, 4.0F, 1.0F, 1.0F, new CubeDeformation(-0.3F)), PartPose.offsetAndRotation(-1.1866F, -9.25F, -0.4586F, 0.0F, -1.0472F, 0.0F));

		PartDefinition cube_r34 = narrow_ring_4.addOrReplaceChild("cube_r34", CubeListBuilder.create().texOffs(82, 90).addBox(-1.0518F, -19.975F, -4.0763F, 4.0F, 1.0F, 1.0F, new CubeDeformation(-0.3F)), PartPose.offsetAndRotation(-1.1866F, -9.25F, -0.4476F, 0.0F, -1.0472F, 0.0F));

		PartDefinition cube_r35 = narrow_ring_4.addOrReplaceChild("cube_r35", CubeListBuilder.create().texOffs(82, 90).addBox(-1.0518F, -19.975F, -4.0763F, 4.0F, 1.0F, 1.0F, new CubeDeformation(-0.3F)), PartPose.offsetAndRotation(-0.2384F, -9.25F, -1.2794F, 0.0F, -2.0944F, 0.0F));

		PartDefinition cube_r36 = narrow_ring_4.addOrReplaceChild("cube_r36", CubeListBuilder.create().texOffs(82, 90).addBox(-1.0518F, -19.975F, 1.4237F, 4.0F, 1.0F, 1.0F, new CubeDeformation(-0.3F)), PartPose.offsetAndRotation(-0.2384F, -9.25F, -1.2904F, 0.0F, -2.0944F, 0.0F));

		PartDefinition wide_ring_4 = band_4.addOrReplaceChild("wide_ring_4", CubeListBuilder.create().texOffs(82, 93).addBox(-2.0651F, -30.225F, -1.4615F, 4.0F, 2.0F, 1.0F, new CubeDeformation(-0.4F))
		.texOffs(82, 93).addBox(-2.0651F, -30.225F, 3.8811F, 4.0F, 2.0F, 1.0F, new CubeDeformation(-0.4F)), PartPose.offset(0.897F, -21.65F, -1.7053F));

		PartDefinition cube_r37 = wide_ring_4.addOrReplaceChild("cube_r37", CubeListBuilder.create().texOffs(82, 93).addBox(-1.0518F, -20.975F, -5.0763F, 4.0F, 2.0F, 1.0F, new CubeDeformation(-0.4F)), PartPose.offsetAndRotation(-1.2408F, -9.25F, -0.0639F, 0.0F, -2.0944F, 0.0F));

		PartDefinition cube_r38 = wide_ring_4.addOrReplaceChild("cube_r38", CubeListBuilder.create().texOffs(82, 93).addBox(-1.0518F, -20.975F, 1.4237F, 4.0F, 2.0F, 1.0F, new CubeDeformation(-0.4F)), PartPose.offsetAndRotation(-0.2384F, -9.25F, 0.5148F, 0.0F, -2.0944F, 0.0F));

		PartDefinition cube_r39 = wide_ring_4.addOrReplaceChild("cube_r39", CubeListBuilder.create().texOffs(82, 93).addBox(-1.0518F, -20.975F, 1.4237F, 4.0F, 2.0F, 1.0F, new CubeDeformation(-0.4F)), PartPose.offsetAndRotation(-1.1866F, -9.25F, 1.2624F, 0.0F, -1.0472F, 0.0F));

		PartDefinition cube_r40 = wide_ring_4.addOrReplaceChild("cube_r40", CubeListBuilder.create().texOffs(82, 93).addBox(-1.0518F, -20.975F, -5.0763F, 4.0F, 2.0F, 1.0F, new CubeDeformation(-0.4F)), PartPose.offsetAndRotation(-2.189F, -9.25F, 1.8412F, 0.0F, -1.0472F, 0.0F));

		PartDefinition band_5 = lower_coil.addOrReplaceChild("band_5", CubeListBuilder.create(), PartPose.offset(0.0F, -15.75F, 0.0F));

		PartDefinition narrow_ring_5 = band_5.addOrReplaceChild("narrow_ring_5", CubeListBuilder.create().texOffs(82, 90).addBox(-2.0017F, -29.225F, -3.3033F, 4.0F, 1.0F, 1.0F, new CubeDeformation(-0.3F))
		.texOffs(82, 90).addBox(-2.0017F, -29.225F, 2.1856F, 4.0F, 1.0F, 1.0F, new CubeDeformation(-0.3F)), PartPose.offset(0.797F, -22.125F, 0.0697F));

		PartDefinition cube_r41 = narrow_ring_5.addOrReplaceChild("cube_r41", CubeListBuilder.create().texOffs(82, 90).addBox(-1.0518F, -19.975F, 1.4237F, 4.0F, 1.0F, 1.0F, new CubeDeformation(-0.3F)), PartPose.offsetAndRotation(-1.1866F, -9.25F, -0.4586F, 0.0F, -1.0472F, 0.0F));

		PartDefinition cube_r42 = narrow_ring_5.addOrReplaceChild("cube_r42", CubeListBuilder.create().texOffs(82, 90).addBox(-1.0518F, -19.975F, -4.0763F, 4.0F, 1.0F, 1.0F, new CubeDeformation(-0.3F)), PartPose.offsetAndRotation(-1.1866F, -9.25F, -0.4476F, 0.0F, -1.0472F, 0.0F));

		PartDefinition cube_r43 = narrow_ring_5.addOrReplaceChild("cube_r43", CubeListBuilder.create().texOffs(82, 90).addBox(-1.0518F, -19.975F, -4.0763F, 4.0F, 1.0F, 1.0F, new CubeDeformation(-0.3F)), PartPose.offsetAndRotation(-0.2384F, -9.25F, -1.2794F, 0.0F, -2.0944F, 0.0F));

		PartDefinition cube_r44 = narrow_ring_5.addOrReplaceChild("cube_r44", CubeListBuilder.create().texOffs(82, 90).addBox(-1.0518F, -19.975F, 1.4237F, 4.0F, 1.0F, 1.0F, new CubeDeformation(-0.3F)), PartPose.offsetAndRotation(-0.2384F, -9.25F, -1.2904F, 0.0F, -2.0944F, 0.0F));

		PartDefinition wide_ring_5 = band_5.addOrReplaceChild("wide_ring_5", CubeListBuilder.create().texOffs(82, 93).addBox(-2.0651F, -30.225F, -1.4615F, 4.0F, 2.0F, 1.0F, new CubeDeformation(-0.4F))
		.texOffs(82, 93).addBox(-2.0651F, -30.225F, 3.8811F, 4.0F, 2.0F, 1.0F, new CubeDeformation(-0.4F)), PartPose.offset(0.897F, -21.65F, -1.7053F));

		PartDefinition cube_r45 = wide_ring_5.addOrReplaceChild("cube_r45", CubeListBuilder.create().texOffs(82, 93).addBox(-1.0518F, -20.975F, -5.0763F, 4.0F, 2.0F, 1.0F, new CubeDeformation(-0.4F)), PartPose.offsetAndRotation(-1.2408F, -9.25F, -0.0639F, 0.0F, -2.0944F, 0.0F));

		PartDefinition cube_r46 = wide_ring_5.addOrReplaceChild("cube_r46", CubeListBuilder.create().texOffs(82, 93).addBox(-1.0518F, -20.975F, 1.4237F, 4.0F, 2.0F, 1.0F, new CubeDeformation(-0.4F)), PartPose.offsetAndRotation(-0.2384F, -9.25F, 0.5148F, 0.0F, -2.0944F, 0.0F));

		PartDefinition cube_r47 = wide_ring_5.addOrReplaceChild("cube_r47", CubeListBuilder.create().texOffs(82, 93).addBox(-1.0518F, -20.975F, 1.4237F, 4.0F, 2.0F, 1.0F, new CubeDeformation(-0.4F)), PartPose.offsetAndRotation(-1.1866F, -9.25F, 1.2624F, 0.0F, -1.0472F, 0.0F));

		PartDefinition cube_r48 = wide_ring_5.addOrReplaceChild("cube_r48", CubeListBuilder.create().texOffs(82, 93).addBox(-1.0518F, -20.975F, -5.0763F, 4.0F, 2.0F, 1.0F, new CubeDeformation(-0.4F)), PartPose.offsetAndRotation(-2.189F, -9.25F, 1.8412F, 0.0F, -1.0472F, 0.0F));

		PartDefinition band_6 = lower_coil.addOrReplaceChild("band_6", CubeListBuilder.create(), PartPose.offset(0.0F, -13.75F, 0.0F));

		PartDefinition narrow_ring_6 = band_6.addOrReplaceChild("narrow_ring_6", CubeListBuilder.create().texOffs(82, 90).addBox(-2.0017F, -29.225F, -3.3033F, 4.0F, 1.0F, 1.0F, new CubeDeformation(-0.3F))
		.texOffs(82, 90).addBox(-2.0017F, -29.225F, 2.1856F, 4.0F, 1.0F, 1.0F, new CubeDeformation(-0.3F)), PartPose.offset(0.797F, -22.125F, 0.0697F));

		PartDefinition cube_r49 = narrow_ring_6.addOrReplaceChild("cube_r49", CubeListBuilder.create().texOffs(82, 90).addBox(-1.0518F, -19.975F, 1.4237F, 4.0F, 1.0F, 1.0F, new CubeDeformation(-0.3F)), PartPose.offsetAndRotation(-1.1866F, -9.25F, -0.4586F, 0.0F, -1.0472F, 0.0F));

		PartDefinition cube_r50 = narrow_ring_6.addOrReplaceChild("cube_r50", CubeListBuilder.create().texOffs(82, 90).addBox(-1.0518F, -19.975F, -4.0763F, 4.0F, 1.0F, 1.0F, new CubeDeformation(-0.3F)), PartPose.offsetAndRotation(-1.1866F, -9.25F, -0.4476F, 0.0F, -1.0472F, 0.0F));

		PartDefinition cube_r51 = narrow_ring_6.addOrReplaceChild("cube_r51", CubeListBuilder.create().texOffs(82, 90).addBox(-1.0518F, -19.975F, -4.0763F, 4.0F, 1.0F, 1.0F, new CubeDeformation(-0.3F)), PartPose.offsetAndRotation(-0.2384F, -9.25F, -1.2794F, 0.0F, -2.0944F, 0.0F));

		PartDefinition cube_r52 = narrow_ring_6.addOrReplaceChild("cube_r52", CubeListBuilder.create().texOffs(82, 90).addBox(-1.0518F, -19.975F, 1.4237F, 4.0F, 1.0F, 1.0F, new CubeDeformation(-0.3F)), PartPose.offsetAndRotation(-0.2384F, -9.25F, -1.2904F, 0.0F, -2.0944F, 0.0F));

		PartDefinition wide_ring_6 = band_6.addOrReplaceChild("wide_ring_6", CubeListBuilder.create().texOffs(82, 93).addBox(-2.0651F, -30.225F, -1.4615F, 4.0F, 2.0F, 1.0F, new CubeDeformation(-0.4F))
		.texOffs(82, 93).addBox(-2.0651F, -30.225F, 3.8811F, 4.0F, 2.0F, 1.0F, new CubeDeformation(-0.4F)), PartPose.offset(0.897F, -21.65F, -1.7053F));

		PartDefinition cube_r53 = wide_ring_6.addOrReplaceChild("cube_r53", CubeListBuilder.create().texOffs(82, 93).addBox(-1.0518F, -20.975F, -5.0763F, 4.0F, 2.0F, 1.0F, new CubeDeformation(-0.4F)), PartPose.offsetAndRotation(-1.2408F, -9.25F, -0.0639F, 0.0F, -2.0944F, 0.0F));

		PartDefinition cube_r54 = wide_ring_6.addOrReplaceChild("cube_r54", CubeListBuilder.create().texOffs(82, 93).addBox(-1.0518F, -20.975F, 1.4237F, 4.0F, 2.0F, 1.0F, new CubeDeformation(-0.4F)), PartPose.offsetAndRotation(-0.2384F, -9.25F, 0.5148F, 0.0F, -2.0944F, 0.0F));

		PartDefinition cube_r55 = wide_ring_6.addOrReplaceChild("cube_r55", CubeListBuilder.create().texOffs(82, 93).addBox(-1.0518F, -20.975F, 1.4237F, 4.0F, 2.0F, 1.0F, new CubeDeformation(-0.4F)), PartPose.offsetAndRotation(-1.1866F, -9.25F, 1.2624F, 0.0F, -1.0472F, 0.0F));

		PartDefinition cube_r56 = wide_ring_6.addOrReplaceChild("cube_r56", CubeListBuilder.create().texOffs(82, 93).addBox(-1.0518F, -20.975F, -5.0763F, 4.0F, 2.0F, 1.0F, new CubeDeformation(-0.4F)), PartPose.offsetAndRotation(-2.189F, -9.25F, 1.8412F, 0.0F, -1.0472F, 0.0F));

		PartDefinition band_7 = lower_coil.addOrReplaceChild("band_7", CubeListBuilder.create(), PartPose.offset(0.0F, -11.75F, 0.0F));

		PartDefinition narrow_ring_7 = band_7.addOrReplaceChild("narrow_ring_7", CubeListBuilder.create().texOffs(82, 90).addBox(-2.0017F, -29.225F, -3.3033F, 4.0F, 1.0F, 1.0F, new CubeDeformation(-0.3F))
		.texOffs(82, 90).addBox(-2.0017F, -29.225F, 2.1856F, 4.0F, 1.0F, 1.0F, new CubeDeformation(-0.3F)), PartPose.offset(0.797F, -22.125F, 0.0697F));

		PartDefinition cube_r57 = narrow_ring_7.addOrReplaceChild("cube_r57", CubeListBuilder.create().texOffs(82, 90).addBox(-1.0518F, -19.975F, 1.4237F, 4.0F, 1.0F, 1.0F, new CubeDeformation(-0.3F)), PartPose.offsetAndRotation(-1.1866F, -9.25F, -0.4586F, 0.0F, -1.0472F, 0.0F));

		PartDefinition cube_r58 = narrow_ring_7.addOrReplaceChild("cube_r58", CubeListBuilder.create().texOffs(82, 90).addBox(-1.0518F, -19.975F, -4.0763F, 4.0F, 1.0F, 1.0F, new CubeDeformation(-0.3F)), PartPose.offsetAndRotation(-1.1866F, -9.25F, -0.4476F, 0.0F, -1.0472F, 0.0F));

		PartDefinition cube_r59 = narrow_ring_7.addOrReplaceChild("cube_r59", CubeListBuilder.create().texOffs(82, 90).addBox(-1.0518F, -19.975F, -4.0763F, 4.0F, 1.0F, 1.0F, new CubeDeformation(-0.3F)), PartPose.offsetAndRotation(-0.2384F, -9.25F, -1.2794F, 0.0F, -2.0944F, 0.0F));

		PartDefinition cube_r60 = narrow_ring_7.addOrReplaceChild("cube_r60", CubeListBuilder.create().texOffs(82, 90).addBox(-1.0518F, -19.975F, 1.4237F, 4.0F, 1.0F, 1.0F, new CubeDeformation(-0.3F)), PartPose.offsetAndRotation(-0.2384F, -9.25F, -1.2904F, 0.0F, -2.0944F, 0.0F));

		PartDefinition wide_ring_7 = band_7.addOrReplaceChild("wide_ring_7", CubeListBuilder.create().texOffs(82, 93).addBox(-2.0651F, -30.225F, -1.4615F, 4.0F, 2.0F, 1.0F, new CubeDeformation(-0.4F))
		.texOffs(82, 93).addBox(-2.0651F, -30.225F, 3.8811F, 4.0F, 2.0F, 1.0F, new CubeDeformation(-0.4F)), PartPose.offset(0.897F, -21.65F, -1.7053F));

		PartDefinition cube_r61 = wide_ring_7.addOrReplaceChild("cube_r61", CubeListBuilder.create().texOffs(82, 93).addBox(-1.0518F, -20.975F, -5.0763F, 4.0F, 2.0F, 1.0F, new CubeDeformation(-0.4F)), PartPose.offsetAndRotation(-1.2408F, -9.25F, -0.0639F, 0.0F, -2.0944F, 0.0F));

		PartDefinition cube_r62 = wide_ring_7.addOrReplaceChild("cube_r62", CubeListBuilder.create().texOffs(82, 93).addBox(-1.0518F, -20.975F, 1.4237F, 4.0F, 2.0F, 1.0F, new CubeDeformation(-0.4F)), PartPose.offsetAndRotation(-0.2384F, -9.25F, 0.5148F, 0.0F, -2.0944F, 0.0F));

		PartDefinition cube_r63 = wide_ring_7.addOrReplaceChild("cube_r63", CubeListBuilder.create().texOffs(82, 93).addBox(-1.0518F, -20.975F, 1.4237F, 4.0F, 2.0F, 1.0F, new CubeDeformation(-0.4F)), PartPose.offsetAndRotation(-1.1866F, -9.25F, 1.2624F, 0.0F, -1.0472F, 0.0F));

		PartDefinition cube_r64 = wide_ring_7.addOrReplaceChild("cube_r64", CubeListBuilder.create().texOffs(82, 93).addBox(-1.0518F, -20.975F, -5.0763F, 4.0F, 2.0F, 1.0F, new CubeDeformation(-0.4F)), PartPose.offsetAndRotation(-2.189F, -9.25F, 1.8412F, 0.0F, -1.0472F, 0.0F));

		PartDefinition band_8 = lower_coil.addOrReplaceChild("band_8", CubeListBuilder.create(), PartPose.offset(0.0F, -9.75F, 0.0F));

		PartDefinition narrow_ring_8 = band_8.addOrReplaceChild("narrow_ring_8", CubeListBuilder.create().texOffs(82, 90).addBox(-2.0017F, -29.225F, -3.3033F, 4.0F, 1.0F, 1.0F, new CubeDeformation(-0.3F))
		.texOffs(82, 90).addBox(-2.0017F, -29.225F, 2.1856F, 4.0F, 1.0F, 1.0F, new CubeDeformation(-0.3F)), PartPose.offset(0.797F, -22.125F, 0.0697F));

		PartDefinition cube_r65 = narrow_ring_8.addOrReplaceChild("cube_r65", CubeListBuilder.create().texOffs(82, 90).addBox(-1.0518F, -19.975F, 1.4237F, 4.0F, 1.0F, 1.0F, new CubeDeformation(-0.3F)), PartPose.offsetAndRotation(-1.1866F, -9.25F, -0.4586F, 0.0F, -1.0472F, 0.0F));

		PartDefinition cube_r66 = narrow_ring_8.addOrReplaceChild("cube_r66", CubeListBuilder.create().texOffs(82, 90).addBox(-1.0518F, -19.975F, -4.0763F, 4.0F, 1.0F, 1.0F, new CubeDeformation(-0.3F)), PartPose.offsetAndRotation(-1.1866F, -9.25F, -0.4476F, 0.0F, -1.0472F, 0.0F));

		PartDefinition cube_r67 = narrow_ring_8.addOrReplaceChild("cube_r67", CubeListBuilder.create().texOffs(82, 90).addBox(-1.0518F, -19.975F, -4.0763F, 4.0F, 1.0F, 1.0F, new CubeDeformation(-0.3F)), PartPose.offsetAndRotation(-0.2384F, -9.25F, -1.2794F, 0.0F, -2.0944F, 0.0F));

		PartDefinition cube_r68 = narrow_ring_8.addOrReplaceChild("cube_r68", CubeListBuilder.create().texOffs(82, 90).addBox(-1.0518F, -19.975F, 1.4237F, 4.0F, 1.0F, 1.0F, new CubeDeformation(-0.3F)), PartPose.offsetAndRotation(-0.2384F, -9.25F, -1.2904F, 0.0F, -2.0944F, 0.0F));

		PartDefinition wide_ring_8 = band_8.addOrReplaceChild("wide_ring_8", CubeListBuilder.create().texOffs(82, 93).addBox(-2.0651F, -30.225F, -1.4615F, 4.0F, 2.0F, 1.0F, new CubeDeformation(-0.4F))
		.texOffs(82, 93).addBox(-2.0651F, -30.225F, 3.8811F, 4.0F, 2.0F, 1.0F, new CubeDeformation(-0.4F)), PartPose.offset(0.897F, -21.65F, -1.7053F));

		PartDefinition cube_r69 = wide_ring_8.addOrReplaceChild("cube_r69", CubeListBuilder.create().texOffs(82, 93).addBox(-1.0518F, -20.975F, -5.0763F, 4.0F, 2.0F, 1.0F, new CubeDeformation(-0.4F)), PartPose.offsetAndRotation(-1.2408F, -9.25F, -0.0639F, 0.0F, -2.0944F, 0.0F));

		PartDefinition cube_r70 = wide_ring_8.addOrReplaceChild("cube_r70", CubeListBuilder.create().texOffs(82, 93).addBox(-1.0518F, -20.975F, 1.4237F, 4.0F, 2.0F, 1.0F, new CubeDeformation(-0.4F)), PartPose.offsetAndRotation(-0.2384F, -9.25F, 0.5148F, 0.0F, -2.0944F, 0.0F));

		PartDefinition cube_r71 = wide_ring_8.addOrReplaceChild("cube_r71", CubeListBuilder.create().texOffs(82, 93).addBox(-1.0518F, -20.975F, 1.4237F, 4.0F, 2.0F, 1.0F, new CubeDeformation(-0.4F)), PartPose.offsetAndRotation(-1.1866F, -9.25F, 1.2624F, 0.0F, -1.0472F, 0.0F));

		PartDefinition cube_r72 = wide_ring_8.addOrReplaceChild("cube_r72", CubeListBuilder.create().texOffs(82, 93).addBox(-1.0518F, -20.975F, -5.0763F, 4.0F, 2.0F, 1.0F, new CubeDeformation(-0.4F)), PartPose.offsetAndRotation(-2.189F, -9.25F, 1.8412F, 0.0F, -1.0472F, 0.0F));

		PartDefinition band_9 = lower_coil.addOrReplaceChild("band_9", CubeListBuilder.create(), PartPose.offset(0.0F, -7.75F, 0.0F));

		PartDefinition narrow_ring_9 = band_9.addOrReplaceChild("narrow_ring_9", CubeListBuilder.create().texOffs(82, 90).addBox(-2.0017F, -29.225F, -3.3033F, 4.0F, 1.0F, 1.0F, new CubeDeformation(-0.3F))
		.texOffs(82, 90).addBox(-2.0017F, -29.225F, 2.1856F, 4.0F, 1.0F, 1.0F, new CubeDeformation(-0.3F)), PartPose.offset(0.797F, -22.125F, 0.0697F));

		PartDefinition cube_r73 = narrow_ring_9.addOrReplaceChild("cube_r73", CubeListBuilder.create().texOffs(82, 90).addBox(-1.0518F, -19.975F, 1.4237F, 4.0F, 1.0F, 1.0F, new CubeDeformation(-0.3F)), PartPose.offsetAndRotation(-1.1866F, -9.25F, -0.4586F, 0.0F, -1.0472F, 0.0F));

		PartDefinition cube_r74 = narrow_ring_9.addOrReplaceChild("cube_r74", CubeListBuilder.create().texOffs(82, 90).addBox(-1.0518F, -19.975F, -4.0763F, 4.0F, 1.0F, 1.0F, new CubeDeformation(-0.3F)), PartPose.offsetAndRotation(-1.1866F, -9.25F, -0.4476F, 0.0F, -1.0472F, 0.0F));

		PartDefinition cube_r75 = narrow_ring_9.addOrReplaceChild("cube_r75", CubeListBuilder.create().texOffs(82, 90).addBox(-1.0518F, -19.975F, -4.0763F, 4.0F, 1.0F, 1.0F, new CubeDeformation(-0.3F)), PartPose.offsetAndRotation(-0.2384F, -9.25F, -1.2794F, 0.0F, -2.0944F, 0.0F));

		PartDefinition cube_r76 = narrow_ring_9.addOrReplaceChild("cube_r76", CubeListBuilder.create().texOffs(82, 90).addBox(-1.0518F, -19.975F, 1.4237F, 4.0F, 1.0F, 1.0F, new CubeDeformation(-0.3F)), PartPose.offsetAndRotation(-0.2384F, -9.25F, -1.2904F, 0.0F, -2.0944F, 0.0F));

		PartDefinition wide_ring_9 = band_9.addOrReplaceChild("wide_ring_9", CubeListBuilder.create().texOffs(82, 93).addBox(-2.0651F, -30.225F, -1.4615F, 4.0F, 2.0F, 1.0F, new CubeDeformation(-0.4F))
		.texOffs(82, 93).addBox(-2.0651F, -30.225F, 3.8811F, 4.0F, 2.0F, 1.0F, new CubeDeformation(-0.4F)), PartPose.offset(0.897F, -21.65F, -1.7053F));

		PartDefinition cube_r77 = wide_ring_9.addOrReplaceChild("cube_r77", CubeListBuilder.create().texOffs(82, 93).addBox(-1.0518F, -20.975F, -5.0763F, 4.0F, 2.0F, 1.0F, new CubeDeformation(-0.4F)), PartPose.offsetAndRotation(-1.2408F, -9.25F, -0.0639F, 0.0F, -2.0944F, 0.0F));

		PartDefinition cube_r78 = wide_ring_9.addOrReplaceChild("cube_r78", CubeListBuilder.create().texOffs(82, 93).addBox(-1.0518F, -20.975F, 1.4237F, 4.0F, 2.0F, 1.0F, new CubeDeformation(-0.4F)), PartPose.offsetAndRotation(-0.2384F, -9.25F, 0.5148F, 0.0F, -2.0944F, 0.0F));

		PartDefinition cube_r79 = wide_ring_9.addOrReplaceChild("cube_r79", CubeListBuilder.create().texOffs(82, 93).addBox(-1.0518F, -20.975F, 1.4237F, 4.0F, 2.0F, 1.0F, new CubeDeformation(-0.4F)), PartPose.offsetAndRotation(-1.1866F, -9.25F, 1.2624F, 0.0F, -1.0472F, 0.0F));

		PartDefinition cube_r80 = wide_ring_9.addOrReplaceChild("cube_r80", CubeListBuilder.create().texOffs(82, 93).addBox(-1.0518F, -20.975F, -5.0763F, 4.0F, 2.0F, 1.0F, new CubeDeformation(-0.4F)), PartPose.offsetAndRotation(-2.189F, -9.25F, 1.8412F, 0.0F, -1.0472F, 0.0F));

		PartDefinition rotor_rotate_y = time_scepter.addOrReplaceChild("rotor_rotate_y", CubeListBuilder.create().texOffs(50, 13).addBox(-6.4875F, -5.4406F, -0.7937F, 13.0F, 1.0F, 1.0F, new CubeDeformation(-0.2F))
		.texOffs(50, 13).addBox(-6.4875F, -5.4406F, -0.1937F, 13.0F, 1.0F, 1.0F, new CubeDeformation(-0.2F))
		.texOffs(50, 13).addBox(-6.4875F, 4.5844F, -0.7937F, 13.0F, 1.0F, 1.0F, new CubeDeformation(-0.2F))
		.texOffs(50, 13).addBox(-6.4875F, 4.5844F, -0.1937F, 13.0F, 1.0F, 1.0F, new CubeDeformation(-0.2F))
		.texOffs(50, 13).addBox(5.5125F, -0.0156F, -0.7937F, 1.0F, 5.0F, 1.0F, new CubeDeformation(-0.2F))
		.texOffs(50, 13).addBox(5.5125F, -0.0156F, -0.1938F, 1.0F, 5.0F, 1.0F, new CubeDeformation(-0.2F))
		.texOffs(50, 13).addBox(5.5125F, -4.8406F, -0.7937F, 1.0F, 5.0F, 1.0F, new CubeDeformation(-0.2F))
		.texOffs(50, 13).addBox(5.5125F, -4.8406F, -0.1938F, 1.0F, 5.0F, 1.0F, new CubeDeformation(-0.2F))
		.texOffs(50, 13).addBox(-6.4875F, -4.8406F, -0.1938F, 1.0F, 5.0F, 1.0F, new CubeDeformation(-0.2F))
		.texOffs(50, 13).addBox(-6.4875F, -4.8406F, -0.7937F, 1.0F, 5.0F, 1.0F, new CubeDeformation(-0.2F))
		.texOffs(50, 13).addBox(-6.4875F, -0.0156F, -0.1938F, 1.0F, 5.0F, 1.0F, new CubeDeformation(-0.2F))
		.texOffs(50, 13).addBox(-6.4875F, -0.0156F, -0.7937F, 1.0F, 5.0F, 1.0F, new CubeDeformation(-0.2F))
		.texOffs(2, 40).addBox(-7.0375F, -1.2156F, -0.0187F, 2.0F, 2.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(3, 40).addBox(-7.0375F, -1.2156F, -1.0187F, 2.0F, 2.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(2, 40).addBox(4.9625F, -1.2156F, -0.0187F, 2.0F, 2.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(3, 40).addBox(4.9625F, -1.2156F, -1.0187F, 2.0F, 2.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition glow_bits = rotor_rotate_y.addOrReplaceChild("glow_bits", CubeListBuilder.create().texOffs(110, 7).addBox(-5.7F, -27.05F, 0.375F, 1.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(110, 7).addBox(-5.7F, -27.05F, -1.2F, 1.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(110, 7).addBox(6.3F, -27.05F, -1.225F, 1.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(110, 7).addBox(6.3F, -27.05F, 0.375F, 1.0F, 1.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offset(-0.7875F, 26.3844F, -0.0937F));

		PartDefinition glow_core = time_scepter.addOrReplaceChild("glow_core", CubeListBuilder.create(), PartPose.offset(-0.7875F, 26.3844F, -0.0937F));

		PartDefinition cube_r81 = glow_core.addOrReplaceChild("cube_r81", CubeListBuilder.create().texOffs(105, 7).addBox(-1.3F, -34.35F, -2.325F, 4.0F, 1.0F, 4.0F, new CubeDeformation(-0.15F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.0F, -0.5236F, 0.0F));

		PartDefinition glow_shaft_3 = glow_core.addOrReplaceChild("glow_shaft_3", CubeListBuilder.create().texOffs(109, 10).addBox(-1.5037F, -34.225F, 1.6557F, 3.0F, 12.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(109, 10).addBox(-1.5037F, -34.25F, -2.5404F, 3.0F, 12.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.797F, 0.0F, 0.0697F, 0.0F, -2.0944F, 0.0F));

		PartDefinition glow_shaft_2 = glow_core.addOrReplaceChild("glow_shaft_2", CubeListBuilder.create().texOffs(109, 10).addBox(-1.5518F, -34.25F, 1.6237F, 3.0F, 12.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(109, 10).addBox(-1.5518F, -34.225F, -2.5725F, 3.0F, 12.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.797F, 0.0F, 0.0697F, 0.0F, -1.0472F, 0.0F));

		PartDefinition glow_shaft_1 = glow_core.addOrReplaceChild("glow_shaft_1", CubeListBuilder.create().texOffs(109, 10).addBox(-1.5481F, -34.225F, 1.566F, 3.0F, 12.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(109, 10).addBox(-1.5481F, -34.25F, -2.6301F, 3.0F, 12.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offset(0.797F, 0.0F, 0.0697F));

		PartDefinition glow_core2 = time_scepter.addOrReplaceChild("glow_core2", CubeListBuilder.create(), PartPose.offset(-0.7875F, 48.1344F, -0.0937F));

		PartDefinition cube_r82 = glow_core2.addOrReplaceChild("cube_r82", CubeListBuilder.create().texOffs(105, 7).addBox(-1.3F, -34.35F, -2.325F, 4.0F, 1.0F, 4.0F, new CubeDeformation(-0.15F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.0F, -0.5236F, 0.0F));

		PartDefinition glow_shaft_4 = glow_core2.addOrReplaceChild("glow_shaft_4", CubeListBuilder.create().texOffs(109, 10).addBox(-1.5037F, -34.225F, 1.6557F, 3.0F, 12.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(109, 10).addBox(-1.5037F, -34.25F, -2.5404F, 3.0F, 12.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.797F, 0.0F, 0.0697F, 0.0F, -2.0944F, 0.0F));

		PartDefinition glow_shaft_5 = glow_core2.addOrReplaceChild("glow_shaft_5", CubeListBuilder.create().texOffs(109, 10).addBox(-1.5518F, -34.25F, 1.6237F, 3.0F, 12.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(109, 10).addBox(-1.5518F, -34.225F, -2.5725F, 3.0F, 12.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.797F, 0.0F, 0.0697F, 0.0F, -1.0472F, 0.0F));

		PartDefinition glow_shaft_6 = glow_core2.addOrReplaceChild("glow_shaft_6", CubeListBuilder.create().texOffs(109, 10).addBox(-1.5481F, -34.225F, 1.566F, 3.0F, 12.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(109, 10).addBox(-1.5481F, -34.25F, -2.6301F, 3.0F, 12.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offset(0.797F, 0.0F, 0.0697F));

		PartDefinition controls = partdefinition.addOrReplaceChild("controls", CubeListBuilder.create(), PartPose.offset(0.0F, 24.0F, 0.0F));

		PartDefinition controls_1 = controls.addOrReplaceChild("controls_1", CubeListBuilder.create(), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition fast_return = controls_1.addOrReplaceChild("fast_return", CubeListBuilder.create(), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition cube_r83 = fast_return.addOrReplaceChild("cube_r83", CubeListBuilder.create().texOffs(5, 100).addBox(2.6F, -20.5F, -3.85F, 3.0F, 2.0F, 3.0F, new CubeDeformation(-0.6F)), PartPose.offsetAndRotation(0.5F, 0.2F, -0.1F, 0.5236F, 0.0F, 0.0F));

		PartDefinition button = fast_return.addOrReplaceChild("button", CubeListBuilder.create().texOffs(50, 4).addBox(7.6F, -0.7268F, -0.1866F, 3.0F, 2.0F, 3.0F, new CubeDeformation(-0.8F)), PartPose.offsetAndRotation(-4.5F, -15.5F, -13.4F, 0.5236F, 0.0F, 0.0F));

		PartDefinition waypoints = controls_1.addOrReplaceChild("waypoints", CubeListBuilder.create(), PartPose.offset(0.0F, 0.1F, 0.3F));

		PartDefinition row_1 = waypoints.addOrReplaceChild("row_1", CubeListBuilder.create(), PartPose.offset(-4.5F, -15.5F, -13.5F));

		PartDefinition way_dial_1 = row_1.addOrReplaceChild("way_dial_1", CubeListBuilder.create().texOffs(5, 100).addBox(1.6F, -0.15F, 4.825F, 2.0F, 1.0F, 1.0F, new CubeDeformation(-0.25F))
		.texOffs(3, 40).addBox(2.85F, -0.3F, 4.975F, 1.0F, 2.0F, 1.0F, new CubeDeformation(-0.35F))
		.texOffs(3, 40).addBox(2.85F, -0.3F, 4.675F, 1.0F, 2.0F, 1.0F, new CubeDeformation(-0.35F))
		.texOffs(3, 40).addBox(1.275F, -0.3F, 4.975F, 1.0F, 2.0F, 1.0F, new CubeDeformation(-0.35F))
		.texOffs(3, 40).addBox(1.275F, -0.3F, 4.675F, 1.0F, 2.0F, 1.0F, new CubeDeformation(-0.35F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.5236F, 0.0F, 0.0F));

		PartDefinition way_dial_2 = row_1.addOrReplaceChild("way_dial_2", CubeListBuilder.create().texOffs(5, 100).addBox(1.6F, -0.15F, 4.825F, 2.0F, 1.0F, 1.0F, new CubeDeformation(-0.25F))
		.texOffs(3, 40).addBox(2.85F, -0.3F, 4.975F, 1.0F, 2.0F, 1.0F, new CubeDeformation(-0.35F))
		.texOffs(3, 40).addBox(2.85F, -0.3F, 4.675F, 1.0F, 2.0F, 1.0F, new CubeDeformation(-0.35F))
		.texOffs(3, 40).addBox(1.275F, -0.3F, 4.975F, 1.0F, 2.0F, 1.0F, new CubeDeformation(-0.35F))
		.texOffs(3, 40).addBox(1.275F, -0.3F, 4.675F, 1.0F, 2.0F, 1.0F, new CubeDeformation(-0.35F)), PartPose.offsetAndRotation(2.0F, 0.0F, 0.0F, 0.5236F, 0.0F, 0.0F));

		PartDefinition way_dial_3 = row_1.addOrReplaceChild("way_dial_3", CubeListBuilder.create().texOffs(5, 100).addBox(1.6F, -0.15F, 4.825F, 2.0F, 1.0F, 1.0F, new CubeDeformation(-0.25F))
		.texOffs(3, 40).addBox(2.85F, -0.3F, 4.975F, 1.0F, 2.0F, 1.0F, new CubeDeformation(-0.35F))
		.texOffs(3, 40).addBox(2.85F, -0.3F, 4.675F, 1.0F, 2.0F, 1.0F, new CubeDeformation(-0.35F))
		.texOffs(3, 40).addBox(1.275F, -0.3F, 4.975F, 1.0F, 2.0F, 1.0F, new CubeDeformation(-0.35F))
		.texOffs(3, 40).addBox(1.275F, -0.3F, 4.675F, 1.0F, 2.0F, 1.0F, new CubeDeformation(-0.35F)), PartPose.offsetAndRotation(4.0F, 0.0F, 0.0F, 0.5236F, 0.0F, 0.0F));

		PartDefinition row_2 = waypoints.addOrReplaceChild("row_2", CubeListBuilder.create(), PartPose.offset(-4.5F, -15.1F, -14.2F));

		PartDefinition way_dial_4 = row_2.addOrReplaceChild("way_dial_4", CubeListBuilder.create().texOffs(5, 100).addBox(1.6F, -0.15F, 4.825F, 2.0F, 1.0F, 1.0F, new CubeDeformation(-0.25F))
		.texOffs(3, 40).addBox(2.85F, -0.3F, 4.975F, 1.0F, 2.0F, 1.0F, new CubeDeformation(-0.35F))
		.texOffs(3, 40).addBox(2.85F, -0.3F, 4.675F, 1.0F, 2.0F, 1.0F, new CubeDeformation(-0.35F))
		.texOffs(3, 40).addBox(1.275F, -0.3F, 4.975F, 1.0F, 2.0F, 1.0F, new CubeDeformation(-0.35F))
		.texOffs(3, 40).addBox(1.275F, -0.3F, 4.675F, 1.0F, 2.0F, 1.0F, new CubeDeformation(-0.35F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.5236F, 0.0F, 0.0F));

		PartDefinition way_dial_5 = row_2.addOrReplaceChild("way_dial_5", CubeListBuilder.create().texOffs(5, 100).addBox(1.6F, -0.15F, 4.825F, 2.0F, 1.0F, 1.0F, new CubeDeformation(-0.25F))
		.texOffs(3, 40).addBox(2.85F, -0.3F, 4.975F, 1.0F, 2.0F, 1.0F, new CubeDeformation(-0.35F))
		.texOffs(3, 40).addBox(2.85F, -0.3F, 4.675F, 1.0F, 2.0F, 1.0F, new CubeDeformation(-0.35F))
		.texOffs(3, 40).addBox(1.275F, -0.3F, 4.975F, 1.0F, 2.0F, 1.0F, new CubeDeformation(-0.35F))
		.texOffs(3, 40).addBox(1.275F, -0.3F, 4.675F, 1.0F, 2.0F, 1.0F, new CubeDeformation(-0.35F)), PartPose.offsetAndRotation(2.0F, 0.0F, 0.0F, 0.5236F, 0.0F, 0.0F));

		PartDefinition way_dial_6 = row_2.addOrReplaceChild("way_dial_6", CubeListBuilder.create().texOffs(5, 100).addBox(1.6F, -0.15F, 4.825F, 2.0F, 1.0F, 1.0F, new CubeDeformation(-0.25F))
		.texOffs(3, 40).addBox(2.85F, -0.3F, 4.975F, 1.0F, 2.0F, 1.0F, new CubeDeformation(-0.35F))
		.texOffs(3, 40).addBox(2.85F, -0.3F, 4.675F, 1.0F, 2.0F, 1.0F, new CubeDeformation(-0.35F))
		.texOffs(3, 40).addBox(1.275F, -0.3F, 4.975F, 1.0F, 2.0F, 1.0F, new CubeDeformation(-0.35F))
		.texOffs(3, 40).addBox(1.275F, -0.3F, 4.675F, 1.0F, 2.0F, 1.0F, new CubeDeformation(-0.35F)), PartPose.offsetAndRotation(4.0F, 0.0F, 0.0F, 0.5236F, 0.0F, 0.0F));

		PartDefinition row_3 = waypoints.addOrReplaceChild("row_3", CubeListBuilder.create(), PartPose.offset(-4.5F, -14.7F, -14.9F));

		PartDefinition way_dial_7 = row_3.addOrReplaceChild("way_dial_7", CubeListBuilder.create().texOffs(5, 100).addBox(1.6F, -0.15F, 4.825F, 2.0F, 1.0F, 1.0F, new CubeDeformation(-0.25F))
		.texOffs(3, 40).addBox(2.85F, -0.3F, 4.975F, 1.0F, 2.0F, 1.0F, new CubeDeformation(-0.35F))
		.texOffs(3, 40).addBox(2.85F, -0.3F, 4.675F, 1.0F, 2.0F, 1.0F, new CubeDeformation(-0.35F))
		.texOffs(3, 40).addBox(1.275F, -0.3F, 4.975F, 1.0F, 2.0F, 1.0F, new CubeDeformation(-0.35F))
		.texOffs(3, 40).addBox(1.275F, -0.3F, 4.675F, 1.0F, 2.0F, 1.0F, new CubeDeformation(-0.35F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.5236F, 0.0F, 0.0F));

		PartDefinition way_dial_8 = row_3.addOrReplaceChild("way_dial_8", CubeListBuilder.create().texOffs(5, 100).addBox(1.6F, -0.15F, 4.825F, 2.0F, 1.0F, 1.0F, new CubeDeformation(-0.25F))
		.texOffs(3, 40).addBox(2.85F, -0.3F, 4.975F, 1.0F, 2.0F, 1.0F, new CubeDeformation(-0.35F))
		.texOffs(3, 40).addBox(2.85F, -0.3F, 4.675F, 1.0F, 2.0F, 1.0F, new CubeDeformation(-0.35F))
		.texOffs(3, 40).addBox(1.275F, -0.3F, 4.975F, 1.0F, 2.0F, 1.0F, new CubeDeformation(-0.35F))
		.texOffs(3, 40).addBox(1.275F, -0.3F, 4.675F, 1.0F, 2.0F, 1.0F, new CubeDeformation(-0.35F)), PartPose.offsetAndRotation(2.0F, 0.0F, 0.0F, 0.5236F, 0.0F, 0.0F));

		PartDefinition way_dial_9 = row_3.addOrReplaceChild("way_dial_9", CubeListBuilder.create().texOffs(5, 100).addBox(1.6F, -0.15F, 4.825F, 2.0F, 1.0F, 1.0F, new CubeDeformation(-0.25F))
		.texOffs(3, 40).addBox(2.85F, -0.3F, 4.975F, 1.0F, 2.0F, 1.0F, new CubeDeformation(-0.35F))
		.texOffs(3, 40).addBox(2.85F, -0.3F, 4.675F, 1.0F, 2.0F, 1.0F, new CubeDeformation(-0.35F))
		.texOffs(3, 40).addBox(1.275F, -0.3F, 4.975F, 1.0F, 2.0F, 1.0F, new CubeDeformation(-0.35F))
		.texOffs(3, 40).addBox(1.275F, -0.3F, 4.675F, 1.0F, 2.0F, 1.0F, new CubeDeformation(-0.35F)), PartPose.offsetAndRotation(4.0F, 0.0F, 0.0F, 0.5236F, 0.0F, 0.0F));

		PartDefinition glow_dummy_aa_1 = controls_1.addOrReplaceChild("glow_dummy_aa_1", CubeListBuilder.create().texOffs(110, 13).addBox(-1.0F, 0.2873F, 7.1975F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.2F)), PartPose.offsetAndRotation(-0.5F, -15.75F, -13.25F, 0.5236F, 0.0F, 0.0F));

		PartDefinition glow_dummy_aa_4 = controls_1.addOrReplaceChild("glow_dummy_aa_4", CubeListBuilder.create().texOffs(110, 13).addBox(-1.0F, 0.2873F, 6.3975F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.2F)), PartPose.offsetAndRotation(-0.5F, -15.75F, -13.25F, 0.5236F, 0.0F, 0.0F));

		PartDefinition glow_dummy_aa_2 = controls_1.addOrReplaceChild("glow_dummy_aa_2", CubeListBuilder.create().texOffs(110, 13).addBox(0.025F, 0.2873F, 7.1975F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.2F)), PartPose.offsetAndRotation(-0.5F, -15.75F, -13.25F, 0.5236F, 0.0F, 0.0F));

		PartDefinition glow_dummy_aa_5 = controls_1.addOrReplaceChild("glow_dummy_aa_5", CubeListBuilder.create().texOffs(110, 13).addBox(0.025F, 0.2873F, 6.3975F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.2F)), PartPose.offsetAndRotation(-0.5F, -15.75F, -13.25F, 0.5236F, 0.0F, 0.0F));

		PartDefinition glow_dummy_aa_3 = controls_1.addOrReplaceChild("glow_dummy_aa_3", CubeListBuilder.create().texOffs(110, 13).addBox(1.125F, 0.2873F, 7.1975F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.2F)), PartPose.offsetAndRotation(-0.5F, -15.75F, -13.25F, 0.5236F, 0.0F, 0.0F));

		PartDefinition glow_dummy_aa_6 = controls_1.addOrReplaceChild("glow_dummy_aa_6", CubeListBuilder.create().texOffs(110, 13).addBox(1.125F, 0.2873F, 6.3975F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.2F)), PartPose.offsetAndRotation(-0.5F, -15.75F, -13.25F, 0.5236F, 0.0F, 0.0F));

		PartDefinition decoration_1 = controls_1.addOrReplaceChild("decoration_1", CubeListBuilder.create().texOffs(3, 40).addBox(2.55F, 0.1F, 6.15F, 4.0F, 1.0F, 1.0F, new CubeDeformation(-0.35F))
		.texOffs(3, 40).addBox(2.55F, 0.1F, 8.15F, 4.0F, 1.0F, 1.0F, new CubeDeformation(-0.35F))
		.texOffs(3, 40).addBox(2.25F, 0.1F, 6.15F, 1.0F, 1.0F, 3.0F, new CubeDeformation(-0.35F))
		.texOffs(3, 40).addBox(5.85F, 0.1F, 6.15F, 1.0F, 1.0F, 3.0F, new CubeDeformation(-0.35F)), PartPose.offsetAndRotation(-4.5F, -15.5F, -13.5F, 0.5236F, 0.0F, 0.0F));

		PartDefinition dummy_aa_7 = controls_1.addOrReplaceChild("dummy_aa_7", CubeListBuilder.create().texOffs(5, 100).addBox(-0.5F, 0.425F, 0.5F, 1.0F, 1.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(-3.65F, -15.65F, -13.25F, 0.5236F, 0.0F, 0.0F));

		PartDefinition toggle_aa_7_rotate_x = dummy_aa_7.addOrReplaceChild("toggle_aa_7_rotate_x", CubeListBuilder.create().texOffs(3, 40).addBox(-0.5F, -0.5723F, -0.585F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.2F))
		.texOffs(3, 40).addBox(-0.66F, -1.3723F, -0.585F, 1.0F, 2.0F, 1.0F, new CubeDeformation(-0.42F))
		.texOffs(3, 40).addBox(-0.5F, -1.3723F, -0.585F, 1.0F, 2.0F, 1.0F, new CubeDeformation(-0.42F))
		.texOffs(3, 40).addBox(-0.34F, -1.3723F, -0.585F, 1.0F, 2.0F, 1.0F, new CubeDeformation(-0.42F)), PartPose.offsetAndRotation(0.0F, 0.4917F, 1.0741F, -0.5236F, 0.0F, 0.0F));

		PartDefinition dummy_aa_8 = controls_1.addOrReplaceChild("dummy_aa_8", CubeListBuilder.create().texOffs(5, 100).addBox(-0.5F, 0.425F, 0.5F, 1.0F, 1.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(-2.15F, -15.65F, -13.25F, 0.5236F, 0.0F, 0.0F));

		PartDefinition toggle_aa_8_rotate_x = dummy_aa_8.addOrReplaceChild("toggle_aa_8_rotate_x", CubeListBuilder.create().texOffs(3, 40).addBox(-0.5F, -0.5723F, -0.585F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.2F))
		.texOffs(3, 40).addBox(-0.66F, -1.3723F, -0.585F, 1.0F, 2.0F, 1.0F, new CubeDeformation(-0.42F))
		.texOffs(3, 40).addBox(-0.5F, -1.3723F, -0.585F, 1.0F, 2.0F, 1.0F, new CubeDeformation(-0.42F))
		.texOffs(3, 40).addBox(-0.34F, -1.3723F, -0.585F, 1.0F, 2.0F, 1.0F, new CubeDeformation(-0.42F)), PartPose.offsetAndRotation(0.0F, 0.4917F, 1.0741F, -0.5236F, 0.0F, 0.0F));

		PartDefinition dummy_aa_9 = controls_1.addOrReplaceChild("dummy_aa_9", CubeListBuilder.create().texOffs(5, 100).addBox(-0.5F, 0.425F, 0.5F, 1.0F, 1.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(-0.65F, -15.65F, -13.25F, 0.5236F, 0.0F, 0.0F));

		PartDefinition toggle_aa_9_rotate_x = dummy_aa_9.addOrReplaceChild("toggle_aa_9_rotate_x", CubeListBuilder.create().texOffs(3, 40).addBox(-0.5F, -0.5723F, -0.585F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.2F))
		.texOffs(3, 40).addBox(-0.66F, -1.3723F, -0.585F, 1.0F, 2.0F, 1.0F, new CubeDeformation(-0.42F))
		.texOffs(3, 40).addBox(-0.5F, -1.3723F, -0.585F, 1.0F, 2.0F, 1.0F, new CubeDeformation(-0.42F))
		.texOffs(3, 40).addBox(-0.34F, -1.3723F, -0.585F, 1.0F, 2.0F, 1.0F, new CubeDeformation(-0.42F)), PartPose.offsetAndRotation(0.0F, 0.4917F, 1.0741F, -0.5236F, 0.0F, 0.0F));

		PartDefinition dummy_aa_10 = controls_1.addOrReplaceChild("dummy_aa_10", CubeListBuilder.create().texOffs(5, 100).addBox(-0.5F, 0.425F, 0.5F, 1.0F, 1.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.85F, -15.65F, -13.25F, 0.5236F, 0.0F, 0.0F));

		PartDefinition toggle_aa_10_rotate_x = dummy_aa_10.addOrReplaceChild("toggle_aa_10_rotate_x", CubeListBuilder.create().texOffs(3, 40).addBox(-0.5F, -0.5723F, -0.585F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.2F))
		.texOffs(3, 40).addBox(-0.66F, -1.3723F, -0.585F, 1.0F, 2.0F, 1.0F, new CubeDeformation(-0.42F))
		.texOffs(3, 40).addBox(-0.5F, -1.3723F, -0.585F, 1.0F, 2.0F, 1.0F, new CubeDeformation(-0.42F))
		.texOffs(3, 40).addBox(-0.34F, -1.3723F, -0.585F, 1.0F, 2.0F, 1.0F, new CubeDeformation(-0.42F)), PartPose.offsetAndRotation(0.0F, 0.4917F, 1.0741F, -0.5236F, 0.0F, 0.0F));

		PartDefinition dummy_aa_11 = controls_1.addOrReplaceChild("dummy_aa_11", CubeListBuilder.create().texOffs(5, 100).addBox(-0.5F, 0.425F, 0.5F, 1.0F, 1.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(2.35F, -15.65F, -13.25F, 0.5236F, 0.0F, 0.0F));

		PartDefinition toggle_aa_11_rotate_x = dummy_aa_11.addOrReplaceChild("toggle_aa_11_rotate_x", CubeListBuilder.create().texOffs(3, 40).addBox(-0.5F, -0.5723F, -0.585F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.2F))
		.texOffs(3, 40).addBox(-0.66F, -1.3723F, -0.585F, 1.0F, 2.0F, 1.0F, new CubeDeformation(-0.42F))
		.texOffs(3, 40).addBox(-0.5F, -1.3723F, -0.585F, 1.0F, 2.0F, 1.0F, new CubeDeformation(-0.42F))
		.texOffs(3, 40).addBox(-0.34F, -1.3723F, -0.585F, 1.0F, 2.0F, 1.0F, new CubeDeformation(-0.42F)), PartPose.offsetAndRotation(0.0F, 0.4917F, 1.0741F, -0.5236F, 0.0F, 0.0F));

		PartDefinition controls_2 = controls.addOrReplaceChild("controls_2", CubeListBuilder.create(), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.0F, 1.0472F, 0.0F));

		PartDefinition stablizers = controls_2.addOrReplaceChild("stablizers", CubeListBuilder.create().texOffs(5, 100).addBox(6.375F, -0.4F, 0.6F, 3.0F, 2.0F, 2.0F, new CubeDeformation(-0.7F))
		.texOffs(3, 40).addBox(6.375F, -0.225F, 0.625F, 3.0F, 2.0F, 2.0F, new CubeDeformation(-0.6F)), PartPose.offsetAndRotation(-3.6F, -15.5F, -13.5F, 0.5236F, 0.0F, 0.0F));

		PartDefinition stabilizer_offset_y = stablizers.addOrReplaceChild("stabilizer_offset_y", CubeListBuilder.create().texOffs(50, 95).addBox(-1.5F, -1.0F, -1.0F, 3.0F, 2.0F, 2.0F, new CubeDeformation(-0.8F)), PartPose.offset(7.875F, 0.1F, 1.6F));

		PartDefinition telpathic_circuits = controls_2.addOrReplaceChild("telpathic_circuits", CubeListBuilder.create(), PartPose.offset(-0.25F, 0.0F, 0.0F));

		PartDefinition tp_screen = telpathic_circuits.addOrReplaceChild("tp_screen", CubeListBuilder.create().texOffs(40, 104).addBox(-0.45F, -0.325F, 2.7F, 4.0F, 1.0F, 2.0F, new CubeDeformation(-0.17F))
		.texOffs(40, 104).addBox(-2.875F, -0.325F, 2.7F, 3.0F, 1.0F, 2.0F, new CubeDeformation(-0.17F))
		.texOffs(40, 104).addBox(-2.875F, -1.675F, 2.7F, 3.0F, 1.0F, 2.0F, new CubeDeformation(-0.17F))
		.texOffs(40, 104).addBox(-0.45F, -1.675F, 2.7F, 4.0F, 1.0F, 2.0F, new CubeDeformation(-0.17F))
		.texOffs(40, 104).addBox(-2.425F, -1.0F, 2.7F, 4.0F, 1.0F, 2.0F, new CubeDeformation(-0.17F))
		.texOffs(40, 104).addBox(1.55F, -1.0F, 2.7F, 2.0F, 1.0F, 2.0F, new CubeDeformation(-0.17F))
		.texOffs(106, 13).addBox(-2.195F, -0.32F, 2.7F, 1.0F, 1.0F, 2.0F, new CubeDeformation(-0.16F)), PartPose.offsetAndRotation(-0.1F, -20.2F, -10.3F, -1.0472F, 0.0F, 0.0F));

		PartDefinition glow_tp = tp_screen.addOrReplaceChild("glow_tp", CubeListBuilder.create().texOffs(106, 13).addBox(-1.975F, -22.1F, -7.6F, 1.0F, 1.0F, 2.0F, new CubeDeformation(-0.16F))
		.texOffs(106, 13).addBox(-2.655F, -21.42F, -7.6F, 1.0F, 1.0F, 2.0F, new CubeDeformation(-0.16F))
		.texOffs(106, 13).addBox(-1.295F, -21.42F, -7.6F, 1.0F, 1.0F, 2.0F, new CubeDeformation(-0.16F))
		.texOffs(106, 13).addBox(-0.615F, -22.1F, -7.6F, 1.0F, 1.0F, 2.0F, new CubeDeformation(-0.16F))
		.texOffs(106, 13).addBox(-0.615F, -20.74F, -7.6F, 1.0F, 1.0F, 2.0F, new CubeDeformation(-0.16F))
		.texOffs(106, 13).addBox(0.065F, -21.42F, -7.6F, 1.0F, 1.0F, 2.0F, new CubeDeformation(-0.16F))
		.texOffs(106, 13).addBox(0.745F, -22.1F, -7.6F, 1.0F, 1.0F, 2.0F, new CubeDeformation(-0.16F))
		.texOffs(106, 13).addBox(1.425F, -21.42F, -7.6F, 1.0F, 1.0F, 2.0F, new CubeDeformation(-0.16F))
		.texOffs(106, 13).addBox(0.745F, -20.74F, -7.6F, 1.0F, 1.0F, 2.0F, new CubeDeformation(-0.16F))
		.texOffs(106, 13).addBox(2.105F, -22.1F, -7.6F, 1.0F, 1.0F, 2.0F, new CubeDeformation(-0.16F))
		.texOffs(106, 13).addBox(2.785F, -21.42F, -7.6F, 1.0F, 1.0F, 2.0F, new CubeDeformation(-0.16F))
		.texOffs(106, 13).addBox(2.105F, -20.74F, -7.6F, 1.0F, 1.0F, 2.0F, new CubeDeformation(-0.16F)), PartPose.offset(-0.22F, 20.42F, 10.3F));

		PartDefinition tp_frame = tp_screen.addOrReplaceChild("tp_frame", CubeListBuilder.create().texOffs(3, 40).addBox(1.95F, -22.675F, -8.75F, 2.0F, 2.0F, 3.0F, new CubeDeformation(-0.9F))
		.texOffs(3, 40).addBox(2.15F, -22.675F, -8.75F, 2.0F, 2.0F, 3.0F, new CubeDeformation(-0.9F))
		.texOffs(3, 40).addBox(1.95F, -20.675F, -8.75F, 2.0F, 2.0F, 3.0F, new CubeDeformation(-0.9F))
		.texOffs(3, 40).addBox(2.15F, -20.675F, -8.75F, 2.0F, 2.0F, 3.0F, new CubeDeformation(-0.9F))
		.texOffs(3, 40).addBox(-3.65F, -22.675F, -8.75F, 2.0F, 2.0F, 3.0F, new CubeDeformation(-0.9F))
		.texOffs(3, 40).addBox(-3.45F, -22.675F, -8.75F, 2.0F, 2.0F, 3.0F, new CubeDeformation(-0.9F))
		.texOffs(3, 40).addBox(-3.45F, -20.675F, -8.75F, 2.0F, 2.0F, 3.0F, new CubeDeformation(-0.9F))
		.texOffs(3, 40).addBox(-3.65F, -20.675F, -8.75F, 2.0F, 2.0F, 3.0F, new CubeDeformation(-0.9F))
		.texOffs(3, 40).addBox(2.35F, -22.675F, -8.75F, 2.0F, 4.0F, 3.0F, new CubeDeformation(-0.9F))
		.texOffs(3, 40).addBox(-3.85F, -22.675F, -8.75F, 2.0F, 4.0F, 3.0F, new CubeDeformation(-0.9F))
		.texOffs(3, 40).addBox(-3.25F, -20.625F, -8.75F, 7.0F, 2.0F, 3.0F, new CubeDeformation(-0.9F))
		.texOffs(3, 40).addBox(-3.25F, -22.8F, -8.75F, 7.0F, 2.0F, 3.0F, new CubeDeformation(-0.9F)), PartPose.offset(0.1F, 20.2F, 10.6F));

		PartDefinition eye = tp_frame.addOrReplaceChild("eye", CubeListBuilder.create().texOffs(3, 40).addBox(-1.825F, -23.075F, -7.875F, 4.0F, 1.0F, 1.0F, new CubeDeformation(-0.25F))
		.texOffs(3, 40).addBox(-0.825F, -23.65F, -7.95F, 2.0F, 2.0F, 1.0F, new CubeDeformation(-0.25F))
		.texOffs(5, 103).addBox(-0.825F, -23.65F, -8.125F, 2.0F, 2.0F, 1.0F, new CubeDeformation(-0.4F))
		.texOffs(41, 104).addBox(-0.825F, -23.65F, -8.375F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.5F))
		.texOffs(3, 40).addBox(-0.825F, -23.65F, -8.65F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.7F))
		.texOffs(5, 103).addBox(-0.125F, -23.65F, -8.625F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.9F))
		.texOffs(5, 103).addBox(-0.125F, -23.85F, -8.625F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.9F))
		.texOffs(5, 103).addBox(-0.125F, -23.45F, -8.625F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.9F))
		.texOffs(5, 103).addBox(-1.525F, -23.65F, -8.625F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.9F))
		.texOffs(5, 103).addBox(-1.525F, -23.85F, -8.625F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.9F))
		.texOffs(5, 103).addBox(-1.525F, -23.45F, -8.625F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.9F))
		.texOffs(3, 40).addBox(-1.675F, -24.1F, -8.575F, 2.0F, 3.0F, 2.0F, new CubeDeformation(-0.9F))
		.texOffs(3, 40).addBox(0.025F, -24.1F, -8.575F, 2.0F, 3.0F, 2.0F, new CubeDeformation(-0.9F))
		.texOffs(3, 40).addBox(-1.875F, -23.225F, -8.525F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.9F))
		.texOffs(2, 40).addBox(0.225F, -23.925F, -8.525F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.9F))
		.texOffs(2, 40).addBox(0.225F, -23.225F, -8.525F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.9F)), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition door = controls_2.addOrReplaceChild("door", CubeListBuilder.create(), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition keyhole_base = door.addOrReplaceChild("keyhole_base", CubeListBuilder.create().texOffs(5, 100).addBox(-1.1F, -1.0F, -0.7F, 2.0F, 2.0F, 3.0F, new CubeDeformation(-0.95F))
		.texOffs(5, 100).addBox(-1.0F, -1.0F, -0.7F, 2.0F, 2.0F, 3.0F, new CubeDeformation(-0.95F))
		.texOffs(5, 100).addBox(-0.3F, -1.0F, -0.7F, 2.0F, 2.0F, 3.0F, new CubeDeformation(-0.95F))
		.texOffs(5, 100).addBox(-0.85F, -0.95F, 0.25F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.9F))
		.texOffs(5, 100).addBox(-0.65F, -0.95F, 0.25F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.9F))
		.texOffs(5, 100).addBox(-0.45F, -0.95F, 0.25F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.9F))
		.texOffs(5, 100).addBox(-0.85F, -0.95F, 0.45F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.9F))
		.texOffs(5, 100).addBox(-0.65F, -0.95F, 0.45F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.9F))
		.texOffs(5, 100).addBox(-0.45F, -0.95F, 0.45F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.9F))
		.texOffs(5, 100).addBox(-0.85F, -0.95F, -0.3F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.9F))
		.texOffs(5, 100).addBox(-0.85F, -0.95F, -0.5F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.9F))
		.texOffs(5, 100).addBox(-0.85F, -0.95F, -0.7F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.9F))
		.texOffs(5, 100).addBox(-0.45F, -0.95F, -0.7F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.9F))
		.texOffs(5, 100).addBox(-0.45F, -0.95F, -0.5F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.9F))
		.texOffs(5, 100).addBox(-0.45F, -0.95F, -0.3F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.9F))
		.texOffs(5, 100).addBox(-0.85F, -0.95F, -0.9F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.9F))
		.texOffs(5, 100).addBox(-0.65F, -0.95F, -0.9F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.9F))
		.texOffs(5, 100).addBox(-0.45F, -0.95F, -0.9F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.9F))
		.texOffs(5, 100).addBox(-0.2F, -1.0F, -0.7F, 2.0F, 2.0F, 3.0F, new CubeDeformation(-0.95F)), PartPose.offsetAndRotation(-4.925F, -15.3F, -12.9F, 0.5236F, 0.0F, 0.0F));

		PartDefinition bone = keyhole_base.addOrReplaceChild("bone", CubeListBuilder.create().texOffs(3, 40).addBox(-4.4F, -16.35F, -11.8F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.8F))
		.texOffs(3, 40).addBox(-4.8F, -16.35F, -11.8F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.8F))
		.texOffs(3, 40).addBox(-5.1F, -16.45F, -13.1F, 3.0F, 2.0F, 3.0F, new CubeDeformation(-0.9F))
		.texOffs(3, 40).addBox(-4.4F, -16.35F, -13.4F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.8F))
		.texOffs(3, 40).addBox(-4.8F, -16.35F, -13.4F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.8F)), PartPose.offset(3.95F, 15.55F, 12.4F));

		PartDefinition key_rotate_y = keyhole_base.addOrReplaceChild("key_rotate_y", CubeListBuilder.create().texOffs(3, 40).addBox(-1.0F, -0.83F, -1.0F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.9F))
		.texOffs(3, 40).addBox(-1.0F, 0.17F, -1.0F, 2.0F, 1.0F, 2.0F, new CubeDeformation(-0.9F))
		.texOffs(3, 40).addBox(-1.2F, -0.83F, -1.0F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.9F))
		.texOffs(3, 40).addBox(-0.8F, -0.83F, -1.0F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.9F))
		.texOffs(3, 40).addBox(-0.8F, -1.03F, -1.0F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.9F))
		.texOffs(3, 40).addBox(-0.8F, -1.23F, -1.0F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.9F))
		.texOffs(3, 40).addBox(-1.0F, -1.23F, -1.0F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.9F))
		.texOffs(3, 40).addBox(-1.0F, -1.43F, -1.0F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.9F))
		.texOffs(3, 40).addBox(-1.2F, -1.03F, -1.0F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.9F))
		.texOffs(3, 40).addBox(-1.2F, -1.23F, -1.0F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.9F)), PartPose.offsetAndRotation(0.3536F, -0.736F, 0.9634F, 0.0F, 0.5236F, 0.0F));

		PartDefinition dummy_b_1 = controls_2.addOrReplaceChild("dummy_b_1", CubeListBuilder.create(), PartPose.offsetAndRotation(-1.75F, -15.2F, -12.0F, 0.5236F, 0.0F, 0.0F));

		PartDefinition toggle_b_1_rotate_x = dummy_b_1.addOrReplaceChild("toggle_b_1_rotate_x", CubeListBuilder.create(), PartPose.offsetAndRotation(0.0F, -0.2F, 0.1778F, -0.3491F, 0.0F, 0.0F));

		PartDefinition togglebottom_b1 = toggle_b_1_rotate_x.addOrReplaceChild("togglebottom_b1", CubeListBuilder.create().texOffs(5, 100).addBox(-0.75F, -0.4F, -0.5F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.2F)), PartPose.offsetAndRotation(0.25F, -0.1364F, -0.2535F, -0.3491F, 0.0F, 0.0F));

		PartDefinition toggletop_b1 = toggle_b_1_rotate_x.addOrReplaceChild("toggletop_b1", CubeListBuilder.create().texOffs(5, 100).addBox(-0.75F, -0.4F, -0.25F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.2F)), PartPose.offsetAndRotation(0.25F, -0.1364F, -0.2535F, 0.2618F, 0.0F, 0.0F));

		PartDefinition dummy_b_2 = controls_2.addOrReplaceChild("dummy_b_2", CubeListBuilder.create(), PartPose.offsetAndRotation(-0.65F, -15.2F, -12.0F, 0.5236F, 0.0F, 0.0F));

		PartDefinition toggle_b_1_rotate_x2 = dummy_b_2.addOrReplaceChild("toggle_b_1_rotate_x2", CubeListBuilder.create(), PartPose.offsetAndRotation(0.0F, -0.2F, 0.1778F, -0.3491F, 0.0F, 0.0F));

		PartDefinition togglebottom_b2 = toggle_b_1_rotate_x2.addOrReplaceChild("togglebottom_b2", CubeListBuilder.create().texOffs(5, 100).addBox(-0.75F, -0.4F, -0.5F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.2F)), PartPose.offsetAndRotation(0.25F, -0.1364F, -0.2535F, -0.3491F, 0.0F, 0.0F));

		PartDefinition toggletop_b2 = toggle_b_1_rotate_x2.addOrReplaceChild("toggletop_b2", CubeListBuilder.create().texOffs(5, 100).addBox(-0.75F, -0.4F, -0.25F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.2F)), PartPose.offsetAndRotation(0.25F, -0.1364F, -0.2535F, 0.2618F, 0.0F, 0.0F));

		PartDefinition dummy_b_3 = controls_2.addOrReplaceChild("dummy_b_3", CubeListBuilder.create(), PartPose.offsetAndRotation(0.55F, -15.2F, -12.0F, 0.5236F, 0.0F, 0.0F));

		PartDefinition toggle_b_1_rotate_x3 = dummy_b_3.addOrReplaceChild("toggle_b_1_rotate_x3", CubeListBuilder.create(), PartPose.offsetAndRotation(0.0F, -0.2F, 0.1778F, 0.1745F, 0.0F, 0.0F));

		PartDefinition togglebottom_b3 = toggle_b_1_rotate_x3.addOrReplaceChild("togglebottom_b3", CubeListBuilder.create().texOffs(5, 100).addBox(-0.75F, -0.4F, -0.5F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.2F)), PartPose.offsetAndRotation(0.25F, -0.1364F, -0.2535F, -0.3491F, 0.0F, 0.0F));

		PartDefinition toggletop_b3 = toggle_b_1_rotate_x3.addOrReplaceChild("toggletop_b3", CubeListBuilder.create().texOffs(5, 100).addBox(-0.75F, -0.4F, -0.25F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.2F)), PartPose.offsetAndRotation(0.25F, -0.1364F, -0.2535F, 0.2618F, 0.0F, 0.0F));

		PartDefinition dummy_b_4 = controls_2.addOrReplaceChild("dummy_b_4", CubeListBuilder.create(), PartPose.offsetAndRotation(1.65F, -15.2F, -12.0F, 0.5236F, 0.0F, 0.0F));

		PartDefinition toggle_b_1_rotate_x4 = dummy_b_4.addOrReplaceChild("toggle_b_1_rotate_x4", CubeListBuilder.create(), PartPose.offsetAndRotation(0.0F, -0.2F, 0.1778F, -0.3491F, 0.0F, 0.0F));

		PartDefinition togglebottom_b4 = toggle_b_1_rotate_x4.addOrReplaceChild("togglebottom_b4", CubeListBuilder.create().texOffs(5, 100).addBox(-0.75F, -0.4F, -0.5F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.2F)), PartPose.offsetAndRotation(0.25F, -0.1364F, -0.2535F, -0.3491F, 0.0F, 0.0F));

		PartDefinition toggletop_b4 = toggle_b_1_rotate_x4.addOrReplaceChild("toggletop_b4", CubeListBuilder.create().texOffs(5, 100).addBox(-0.75F, -0.4F, -0.25F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.2F)), PartPose.offsetAndRotation(0.25F, -0.1364F, -0.2535F, 0.2618F, 0.0F, 0.0F));

		PartDefinition dummy_b_5 = controls_2.addOrReplaceChild("dummy_b_5", CubeListBuilder.create().texOffs(3, 40).addBox(2.25F, 0.0F, 2.75F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.1F)), PartPose.offsetAndRotation(-4.5F, -15.5F, -13.5F, 0.5236F, 0.0F, 0.0F));

		PartDefinition dummy_b_6 = controls_2.addOrReplaceChild("dummy_b_6", CubeListBuilder.create().texOffs(3, 40).addBox(4.0F, 0.0F, 2.75F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.1F)), PartPose.offsetAndRotation(-4.5F, -15.5F, -13.5F, 0.5236F, 0.0F, 0.0F));

		PartDefinition dummy_b_7 = controls_2.addOrReplaceChild("dummy_b_7", CubeListBuilder.create().texOffs(3, 40).addBox(5.75F, 0.0F, 2.75F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.1F)), PartPose.offsetAndRotation(-4.5F, -15.5F, -13.5F, 0.5236F, 0.0F, 0.0F));

		PartDefinition decoration_2 = controls_2.addOrReplaceChild("decoration_2", CubeListBuilder.create().texOffs(3, 40).addBox(1.75F, 0.1F, -0.1F, 5.0F, 1.0F, 1.0F, new CubeDeformation(-0.35F))
		.texOffs(3, 40).addBox(1.75F, 0.1F, 1.5F, 5.0F, 1.0F, 1.0F, new CubeDeformation(-0.35F))
		.texOffs(3, 40).addBox(1.45F, 0.1F, 0.2F, 1.0F, 1.0F, 2.0F, new CubeDeformation(-0.35F))
		.texOffs(2, 40).addBox(6.05F, 0.1F, 0.2F, 1.0F, 1.0F, 2.0F, new CubeDeformation(-0.35F))
		.texOffs(3, 40).addBox(6.05F, 0.1F, -0.1F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.35F))
		.texOffs(3, 40).addBox(1.45F, 0.1F, -0.1F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.35F))
		.texOffs(3, 40).addBox(6.05F, 0.1F, 1.5F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.35F))
		.texOffs(3, 40).addBox(1.45F, 0.1F, 1.5F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.35F)), PartPose.offsetAndRotation(-4.3F, -15.5F, -13.5F, 0.5236F, 0.0F, 0.0F));

		PartDefinition controls_3 = controls.addOrReplaceChild("controls_3", CubeListBuilder.create(), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.0F, 2.0944F, 0.0F));

		PartDefinition refueler = controls_3.addOrReplaceChild("refueler", CubeListBuilder.create(), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition dial_base = refueler.addOrReplaceChild("dial_base", CubeListBuilder.create().texOffs(3, 40).addBox(-1.0F, -0.05F, -1.0F, 2.0F, 1.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(3.75F, -15.75F, -12.25F, 0.6109F, 0.0F, 0.0F));

		PartDefinition round = dial_base.addOrReplaceChild("round", CubeListBuilder.create().texOffs(5, 100).addBox(3.25F, -15.65F, -12.8F, 1.0F, 1.0F, 1.0F, new CubeDeformation(0.2F))
		.texOffs(5, 100).addBox(1.95F, -16.75F, -12.9F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.9F))
		.texOffs(5, 100).addBox(1.95F, -16.75F, -13.1F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.9F))
		.texOffs(5, 100).addBox(1.95F, -16.75F, -13.5F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.9F))
		.texOffs(5, 100).addBox(1.95F, -16.75F, -13.3F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.9F))
		.texOffs(5, 100).addBox(1.95F, -16.75F, -13.7F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.9F))
		.texOffs(5, 100).addBox(3.55F, -16.75F, -12.9F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.9F))
		.texOffs(5, 100).addBox(3.15F, -16.75F, -12.5F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.9F))
		.texOffs(5, 100).addBox(2.95F, -16.75F, -12.5F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.9F))
		.texOffs(5, 100).addBox(2.75F, -16.75F, -12.5F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.9F))
		.texOffs(5, 100).addBox(2.55F, -16.75F, -12.5F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.9F))
		.texOffs(5, 100).addBox(2.35F, -16.75F, -12.5F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.9F))
		.texOffs(3, 40).addBox(2.35F, -16.85F, -13.85F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.9F))
		.texOffs(3, 40).addBox(2.55F, -16.85F, -13.85F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.9F))
		.texOffs(3, 40).addBox(2.75F, -16.85F, -13.85F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.9F))
		.texOffs(3, 40).addBox(2.95F, -16.85F, -13.85F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.9F))
		.texOffs(3, 40).addBox(3.15F, -16.85F, -13.85F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.9F))
		.texOffs(3, 40).addBox(2.25F, -16.85F, -14.05F, 3.0F, 2.0F, 2.0F, new CubeDeformation(-0.9F))
		.texOffs(5, 100).addBox(3.55F, -16.75F, -13.1F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.9F))
		.texOffs(5, 100).addBox(3.55F, -16.75F, -13.3F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.9F))
		.texOffs(5, 100).addBox(3.55F, -16.75F, -13.5F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.9F))
		.texOffs(5, 100).addBox(3.55F, -16.75F, -13.7F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.9F)), PartPose.offset(-3.75F, 15.75F, 12.25F));

		PartDefinition curve = dial_base.addOrReplaceChild("curve", CubeListBuilder.create().texOffs(5, 100).addBox(4.5F, -16.5F, -13.225F, 0.0F, 1.0F, 0.0F, new CubeDeformation(0.0F))
		.texOffs(5, 100).addBox(3.25F, -16.5F, -13.225F, 0.0F, 1.0F, 0.0F, new CubeDeformation(0.0F)), PartPose.offset(-3.75F, 15.75F, 12.25F));

		PartDefinition pointer_rotate_y = dial_base.addOrReplaceChild("pointer_rotate_y", CubeListBuilder.create().texOffs(56, 4).addBox(-1.0F, -0.85F, -0.825F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.9F))
		.texOffs(56, 4).addBox(-1.0F, -0.85F, -0.625F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.9F))
		.texOffs(56, 4).addBox(-1.0F, -0.85F, -0.425F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.9F))
		.texOffs(56, 4).addBox(-1.0F, -0.85F, -0.225F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.9F))
		.texOffs(56, 4).addBox(-1.0F, -0.85F, -0.025F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.9F)), PartPose.offsetAndRotation(0.025F, -0.2F, -0.675F, 0.0F, -0.829F, 0.0F));

		PartDefinition sonic_port = controls_3.addOrReplaceChild("sonic_port", CubeListBuilder.create(), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition sonic_base = sonic_port.addOrReplaceChild("sonic_base", CubeListBuilder.create().texOffs(3, 40).addBox(-0.5F, 0.1995F, -0.3905F, 1.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(4, 88).addBox(-1.35F, -0.7255F, -1.5405F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.85F))
		.texOffs(4, 88).addBox(-1.0F, -0.6755F, -0.1905F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.8F))
		.texOffs(4, 88).addBox(-0.65F, -0.7255F, -0.2405F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.85F))
		.texOffs(4, 88).addBox(-1.35F, -0.7255F, -0.2405F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.85F))
		.texOffs(4, 88).addBox(-1.0F, -0.6755F, -1.5905F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.8F))
		.texOffs(4, 88).addBox(-0.65F, -0.7255F, -1.5405F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.85F))
		.texOffs(4, 88).addBox(-0.35F, -0.7255F, -1.3905F, 2.0F, 2.0F, 3.0F, new CubeDeformation(-0.85F))
		.texOffs(4, 88).addBox(-3.3F, -0.5755F, -0.9155F, 4.0F, 2.0F, 2.0F, new CubeDeformation(-0.8F))
		.texOffs(4, 88).addBox(-2.9F, -0.7255F, -0.6155F, 3.0F, 2.0F, 2.0F, new CubeDeformation(-0.9F))
		.texOffs(4, 88).addBox(-2.9F, -0.7255F, -1.2155F, 3.0F, 2.0F, 2.0F, new CubeDeformation(-0.9F))
		.texOffs(4, 88).addBox(-0.75F, -0.5755F, -0.9155F, 4.0F, 2.0F, 2.0F, new CubeDeformation(-0.8F))
		.texOffs(4, 88).addBox(-0.15F, -0.7255F, -1.2155F, 3.0F, 2.0F, 2.0F, new CubeDeformation(-0.9F))
		.texOffs(4, 88).addBox(-0.15F, -0.7255F, -0.6155F, 3.0F, 2.0F, 2.0F, new CubeDeformation(-0.9F))
		.texOffs(4, 88).addBox(-1.65F, -0.7255F, -1.3905F, 2.0F, 2.0F, 3.0F, new CubeDeformation(-0.85F)), PartPose.offsetAndRotation(0.0F, -18.25F, -8.25F, 0.5236F, 0.0F, 0.0F));

		PartDefinition bone2 = sonic_base.addOrReplaceChild("bone2", CubeListBuilder.create().texOffs(4, 103).addBox(-1.08F, -18.43F, -9.92F, 2.0F, 2.0F, 3.0F, new CubeDeformation(-0.9F))
		.texOffs(4, 103).addBox(-0.04F, -18.43F, -9.92F, 2.0F, 2.0F, 3.0F, new CubeDeformation(-0.9F))
		.texOffs(4, 103).addBox(-0.27F, -18.4F, -9.95F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.87F))
		.texOffs(4, 103).addBox(-0.56F, -18.37F, -9.98F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.84F))
		.texOffs(4, 103).addBox(-0.85F, -18.4F, -9.95F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.87F))
		.texOffs(4, 103).addBox(-0.27F, -18.4F, -8.875F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.87F))
		.texOffs(4, 103).addBox(-0.56F, -18.37F, -8.845F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.84F))
		.texOffs(4, 103).addBox(-0.85F, -18.4F, -8.875F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.87F)), PartPose.offset(-0.45F, 17.5545F, 8.5295F));

		PartDefinition randomizer = controls_3.addOrReplaceChild("randomizer", CubeListBuilder.create(), PartPose.offset(0.0F, 0.15F, 0.725F));

		PartDefinition rando_plate = randomizer.addOrReplaceChild("rando_plate", CubeListBuilder.create().texOffs(5, 100).addBox(-3.0F, -10.25F, -2.35F, 2.0F, 1.0F, 2.0F, new CubeDeformation(0.0F))
		.texOffs(55, 2).addBox(-2.5F, -10.5F, -1.85F, 1.0F, 1.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(-2.05F, -7.825F, -6.525F, 0.5236F, 0.0F, 0.0F));

		PartDefinition randomizer_rotate_y = rando_plate.addOrReplaceChild("randomizer_rotate_y", CubeListBuilder.create().texOffs(3, 40).addBox(-1.0F, -0.4F, -1.0F, 2.0F, 1.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(-2.0F, -9.75F, -1.35F, 0.0F, -0.7854F, 0.0F));

		PartDefinition dummy_c_1 = controls_3.addOrReplaceChild("dummy_c_1", CubeListBuilder.create().texOffs(5, 100).addBox(-1.0F, -1.0F, -1.0F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.4F)), PartPose.offsetAndRotation(-0.6F, -15.7974F, -10.9679F, 0.5236F, 0.0F, 0.0F));

		PartDefinition dummy_c_2 = controls_3.addOrReplaceChild("dummy_c_2", CubeListBuilder.create().texOffs(5, 100).addBox(1.3F, 0.3F, -0.3F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.2F))
		.texOffs(17, 102).addBox(0.7F, 0.3F, 0.3F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.2F))
		.texOffs(46, 97).addBox(0.7F, 0.3F, -0.3F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.2F))
		.texOffs(46, 97).addBox(1.3F, 0.3F, 0.3F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.2F)), PartPose.offsetAndRotation(-2.1F, -15.75F, -13.25F, 0.5236F, 0.0F, 0.0F));

		PartDefinition dummy_c_3 = controls_3.addOrReplaceChild("dummy_c_3", CubeListBuilder.create().texOffs(5, 100).addBox(-0.8F, -0.8F, -0.8F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.2F))
		.texOffs(46, 100).addBox(-0.8F, -0.8F, -0.2F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.2F))
		.texOffs(46, 100).addBox(-0.2F, -0.8F, -0.8F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.2F))
		.texOffs(5, 100).addBox(-0.2F, -0.8F, -0.2F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.2F)), PartPose.offsetAndRotation(0.8F, -15.7974F, -10.9679F, 0.5236F, 0.0F, 0.0F));

		PartDefinition dummy_c_4 = controls_3.addOrReplaceChild("dummy_c_4", CubeListBuilder.create().texOffs(5, 100).addBox(-1.0F, -1.0F, -1.0F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.4F)), PartPose.offsetAndRotation(0.8F, -15.0474F, -12.267F, 0.5236F, 0.0F, 0.0F));

		PartDefinition dummy_c_5 = controls_3.addOrReplaceChild("dummy_c_5", CubeListBuilder.create().texOffs(5, 100).addBox(-0.5F, -0.5F, -0.5F, 1.0F, 1.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(-1.9F, -16.8489F, -9.4215F, 0.5236F, 0.0F, 0.0F));

		PartDefinition toggle_c_5_rotate_x = dummy_c_5.addOrReplaceChild("toggle_c_5_rotate_x", CubeListBuilder.create().texOffs(3, 40).addBox(-0.5F, -0.5723F, -0.585F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.2F))
		.texOffs(3, 40).addBox(-0.66F, -1.3723F, -0.585F, 1.0F, 2.0F, 1.0F, new CubeDeformation(-0.42F))
		.texOffs(3, 40).addBox(-0.5F, -1.3723F, -0.585F, 1.0F, 2.0F, 1.0F, new CubeDeformation(-0.42F))
		.texOffs(3, 40).addBox(-0.34F, -1.3723F, -0.585F, 1.0F, 2.0F, 1.0F, new CubeDeformation(-0.42F)), PartPose.offsetAndRotation(0.0F, -0.4333F, 0.0741F, 0.3491F, 0.0F, 0.0F));

		PartDefinition dummy_c_6 = controls_3.addOrReplaceChild("dummy_c_6", CubeListBuilder.create().texOffs(5, 100).addBox(-0.5F, -0.5F, -0.5F, 1.0F, 1.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.1F, -16.8489F, -9.4215F, 0.5236F, 0.0F, 0.0F));

		PartDefinition toggle_c_5_rotate_x2 = dummy_c_6.addOrReplaceChild("toggle_c_5_rotate_x2", CubeListBuilder.create().texOffs(3, 40).addBox(-0.5F, -0.5723F, -0.585F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.2F))
		.texOffs(3, 40).addBox(-0.66F, -1.3723F, -0.585F, 1.0F, 2.0F, 1.0F, new CubeDeformation(-0.42F))
		.texOffs(3, 40).addBox(-0.5F, -1.3723F, -0.585F, 1.0F, 2.0F, 1.0F, new CubeDeformation(-0.42F))
		.texOffs(3, 40).addBox(-0.34F, -1.3723F, -0.585F, 1.0F, 2.0F, 1.0F, new CubeDeformation(-0.42F)), PartPose.offsetAndRotation(0.0F, -0.4333F, 0.0741F, 0.3491F, 0.0F, 0.0F));

		PartDefinition dummy_c_7 = controls_3.addOrReplaceChild("dummy_c_7", CubeListBuilder.create().texOffs(5, 100).addBox(-0.5F, -0.5F, -0.5F, 1.0F, 1.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(2.1F, -16.8489F, -9.4215F, 0.5236F, 0.0F, 0.0F));

		PartDefinition toggle_c_5_rotate_x3 = dummy_c_7.addOrReplaceChild("toggle_c_5_rotate_x3", CubeListBuilder.create().texOffs(3, 40).addBox(-0.5F, -0.5723F, -0.585F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.2F))
		.texOffs(3, 40).addBox(-0.66F, -1.3723F, -0.585F, 1.0F, 2.0F, 1.0F, new CubeDeformation(-0.42F))
		.texOffs(3, 40).addBox(-0.5F, -1.3723F, -0.585F, 1.0F, 2.0F, 1.0F, new CubeDeformation(-0.42F))
		.texOffs(3, 40).addBox(-0.34F, -1.3723F, -0.585F, 1.0F, 2.0F, 1.0F, new CubeDeformation(-0.42F)), PartPose.offsetAndRotation(0.0F, -0.4333F, 0.0741F, 0.3491F, 0.0F, 0.0F));

		PartDefinition dummy_c_8 = controls_3.addOrReplaceChild("dummy_c_8", CubeListBuilder.create().texOffs(5, 100).addBox(-0.5F, -0.5F, -0.5F, 1.0F, 1.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(-1.1F, -18.7739F, -6.1715F, 0.5236F, 0.0F, 0.0F));

		PartDefinition toggle_c_5_rotate_x4 = dummy_c_8.addOrReplaceChild("toggle_c_5_rotate_x4", CubeListBuilder.create().texOffs(3, 40).addBox(-0.5F, -0.5723F, -0.585F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.2F))
		.texOffs(3, 40).addBox(-0.66F, -1.3723F, -0.585F, 1.0F, 2.0F, 1.0F, new CubeDeformation(-0.42F))
		.texOffs(3, 40).addBox(-0.5F, -1.3723F, -0.585F, 1.0F, 2.0F, 1.0F, new CubeDeformation(-0.42F))
		.texOffs(3, 40).addBox(-0.34F, -1.3723F, -0.585F, 1.0F, 2.0F, 1.0F, new CubeDeformation(-0.42F)), PartPose.offsetAndRotation(0.0F, -0.4333F, 0.0741F, -0.5236F, 0.0F, 0.0F));

		PartDefinition dummy_c_9 = controls_3.addOrReplaceChild("dummy_c_9", CubeListBuilder.create().texOffs(5, 100).addBox(-0.5F, -0.5F, -0.5F, 1.0F, 1.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.025F, -18.7739F, -6.1715F, 0.5236F, 0.0F, 0.0F));

		PartDefinition toggle_c_5_rotate_x5 = dummy_c_9.addOrReplaceChild("toggle_c_5_rotate_x5", CubeListBuilder.create().texOffs(3, 40).addBox(-0.5F, -0.5723F, -0.585F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.2F))
		.texOffs(3, 40).addBox(-0.66F, -1.3723F, -0.585F, 1.0F, 2.0F, 1.0F, new CubeDeformation(-0.42F))
		.texOffs(3, 40).addBox(-0.5F, -1.3723F, -0.585F, 1.0F, 2.0F, 1.0F, new CubeDeformation(-0.42F))
		.texOffs(3, 40).addBox(-0.34F, -1.3723F, -0.585F, 1.0F, 2.0F, 1.0F, new CubeDeformation(-0.42F)), PartPose.offsetAndRotation(0.0F, -0.4333F, 0.0741F, -0.5236F, 0.0F, 0.0F));

		PartDefinition dummy_c_10 = controls_3.addOrReplaceChild("dummy_c_10", CubeListBuilder.create().texOffs(5, 100).addBox(-0.5F, -0.5F, -0.5F, 1.0F, 1.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(1.15F, -18.7739F, -6.1715F, 0.5236F, 0.0F, 0.0F));

		PartDefinition toggle_c_5_rotate_x6 = dummy_c_10.addOrReplaceChild("toggle_c_5_rotate_x6", CubeListBuilder.create().texOffs(3, 40).addBox(-0.5F, -0.5723F, -0.585F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.2F))
		.texOffs(3, 40).addBox(-0.66F, -1.3723F, -0.585F, 1.0F, 2.0F, 1.0F, new CubeDeformation(-0.42F))
		.texOffs(3, 40).addBox(-0.5F, -1.3723F, -0.585F, 1.0F, 2.0F, 1.0F, new CubeDeformation(-0.42F))
		.texOffs(3, 40).addBox(-0.34F, -1.3723F, -0.585F, 1.0F, 2.0F, 1.0F, new CubeDeformation(-0.42F)), PartPose.offsetAndRotation(0.0F, -0.4333F, 0.0741F, -0.5236F, 0.0F, 0.0F));

		PartDefinition controls_4 = controls.addOrReplaceChild("controls_4", CubeListBuilder.create(), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.0F, 3.1416F, 0.0F));

		PartDefinition throttle = controls_4.addOrReplaceChild("throttle", CubeListBuilder.create(), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition throttle_plate = throttle.addOrReplaceChild("throttle_plate", CubeListBuilder.create().texOffs(50, 13).addBox(6.75F, -0.125F, -0.75F, 3.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(7, 103).addBox(6.75F, -0.325F, -0.75F, 3.0F, 1.0F, 1.0F, new CubeDeformation(-0.1F))
		.texOffs(3, 40).addBox(6.4F, -1.3F, -1.75F, 2.0F, 3.0F, 3.0F, new CubeDeformation(-0.9F))
		.texOffs(3, 40).addBox(6.9F, -1.3F, -1.75F, 2.0F, 3.0F, 3.0F, new CubeDeformation(-0.9F))
		.texOffs(3, 40).addBox(8.15F, -1.3F, -1.75F, 2.0F, 3.0F, 3.0F, new CubeDeformation(-0.9F))
		.texOffs(3, 40).addBox(7.65F, -1.3F, -1.75F, 2.0F, 3.0F, 3.0F, new CubeDeformation(-0.9F)), PartPose.offsetAndRotation(-4.25F, -15.5F, -12.5F, 0.5236F, 0.0F, 0.0F));

		PartDefinition throttle_rotate_x = throttle_plate.addOrReplaceChild("throttle_rotate_x", CubeListBuilder.create().texOffs(5, 100).addBox(-2.0442F, -2.7904F, -1.0F, 4.0F, 2.0F, 2.0F, new CubeDeformation(-0.7F))
		.texOffs(50, 4).addBox(-2.2942F, -2.7904F, -1.0F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.8F))
		.texOffs(3, 40).addBox(-1.5692F, -2.7904F, -1.0F, 1.0F, 2.0F, 2.0F, new CubeDeformation(-0.6F))
		.texOffs(3, 40).addBox(-1.3192F, -2.7904F, -1.0F, 1.0F, 2.0F, 2.0F, new CubeDeformation(-0.6F))
		.texOffs(2, 40).addBox(-0.8192F, -2.7904F, -1.0F, 1.0F, 2.0F, 2.0F, new CubeDeformation(-0.6F))
		.texOffs(3, 40).addBox(-1.0692F, -2.7904F, -1.0F, 1.0F, 2.0F, 2.0F, new CubeDeformation(-0.6F))
		.texOffs(50, 13).addBox(-1.1192F, -1.4154F, -0.5F, 1.0F, 2.0F, 1.0F, new CubeDeformation(-0.35F))
		.texOffs(50, 13).addBox(0.1308F, -1.4154F, -0.5F, 1.0F, 2.0F, 1.0F, new CubeDeformation(-0.35F))
		.texOffs(50, 13).addBox(-1.1192F, -1.7154F, -0.5F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.35F))
		.texOffs(50, 13).addBox(0.1308F, -1.7154F, -0.5F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.35F))
		.texOffs(3, 40).addBox(0.1808F, -2.7904F, -1.0F, 1.0F, 2.0F, 2.0F, new CubeDeformation(-0.6F))
		.texOffs(2, 40).addBox(-0.0692F, -2.7904F, -1.0F, 1.0F, 2.0F, 2.0F, new CubeDeformation(-0.6F))
		.texOffs(2, 40).addBox(-0.3192F, -2.7904F, -1.0F, 1.0F, 2.0F, 2.0F, new CubeDeformation(-0.6F))
		.texOffs(3, 40).addBox(0.4308F, -2.7904F, -1.0F, 1.0F, 2.0F, 2.0F, new CubeDeformation(-0.6F))
		.texOffs(2, 40).addBox(-0.5692F, -2.7904F, -1.0F, 1.0F, 2.0F, 2.0F, new CubeDeformation(-0.6F)), PartPose.offsetAndRotation(8.2692F, -0.1756F, -0.2634F, 1.0908F, 0.0F, 0.0F));

		PartDefinition dimentional_con = controls_4.addOrReplaceChild("dimentional_con", CubeListBuilder.create(), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition dim_screen = dimentional_con.addOrReplaceChild("dim_screen", CubeListBuilder.create().texOffs(46, 98).addBox(-0.95F, 1.8F, 2.45F, 2.0F, 2.0F, 2.0F, new CubeDeformation(0.2F))
		.texOffs(3, 40).addBox(-2.25F, 0.575F, -0.3F, 2.0F, 4.0F, 6.0F, new CubeDeformation(-0.9F))
		.texOffs(3, 40).addBox(0.35F, 0.575F, -0.3F, 2.0F, 4.0F, 6.0F, new CubeDeformation(-0.9F))
		.texOffs(3, 40).addBox(0.55F, 0.575F, 0.2F, 2.0F, 4.0F, 4.0F, new CubeDeformation(-0.9F))
		.texOffs(3, 40).addBox(-2.45F, 0.575F, 0.2F, 2.0F, 4.0F, 4.0F, new CubeDeformation(-0.9F))
		.texOffs(3, 40).addBox(-2.05F, 0.575F, 3.7F, 3.0F, 4.0F, 2.0F, new CubeDeformation(-0.9F))
		.texOffs(3, 40).addBox(-0.85F, 0.575F, 3.7F, 3.0F, 4.0F, 2.0F, new CubeDeformation(-0.9F))
		.texOffs(3, 40).addBox(-2.05F, 0.575F, 1.2F, 3.0F, 4.0F, 2.0F, new CubeDeformation(-0.9F))
		.texOffs(3, 40).addBox(-0.85F, 0.575F, 1.2F, 3.0F, 4.0F, 2.0F, new CubeDeformation(-0.9F))
		.texOffs(3, 40).addBox(-2.05F, 0.575F, 0.0F, 3.0F, 4.0F, 3.0F, new CubeDeformation(-0.9F))
		.texOffs(24, 103).addBox(-0.925F, 0.7F, 0.7F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.7F))
		.texOffs(24, 103).addBox(-1.975F, 0.7F, 0.7F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.7F))
		.texOffs(50, 13).addBox(-1.975F, 0.5F, 0.7F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.8F))
		.texOffs(50, 13).addBox(-0.925F, 0.5F, 0.7F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.8F))
		.texOffs(50, 13).addBox(0.125F, 0.5F, 0.7F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.8F))
		.texOffs(24, 103).addBox(0.125F, 0.7F, 0.7F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.7F))
		.texOffs(3, 40).addBox(-0.85F, 0.575F, 0.0F, 3.0F, 4.0F, 3.0F, new CubeDeformation(-0.9F)), PartPose.offsetAndRotation(0.0F, -17.5F, -10.6F, 0.9599F, 0.0F, 0.0F));

		PartDefinition dim_screen_text = dim_screen.addOrReplaceChild("dim_screen_text", CubeListBuilder.create().texOffs(3, 112).addBox(-0.95F, -15.725F, -8.05F, 2.0F, 2.0F, 2.0F, new CubeDeformation(0.2F)), PartPose.offset(0.0F, 17.5F, 10.5F));

		PartDefinition handbreak = controls_4.addOrReplaceChild("handbreak", CubeListBuilder.create(), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition handbreak_plate = handbreak.addOrReplaceChild("handbreak_plate", CubeListBuilder.create().texOffs(3, 40).addBox(-0.125F, 0.1F, -0.1F, 1.0F, 1.0F, 1.0F, new CubeDeformation(0.2F))
		.texOffs(50, 13).addBox(-0.1F, -0.766F, -0.1384F, 1.0F, 1.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(-4.25F, -15.5F, -12.5F, 0.5236F, 0.0F, 0.0F));

		PartDefinition handbreak_rotate_y = handbreak_plate.addOrReplaceChild("handbreak_rotate_y", CubeListBuilder.create().texOffs(2, 105).addBox(-0.525F, -0.625F, -0.6042F, 1.0F, 2.0F, 1.0F, new CubeDeformation(-0.3F))
		.texOffs(3, 40).addBox(-0.5F, -0.625F, -0.6292F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.1F))
		.texOffs(5, 100).addBox(-0.5F, -0.625F, -3.2042F, 1.0F, 1.0F, 3.0F, new CubeDeformation(-0.3F))
		.texOffs(50, 4).addBox(-1.0F, -1.125F, -1.9042F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.7F))
		.texOffs(50, 4).addBox(-1.0F, -1.125F, -2.6542F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.7F))
		.texOffs(50, 4).addBox(-1.0F, -1.125F, -3.4042F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.7F))
		.texOffs(50, 4).addBox(-1.0F, -1.125F, -4.1292F, 2.0F, 2.0F, 2.0F, new CubeDeformation(-0.7F)), PartPose.offsetAndRotation(0.5F, -1.416F, 0.3908F, 0.0F, 0.6981F, 0.0F));

		PartDefinition dummy_keyboard_d_1 = controls_4.addOrReplaceChild("dummy_keyboard_d_1", CubeListBuilder.create(), PartPose.offsetAndRotation(-0.5F, -15.75F, -13.25F, 0.5236F, 0.0F, 0.0F));

		PartDefinition keyrow_1 = dummy_keyboard_d_1.addOrReplaceChild("keyrow_1", CubeListBuilder.create().texOffs(5, 100).addBox(4.15F, -15.4F, -10.5F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.3F))
		.texOffs(5, 100).addBox(3.55F, -15.4F, -10.5F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.3F))
		.texOffs(5, 100).addBox(2.9F, -15.4F, -10.5F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.3F))
		.texOffs(5, 100).addBox(2.3F, -15.4F, -10.5F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.3F))
		.texOffs(5, 100).addBox(5.325F, -15.4F, -10.5F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.3F))
		.texOffs(5, 100).addBox(5.875F, -15.4F, -10.5F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.3F))
		.texOffs(5, 100).addBox(4.725F, -15.4F, -10.5F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.3F)), PartPose.offset(-4.0F, 15.5F, 13.5F));

		PartDefinition keyrow_2 = dummy_keyboard_d_1.addOrReplaceChild("keyrow_2", CubeListBuilder.create().texOffs(5, 100).addBox(4.45F, -15.4F, -10.5F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.3F))
		.texOffs(5, 100).addBox(3.85F, -15.4F, -10.5F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.3F))
		.texOffs(5, 100).addBox(3.2F, -15.4F, -10.5F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.3F))
		.texOffs(5, 100).addBox(2.6F, -15.4F, -10.5F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.3F))
		.texOffs(5, 100).addBox(5.625F, -15.4F, -10.5F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.3F))
		.texOffs(5, 100).addBox(5.025F, -15.4F, -10.5F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.3F)), PartPose.offset(-4.0F, 15.5F, 12.85F));

		PartDefinition keyrow_3 = dummy_keyboard_d_1.addOrReplaceChild("keyrow_3", CubeListBuilder.create().texOffs(5, 100).addBox(4.15F, -15.4F, -10.5F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.3F))
		.texOffs(5, 100).addBox(3.55F, -15.4F, -10.5F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.3F))
		.texOffs(5, 100).addBox(2.9F, -15.4F, -10.5F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.3F))
		.texOffs(5, 100).addBox(2.3F, -15.4F, -10.5F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.3F))
		.texOffs(5, 100).addBox(5.325F, -15.4F, -10.5F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.3F))
		.texOffs(5, 100).addBox(5.875F, -15.4F, -10.5F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.3F))
		.texOffs(5, 100).addBox(4.725F, -15.4F, -10.5F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.3F)), PartPose.offset(-4.0F, 15.5F, 12.275F));

		PartDefinition keyrow_4 = dummy_keyboard_d_1.addOrReplaceChild("keyrow_4", CubeListBuilder.create().texOffs(50, 13).addBox(3.15F, -15.4F, -10.5F, 3.0F, 1.0F, 1.0F, new CubeDeformation(-0.3F))
		.texOffs(5, 100).addBox(2.3F, -15.4F, -10.5F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.3F))
		.texOffs(5, 100).addBox(5.875F, -15.4F, -10.5F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.3F)), PartPose.offset(-4.0F, 15.5F, 11.7F));

		PartDefinition dummy_d_2 = controls_4.addOrReplaceChild("dummy_d_2", CubeListBuilder.create().texOffs(5, 100).addBox(-0.5F, -0.5F, -0.5F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.2F)), PartPose.offsetAndRotation(-2.45F, -17.7188F, -8.44F, 0.5236F, 0.0F, 0.0F));

		PartDefinition dummy_d_4 = controls_4.addOrReplaceChild("dummy_d_4", CubeListBuilder.create().texOffs(5, 100).addBox(-0.5F, -0.5F, -0.5F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.2F)), PartPose.offsetAndRotation(2.55F, -17.7188F, -8.44F, 0.5236F, 0.0F, 0.0F));

		PartDefinition dummy_d_3 = controls_4.addOrReplaceChild("dummy_d_3", CubeListBuilder.create().texOffs(5, 100).addBox(-0.5F, -0.5F, -0.5F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.2F))
		.texOffs(5, 100).addBox(4.5F, -0.5F, -0.5F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.2F)), PartPose.offsetAndRotation(-2.45F, -18.1688F, -7.6605F, 0.5236F, 0.0F, 0.0F));

		PartDefinition decoration_4 = controls_4.addOrReplaceChild("decoration_4", CubeListBuilder.create().texOffs(3, 40).addBox(3.05F, 0.5F, 6.97F, 3.0F, 1.0F, 1.0F, new CubeDeformation(0.09F))
		.texOffs(3, 40).addBox(3.05F, 0.5F, 5.79F, 3.0F, 1.0F, 1.0F, new CubeDeformation(0.09F))
		.texOffs(3, 40).addBox(3.05F, 0.5F, 8.15F, 3.0F, 1.0F, 1.0F, new CubeDeformation(0.09F)), PartPose.offsetAndRotation(-4.5F, -15.5F, -13.5F, 0.5236F, 0.0F, 0.0F));

		PartDefinition controls_5 = controls.addOrReplaceChild("controls_5", CubeListBuilder.create(), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.0F, -2.0944F, 0.0F));

		PartDefinition landing_type = controls_5.addOrReplaceChild("landing_type", CubeListBuilder.create(), PartPose.offset(0.4F, 0.0F, 0.0F));

		PartDefinition landing_screen = landing_type.addOrReplaceChild("landing_screen", CubeListBuilder.create().texOffs(43, 102).addBox(2.3F, -0.825F, -2.75F, 2.0F, 1.0F, 2.0F, new CubeDeformation(0.0F))
		.texOffs(3, 40).addBox(1.275F, -1.775F, -3.95F, 2.0F, 4.0F, 4.0F, new CubeDeformation(-0.9F))
		.texOffs(3, 40).addBox(3.275F, -1.775F, -3.95F, 2.0F, 4.0F, 4.0F, new CubeDeformation(-0.9F))
		.texOffs(3, 40).addBox(1.275F, -1.775F, -1.75F, 4.0F, 4.0F, 2.0F, new CubeDeformation(-0.9F)), PartPose.offsetAndRotation(0.0F, -17.5F, -10.5F, 0.9599F, 0.0F, 0.0F));

		PartDefinition cube_r84 = landing_screen.addOrReplaceChild("cube_r84", CubeListBuilder.create().texOffs(3, 40).addBox(-1.175F, -1.175F, -0.925F, 4.0F, 3.0F, 2.0F, new CubeDeformation(-0.85F)), PartPose.offsetAndRotation(2.45F, -0.6438F, -3.0847F, -0.4363F, 0.0F, 0.0F));

		PartDefinition cube_r85 = landing_screen.addOrReplaceChild("cube_r85", CubeListBuilder.create().texOffs(3, 40).addBox(0.875F, -1.25F, -1.0F, 2.0F, 3.0F, 2.0F, new CubeDeformation(-0.8F))
		.texOffs(3, 40).addBox(-1.125F, -1.25F, -1.0F, 2.0F, 3.0F, 2.0F, new CubeDeformation(-0.8F)), PartPose.offsetAndRotation(2.4F, -0.6F, -3.05F, -0.4363F, 0.0F, 0.0F));

		PartDefinition landing_text = landing_screen.addOrReplaceChild("landing_text", CubeListBuilder.create().texOffs(101, 49).addBox(-1.0F, 0.0F, -1.0F, 2.0F, 1.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offset(3.3F, -0.85F, -1.75F));

		PartDefinition increments = controls_5.addOrReplaceChild("increments", CubeListBuilder.create(), PartPose.offset(0.0F, 0.2F, -0.35F));

		PartDefinition inc_slider = increments.addOrReplaceChild("inc_slider", CubeListBuilder.create().texOffs(3, 89).addBox(-0.4F, 0.025F, -3.1F, 1.0F, 2.0F, 1.0F, new CubeDeformation(-0.4F))
		.texOffs(3, 89).addBox(-0.4F, 0.025F, -3.3F, 1.0F, 2.0F, 1.0F, new CubeDeformation(-0.4F))
		.texOffs(3, 89).addBox(-0.4F, 0.025F, -0.7F, 1.0F, 2.0F, 1.0F, new CubeDeformation(-0.4F))
		.texOffs(3, 89).addBox(-0.4F, 0.025F, -0.9F, 1.0F, 2.0F, 1.0F, new CubeDeformation(-0.4F))
		.texOffs(3, 89).addBox(-0.4F, 0.05F, -3.5F, 1.0F, 2.0F, 1.0F, new CubeDeformation(-0.4F))
		.texOffs(3, 89).addBox(-0.6F, 0.05F, -3.5F, 1.0F, 2.0F, 1.0F, new CubeDeformation(-0.4F))
		.texOffs(3, 89).addBox(-0.2F, 0.05F, -3.5F, 1.0F, 2.0F, 1.0F, new CubeDeformation(-0.4F))
		.texOffs(3, 89).addBox(-0.6F, 0.05F, -0.5F, 1.0F, 2.0F, 1.0F, new CubeDeformation(-0.4F))
		.texOffs(3, 89).addBox(-0.4F, 0.05F, -0.5F, 1.0F, 2.0F, 1.0F, new CubeDeformation(-0.4F))
		.texOffs(3, 89).addBox(-0.2F, 0.05F, -0.5F, 1.0F, 2.0F, 1.0F, new CubeDeformation(-0.4F))
		.texOffs(3, 105).addBox(-0.7F, 0.125F, -3.2F, 1.0F, 2.0F, 2.0F, new CubeDeformation(-0.3F))
		.texOffs(3, 105).addBox(-0.7F, 0.125F, -1.8F, 1.0F, 2.0F, 2.0F, new CubeDeformation(-0.3F))
		.texOffs(3, 105).addBox(-0.1F, 0.125F, -3.2F, 1.0F, 2.0F, 2.0F, new CubeDeformation(-0.3F))
		.texOffs(3, 40).addBox(-0.1F, 0.3F, -3.3F, 1.0F, 2.0F, 2.0F, new CubeDeformation(-0.2F))
		.texOffs(3, 40).addBox(-0.1F, 0.3F, -1.7F, 1.0F, 2.0F, 2.0F, new CubeDeformation(-0.2F))
		.texOffs(3, 40).addBox(-0.7F, 0.3F, -3.3F, 1.0F, 2.0F, 2.0F, new CubeDeformation(-0.2F))
		.texOffs(3, 40).addBox(-0.7F, 0.3F, -1.7F, 1.0F, 2.0F, 2.0F, new CubeDeformation(-0.2F))
		.texOffs(3, 40).addBox(-0.9F, 0.05F, -0.35F, 2.0F, 2.0F, 1.0F, new CubeDeformation(-0.45F))
		.texOffs(3, 40).addBox(-0.9F, 0.05F, -0.25F, 2.0F, 2.0F, 1.0F, new CubeDeformation(-0.45F))
		.texOffs(3, 40).addBox(-0.9F, 0.05F, -3.75F, 2.0F, 2.0F, 1.0F, new CubeDeformation(-0.45F))
		.texOffs(3, 40).addBox(-0.9F, 0.05F, -3.65F, 2.0F, 2.0F, 1.0F, new CubeDeformation(-0.45F))
		.texOffs(3, 105).addBox(-0.1F, 0.125F, -1.8F, 1.0F, 2.0F, 2.0F, new CubeDeformation(-0.3F)), PartPose.offsetAndRotation(-0.075F, -17.5F, -10.5F, 0.6109F, 0.0F, 0.0F));

		PartDefinition position_rotate_x = inc_slider.addOrReplaceChild("position_rotate_x", CubeListBuilder.create().texOffs(50, 13).addBox(-0.5F, -4.64F, -0.5F, 1.0F, 2.0F, 1.0F, new CubeDeformation(-0.4F))
		.texOffs(50, 2).addBox(-0.4F, -4.84F, -0.5F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.4F))
		.texOffs(50, 2).addBox(-0.6F, -4.84F, -0.5F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.4F))
		.texOffs(50, 2).addBox(-0.8F, -4.84F, -0.5F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.4F))
		.texOffs(50, 2).addBox(-0.2F, -4.84F, -0.5F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.4F)), PartPose.offsetAndRotation(0.1F, 4.565F, -1.6F, 0.2182F, 0.0F, 0.0F));

		PartDefinition xyz = controls_5.addOrReplaceChild("xyz", CubeListBuilder.create(), PartPose.offset(0.0F, 0.2F, -0.5F));

		PartDefinition cube_r86 = xyz.addOrReplaceChild("cube_r86", CubeListBuilder.create().texOffs(3, 40).addBox(2.05F, -20.075F, 1.75F, 1.0F, 1.0F, 2.0F, new CubeDeformation(-0.3F))
		.texOffs(3, 40).addBox(-3.15F, -20.075F, 1.75F, 1.0F, 1.0F, 2.0F, new CubeDeformation(-0.3F)), PartPose.offsetAndRotation(0.15F, 0.1866F, -0.1232F, 0.5236F, 0.0F, 0.0F));

		PartDefinition cube_r87 = xyz.addOrReplaceChild("cube_r87", CubeListBuilder.create().texOffs(3, 40).addBox(2.15F, -20.175F, 1.85F, 1.0F, 1.0F, 2.0F, new CubeDeformation(-0.4F))
		.texOffs(3, 40).addBox(-3.65F, -20.175F, 1.85F, 1.0F, 1.0F, 2.0F, new CubeDeformation(-0.4F)), PartPose.offsetAndRotation(0.35F, 0.2366F, -0.2098F, 0.5236F, 0.0F, 0.0F));

		PartDefinition cube_r88 = xyz.addOrReplaceChild("cube_r88", CubeListBuilder.create().texOffs(3, 40).addBox(-2.4F, -19.775F, 1.55F, 5.0F, 1.0F, 2.0F, new CubeDeformation(-0.1F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.5236F, 0.0F, 0.0F));

		PartDefinition dial_x = xyz.addOrReplaceChild("dial_x", CubeListBuilder.create().texOffs(5, 100).addBox(-0.4F, 0.825F, 2.25F, 1.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(75, 86).addBox(-0.25F, 0.3428F, 2.4533F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.35F))
		.texOffs(75, 86).addBox(-0.55F, 0.3428F, 2.4533F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.35F))
		.texOffs(75, 86).addBox(-0.6F, 0.3928F, 2.1033F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.3F))
		.texOffs(75, 86).addBox(-0.2F, 0.3928F, 2.1033F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.3F)), PartPose.offsetAndRotation(-1.65F, -17.5F, -10.5F, 0.6109F, 0.0F, 0.0F));

		PartDefinition dial_y = xyz.addOrReplaceChild("dial_y", CubeListBuilder.create().texOffs(5, 100).addBox(-0.4F, 0.825F, 2.25F, 1.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(80, 86).addBox(-0.25F, 0.3428F, 2.4533F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.35F))
		.texOffs(80, 86).addBox(-0.55F, 0.3428F, 2.4533F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.35F))
		.texOffs(80, 86).addBox(-0.6F, 0.3928F, 2.1033F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.3F))
		.texOffs(80, 86).addBox(-0.2F, 0.3928F, 2.1033F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.3F)), PartPose.offsetAndRotation(0.0F, -17.5F, -10.5F, 0.6109F, 0.0F, 0.0F));

		PartDefinition dial_z = xyz.addOrReplaceChild("dial_z", CubeListBuilder.create().texOffs(5, 100).addBox(-0.4F, 0.825F, 2.25F, 1.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(85, 86).addBox(-0.25F, 0.3428F, 2.4533F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.35F))
		.texOffs(85, 86).addBox(-0.55F, 0.3428F, 2.4533F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.35F))
		.texOffs(85, 86).addBox(-0.6F, 0.3928F, 2.1033F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.3F))
		.texOffs(85, 86).addBox(-0.2F, 0.3928F, 2.1033F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.3F)), PartPose.offsetAndRotation(1.6F, -17.5F, -10.5F, 0.6109F, 0.0F, 0.0F));

		PartDefinition direction = controls_5.addOrReplaceChild("direction", CubeListBuilder.create(), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition arrows = direction.addOrReplaceChild("arrows", CubeListBuilder.create(), PartPose.offsetAndRotation(-3.3F, -15.3F, -11.2F, 0.5236F, 0.0F, 0.0F));

		PartDefinition cube_r89 = arrows.addOrReplaceChild("cube_r89", CubeListBuilder.create().texOffs(3, 103).addBox(-0.575F, -0.3F, -1.525F, 2.0F, 3.0F, 2.0F, new CubeDeformation(-0.3F))
		.texOffs(3, 103).addBox(-1.525F, -0.3F, -1.725F, 2.0F, 3.0F, 2.0F, new CubeDeformation(-0.3F))
		.texOffs(3, 103).addBox(-1.35F, -0.3F, -0.825F, 2.0F, 3.0F, 2.0F, new CubeDeformation(-0.3F))
		.texOffs(3, 103).addBox(-0.45F, -0.3F, -0.725F, 2.0F, 3.0F, 2.0F, new CubeDeformation(-0.3F)), PartPose.offsetAndRotation(-0.3F, -1.05F, 0.05F, 0.0F, -0.7854F, 0.0F));

		PartDefinition direction_screen = direction.addOrReplaceChild("direction_screen", CubeListBuilder.create().texOffs(43, 102).addBox(-4.425F, -0.825F, -2.75F, 2.0F, 1.0F, 2.0F, new CubeDeformation(0.0F))
		.texOffs(3, 40).addBox(-5.45F, -1.775F, -3.95F, 2.0F, 4.0F, 4.0F, new CubeDeformation(-0.9F))
		.texOffs(3, 40).addBox(-3.45F, -1.775F, -3.95F, 2.0F, 4.0F, 4.0F, new CubeDeformation(-0.9F))
		.texOffs(3, 40).addBox(-5.45F, -1.775F, -1.75F, 4.0F, 4.0F, 2.0F, new CubeDeformation(-0.9F)), PartPose.offsetAndRotation(0.0F, -17.5F, -10.5F, 0.9599F, 0.0F, 0.0F));

		PartDefinition cube_r90 = direction_screen.addOrReplaceChild("cube_r90", CubeListBuilder.create().texOffs(3, 40).addBox(-1.175F, -1.175F, -0.925F, 4.0F, 3.0F, 2.0F, new CubeDeformation(-0.85F)), PartPose.offsetAndRotation(-4.275F, -0.6438F, -3.0847F, -0.4363F, 0.0F, 0.0F));

		PartDefinition cube_r91 = direction_screen.addOrReplaceChild("cube_r91", CubeListBuilder.create().texOffs(3, 40).addBox(0.875F, -1.25F, -1.0F, 2.0F, 3.0F, 2.0F, new CubeDeformation(-0.8F))
		.texOffs(3, 40).addBox(-1.125F, -1.25F, -1.0F, 2.0F, 3.0F, 2.0F, new CubeDeformation(-0.8F)), PartPose.offsetAndRotation(-4.325F, -0.6F, -3.05F, -0.4363F, 0.0F, 0.0F));

		PartDefinition direction_text = direction_screen.addOrReplaceChild("direction_text", CubeListBuilder.create().texOffs(100, 50).addBox(-1.0F, 0.0F, -1.0F, 2.0F, 1.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offset(-3.425F, -0.85F, -1.75F));

		PartDefinition dummy_e_1 = controls_5.addOrReplaceChild("dummy_e_1", CubeListBuilder.create().texOffs(5, 100).addBox(-1.3F, 0.0F, 7.25F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.1F)), PartPose.offsetAndRotation(-0.5F, -15.75F, -13.25F, 0.5236F, 0.0F, 0.0F));

		PartDefinition dummy_e_2 = controls_5.addOrReplaceChild("dummy_e_2", CubeListBuilder.create().texOffs(5, 100).addBox(0.05F, 0.0F, 7.25F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.1F)), PartPose.offsetAndRotation(-0.5F, -15.75F, -13.25F, 0.5236F, 0.0F, 0.0F));

		PartDefinition dummy_e_3 = controls_5.addOrReplaceChild("dummy_e_3", CubeListBuilder.create().texOffs(5, 100).addBox(1.4F, 0.0F, 7.25F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.1F)), PartPose.offsetAndRotation(-0.5F, -15.75F, -13.25F, 0.5236F, 0.0F, 0.0F));

		PartDefinition decoration_5 = controls_5.addOrReplaceChild("decoration_5", CubeListBuilder.create().texOffs(3, 40).addBox(6.675F, 0.25F, 0.0F, 3.0F, 1.0F, 3.0F, new CubeDeformation(-0.2F))
		.texOffs(3, 40).addBox(6.675F, 0.25F, 2.6F, 3.0F, 1.0F, 1.0F, new CubeDeformation(-0.2F))
		.texOffs(3, 40).addBox(-0.45F, 0.25F, 0.0F, 3.0F, 1.0F, 3.0F, new CubeDeformation(-0.2F))
		.texOffs(3, 40).addBox(-0.45F, 0.25F, 2.6F, 3.0F, 1.0F, 1.0F, new CubeDeformation(-0.2F))
		.texOffs(5, 100).addBox(5.05F, 0.1F, 0.35F, 0.0F, 1.0F, 0.0F, new CubeDeformation(0.0F))
		.texOffs(5, 100).addBox(5.05F, 0.1F, 3.6F, 0.0F, 1.0F, 0.0F, new CubeDeformation(0.0F))
		.texOffs(5, 100).addBox(4.3F, 0.1F, 0.35F, 0.0F, 1.0F, 0.0F, new CubeDeformation(0.0F))
		.texOffs(5, 100).addBox(4.3F, 0.1F, 3.6F, 0.0F, 1.0F, 0.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(-4.5F, -15.5F, -13.5F, 0.5236F, 0.0F, 0.0F));

		PartDefinition arrows2 = decoration_5.addOrReplaceChild("arrows2", CubeListBuilder.create().texOffs(5, 103).addBox(3.125F, -0.55F, -6.65F, 2.0F, 1.0F, 2.0F, new CubeDeformation(-0.3F))
		.texOffs(5, 103).addBox(4.225F, -0.55F, -5.525F, 2.0F, 1.0F, 2.0F, new CubeDeformation(-0.3F)), PartPose.offsetAndRotation(1.3F, 0.6F, 2.1F, 0.0F, -0.7854F, 0.0F));

		PartDefinition controls_6 = controls.addOrReplaceChild("controls_6", CubeListBuilder.create(), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.0F, -1.0472F, 0.0F));

		PartDefinition monitor = controls_6.addOrReplaceChild("monitor", CubeListBuilder.create(), PartPose.offset(0.0F, -0.2F, 0.4F));

		PartDefinition screen = monitor.addOrReplaceChild("screen", CubeListBuilder.create().texOffs(39, 102).addBox(-2.525F, 0.15F, -0.975F, 5.0F, 1.0F, 3.0F, new CubeDeformation(0.2F))
		.texOffs(39, 102).addBox(-2.525F, 0.15F, -1.375F, 5.0F, 1.0F, 0.0F, new CubeDeformation(0.2F)), PartPose.offsetAndRotation(0.0F, -17.5F, -10.5F, 0.9599F, 0.0F, 0.0F));

		PartDefinition text_screen = screen.addOrReplaceChild("text_screen", CubeListBuilder.create().texOffs(100, 47).addBox(-3.0F, -18.025F, -12.0F, 6.0F, 1.0F, 4.0F, new CubeDeformation(-0.4F)), PartPose.offset(0.0F, 17.5F, 10.5F));

		PartDefinition circles = screen.addOrReplaceChild("circles", CubeListBuilder.create().texOffs(75, 91).addBox(0.9F, -17.925F, -10.5F, 2.0F, 1.0F, 1.0F, new CubeDeformation(-0.35F))
		.texOffs(75, 91).addBox(-0.4F, -17.925F, -10.2F, 2.0F, 1.0F, 1.0F, new CubeDeformation(-0.35F))
		.texOffs(75, 91).addBox(-0.4F, -17.925F, -9.9F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.35F))
		.texOffs(75, 91).addBox(-0.7F, -17.925F, -10.5F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.35F))
		.texOffs(75, 91).addBox(1.55F, -17.925F, -10.0F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.35F))
		.texOffs(75, 91).addBox(1.55F, -17.925F, -11.0F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.35F))
		.texOffs(75, 91).addBox(1.15F, -17.925F, -9.2F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.35F))
		.texOffs(75, 91).addBox(1.15F, -17.925F, -11.7F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.35F))
		.texOffs(75, 91).addBox(0.55F, -17.925F, -12.3F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.35F))
		.texOffs(75, 91).addBox(-0.7F, -17.925F, -10.8F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.35F))
		.texOffs(75, 91).addBox(-0.7F, -17.925F, -11.1F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.35F))
		.texOffs(75, 91).addBox(-0.7F, -17.925F, -11.4F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.35F))
		.texOffs(75, 91).addBox(-0.7F, -17.925F, -11.7F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.35F))
		.texOffs(75, 91).addBox(-1.0F, -17.925F, -11.7F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.35F))
		.texOffs(75, 91).addBox(-1.6F, -17.925F, -11.7F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.35F))
		.texOffs(75, 91).addBox(-1.3F, -17.925F, -11.7F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.35F))
		.texOffs(75, 91).addBox(-1.6F, -17.925F, -11.1F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.35F))
		.texOffs(75, 91).addBox(-1.6F, -17.925F, -11.4F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.35F))
		.texOffs(75, 91).addBox(-1.9F, -17.925F, -11.4F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.35F))
		.texOffs(75, 91).addBox(-2.2F, -17.925F, -11.7F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.35F))
		.texOffs(75, 91).addBox(-2.5F, -17.925F, -11.7F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.35F))
		.texOffs(75, 91).addBox(-2.5F, -17.925F, -11.1F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.35F))
		.texOffs(75, 91).addBox(-2.8F, -17.925F, -11.1F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.35F))
		.texOffs(75, 91).addBox(-2.2F, -17.925F, -10.8F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.35F))
		.texOffs(75, 91).addBox(-1.9F, -17.925F, -10.5F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.35F))
		.texOffs(75, 91).addBox(-1.6F, -17.925F, -10.2F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.35F))
		.texOffs(75, 91).addBox(-1.6F, -17.925F, -9.9F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.35F))
		.texOffs(75, 91).addBox(-1.3F, -17.925F, -9.3F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.35F))
		.texOffs(75, 91).addBox(-1.0F, -17.925F, -9.0F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.35F))
		.texOffs(75, 91).addBox(-3.1F, -17.925F, -9.0F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.35F))
		.texOffs(75, 91).addBox(-2.8F, -17.925F, -9.0F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.35F))
		.texOffs(75, 91).addBox(-2.8F, -17.925F, -9.3F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.35F))
		.texOffs(75, 91).addBox(-2.5F, -17.925F, -9.6F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.35F))
		.texOffs(75, 91).addBox(-2.2F, -17.925F, -9.6F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.35F))
		.texOffs(75, 91).addBox(-1.9F, -17.925F, -9.9F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.35F))
		.texOffs(75, 91).addBox(-3.1F, -17.925F, -9.3F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.35F))
		.texOffs(75, 91).addBox(-1.3F, -17.925F, -9.6F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.35F))
		.texOffs(75, 91).addBox(-3.1F, -17.925F, -11.4F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.35F))
		.texOffs(75, 91).addBox(-3.1F, -17.925F, -12.0F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.35F))
		.texOffs(75, 91).addBox(-2.8F, -17.925F, -12.0F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.35F))
		.texOffs(75, 91).addBox(-1.3F, -17.925F, -10.8F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.35F))
		.texOffs(75, 91).addBox(-1.6F, -17.925F, -10.8F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.35F))
		.texOffs(75, 91).addBox(-1.0F, -17.925F, -10.8F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.35F))
		.texOffs(75, 91).addBox(-0.1F, -17.925F, -9.6F, 1.0F, 1.0F, 2.0F, new CubeDeformation(-0.35F)), PartPose.offset(0.0F, 17.5F, 10.5F));

		PartDefinition frame = screen.addOrReplaceChild("frame", CubeListBuilder.create().texOffs(3, 40).addBox(2.225F, -18.0582F, -12.742F, 1.0F, 3.0F, 4.0F, new CubeDeformation(-0.4F))
		.texOffs(3, 40).addBox(2.225F, -18.0582F, -9.342F, 1.0F, 3.0F, 1.0F, new CubeDeformation(-0.4F))
		.texOffs(3, 40).addBox(2.225F, -18.0582F, -9.142F, 1.0F, 3.0F, 1.0F, new CubeDeformation(-0.4F))
		.texOffs(3, 40).addBox(2.225F, -18.0582F, -9.542F, 1.0F, 3.0F, 1.0F, new CubeDeformation(-0.4F))
		.texOffs(3, 40).addBox(-3.175F, -18.0582F, -9.142F, 1.0F, 3.0F, 1.0F, new CubeDeformation(-0.4F))
		.texOffs(3, 40).addBox(-3.175F, -18.0582F, -9.342F, 1.0F, 3.0F, 1.0F, new CubeDeformation(-0.4F))
		.texOffs(3, 40).addBox(-3.175F, -18.0582F, -9.542F, 1.0F, 3.0F, 1.0F, new CubeDeformation(-0.4F))
		.texOffs(3, 40).addBox(-3.175F, -18.0582F, -12.942F, 1.0F, 3.0F, 1.0F, new CubeDeformation(-0.4F))
		.texOffs(3, 40).addBox(2.225F, -18.0582F, -12.942F, 1.0F, 3.0F, 1.0F, new CubeDeformation(-0.4F))
		.texOffs(3, 40).addBox(-2.975F, -18.0582F, -12.942F, 6.0F, 3.0F, 1.0F, new CubeDeformation(-0.4F))
		.texOffs(3, 40).addBox(-2.975F, -18.0582F, -9.142F, 6.0F, 3.0F, 1.0F, new CubeDeformation(-0.4F))
		.texOffs(3, 40).addBox(-0.925F, -18.0082F, -8.892F, 2.0F, 4.0F, 1.0F, new CubeDeformation(-0.35F))
		.texOffs(3, 40).addBox(-0.425F, -17.9082F, -8.492F, 1.0F, 4.0F, 1.0F, new CubeDeformation(-0.25F))
		.texOffs(3, 40).addBox(-3.175F, -18.0582F, -12.742F, 1.0F, 3.0F, 4.0F, new CubeDeformation(-0.4F))
		.texOffs(5, 100).addBox(-0.275F, -18.25F, -7.75F, 0.0F, 3.0F, 0.0F, new CubeDeformation(0.0F))
		.texOffs(5, 100).addBox(0.475F, -18.25F, -7.75F, 0.0F, 3.0F, 0.0F, new CubeDeformation(0.0F))
		.texOffs(5, 100).addBox(0.125F, -17.85F, -7.625F, 0.0F, 0.0F, 0.0F, new CubeDeformation(0.0F)), PartPose.offset(-0.075F, 17.5F, 10.925F));

		PartDefinition coms = controls_6.addOrReplaceChild("coms", CubeListBuilder.create(), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition speaker = coms.addOrReplaceChild("speaker", CubeListBuilder.create().texOffs(50, 13).addBox(-1.2F, 0.25F, -0.25F, 2.0F, 1.0F, 2.0F, new CubeDeformation(0.0F))
		.texOffs(3, 40).addBox(-1.2F, -0.3F, 0.9F, 2.0F, 1.0F, 1.0F, new CubeDeformation(-0.4F))
		.texOffs(3, 40).addBox(-1.2F, -0.3F, 0.55F, 2.0F, 1.0F, 1.0F, new CubeDeformation(-0.4F))
		.texOffs(3, 40).addBox(0.0F, -0.3F, 0.55F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.4F))
		.texOffs(3, 40).addBox(-1.4F, -0.3F, 0.55F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.4F))
		.texOffs(3, 40).addBox(-1.2F, -0.3F, 0.225F, 2.0F, 1.0F, 1.0F, new CubeDeformation(-0.4F))
		.texOffs(4, 87).addBox(-0.125F, -0.15F, -0.35F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.3F))
		.texOffs(4, 87).addBox(-1.3F, -0.15F, -0.35F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.3F)), PartPose.offsetAndRotation(-4.5F, -15.5F, -13.5F, 0.5236F, 0.0F, 0.0F));

		PartDefinition dummy_ff_5 = controls_6.addOrReplaceChild("dummy_ff_5", CubeListBuilder.create(), PartPose.offsetAndRotation(5.25F, -15.5F, -13.5F, 0.5236F, 0.0F, 0.0F));

		PartDefinition toggle_ff_5 = dummy_ff_5.addOrReplaceChild("toggle_ff_5", CubeListBuilder.create(), PartPose.offsetAndRotation(0.0F, 0.25F, 1.0F, -0.5236F, 0.0F, 0.0F));

		PartDefinition dummy_ff_1 = controls_6.addOrReplaceChild("dummy_ff_1", CubeListBuilder.create().texOffs(5, 100).addBox(2.5F, 0.2598F, 0.65F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.1F)), PartPose.offsetAndRotation(-4.5F, -15.5F, -13.5F, 0.5236F, 0.0F, 0.0F));

		PartDefinition dummy_ff_2 = controls_6.addOrReplaceChild("dummy_ff_2", CubeListBuilder.create().texOffs(5, 100).addBox(5.4F, 0.2598F, 0.65F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.1F)), PartPose.offsetAndRotation(-4.5F, -15.5F, -13.5F, 0.5236F, 0.0F, 0.0F));

		PartDefinition dummy_ff_3 = controls_6.addOrReplaceChild("dummy_ff_3", CubeListBuilder.create(), PartPose.offsetAndRotation(4.0F, -15.5F, -13.5F, 0.5236F, 0.0F, 0.0F));

		PartDefinition toggle_ff_3 = dummy_ff_3.addOrReplaceChild("toggle_ff_3", CubeListBuilder.create(), PartPose.offsetAndRotation(0.0F, 0.25F, 1.0F, -0.5236F, 0.0F, 0.0F));

		PartDefinition dummy_ff_4 = controls_6.addOrReplaceChild("dummy_ff_4", CubeListBuilder.create().texOffs(5, 100).addBox(-0.5F, 0.425F, 0.5F, 1.0F, 1.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(4.0F, -16.25F, -12.25F, 0.5236F, 0.0F, 0.0F));

		PartDefinition toggle_ff_4_rotate_x = dummy_ff_4.addOrReplaceChild("toggle_ff_4_rotate_x", CubeListBuilder.create().texOffs(3, 40).addBox(-0.5F, -0.5723F, -0.585F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.2F))
		.texOffs(3, 40).addBox(-0.66F, -1.3723F, -0.585F, 1.0F, 2.0F, 1.0F, new CubeDeformation(-0.42F))
		.texOffs(3, 40).addBox(-0.5F, -1.3723F, -0.585F, 1.0F, 2.0F, 1.0F, new CubeDeformation(-0.42F))
		.texOffs(3, 40).addBox(-0.34F, -1.3723F, -0.585F, 1.0F, 2.0F, 1.0F, new CubeDeformation(-0.42F)), PartPose.offsetAndRotation(0.0F, 0.4917F, 1.0741F, -0.5236F, 0.0F, 0.0F));

		PartDefinition dummy_ff_9 = controls_6.addOrReplaceChild("dummy_ff_9", CubeListBuilder.create().texOffs(5, 100).addBox(-0.5F, 0.425F, 0.5F, 1.0F, 1.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(4.0F, -15.55F, -13.35F, 0.5236F, 0.0F, 0.0F));

		PartDefinition toggle_ff_2_rotate_x = dummy_ff_9.addOrReplaceChild("toggle_ff_2_rotate_x", CubeListBuilder.create().texOffs(3, 40).addBox(-0.5F, -0.5723F, -0.585F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.2F))
		.texOffs(3, 40).addBox(-0.66F, -1.3723F, -0.585F, 1.0F, 2.0F, 1.0F, new CubeDeformation(-0.42F))
		.texOffs(3, 40).addBox(-0.5F, -1.3723F, -0.585F, 1.0F, 2.0F, 1.0F, new CubeDeformation(-0.42F))
		.texOffs(3, 40).addBox(-0.34F, -1.3723F, -0.585F, 1.0F, 2.0F, 1.0F, new CubeDeformation(-0.42F)), PartPose.offsetAndRotation(0.0F, 0.4917F, 1.0741F, -0.5236F, 0.0F, 0.0F));

		PartDefinition dummy_ff_10 = controls_6.addOrReplaceChild("dummy_ff_10", CubeListBuilder.create().texOffs(5, 100).addBox(-0.5F, 0.425F, 0.5F, 1.0F, 1.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(5.325F, -15.55F, -13.35F, 0.5236F, 0.0F, 0.0F));

		PartDefinition toggle_ff_10_rotate_x = dummy_ff_10.addOrReplaceChild("toggle_ff_10_rotate_x", CubeListBuilder.create().texOffs(3, 40).addBox(-0.5F, -0.5723F, -0.585F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.2F))
		.texOffs(3, 40).addBox(-0.66F, -1.3723F, -0.585F, 1.0F, 2.0F, 1.0F, new CubeDeformation(-0.42F))
		.texOffs(3, 40).addBox(-0.5F, -1.3723F, -0.585F, 1.0F, 2.0F, 1.0F, new CubeDeformation(-0.42F))
		.texOffs(3, 40).addBox(-0.34F, -1.3723F, -0.585F, 1.0F, 2.0F, 1.0F, new CubeDeformation(-0.42F)), PartPose.offsetAndRotation(0.0F, 0.4917F, 1.0741F, -0.5236F, 0.0F, 0.0F));

		PartDefinition dummy_ff_6 = controls_6.addOrReplaceChild("dummy_ff_6", CubeListBuilder.create().texOffs(5, 100).addBox(2.75F, 3.4641F, 5.5F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.1F)), PartPose.offsetAndRotation(-4.5F, -19.5F, -13.5F, 0.5236F, 0.0F, 0.0F));

		PartDefinition dummy_ff_7 = controls_6.addOrReplaceChild("dummy_ff_7", CubeListBuilder.create().texOffs(5, 100).addBox(4.0F, 0.0F, 8.0F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.1F)), PartPose.offsetAndRotation(-4.5F, -15.5F, -13.5F, 0.5236F, 0.0F, 0.0F));

		PartDefinition dummy_ff_8 = controls_6.addOrReplaceChild("dummy_ff_8", CubeListBuilder.create().texOffs(5, 100).addBox(5.25F, 0.0F, 7.5F, 1.0F, 1.0F, 1.0F, new CubeDeformation(-0.1F)), PartPose.offsetAndRotation(-4.5F, -15.5F, -13.5F, 0.5236F, 0.0F, 0.0F));

		PartDefinition decoration_6 = controls_6.addOrReplaceChild("decoration_6", CubeListBuilder.create().texOffs(3, 40).addBox(1.45F, 0.4F, 2.25F, 6.0F, 1.0F, 5.0F, new CubeDeformation(0.0F))
		.texOffs(5, 100).addBox(5.1F, 0.1F, 7.0F, 0.0F, 1.0F, 0.0F, new CubeDeformation(0.0F))
		.texOffs(5, 100).addBox(4.1F, 0.1F, 7.0F, 0.0F, 1.0F, 0.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(-4.5F, -15.5F, -13.5F, 0.5236F, 0.0F, 0.0F));

		PartDefinition bb_main = partdefinition.addOrReplaceChild("bb_main", CubeListBuilder.create().texOffs(108, 121).addBox(-5.475F, -17.65F, 12.3F, 3.0F, 3.0F, 2.0F, new CubeDeformation(0.0F))
		.texOffs(104, 119).addBox(2.8F, -17.6F, 11.5F, 3.0F, 3.0F, 4.0F, new CubeDeformation(0.0F))
		.texOffs(110, 122).addBox(-13.5F, -16.5F, 1.925F, 2.0F, 2.0F, 2.0F, new CubeDeformation(0.0F))
		.texOffs(109, 121).addBox(-9.0F, -16.5F, -10.6F, 2.0F, 2.0F, 2.0F, new CubeDeformation(0.0F))
		.texOffs(105, 121).addBox(9.3F, -17.0F, 5.0F, 2.0F, 2.0F, 2.0F, new CubeDeformation(0.0F))
		.texOffs(114, 124).addBox(7.45F, -18.575F, 2.3F, 1.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(114, 123).addBox(6.7F, -18.65F, 3.75F, 1.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(113, 124).addBox(5.875F, -18.5F, 5.1F, 1.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(106, 120).addBox(7.0F, -17.5F, 7.75F, 3.0F, 3.0F, 3.0F, new CubeDeformation(0.0F))
		.texOffs(106, 120).addBox(10.7F, -17.5F, 1.4F, 3.0F, 3.0F, 3.0F, new CubeDeformation(0.0F))
		.texOffs(106, 121).addBox(-9.8F, -16.6F, 8.2F, 3.0F, 2.0F, 3.0F, new CubeDeformation(0.0F))
		.texOffs(101, 118).addBox(-2.05F, -20.5F, 4.975F, 4.0F, 3.0F, 4.0F, new CubeDeformation(0.0F))
		.texOffs(100, 117).addBox(-8.9F, -19.3F, -6.5F, 4.0F, 3.0F, 5.0F, new CubeDeformation(0.0F))
		.texOffs(110, 122).addBox(3.625F, -16.525F, -13.0F, 2.0F, 2.0F, 2.0F, new CubeDeformation(0.0F))
		.texOffs(110, 122).addBox(7.6F, -16.3F, -11.4F, 2.0F, 2.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 24.0F, 0.0F));

		return LayerDefinition.create(meshdefinition, 128, 128);
	}

	@Override
	public void renderToBuffer(PoseStack poseStack, VertexConsumer vertexConsumer, int packedLight, int packedOverlay, float red, float green, float blue, float alpha) {
		console.render(poseStack, vertexConsumer, packedLight, packedOverlay, red, green, blue, alpha);
		time_scepter.render(poseStack, vertexConsumer, packedLight, packedOverlay, red, green, blue, alpha);
		controls.render(poseStack, vertexConsumer, packedLight, packedOverlay, red, green, blue, alpha);
		bb_main.render(poseStack, vertexConsumer, packedLight, packedOverlay, red, green, blue, alpha);
	}

	@Override
	public void setupAnimations(T tile, float ageInTicks) {
		this.root().getAllParts().forEach(ModelPart::resetPose);
		this.animate(tile.rotorAnimationState, GalvanicConsoleAnimations.ROTOR, ageInTicks);
		Capabilities.getCap(Capabilities.TARDIS, Minecraft.getInstance().level).ifPresent(tardis -> {
			this.animate(tardis.getControlDataOrCreate(ControlRegistry.REFUELER.get()).getUseAnimationState(), GalvanicConsoleAnimations.ARTRON_USE, ageInTicks);
			this.animate(tardis.getControlDataOrCreate(ControlRegistry.FAST_RETURN.get()).getUseAnimationState(), GalvanicConsoleAnimations.FAST_RETURN, ageInTicks);
			this.animate(tardis.getControlDataOrCreate(ControlRegistry.RANDOMIZER.get()).getUseAnimationState(), GalvanicConsoleAnimations.RANDOMIZER, ageInTicks);
			this.animate(tardis.getControlDataOrCreate(ControlRegistry.DOOR.get()).getUseAnimationState(), GalvanicConsoleAnimations.DOOR, ageInTicks);
			GalvanicConsoleAnimations.animateConditional(tardis, this, ageInTicks);
		});
	}

	@Override
	public Optional<String> getPartForControl(ControlType<?> type) {
		if(type == ControlRegistry.SONIC_PORT.get()){
			return Optional.of("controls/controls_3/sonic_port/sonic_base");
		}
		return Optional.empty();
	}
}