package net.tardis.mod.client.models.exteriors.interior_door;
// Exported for Minecraft version 1.17 or later with Mojang mappings
// Paste this class into your mod and generate all required imports


import com.mojang.blaze3d.vertex.PoseStack;
import com.mojang.blaze3d.vertex.VertexConsumer;
import net.minecraft.client.model.geom.ModelLayerLocation;
import net.minecraft.client.model.geom.ModelPart;
import net.minecraft.client.model.geom.PartPose;
import net.minecraft.client.model.geom.builders.*;
import net.tardis.mod.blockentities.InteriorDoorTile;
import net.tardis.mod.client.animations.AnimationHelper;
import net.tardis.mod.client.models.BasicTileHierarchicalModel;
import net.tardis.mod.helpers.Helper;

public class TrunkInteriorModel extends BasicTileHierarchicalModel<InteriorDoorTile> {
	// This layer location should be baked with EntityRendererProvider.Context in the entity renderer and passed into this model's constructor
	public static final ModelLayerLocation LAYER_LOCATION = new ModelLayerLocation(Helper.createRL("exteriors/interiors/trunk"), "main");
	private final ModelPart interior_door_trunk;
	private final ModelPart frame;
	private final ModelPart walls;
	private final ModelPart corners;
	private final ModelPart lid_rotate_y;
	private final ModelPart walls2;
	private final ModelPart corners2;

	public TrunkInteriorModel(ModelPart root) {
		super(root);
		this.interior_door_trunk = root.getChild("interior_door_trunk");
		this.frame = this.interior_door_trunk.getChild("frame");
		this.walls = this.frame.getChild("walls");
		this.corners = this.frame.getChild("corners");
		this.lid_rotate_y = this.interior_door_trunk.getChild("lid_rotate_y");
		this.walls2 = this.lid_rotate_y.getChild("walls2");
		this.corners2 = this.lid_rotate_y.getChild("corners2");
	}

	public static LayerDefinition createBodyLayer() {
		MeshDefinition meshdefinition = new MeshDefinition();
		PartDefinition partdefinition = meshdefinition.getRoot();

		PartDefinition interior_door_trunk = partdefinition.addOrReplaceChild("interior_door_trunk", CubeListBuilder.create(), PartPose.offsetAndRotation(0.0F, 24.0F, 0.0F, 0.0F, 3.1416F, 0.0F));

		PartDefinition frame = interior_door_trunk.addOrReplaceChild("frame", CubeListBuilder.create(), PartPose.offset(0.0F, 0.0F, -5.55F));

		PartDefinition walls = frame.addOrReplaceChild("walls", CubeListBuilder.create(), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition back_wall = walls.addOrReplaceChild("back_wall", CubeListBuilder.create(), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition sidewall_right = walls.addOrReplaceChild("sidewall_right", CubeListBuilder.create(), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition sidewall_left = walls.addOrReplaceChild("sidewall_left", CubeListBuilder.create(), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition top_wall = walls.addOrReplaceChild("top_wall", CubeListBuilder.create(), PartPose.offsetAndRotation(0.5F, 0.0F, 5.5F, 0.0F, 3.1416F, 0.0F));

		PartDefinition bottom_wall = walls.addOrReplaceChild("bottom_wall", CubeListBuilder.create(), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition trim = frame.addOrReplaceChild("trim", CubeListBuilder.create().texOffs(99, 95).addBox(7.5F, -31.0F, -2.5F, 1.0F, 29.0F, 1.0F, new CubeDeformation(0.1F))
		.texOffs(103, 95).addBox(-8.0F, -31.0F, -2.5F, 1.0F, 29.0F, 1.0F, new CubeDeformation(0.1F))
		.texOffs(85, 86).addBox(-6.25F, -32.75F, -2.5F, 13.0F, 1.0F, 1.0F, new CubeDeformation(0.1F))
		.texOffs(85, 86).addBox(-6.25F, -1.25F, -2.5F, 13.0F, 1.0F, 1.0F, new CubeDeformation(0.1F)), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition corners = frame.addOrReplaceChild("corners", CubeListBuilder.create(), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition knobs5 = corners.addOrReplaceChild("knobs5", CubeListBuilder.create().texOffs(118, 121).addBox(-8.25F, -33.0F, -2.5F, 2.0F, 2.0F, 2.0F, new CubeDeformation(0.02F))
		.texOffs(75, 117).addBox(-8.5F, -32.5F, -2.0F, 1.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(75, 117).addBox(-7.75F, -33.25F, -2.0F, 1.0F, 1.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 0.0F, -0.25F));

		PartDefinition knobs6 = corners.addOrReplaceChild("knobs6", CubeListBuilder.create().texOffs(118, 121).addBox(6.75F, -33.0F, -2.5F, 2.0F, 2.0F, 2.0F, new CubeDeformation(0.02F))
		.texOffs(75, 117).addBox(8.0F, -32.5F, -2.0F, 1.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(75, 117).addBox(7.25F, -33.25F, -2.0F, 1.0F, 1.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 0.0F, -0.25F));

		PartDefinition knobs7 = corners.addOrReplaceChild("knobs7", CubeListBuilder.create().texOffs(118, 121).addBox(6.75F, -2.0F, -2.5F, 2.0F, 2.0F, 2.0F, new CubeDeformation(0.02F))
		.texOffs(75, 117).addBox(8.0F, -1.5F, -2.0F, 1.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(75, 117).addBox(7.25F, -0.75F, -2.0F, 1.0F, 1.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 0.0F, -0.25F));

		PartDefinition knobs8 = corners.addOrReplaceChild("knobs8", CubeListBuilder.create().texOffs(118, 121).addBox(-8.25F, -2.0F, -2.5F, 2.0F, 2.0F, 2.0F, new CubeDeformation(0.02F))
		.texOffs(75, 117).addBox(-8.5F, -1.5F, -2.0F, 1.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(75, 117).addBox(-7.75F, -0.75F, -2.0F, 1.0F, 1.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 0.0F, -0.25F));

		PartDefinition hinge = frame.addOrReplaceChild("hinge", CubeListBuilder.create().texOffs(117, 106).addBox(-8.9978F, 13.3333F, -4.6694F, 2.0F, 5.0F, 2.0F, new CubeDeformation(-0.5F))
		.texOffs(117, 106).addBox(-8.9978F, 3.3333F, -4.6694F, 2.0F, 5.0F, 2.0F, new CubeDeformation(-0.5F))
		.texOffs(117, 106).addBox(-8.9978F, 25.3333F, -4.6694F, 2.0F, 5.0F, 2.0F, new CubeDeformation(-0.5F)), PartPose.offsetAndRotation(17.0F, -33.0833F, -5.5F, 0.0F, 0.7854F, 0.0F));

		PartDefinition lid_rotate_y = interior_door_trunk.addOrReplaceChild("lid_rotate_y", CubeListBuilder.create(), PartPose.offset(8.75F, -17.25F, -7.55F));

		PartDefinition walls2 = lid_rotate_y.addOrReplaceChild("walls2", CubeListBuilder.create(), PartPose.offset(-0.25F, 17.25F, 0.25F));

		PartDefinition back_wall2 = walls2.addOrReplaceChild("back_wall2", CubeListBuilder.create().texOffs(0, 0).addBox(-16.25F, -32.5F, -5.5F, 16.0F, 32.0F, 5.0F, new CubeDeformation(-0.002F)), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition sidewall_right2 = walls2.addOrReplaceChild("sidewall_right2", CubeListBuilder.create().texOffs(81, 2).addBox(-1.25F, -32.0F, -5.0F, 1.0F, 31.0F, 4.0F, new CubeDeformation(0.002F)), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition sidewall_left2 = walls2.addOrReplaceChild("sidewall_left2", CubeListBuilder.create().texOffs(92, 2).addBox(-16.25F, -32.0F, -5.0F, 1.0F, 31.0F, 4.0F, new CubeDeformation(0.002F)), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition top_wall2 = walls2.addOrReplaceChild("top_wall2", CubeListBuilder.create().texOffs(64, 53).addBox(-8.0F, -0.5F, -2.0F, 16.0F, 1.0F, 4.0F, new CubeDeformation(-0.001F)), PartPose.offset(-8.25F, -32.0F, -3.0F));

		PartDefinition bottom_wall2 = walls2.addOrReplaceChild("bottom_wall2", CubeListBuilder.create().texOffs(4, 41).addBox(-16.25F, -1.5F, -5.0F, 16.0F, 1.0F, 4.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition trim2 = lid_rotate_y.addOrReplaceChild("trim2", CubeListBuilder.create().texOffs(107, 95).addBox(-16.5F, -31.0F, -1.25F, 1.0F, 29.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(107, 95).addBox(-16.5F, -31.0F, -5.75F, 1.0F, 29.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(111, 95).addBox(-1.0F, -31.0F, -1.25F, 1.0F, 29.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(83, 89).addBox(-15.75F, -32.75F, -5.75F, 15.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(83, 89).addBox(-15.75F, -1.25F, -5.75F, 15.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(74, 120).addBox(-1.0F, -1.25F, -4.0F, 1.0F, 1.0F, 2.0F, new CubeDeformation(0.0F))
		.texOffs(74, 120).addBox(-16.5F, -1.25F, -4.0F, 1.0F, 1.0F, 2.0F, new CubeDeformation(0.0F))
		.texOffs(74, 100).addBox(-16.5F, -32.75F, -4.0F, 1.0F, 1.0F, 2.0F, new CubeDeformation(0.0F))
		.texOffs(74, 120).addBox(-1.0F, -32.75F, -4.0F, 1.0F, 1.0F, 2.0F, new CubeDeformation(0.0F))
		.texOffs(83, 93).addBox(-1.0F, -32.0F, -5.75F, 1.0F, 31.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(85, 86).addBox(-14.75F, -1.25F, -1.25F, 13.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(85, 86).addBox(-14.75F, -32.75F, -1.25F, 13.0F, 1.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offset(-0.25F, 17.25F, 0.25F));

		PartDefinition corners2 = lid_rotate_y.addOrReplaceChild("corners2", CubeListBuilder.create(), PartPose.offset(-0.25F, 17.25F, 0.25F));

		PartDefinition knobs9 = corners2.addOrReplaceChild("knobs9", CubeListBuilder.create().texOffs(118, 121).addBox(-16.75F, -2.0F, -6.0F, 2.0F, 2.0F, 2.0F, new CubeDeformation(0.0F))
		.texOffs(75, 117).addBox(-16.25F, -1.5F, -6.25F, 1.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(75, 117).addBox(-17.0F, -1.5F, -5.5F, 1.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(75, 117).addBox(-16.25F, -0.75F, -5.5F, 1.0F, 1.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition knobs10 = corners2.addOrReplaceChild("knobs10", CubeListBuilder.create().texOffs(118, 121).addBox(-1.75F, -2.0F, -6.0F, 2.0F, 2.0F, 2.0F, new CubeDeformation(0.0F))
		.texOffs(75, 117).addBox(-1.25F, -1.5F, -6.25F, 1.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(75, 117).addBox(-0.5F, -1.5F, -5.5F, 1.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(75, 117).addBox(-1.25F, -0.75F, -5.5F, 1.0F, 1.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition knobs11 = corners2.addOrReplaceChild("knobs11", CubeListBuilder.create().texOffs(118, 121).addBox(-1.75F, -33.0F, -6.0F, 2.0F, 2.0F, 2.0F, new CubeDeformation(0.0F))
		.texOffs(75, 117).addBox(-1.25F, -32.5F, -6.25F, 1.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(75, 117).addBox(-0.5F, -32.5F, -5.5F, 1.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(75, 117).addBox(-1.25F, -33.25F, -5.5F, 1.0F, 1.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition knobs12 = corners2.addOrReplaceChild("knobs12", CubeListBuilder.create().texOffs(118, 121).addBox(-16.75F, -33.0F, -6.0F, 2.0F, 2.0F, 2.0F, new CubeDeformation(0.0F))
		.texOffs(75, 117).addBox(-16.25F, -32.5F, -6.25F, 1.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(75, 117).addBox(-17.0F, -32.5F, -5.5F, 1.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(75, 117).addBox(-16.25F, -33.25F, -5.5F, 1.0F, 1.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition knobs13 = corners2.addOrReplaceChild("knobs13", CubeListBuilder.create().texOffs(118, 121).addBox(-16.75F, -33.0F, -2.25F, 2.0F, 2.0F, 2.0F, new CubeDeformation(0.0F))
		.texOffs(75, 117).addBox(-17.0F, -32.5F, -1.75F, 1.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(75, 117).addBox(-16.25F, -33.25F, -1.75F, 1.0F, 1.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition knobs14 = corners2.addOrReplaceChild("knobs14", CubeListBuilder.create().texOffs(118, 121).addBox(-1.75F, -33.0F, -2.25F, 2.0F, 2.0F, 2.0F, new CubeDeformation(0.0F))
		.texOffs(75, 117).addBox(-0.5F, -32.5F, -1.75F, 1.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(75, 117).addBox(-1.25F, -33.25F, -1.75F, 1.0F, 1.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition knobs15 = corners2.addOrReplaceChild("knobs15", CubeListBuilder.create().texOffs(118, 121).addBox(-1.75F, -2.0F, -2.25F, 2.0F, 2.0F, 2.0F, new CubeDeformation(0.0F))
		.texOffs(75, 117).addBox(-0.5F, -1.5F, -1.75F, 1.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(75, 117).addBox(-1.25F, -0.75F, -1.75F, 1.0F, 1.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition knobs16 = corners2.addOrReplaceChild("knobs16", CubeListBuilder.create().texOffs(118, 121).addBox(-16.75F, -2.0F, -2.25F, 2.0F, 2.0F, 2.0F, new CubeDeformation(0.0F))
		.texOffs(75, 117).addBox(-17.0F, -1.5F, -1.75F, 1.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(75, 117).addBox(-16.25F, -0.75F, -1.75F, 1.0F, 1.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition mount2 = lid_rotate_y.addOrReplaceChild("mount2", CubeListBuilder.create().texOffs(116, 114).addBox(-1.425F, -6.75F, -3.0F, 2.0F, 3.0F, 3.0F, new CubeDeformation(-0.5F))
		.texOffs(116, 114).addBox(-1.425F, -18.75F, -3.0F, 2.0F, 3.0F, 3.0F, new CubeDeformation(-0.5F))
		.texOffs(116, 114).addBox(-1.425F, -28.75F, -3.0F, 2.0F, 3.0F, 3.0F, new CubeDeformation(-0.5F)), PartPose.offset(-0.25F, 17.25F, 0.25F));

		PartDefinition sticker3 = lid_rotate_y.addOrReplaceChild("sticker3", CubeListBuilder.create().texOffs(110, 2).addBox(-10.258F, -6.232F, 2.2F, 7.0F, 3.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(-1.75F, -5.25F, -7.5F, 0.0F, 0.0F, -0.6109F));

		PartDefinition sticker4 = lid_rotate_y.addOrReplaceChild("sticker4", CubeListBuilder.create().texOffs(115, 7).addBox(-10.1247F, -0.5674F, 2.4F, 4.0F, 4.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(-1.75F, -5.25F, -7.75F, 0.0F, 0.0F, 0.7854F));

		PartDefinition boti = interior_door_trunk.addOrReplaceChild("boti", CubeListBuilder.create().texOffs(1, 95).addBox(-7.501F, -35.751F, 2.399F, 15.0F, 31.0F, 1.0F, new CubeDeformation(-0.002F)), PartPose.offset(0.0F, 4.0F, -11.3F));

		return LayerDefinition.create(meshdefinition, 128, 128);
	}

	@Override
	public void renderToBuffer(PoseStack poseStack, VertexConsumer vertexConsumer, int packedLight, int packedOverlay, float red, float green, float blue, float alpha) {
		interior_door_trunk.render(poseStack, vertexConsumer, packedLight, packedOverlay, red, green, blue, alpha);
	}

	@Override
	public void setupAnimations(InteriorDoorTile tile, float ageInTicks) {
		this.root().getAllParts().forEach(ModelPart::resetPose);
		if(tile.getDoorHandler().getDoorState().isOpen()){
			this.lid_rotate_y.offsetRotation(AnimationHelper.degrees(-90, AnimationHelper.Axis.Y));
		}
	}
}