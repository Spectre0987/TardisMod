package net.tardis.mod.client.models;

import net.minecraft.world.level.block.entity.BlockEntity;

public interface IAnimatableTileModel<T extends BlockEntity> {

    void setupAnimations(T tile, float ageInTicks);

}
