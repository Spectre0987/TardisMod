package net.tardis.mod.client.models.exteriors;


import com.mojang.blaze3d.vertex.PoseStack;
import com.mojang.blaze3d.vertex.VertexConsumer;
import net.minecraft.client.model.geom.ModelLayerLocation;
import net.minecraft.client.model.geom.ModelPart;
import net.minecraft.client.model.geom.PartPose;
import net.minecraft.client.model.geom.builders.*;
import net.minecraft.client.renderer.RenderType;
import net.tardis.mod.blockentities.exteriors.ExteriorTile;
import net.tardis.mod.client.animations.AnimationHelper;
import net.tardis.mod.client.models.BasicTileHierarchicalModel;
import net.tardis.mod.helpers.Helper;

public class TrunkExteriorModel<T extends ExteriorTile> extends BasicTileHierarchicalModel<T> implements IExteriorModel<T> {
	// This layer location should be baked with EntityRendererProvider.Context in the entity renderer and passed into this model's constructor
	public static final ModelLayerLocation LAYER_LOCATION = new ModelLayerLocation(Helper.createRL("exteriors/trunk"), "main");
	private final ModelPart box;
	private final ModelPart walls;
	private final ModelPart strap;
	private final ModelPart corners;
	private final ModelPart lid_rotate_y;
	private final ModelPart walls2;
	private final ModelPart corners2;
	private final ModelPart boti;

	public TrunkExteriorModel(ModelPart root) {
		super(root, RenderType::entityTranslucent);
		this.box = root.getChild("box");
		this.walls = this.box.getChild("walls");
		this.strap = this.box.getChild("strap");
		this.corners = this.box.getChild("corners");
		this.lid_rotate_y = root.getChild("lid_rotate_y");
		this.walls2 = this.lid_rotate_y.getChild("walls2");
		this.corners2 = this.lid_rotate_y.getChild("corners2");
		this.boti = root.getChild("boti");
	}

	public static LayerDefinition createBodyLayer() {
		MeshDefinition meshdefinition = new MeshDefinition();
		PartDefinition partdefinition = meshdefinition.getRoot();

		PartDefinition box = partdefinition.addOrReplaceChild("box", CubeListBuilder.create(), PartPose.offset(0.0F, 24.0F, 0.0F));

		PartDefinition walls = box.addOrReplaceChild("walls", CubeListBuilder.create(), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition back_wall = walls.addOrReplaceChild("back_wall", CubeListBuilder.create().texOffs(44, 3).addBox(-7.75F, -32.35F, 5.8F, 16.0F, 32.0F, 2.0F, new CubeDeformation(-0.01F)), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition sidewall_right = walls.addOrReplaceChild("sidewall_right", CubeListBuilder.create().texOffs(2, 53).addBox(7.25F, -32.25F, -2.0F, 1.0F, 31.0F, 9.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition sidewall_left = walls.addOrReplaceChild("sidewall_left", CubeListBuilder.create().texOffs(35, 87).addBox(-7.875F, -32.0F, -1.75F, 1.0F, 31.0F, 9.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition top_wall = walls.addOrReplaceChild("top_wall", CubeListBuilder.create().texOffs(20, 49).addBox(-7.75F, -32.5F, -2.25F, 16.0F, 1.0F, 10.0F, new CubeDeformation(-0.02F)), PartPose.offsetAndRotation(0.5F, 0.0F, 5.5F, 0.0F, 3.1416F, 0.0F));

		PartDefinition bottom_wall = walls.addOrReplaceChild("bottom_wall", CubeListBuilder.create().texOffs(37, 38).addBox(-7.75F, -1.25F, -1.75F, 16.0F, 1.0F, 9.0F, new CubeDeformation(-0.01F)), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition trim = box.addOrReplaceChild("trim", CubeListBuilder.create().texOffs(91, 93).addBox(-8.0F, -32.0F, 7.0F, 1.0F, 31.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(95, 93).addBox(7.5F, -32.0F, 7.0F, 1.0F, 31.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(83, 89).addBox(-7.25F, -32.75F, 7.0F, 15.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(83, 89).addBox(-7.25F, -1.25F, 7.0F, 15.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(61, 115).addBox(7.5F, -1.25F, -1.75F, 1.0F, 1.0F, 9.0F, new CubeDeformation(0.0F))
		.texOffs(61, 115).addBox(-8.0F, -1.25F, -1.75F, 1.0F, 1.0F, 9.0F, new CubeDeformation(0.0F))
		.texOffs(61, 115).addBox(-8.0F, -32.75F, -1.75F, 1.0F, 1.0F, 9.0F, new CubeDeformation(0.0F))
		.texOffs(61, 115).addBox(7.5F, -32.75F, -1.75F, 1.0F, 1.0F, 9.0F, new CubeDeformation(0.0F))
		.texOffs(99, 95).addBox(7.5F, -31.0F, -2.5F, 1.0F, 29.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(103, 95).addBox(-8.0F, -31.0F, -2.5F, 1.0F, 29.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(85, 86).addBox(-6.25F, -32.75F, -2.5F, 13.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(85, 86).addBox(-6.25F, -1.25F, -2.5F, 13.0F, 1.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition strap = box.addOrReplaceChild("strap", CubeListBuilder.create(), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition leather = strap.addOrReplaceChild("leather", CubeListBuilder.create().texOffs(75, 60).addBox(-8.25F, -33.0F, 2.0F, 16.0F, 1.0F, 2.0F, new CubeDeformation(0.01F))
		.texOffs(119, 47).addBox(-8.25F, -33.0F, 2.0F, 1.0F, 33.0F, 2.0F, new CubeDeformation(0.0F))
		.texOffs(75, 64).addBox(-7.25F, -1.0F, 2.0F, 16.0F, 1.0F, 2.0F, new CubeDeformation(0.0F))
		.texOffs(112, 48).addBox(7.75F, -33.0F, 2.0F, 1.0F, 32.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition buckle = strap.addOrReplaceChild("buckle", CubeListBuilder.create().texOffs(117, 101).addBox(-8.5F, -29.0F, 1.5F, 1.0F, 1.0F, 3.0F, new CubeDeformation(0.0F))
		.texOffs(121, 91).addBox(-8.5F, -26.0F, 2.5F, 1.0F, 1.0F, 2.0F, new CubeDeformation(0.0F))
		.texOffs(123, 95).addBox(-8.5F, -28.0F, 3.5F, 1.0F, 2.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(123, 99).addBox(-8.5F, -28.0F, 1.5F, 1.0F, 3.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition corners = box.addOrReplaceChild("corners", CubeListBuilder.create(), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition knobs1 = corners.addOrReplaceChild("knobs1", CubeListBuilder.create().texOffs(118, 121).addBox(-8.25F, -2.0F, 6.25F, 2.0F, 2.0F, 2.0F, new CubeDeformation(0.0F))
		.texOffs(75, 117).addBox(-7.75F, -1.5F, 7.5F, 1.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(75, 117).addBox(-8.5F, -1.5F, 6.75F, 1.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(75, 117).addBox(-7.75F, -0.75F, 6.75F, 1.0F, 1.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition knobs2 = corners.addOrReplaceChild("knobs2", CubeListBuilder.create().texOffs(118, 121).addBox(6.75F, -2.0F, 6.25F, 2.0F, 2.0F, 2.0F, new CubeDeformation(0.0F))
		.texOffs(75, 117).addBox(7.25F, -1.5F, 7.5F, 1.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(75, 117).addBox(8.0F, -1.5F, 6.75F, 1.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(75, 117).addBox(7.25F, -0.75F, 6.75F, 1.0F, 1.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition knobs3 = corners.addOrReplaceChild("knobs3", CubeListBuilder.create().texOffs(118, 121).addBox(6.75F, -33.0F, 6.25F, 2.0F, 2.0F, 2.0F, new CubeDeformation(0.0F))
		.texOffs(75, 117).addBox(7.25F, -32.5F, 7.5F, 1.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(75, 117).addBox(8.0F, -32.5F, 6.75F, 1.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(75, 117).addBox(7.25F, -33.25F, 6.75F, 1.0F, 1.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition knobs4 = corners.addOrReplaceChild("knobs4", CubeListBuilder.create().texOffs(118, 121).addBox(-8.25F, -33.0F, 6.25F, 2.0F, 2.0F, 2.0F, new CubeDeformation(0.0F))
		.texOffs(75, 117).addBox(-7.75F, -32.5F, 7.5F, 1.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(75, 117).addBox(-8.5F, -32.5F, 6.75F, 1.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(75, 117).addBox(-7.75F, -33.25F, 6.75F, 1.0F, 1.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition knobs5 = corners.addOrReplaceChild("knobs5", CubeListBuilder.create().texOffs(118, 121).addBox(-8.25F, -33.0F, -2.5F, 2.0F, 2.0F, 2.0F, new CubeDeformation(0.0F))
		.texOffs(75, 117).addBox(-8.5F, -32.5F, -2.0F, 1.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(75, 117).addBox(-7.75F, -33.25F, -2.0F, 1.0F, 1.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition knobs6 = corners.addOrReplaceChild("knobs6", CubeListBuilder.create().texOffs(118, 121).addBox(6.75F, -33.0F, -2.5F, 2.0F, 2.0F, 2.0F, new CubeDeformation(0.0F))
		.texOffs(75, 117).addBox(8.0F, -32.5F, -2.0F, 1.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(75, 117).addBox(7.25F, -33.25F, -2.0F, 1.0F, 1.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition knobs7 = corners.addOrReplaceChild("knobs7", CubeListBuilder.create().texOffs(118, 121).addBox(6.75F, -2.0F, -2.5F, 2.0F, 2.0F, 2.0F, new CubeDeformation(0.0F))
		.texOffs(75, 117).addBox(8.0F, -1.5F, -2.0F, 1.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(75, 117).addBox(7.25F, -0.75F, -2.0F, 1.0F, 1.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition knobs8 = corners.addOrReplaceChild("knobs8", CubeListBuilder.create().texOffs(118, 121).addBox(-8.25F, -2.0F, -2.5F, 2.0F, 2.0F, 2.0F, new CubeDeformation(0.0F))
		.texOffs(75, 117).addBox(-8.5F, -1.5F, -2.0F, 1.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(75, 117).addBox(-7.75F, -0.75F, -2.0F, 1.0F, 1.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition hinge = box.addOrReplaceChild("hinge", CubeListBuilder.create().texOffs(117, 106).addBox(-8.9978F, 13.3333F, -4.6694F, 2.0F, 5.0F, 2.0F, new CubeDeformation(-0.5F))
		.texOffs(117, 106).addBox(-8.9978F, 3.3333F, -4.6694F, 2.0F, 5.0F, 2.0F, new CubeDeformation(-0.5F))
		.texOffs(117, 106).addBox(-8.9978F, 25.3333F, -4.6694F, 2.0F, 5.0F, 2.0F, new CubeDeformation(-0.5F)), PartPose.offsetAndRotation(17.0F, -33.0833F, -5.5F, 0.0F, 0.7854F, 0.0F));

		PartDefinition mount = box.addOrReplaceChild("mount", CubeListBuilder.create().texOffs(116, 114).addBox(7.275F, -28.75F, -5.25F, 2.0F, 3.0F, 3.0F, new CubeDeformation(-0.5F))
		.texOffs(116, 114).addBox(7.275F, -18.75F, -5.25F, 2.0F, 3.0F, 3.0F, new CubeDeformation(-0.5F))
		.texOffs(116, 114).addBox(7.275F, -6.75F, -5.25F, 2.0F, 3.0F, 3.0F, new CubeDeformation(-0.5F)), PartPose.offset(0.0F, 0.0F, 2.75F));

		PartDefinition latch = box.addOrReplaceChild("latch", CubeListBuilder.create().texOffs(76, 105).addBox(-8.5F, -18.0F, -1.625F, 1.0F, 2.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition lock = box.addOrReplaceChild("lock", CubeListBuilder.create().texOffs(104, 11).addBox(8.5F, 7.4142F, -8.0858F, 1.0F, 2.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(-16.5F, -34.0F, 0.0F, 0.7854F, 0.0F, 0.0F));

		PartDefinition sticker1 = box.addOrReplaceChild("sticker1", CubeListBuilder.create().texOffs(103, 3).addBox(8.1F, 21.5356F, -14.9679F, 1.0F, 3.0F, 4.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(-16.0F, -47.0F, 0.0F, 0.6981F, 0.0F, 0.0F));

		PartDefinition sticker2 = box.addOrReplaceChild("sticker2", CubeListBuilder.create().texOffs(111, 13).addBox(1.3553F, 12.125F, -8.975F, 6.0F, 4.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(2.0F, -20.0F, 16.0F, 0.0F, 0.0F, 0.5236F));

		PartDefinition lid_rotate_y = partdefinition.addOrReplaceChild("lid_rotate_y", CubeListBuilder.create(), PartPose.offset(8.75F, 6.75F, -2.5F));

		PartDefinition walls2 = lid_rotate_y.addOrReplaceChild("walls2", CubeListBuilder.create(), PartPose.offset(-0.25F, 17.25F, 0.25F));

		PartDefinition back_wall2 = walls2.addOrReplaceChild("back_wall2", CubeListBuilder.create().texOffs(0, 0).addBox(-16.25F, -32.5F, -5.5F, 16.0F, 32.0F, 5.0F, new CubeDeformation(-0.002F)), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition sidewall_right2 = walls2.addOrReplaceChild("sidewall_right2", CubeListBuilder.create().texOffs(81, 2).addBox(-1.25F, -32.0F, -5.0F, 1.0F, 31.0F, 4.0F, new CubeDeformation(0.002F)), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition sidewall_left2 = walls2.addOrReplaceChild("sidewall_left2", CubeListBuilder.create().texOffs(92, 2).addBox(-16.25F, -32.0F, -5.0F, 1.0F, 31.0F, 4.0F, new CubeDeformation(0.002F)), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition top_wall2 = walls2.addOrReplaceChild("top_wall2", CubeListBuilder.create().texOffs(64, 53).addBox(-8.0F, -0.5F, -2.0F, 16.0F, 1.0F, 4.0F, new CubeDeformation(-0.001F)), PartPose.offset(-8.25F, -32.0F, -3.0F));

		PartDefinition bottom_wall2 = walls2.addOrReplaceChild("bottom_wall2", CubeListBuilder.create().texOffs(4, 41).addBox(-16.25F, -1.5F, -5.0F, 16.0F, 1.0F, 4.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition trim2 = lid_rotate_y.addOrReplaceChild("trim2", CubeListBuilder.create().texOffs(107, 95).addBox(-16.5F, -31.0F, -1.25F, 1.0F, 29.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(107, 95).addBox(-16.5F, -31.0F, -5.75F, 1.0F, 29.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(111, 95).addBox(-1.0F, -31.0F, -1.25F, 1.0F, 29.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(83, 89).addBox(-15.75F, -32.75F, -5.75F, 15.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(83, 89).addBox(-15.75F, -1.25F, -5.75F, 15.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(74, 120).addBox(-1.0F, -1.25F, -4.0F, 1.0F, 1.0F, 2.0F, new CubeDeformation(0.0F))
		.texOffs(74, 120).addBox(-16.5F, -1.25F, -4.0F, 1.0F, 1.0F, 2.0F, new CubeDeformation(0.0F))
		.texOffs(74, 100).addBox(-16.5F, -32.75F, -4.0F, 1.0F, 1.0F, 2.0F, new CubeDeformation(0.0F))
		.texOffs(74, 120).addBox(-1.0F, -32.75F, -4.0F, 1.0F, 1.0F, 2.0F, new CubeDeformation(0.0F))
		.texOffs(83, 93).addBox(-1.0F, -32.0F, -5.75F, 1.0F, 31.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(85, 86).addBox(-14.75F, -1.25F, -1.25F, 13.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(85, 86).addBox(-14.75F, -32.75F, -1.25F, 13.0F, 1.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offset(-0.25F, 17.25F, 0.25F));

		PartDefinition corners2 = lid_rotate_y.addOrReplaceChild("corners2", CubeListBuilder.create(), PartPose.offset(-0.25F, 17.25F, 0.25F));

		PartDefinition knobs9 = corners2.addOrReplaceChild("knobs9", CubeListBuilder.create().texOffs(118, 121).addBox(-16.75F, -2.0F, -6.0F, 2.0F, 2.0F, 2.0F, new CubeDeformation(0.0F))
		.texOffs(75, 117).addBox(-16.25F, -1.5F, -6.25F, 1.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(75, 117).addBox(-17.0F, -1.5F, -5.5F, 1.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(75, 117).addBox(-16.25F, -0.75F, -5.5F, 1.0F, 1.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition knobs10 = corners2.addOrReplaceChild("knobs10", CubeListBuilder.create().texOffs(118, 121).addBox(-1.75F, -2.0F, -6.0F, 2.0F, 2.0F, 2.0F, new CubeDeformation(0.0F))
		.texOffs(75, 117).addBox(-1.25F, -1.5F, -6.25F, 1.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(75, 117).addBox(-0.5F, -1.5F, -5.5F, 1.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(75, 117).addBox(-1.25F, -0.75F, -5.5F, 1.0F, 1.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition knobs11 = corners2.addOrReplaceChild("knobs11", CubeListBuilder.create().texOffs(118, 121).addBox(-1.75F, -33.0F, -6.0F, 2.0F, 2.0F, 2.0F, new CubeDeformation(0.0F))
		.texOffs(75, 117).addBox(-1.25F, -32.5F, -6.25F, 1.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(75, 117).addBox(-0.5F, -32.5F, -5.5F, 1.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(75, 117).addBox(-1.25F, -33.25F, -5.5F, 1.0F, 1.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition knobs12 = corners2.addOrReplaceChild("knobs12", CubeListBuilder.create().texOffs(118, 121).addBox(-16.75F, -33.0F, -6.0F, 2.0F, 2.0F, 2.0F, new CubeDeformation(0.0F))
		.texOffs(75, 117).addBox(-16.25F, -32.5F, -6.25F, 1.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(75, 117).addBox(-17.0F, -32.5F, -5.5F, 1.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(75, 117).addBox(-16.25F, -33.25F, -5.5F, 1.0F, 1.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition knobs13 = corners2.addOrReplaceChild("knobs13", CubeListBuilder.create().texOffs(118, 121).addBox(-16.75F, -33.0F, -2.25F, 2.0F, 2.0F, 2.0F, new CubeDeformation(0.0F))
		.texOffs(75, 117).addBox(-17.0F, -32.5F, -1.75F, 1.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(75, 117).addBox(-16.25F, -33.25F, -1.75F, 1.0F, 1.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition knobs14 = corners2.addOrReplaceChild("knobs14", CubeListBuilder.create().texOffs(118, 121).addBox(-1.75F, -33.0F, -2.25F, 2.0F, 2.0F, 2.0F, new CubeDeformation(0.0F))
		.texOffs(75, 117).addBox(-0.5F, -32.5F, -1.75F, 1.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(75, 117).addBox(-1.25F, -33.25F, -1.75F, 1.0F, 1.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition knobs15 = corners2.addOrReplaceChild("knobs15", CubeListBuilder.create().texOffs(118, 121).addBox(-1.75F, -2.0F, -2.25F, 2.0F, 2.0F, 2.0F, new CubeDeformation(0.0F))
		.texOffs(75, 117).addBox(-0.5F, -1.5F, -1.75F, 1.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(75, 117).addBox(-1.25F, -0.75F, -1.75F, 1.0F, 1.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition knobs16 = corners2.addOrReplaceChild("knobs16", CubeListBuilder.create().texOffs(118, 121).addBox(-16.75F, -2.0F, -2.25F, 2.0F, 2.0F, 2.0F, new CubeDeformation(0.0F))
		.texOffs(75, 117).addBox(-17.0F, -1.5F, -1.75F, 1.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(75, 117).addBox(-16.25F, -0.75F, -1.75F, 1.0F, 1.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition mount2 = lid_rotate_y.addOrReplaceChild("mount2", CubeListBuilder.create().texOffs(116, 114).addBox(-1.425F, -6.75F, -3.0F, 2.0F, 3.0F, 3.0F, new CubeDeformation(-0.5F))
		.texOffs(116, 114).addBox(-1.425F, -18.75F, -3.0F, 2.0F, 3.0F, 3.0F, new CubeDeformation(-0.5F))
		.texOffs(116, 114).addBox(-1.425F, -28.75F, -3.0F, 2.0F, 3.0F, 3.0F, new CubeDeformation(-0.5F)), PartPose.offset(-0.25F, 17.25F, 0.25F));

		PartDefinition latch2 = lid_rotate_y.addOrReplaceChild("latch2", CubeListBuilder.create().texOffs(74, 110).addBox(-17.25F, -18.5F, -2.0F, 1.0F, 3.0F, 3.0F, new CubeDeformation(0.0F)), PartPose.offset(-0.25F, 17.25F, 0.25F));

		PartDefinition sticker3 = lid_rotate_y.addOrReplaceChild("sticker3", CubeListBuilder.create().texOffs(110, 2).addBox(-10.258F, -6.232F, 2.2F, 7.0F, 3.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(-1.75F, -5.25F, -7.5F, 0.0F, 0.0F, -0.6109F));

		PartDefinition sticker4 = lid_rotate_y.addOrReplaceChild("sticker4", CubeListBuilder.create().texOffs(115, 7).addBox(-10.1247F, -0.5674F, 2.4F, 4.0F, 4.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(-1.75F, -5.25F, -7.75F, 0.0F, 0.0F, 0.7854F));

		PartDefinition boti = partdefinition.addOrReplaceChild("boti", CubeListBuilder.create().texOffs(1, 95).addBox(-7.25F, -32.0F, -0.375F, 15.0F, 31.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 24.0F, 0.0F));

		return LayerDefinition.create(meshdefinition, 128, 128);
	}

	@Override
	public void renderToBuffer(PoseStack poseStack, VertexConsumer vertexConsumer, int packedLight, int packedOverlay, float red, float green, float blue, float alpha) {
		box.render(poseStack, vertexConsumer, packedLight, packedOverlay, red, green, blue, alpha);
		lid_rotate_y.render(poseStack, vertexConsumer, packedLight, packedOverlay, red, green, blue, alpha);
		boti.render(poseStack, vertexConsumer, packedLight, packedOverlay, red, green, blue, alpha);
	}

	@Override
	public void setupAnimations(T tile, float ageInTicks) {
		this.root().getAllParts().forEach(ModelPart::resetPose);
		if(tile.getDoorHandler().getDoorState().isOpen()){
			lid_rotate_y.offsetRotation(AnimationHelper.degrees(-90, AnimationHelper.Axis.Y));
		}
	}

	@Override
	public void animateDemat(T exterior, float age) {

	}

	@Override
	public void animateRemat(T exterior, float age) {

	}

	@Override
	public void animateSolid(T exterior, float age) {

	}
}