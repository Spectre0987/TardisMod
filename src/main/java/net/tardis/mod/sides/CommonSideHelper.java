package net.tardis.mod.sides;

import net.minecraft.sounds.SoundEvent;
import net.minecraft.sounds.SoundSource;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.level.Level;

import java.util.Optional;

public class CommonSideHelper implements ISideHelper{
    @Override
    public void openGui(int id) {

    }

    @Override
    public Optional<Level> getClientLevel() {
        return Optional.empty();
    }

    @Override
    public Optional<Player> getClientPlayer() {
        return Optional.empty();
    }

    @Override
    public Optional<Integer> getTARDISLightLevel() {
        return Optional.empty();
    }

    @Override
    public void playMovingSound(Entity target, SoundEvent soundEvent, SoundSource soundSource, float vol) {

    }
}
