package net.tardis.mod.control;

import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResult;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.LeverBlock;
import net.minecraftforge.common.capabilities.ForgeCapabilities;
import net.tardis.mod.cap.level.ITardisLevel;
import net.tardis.mod.control.datas.ControlDataItemStack;
import net.tardis.mod.helpers.InventoryHelper;
import net.tardis.mod.item.ISonicPortAction;
import net.tardis.mod.tags.ItemTags;

public class SonicPortControl extends Control<ControlDataItemStack>{

    public SonicPortControl(ControlType<ControlDataItemStack> type) {
        super(type);
    }

    @Override
    public InteractionResult onUse(Player player, InteractionHand hand, ITardisLevel level) {

        //Take items from this slot
        if(!getData(level).get().isEmpty()){
            if(hand == InteractionHand.MAIN_HAND) {
                if (!level.isClient()) {
                    InventoryHelper.addItem(player, getData(level).get());
                    getData(level).set(ItemStack.EMPTY);
                }
                return InteractionResult.sidedSuccess(level.isClient());
            }
            return InteractionResult.PASS;
        }

        //Put items in this slot
        ItemStack stack = player.getItemInHand(hand);
        if(stack.is(ItemTags.SONIC_PORT_ACCEPTS) || stack.getCapability(ForgeCapabilities.ENERGY).isPresent()){

            if(!level.isClient()){

                if(stack.getItem() instanceof ISonicPortAction item){
                    stack = item.onAddedToPort(stack, level);
                }

                getData(level).set(stack);
                player.setItemInHand(hand, ItemStack.EMPTY);
            }

            return InteractionResult.sidedSuccess(level.isClient());
        }

        return InteractionResult.PASS;
    }
}
