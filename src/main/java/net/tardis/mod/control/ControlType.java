package net.tardis.mod.control;

import java.util.EnumMap;
import java.util.Optional;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.function.Supplier;

import net.minecraft.resources.ResourceLocation;
import net.minecraft.sounds.SoundEvent;
import net.tardis.mod.Constants;
import net.tardis.mod.cap.level.ITardisLevel;
import net.tardis.mod.control.datas.ControlData;
import net.tardis.mod.registry.ControlRegistry;

public class ControlType<T extends ControlData<?>> {

    private final Function<ControlType, Control> factory;
    private final BiFunction<ControlType, ITardisLevel, T> dataFactory;
    private final EnumMap<ControlSoundAction, Supplier<SoundEvent>> soundMap = new EnumMap<>(ControlSoundAction.class);

    private String translationKey = null;


    public ControlType(Function<ControlType, Control> factory, BiFunction<ControlType, ITardisLevel, T> data){
        this.factory = factory;
        this.dataFactory = data;
    }

    public T createData(ITardisLevel level){
        return this.dataFactory.apply(this, level);
    }

    public Control create(){
        return this.factory.apply(this);
    }

    public String getTranslationKey(){
        if(this.translationKey == null){
            this.translationKey = makeTranslation();
        }
        return this.translationKey;
    }

    private String makeTranslation(){
        return Constants.Translation.makeGenericTranslation(ControlRegistry.REGISTRY.get(), this);
    }

    public ControlType<T> addSound(ControlSoundAction action, Supplier<SoundEvent> event){
        this.soundMap.put(action, event);
        return this;
    }

    public Optional<SoundEvent> getSound(ControlSoundAction action){
        if(this.soundMap.containsKey(action) && this.soundMap.get(action) != null){
            return Optional.of(this.soundMap.get(action).get());
        }
        return Optional.empty();
    }

    public enum ControlSoundAction{
        SUCCESS, FAIL, NOTIF
    }

}
