package net.tardis.mod.control;

import net.minecraft.util.Mth;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResult;
import net.minecraft.world.entity.player.Player;
import net.tardis.mod.cap.level.ITardisLevel;
import net.tardis.mod.control.datas.ControlDataBool;
import net.tardis.mod.control.datas.ControlDataFloat;
import net.tardis.mod.registry.ControlRegistry;

public class ThrottleControl extends Control<ControlDataFloat>{

    public ThrottleControl(ControlType<ControlDataFloat> type) {
        super(type);
    }

    @Override
    public InteractionResult onUse(Player player, InteractionHand hand, ITardisLevel level) {
        if(hand == InteractionHand.MAIN_HAND){
            if(!level.isClient()){
                addThrottle(level, player.isShiftKeyDown() ? -0.1F : 0.1F);
            }
            return InteractionResult.sidedSuccess(player.level.isClientSide);
        }
        return InteractionResult.PASS;
    }

    @Override
    public InteractionResult onPunch(Player player, ITardisLevel level) {

        if(!level.isClient()){
            addThrottle(level, player.isShiftKeyDown() ? -1.0F : 1.0F);
        }

        return InteractionResult.sidedSuccess(level.isClient());
    }

    public void addThrottle(ITardisLevel level, float delta){
        ControlDataFloat throttleData = level.getControlDataOrCreate(this.getType());
        float last = throttleData.get();

        throttleData.set(Mth.clamp(last + delta, 0.0F, 1.0F));
        checkAndTakeoff(level);
    }

    public static boolean checkAndTakeoff(ITardisLevel level){
        ControlDataBool brake = level.getControlDataOrCreate(ControlRegistry.HANDBRAKE.get());
        ControlDataFloat throttle = level.getControlDataOrCreate(ControlRegistry.THROTTLE.get());

        //Land if we throttle down
        if(!level.isInVortex()){
            //take off if we throttle up
            boolean takeoff = !brake.get() && throttle.get() > 0.0F;
            if(takeoff)
                level.takeoff();
            return takeoff;
        }
        return false;
    }
}
