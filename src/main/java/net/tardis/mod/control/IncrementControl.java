package net.tardis.mod.control;

import net.minecraft.network.chat.Component;
import net.minecraft.sounds.SoundEvent;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResult;
import net.minecraft.world.entity.player.Player;
import net.tardis.mod.cap.level.ITardisLevel;
import net.tardis.mod.control.datas.ControlDataInt;
import net.tardis.mod.registry.ControlRegistry;
import net.tardis.mod.sound.SoundRegistry;

public class IncrementControl extends Control<ControlDataInt>{

    public static final String MESSAGE_KEY = makeTransKey("increment");

    public static final int[] VALUES = {1, 10, 100, 1000, 10000, 100000};

    public IncrementControl(ControlType<ControlDataInt> type) {
        super(type);
    }

    @Override
    public InteractionResult onUse(Player player, InteractionHand hand, ITardisLevel level) {
        if(hand == InteractionHand.OFF_HAND)
            return InteractionResult.PASS;

        ControlDataInt data = level.getControlDataOrCreate(this.getType());

        data.set(validateIndex(data.get() + (player.isShiftKeyDown() ? -1 : 1)));
        player.displayClientMessage(Component.translatable(MESSAGE_KEY, VALUES[data.get()]), true);

        return InteractionResult.sidedSuccess(level.getLevel().isClientSide);
    }

    @Override
    public InteractionResult onPunch(Player player, ITardisLevel level) {
        ControlDataInt index = level.getControlDataOrCreate(this.getType());
        index.set(player.isShiftKeyDown() ? 0 : VALUES.length - 1);
        player.displayClientMessage(Component.translatable(MESSAGE_KEY, VALUES[index.get()]), true);
        return InteractionResult.sidedSuccess(level.isClient());
    }

    public static int validateIndex(int newIndex){
        if(newIndex >= VALUES.length){
            return 0;
        }
        if(newIndex < 0)
            return VALUES.length - 1;
        return newIndex;
    }

    public static int getActualAmount(ITardisLevel tardis){
        ControlDataInt increment = tardis.getControlDataOrCreate(ControlRegistry.INCREMENT.get());
        return VALUES[validateIndex(increment.get())];
    }

    @Override
    public SoundEvent getDefaultSuccessSound(ControlDataInt data) {
        return SoundRegistry.AXIS_CONTROL_SOUND.get();
    }
}
