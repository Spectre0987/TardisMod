package net.tardis.mod.control;

import net.minecraft.sounds.SoundEvent;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResult;
import net.minecraft.world.entity.player.Player;
import net.tardis.mod.cap.level.ITardisLevel;
import net.tardis.mod.control.datas.ControlData;
import net.tardis.mod.control.datas.ControlDataSpaceTimeCoord;
import net.tardis.mod.misc.SpaceTimeCoord;
import net.tardis.mod.sound.SoundRegistry;

public class FastReturnControl extends Control<ControlDataSpaceTimeCoord>{

    public FastReturnControl(ControlType<ControlDataSpaceTimeCoord> type) {
        super(type);
    }

    @Override
    public InteractionResult onUse(Player player, InteractionHand hand, ITardisLevel level) {

        if(hand != player.getUsedItemHand())
            return InteractionResult.PASS;

        ControlData<SpaceTimeCoord> coord = level.getControlDataOrCreate(this.getType());

        if(!SpaceTimeCoord.ZERO.equals(coord.get())){
            level.setDestination(coord.get());
            return InteractionResult.sidedSuccess(level.getLevel().isClientSide);
        }

        return InteractionResult.PASS;
    }

    @Override
    public SoundEvent getDefaultSuccessSound(ControlDataSpaceTimeCoord data) {
        return SoundRegistry.FAST_RETURN.get();
    }
}
