package net.tardis.mod.control;

import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResult;
import net.minecraft.world.entity.player.Player;
import net.tardis.mod.Tardis;
import net.tardis.mod.cap.level.ITardisLevel;
import net.tardis.mod.control.datas.ControlDataBool;
import net.tardis.mod.registry.ControlRegistry;
import net.tardis.mod.registry.SubsystemRegistry;
import net.tardis.mod.subsystem.Subsystem;
import org.apache.logging.log4j.Level;

public class StabilizerControl extends RequiresSubsystemControl<ControlDataBool>{

    public StabilizerControl(ControlType<ControlDataBool> type) {
        super(type, SubsystemRegistry.STABILIZERS);
    }

    @Override
    public InteractionResult useWithSubsystem(Player player, InteractionHand hand, Subsystem system, ITardisLevel level) {

        if(hand == InteractionHand.OFF_HAND)
            return InteractionResult.PASS;

        level.getControlDataOrCreate(this.getType()).set(!level.getControlDataOrCreate(this.getType()).get());
        level.setCurrentFlightEvent(null);
        return InteractionResult.sidedSuccess(level.isClient());
    }
}
