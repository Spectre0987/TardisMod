package net.tardis.mod.entity;

import net.minecraft.nbt.CompoundTag;
import net.minecraft.network.FriendlyByteBuf;
import net.minecraft.network.chat.Component;
import net.minecraft.network.protocol.Packet;
import net.minecraft.network.protocol.game.ClientGamePacketListener;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.sounds.SoundEvent;
import net.minecraft.sounds.SoundSource;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResult;
import net.minecraft.world.damagesource.DamageSource;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.level.Level;
import net.minecraft.world.phys.AABB;
import net.minecraft.world.phys.Vec3;
import net.minecraftforge.common.util.LazyOptional;
import net.minecraftforge.entity.IEntityAdditionalSpawnData;
import net.minecraftforge.network.NetworkHooks;
import net.minecraftforge.registries.ForgeRegistries;
import net.tardis.mod.Constants;
import net.tardis.mod.blockentities.consoles.ConsoleTile;
import net.tardis.mod.cap.Capabilities;
import net.tardis.mod.cap.level.ITardisLevel;
import net.tardis.mod.cap.level.TardisCap;
import net.tardis.mod.control.Control;
import net.tardis.mod.control.ControlPositionData;
import net.tardis.mod.control.ControlPositionDataRegistry;
import net.tardis.mod.control.ControlType;
import net.tardis.mod.control.datas.ControlData;
import net.tardis.mod.helpers.Helper;
import net.tardis.mod.network.Network;
import net.tardis.mod.network.packets.ControlEntitySizeMessage;
import net.tardis.mod.registry.ControlRegistry;

import javax.annotation.Nullable;
import java.util.List;

public class ControlEntity<T extends ControlData<?>> extends Entity implements IEntityAdditionalSpawnData {

    private Control<T> control;
    private Vec3 size;
    private ResourceLocation consoleLocation;

    private AABB controlAABB;

    public ControlEntity(Level level, ControlType<T> type, ControlPositionData data, ConsoleTile tile) {
        super(EntityRegistry.CONTROL.get(), level);
        this.control = type.create();
        this.size = data.size();
        this.consoleLocation = ForgeRegistries.BLOCK_ENTITY_TYPES.getKey(tile.getType());

    }

    public ControlEntity(EntityType<?> type, Level level){
        super(type, level);
    }

    @Nullable
    public ControlPositionData getData(ResourceLocation loc){
        return ControlPositionDataRegistry.POSITION_DATAS.containsKey(loc) ? ControlPositionDataRegistry.POSITION_DATAS.get(loc).get(this.control.getType()) : null;
    }

    public void setControl(ControlType<T> type, @Nullable ResourceLocation consoleLoc){
        this.control = type.create();
        if(consoleLoc != null){
            ControlPositionData data = this.getData(consoleLoc);
            if(data != null) {
                this.size = data.size();
                this.reapplyPosition(); //DO NOT delete, or else the entity's hitbox becomes stuck at 0,0
                if(!this.level.isClientSide)
                    Network.sendToTracking(this, new ControlEntitySizeMessage(this.getId(), this.size));
            }
        }

    }

    public Control<T> getControl(){
        return this.control;
    }

    public void setSize(Vec3 size){
        this.size = size;
        this.reapplyPosition();
    }

    @Override
    public Component getName() {
        if(this.control != null)
            return Component.translatable(Constants.Translation.makeGenericTranslation(ControlRegistry.REGISTRY.get(), this.control.getType()));
        return super.getName();
    }

    @Override
    protected void defineSynchedData() {}

    @Override
    protected void readAdditionalSaveData(CompoundTag tag) {
        ControlType<T> type = (ControlType<T>)Helper.readRegistryFromString(tag, ControlRegistry.REGISTRY.get(), "control_type").get();
        this.consoleLocation = tag.contains("console_key") ? new ResourceLocation(tag.getString("console_key")) : null;
        this.setControl(type, this.consoleLocation);
    }

    @Override
    protected void addAdditionalSaveData(CompoundTag tag) {
        Helper.writeRegistryToNBT(tag, ControlRegistry.REGISTRY.get(), this.control.getType(), "control_type");
        if(this.consoleLocation != null)
            tag.putString("console_key", this.consoleLocation.toString());

    }

    public AABB makeAABBFromControlData(Vec3 size){
        return new AABB(0, 0, 0, size.x, size.y, size.z);
    }

    public AABB getControlAABB(){
        if(this.controlAABB == null){
            if(this.size != null)
                return this.controlAABB = makeAABBFromControlData(this.size);
            return this.getBoundingBox();

        }
        return this.controlAABB;
    }

    @Override
    protected AABB makeBoundingBox() {
        return this.getControlAABB() != null ? this.getControlAABB().move(this.position()) : super.makeBoundingBox();
    }

    @Override
    public Packet<ClientGamePacketListener> getAddEntityPacket() {
        return NetworkHooks.getEntitySpawningPacket(this);
    }

    @Override
    public boolean isPickable() {
        return true;
    }

    @Override
    public boolean canBeCollidedWith() {
        return true;
    }

    @Override
    public InteractionResult interact(Player player, InteractionHand hand) {

        if(this.control != null){
            LazyOptional<ITardisLevel> tardisHolder = this.level.getCapability(Capabilities.TARDIS);

            if(tardisHolder.isPresent()){
                ITardisLevel tardis = tardisHolder.orElseThrow(NullPointerException::new);
                tardis.setLastPilot(player);
                T data = tardis.getControlDataOrCreate(this.control.getType());

                //If this has a flight event and it prevents further control proccessing
                if(TardisCap.IsFlightEventActive(tardis.getCurrentFlightEvent()) && tardis.getCurrentFlightEvent().get().onControlUse(data)){
                    data.playAnimation((int)tardis.getAnimationTicks());
                    tardis.getInteriorManager().playControlSound(this.control.getData(tardis));
                    return InteractionResult.sidedSuccess(player.getLevel().isClientSide);
                }

                //Do normal control action
                InteractionResult result = this.control.onUse(player, hand, tardis);

                //If this has a flight event and that event wants to know the control state after normal use
                tardis.getCurrentFlightEvent().ifPresent(event -> {
                    if(!event.isComplete())
                        event.onControlPostUse(data);
                });


                //Play control animation if the control did something
                if(result != InteractionResult.FAIL) {
                    data.playAnimation((int) tardis.getAnimationTicks());
                }
                playControlSound(result, data, getData(this.consoleLocation));
                return result;
            }
        }

        return InteractionResult.PASS;
    }

    public void playControlSound(InteractionResult result, T control, @Nullable ControlPositionData data){
        if(result == InteractionResult.FAIL){
            if(data != null && data.failSound().isPresent()){
                playSound(data.failSound().get().get());
                return;
            }
            playSound(this.getControl().getDefaultFailSound(control));
        }
        else if(result != InteractionResult.PASS){
            if(data != null && data.successSound().isPresent()){
                playSound(data.successSound().get().get());
                return;
            }
            playSound(getControl().getDefaultSuccessSound(control));
        }
    }

    public void playSound(SoundEvent event){
        if(!this.level.isClientSide()){
            this.level.playSound(null, this.getOnPos(), event, SoundSource.NEUTRAL, 1.0F, 1.0F);
        }
    }

    @Override
    public boolean hurt(DamageSource pSource, float pAmount) {
        this.level.getCapability(Capabilities.TARDIS).ifPresent(tardis -> {
            if(pSource.getEntity() instanceof Player player){

                final ControlData<?> data = tardis.getControlDataOrCreate(this.control.getType());
                //Handle flight event
                if(TardisCap.IsFlightEventActive(tardis.getCurrentFlightEvent())){

                    if(tardis.getCurrentFlightEvent().get().onControlUse(data)){
                        data.playAnimation((int)tardis.getAnimationTicks());
                        tardis.getInteriorManager().playControlSound(data);
                        return;
                    }
                    else data.playAnimation((int)tardis.getAnimationTicks());

                }

                InteractionResult result = this.getControl().onPunch(player, tardis);
                if(result != InteractionResult.PASS){
                    tardis.getInteriorManager().playControlSound(data);
                    tardis.getControlDataOrCreate(this.getControl().getType()).playAnimation((int)tardis.getAnimationTicks());
                }
            }
        });
        return false;
    }

    public void writeSpawnData(FriendlyByteBuf buf) {

        buf.writeRegistryId(ControlRegistry.REGISTRY.get(), this.control.getType());

        boolean hasSize = this.size != null;
        buf.writeBoolean(hasSize);

        if(hasSize){
            buf.writeDouble(this.size.x);
            buf.writeDouble(this.size.y);
            buf.writeDouble(this.size.z);
        }

    }

    @Override
    public void readSpawnData(FriendlyByteBuf buf) {

        this.control = buf.readRegistryIdSafe(ControlType.class).create();

        boolean hasSize = buf.readBoolean();

        if(hasSize){
            this.size = new Vec3(buf.readDouble(), buf.readDouble(), buf.readDouble());
            this.reapplyPosition();
        }
    }

    @Override
    public void setDeltaMovement(Vec3 pMotion) {}

    @Override
    public boolean shouldBeSaved() {
        return false;
    }
}
