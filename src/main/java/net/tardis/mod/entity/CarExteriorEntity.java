package net.tardis.mod.entity;

import net.minecraft.core.registries.Registries;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.network.FriendlyByteBuf;
import net.minecraft.network.syncher.EntityDataAccessor;
import net.minecraft.network.syncher.EntityDataSerializers;
import net.minecraft.network.syncher.SynchedEntityData;
import net.minecraft.resources.ResourceKey;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResult;
import net.minecraft.world.entity.*;
import net.minecraft.world.entity.boss.enderdragon.EnderDragon;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.phys.Vec3;
import net.minecraftforge.common.util.LazyOptional;
import net.minecraftforge.entity.IEntityAdditionalSpawnData;
import net.minecraftforge.entity.PartEntity;
import net.tardis.api.events.TardisEvent;
import net.tardis.mod.blockentities.exteriors.IExteriorObject;
import net.tardis.mod.cap.Capabilities;
import net.tardis.mod.cap.level.ITardisLevel;
import net.tardis.mod.item.KeyItem;
import net.tardis.mod.misc.*;
import net.tardis.mod.misc.enums.DoorState;
import net.tardis.mod.misc.enums.MatterState;
import net.tardis.mod.network.Network;
import net.tardis.mod.network.packets.DoorHandlerUpdateEntityMessage;
import net.tardis.mod.network.packets.EntityMatterStateMessage;
import net.tardis.mod.registry.EntityDataRegistry;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class CarExteriorEntity extends Entity implements IDrivable, IHaveMatterState, IExteriorObject, IDoor, ITeleportEntities<CarExteriorEntity>, IEntityAdditionalSpawnData {

    static final EntityDataAccessor<Float> THROTTLE = SynchedEntityData.defineId(CarExteriorEntity.class, EntityDataSerializers.FLOAT);
    static final EntityDataAccessor<Float> TURN_AMOUNT = SynchedEntityData.defineId(CarExteriorEntity.class, EntityDataSerializers.FLOAT);
    static final EntityDataAccessor<Optional<ResourceKey<Level>>> TARDIS_DATA = SynchedEntityData.defineId(CarExteriorEntity.class, EntityDataRegistry.TARDIS_SERIALIZER.get());


    private CarTrunkEntityPart<CarExteriorEntity> trunkPart;
    private PartEntity<?>[] parts = new PartEntity[1];

    final List<Vec3> seatPositions = new ArrayList<>();
    final MatterStateHandler matterStateHandler;
    public ITardisLevel cachedTardis;

    final TeleportHandler<CarExteriorEntity> teleportHandler = new TeleportHandler<>(this,
                level -> getTardis().isPresent() ? level.getServer().getLevel(getTardis().get()) : null,
                () -> this.trunkPart.getBoundingBox(),
                (level, entity) -> this.teleportHandler.interiorDoorPos(level, entity)
    ).setPlaceAtMePosition(target -> new TeleportEntry.LocationData(this.trunkPart.position(), -this.getYRot() - 180))
    .setTeleportPreEvent(e -> cachedTardis == null ? null : new TardisEvent.EnterEvent.Pre(cachedTardis, e));

    private final DoorHandler doorHandler = new DoorHandler(stack -> {
        Optional<ResourceKey<Level>> tardis = KeyItem.getKeyData(stack);
        if(tardis.isPresent() && this.getTardis().isPresent()){
            return tardis.get().equals(this.getTardis().get());
        }
        return false;
    }, DoorState.CLOSED, DoorState.BOTH)
            .setPacketSender(() -> {
                Network.sendToTracking(this, new DoorHandlerUpdateEntityMessage(this.getId(), this.getDoorHandler()));
            })
            .setLinkedDoor(() -> {
                if(getLevel().getServer() != null){
                    ServerLevel tardisLevel = getLevel().getServer().getLevel(this.getTardis().get());
                    Capabilities.getCap(Capabilities.TARDIS, tardisLevel).ifPresent(tardis -> {
                        tardis.getInteriorManager().getDoorHandler().update(this.getDoorHandler(), tardisLevel);
                    });
                }
            });

    public CarExteriorEntity(EntityType<?> pEntityType, Level pLevel) {
        super(pEntityType, pLevel);
        this.matterStateHandler = new MatterStateHandler(this::position)
                .setRemovedAction(this::kill)
                .setChangedAction(() -> Network.sendToTracking(this, new EntityMatterStateMessage(this.getId(), this.getMatterStateHandler())))
                .setRadius(() -> 2.0F);
        this.maxUpStep = 1.1F;
        this.setupCar();
    }

    public void setupCar(){
        this.parts[0] = this.trunkPart = new CarTrunkEntityPart<>(this, 1F);
        this.addSeat(0.5, -0.2, 0.1);
        this.addSeat(-0.5, -0.2, 0.2);
    }

    public void addSeat(Vec3 seat){
        this.seatPositions.add(seat);
    }

    public void addSeat(double x, double y, double z){
        this.addSeat(new Vec3(x, y, z));
    }

    public void cacheTardis(){
        if(this.cachedTardis == null){
            if(getLevel() instanceof ServerLevel level && this.getTardis().isPresent()){
                ServerLevel tardisLevel = level.getServer().getLevel(this.getTardis().get());
                Capabilities.getCap(Capabilities.TARDIS, tardisLevel).ifPresent(t -> this.cachedTardis = t);
            }
        }
    }



    @Override
    public void tick() {
        super.tick();
        this.cacheTardis();
        this.matterStateHandler.tick(this.getLevel());

        this.xOld = this.getX();
        this.yOld = this.getY();
        this.zOld = this.getZ();
        this.move(MoverType.SELF, this.getDeltaMovement());
        //Apply friction
        this.setDeltaMovement(this.getDeltaMovement().scale(0.8));

        //Update tardis location
        if(this.cachedTardis != null && this.matterStateHandler.getMatterState() == MatterState.SOLID){
            this.cachedTardis.setLocation(new SpaceTimeCoord(getLevel().dimension(), this.getOnPos()));
        }

        //gravity
        this.setDeltaMovement(this.getDeltaMovement().add(0, -0.03, 0));

        if(this.getFirstPassenger() != null){

            float speed = this.getEntityData().get(THROTTLE);
            this.moveRelative(speed, new Vec3(0, 0, 1));

            this.setYRot(this.getYRot() + getEntityData().get(TURN_AMOUNT));



        }
        else this.stop();

        //Update Subentities

        Vec3 trunkOffset = new Vec3(0, 0.2, -2);

        Vec3 trunkPos = trunkOffset.yRot((float)Math.toRadians(-this.getYRot())).add(this.position());
        Vec3 oldPos = trunkOffset.yRot((float)Math.toRadians(-this.getYRot())).add(xOld, yOld, zOld);

        this.trunkPart.xOld = oldPos.x;
        this.trunkPart.yOld = oldPos.y;
        this.trunkPart.zOld = oldPos.z;
        this.trunkPart.setPos(trunkPos);
        this.trunkPart.tick();
    }

    @Override
    public void setId(int pId) {
        super.setId(pId);
        for(int i = 0; i < getParts().length; ++i){
            getParts()[i].setId(pId + i + 1);
        }
    }

    @Override
    public boolean is(Entity entity){
        for(PartEntity part : this.getParts()){
            if(entity == part)
                return true;
        }
        return entity == this;
    }

    public int getSeats(){
        return this.seatPositions.size();
    }

    public Vec3 getSeatOffset(int seatIndex){
        return seatIndex < this.seatPositions.size() ? this.seatPositions.get(seatIndex) : Vec3.ZERO;
    }

    @Override
    public float getMaxSpeed(){
        return 0.075F;
    }

    @Override
    public void stop() {
        this.getEntityData().set(THROTTLE, 0F);
        this.getEntityData().set(TURN_AMOUNT, 0F);

    }

    @Override
    public boolean isNoGravity() {
        return super.isNoGravity();
    }

    @Override
    public boolean canBeCollidedWith() {
        return true;
    }

    @Override
    protected boolean canRide(Entity pVehicle) {
        return true;
    }

    @Override
    public boolean isPickable() {
        return true;
    }

    @Override
    public boolean isVehicle() {
        return true;
    }

    @Override
    public InteractionResult interact(Player pPlayer, InteractionHand pHand) {
        if(!this.getLevel().isClientSide()){
            pPlayer.startRiding(this);
        }
        return InteractionResult.sidedSuccess(this.getLevel().isClientSide());
    }

    @Override
    public boolean isMultipartEntity() {
        return true;
    }

    @Override
    public @Nullable PartEntity<?>[] getParts() {
        return this.parts;
    }

    @Override
    public void positionRider(Entity pass) {
        //super.positionRider(pPassenger);
        if(hasPassenger(pass)){
            for(int i = 0; i < this.getPassengers().size(); ++i){
                if(this.getPassengers().get(i) == pass){
                    pass.setPos(this.getSeatOffset(i).yRot((float)Math.toRadians(-this.getYRot())).add(this.position()));
                }
            }
            if(pass instanceof LivingEntity live){
                live.yBodyRot = this.getYRot();
            }
        }
    }



    @Override
    protected void defineSynchedData() {
        this.getEntityData().define(THROTTLE, 0.0F);
        this.getEntityData().define(TURN_AMOUNT, 0.0F);
        this.getEntityData().define(TARDIS_DATA, Optional.empty());
    }

    @Override
    protected void readAdditionalSaveData(CompoundTag tag) {
        if(tag.contains("tardis"))
            this.setInterior(ResourceKey.create(Registries.DIMENSION, new ResourceLocation(tag.getString("tardis"))));
        this.matterStateHandler.deserializeNBT(tag.getCompound("matter_state"));
        this.doorHandler.deserializeNBT(tag.getCompound("door"));
    }

    @Override
    protected void addAdditionalSaveData(CompoundTag tag) {
        this.getTardis().ifPresent(tardis -> tag.putString("tardis", tardis.location().toString()));
        tag.put("matter_state", this.matterStateHandler.serializeNBT());
        tag.put("door", this.getDoorHandler().serializeNBT());
    }

    public Optional<ResourceKey<Level>> getTardis(){
        return this.entityData.get(TARDIS_DATA);
    }

    @Override
    public void drive(float turnAmount, float speed) {
        this.getEntityData().set(THROTTLE, speed);
        this.getEntityData().set(CarExteriorEntity.TURN_AMOUNT, turnAmount);
    }

    @Override
    public void setInterior(ResourceKey<Level> interior) {
        this.getEntityData().set(TARDIS_DATA, Optional.of(interior));
    }

    @Override
    public MatterStateHandler getMatterStateHandler() {
        return this.matterStateHandler;
    }

    @Override
    public DoorHandler getDoorHandler() {
        return this.doorHandler;
    }

    @Override
    public TeleportHandler<CarExteriorEntity> getTeleportHandler() {
        return this.teleportHandler;
    }

    public double getTurnAmount() {
        return this.getEntityData().get(TURN_AMOUNT);
    }


    @Override
    public void writeSpawnData(FriendlyByteBuf buffer) {
        this.doorHandler.encode(buffer);
    }

    @Override
    public void readSpawnData(FriendlyByteBuf additionalData) {
        this.doorHandler.decode(additionalData);
    }
}
