package net.tardis.mod.registry;

import net.minecraft.network.chat.Component;
import net.minecraft.resources.ResourceLocation;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.IForgeRegistry;
import net.minecraftforge.registries.RegistryBuilder;
import net.minecraftforge.registries.RegistryObject;
import net.tardis.mod.Constants;
import net.tardis.mod.Tardis;
import net.tardis.mod.client.animations.demat.DematAnimation;
import net.tardis.mod.helpers.Helper;

import java.util.function.Supplier;

public class DematAnimationRegistry {

    public static final DeferredRegister<DematAnimationType> ANIMATION = DeferredRegister.create(Helper.createRL("demat_animations"), Tardis.MODID);
    public static final Supplier<IForgeRegistry<DematAnimationType>> REGISTRY = ANIMATION.makeRegistry(RegistryBuilder::new);

    public static final RegistryObject<DematAnimationType> CLASSIC = ANIMATION.register("classic", DematAnimationType::new);
    public static final RegistryObject<DematAnimationType> NEW_WHO = ANIMATION.register("new_who", DematAnimationType::new);
    public static final RegistryObject<DematAnimationType> PARTICLE_FALL = ANIMATION.register("particle_fall", DematAnimationType::new);


    public static class DematAnimationType{}

    public static Component makeTranslation(DematAnimationType type){
        return Component.translatable(Constants.Translation.makeGenericTranslation(REGISTRY.get(), type));
    }

}
