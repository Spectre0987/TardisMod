package net.tardis.mod.registry;

import net.minecraft.nbt.CompoundTag;
import net.minecraft.network.chat.Component;
import net.minecraft.world.item.BlockItem;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.Items;
import net.minecraft.world.item.RecordItem;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.state.properties.BlockStateProperties;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.ForgeRegistries;
import net.minecraftforge.registries.RegistryObject;
import net.tardis.mod.Tardis;
import net.tardis.mod.block.BasicProps;
import net.tardis.mod.item.*;
import net.tardis.mod.item.components.ArtronCapacitorItem;
import net.tardis.mod.item.components.SubsystemItem;
import net.tardis.mod.item.components.UpgradeItem;
import net.tardis.mod.item.dev.PhasedOpticShellItem;
import net.tardis.mod.item.dev.TapeMeasureItem;
import net.tardis.mod.item.tools.CFLItem;
import net.tardis.mod.item.tools.PistonHammerItem;
import net.tardis.mod.item.tools.SonicItem;
import net.tardis.mod.item.tools.WeldingTorchItem;
import net.tardis.mod.registry.SubsystemRegistry;
import net.tardis.mod.registry.UpgradeRegistry;
import net.tardis.mod.sound.SoundRegistry;
import net.tardis.mod.upgrade.sonic.EmptySonicUpgrade;
import net.tardis.mod.upgrade.tardis.EmptyTardisUpgrade;
import net.tardis.mod.upgrade.tardis.EnergySiphonTardisUpgrade;
import net.tardis.mod.upgrade.tardis.LaserMinerTardisUpgrade;
import net.tardis.mod.upgrade.tardis.NanoGeneTardisUpgrade;
import net.tardis.mod.upgrade.types.SonicUpgradeType;
import net.tardis.mod.upgrade.types.TardisUpgradeType;

import java.util.UUID;
import java.util.function.Function;

public class ItemRegistry {

    public static final DeferredRegister<Item> ITEMS = DeferredRegister.create(ForgeRegistries.ITEMS, Tardis.MODID);

    //Tools
    public static final RegistryObject<Item> CHRONOFAULT_LOCATOR = ITEMS.register("tools/diagnostic_tool", CFLItem::new);
    public static final RegistryObject<Item> WELDING_TORCH = ITEMS.register("tools/welding_torch", WeldingTorchItem::create);
    public static final RegistryObject<Item> PISTON_HAMMER = ITEMS.register("tools/piston_hammer", PistonHammerItem::create);
    public static final RegistryObject<Item> TAPE_MEASURE = ITEMS.register("tools/tape_measure", TapeMeasureItem::create);
    public static final RegistryObject<Item> SONIC = ITEMS.register("tools/sonic", SonicItem::create);
    public static final RegistryObject<Item> STATTENHEIM_REMOTE = ITEMS.register("tools/stattenheim_remote", StattenheimRemoteItem::create);
    public static final RegistryObject<Item> PERSONAL_SHIELD = ITEMS.register("tools/personal_shield", PersonalShieldItem::create);
    public static final RegistryObject<Item> PHASED_OPTIC_SHELL_GENERATOR = ITEMS.register("tools/phased_optic_shell", PhasedOpticShellItem::new);

    public static final RegistryObject<Item> MANUAL = ITEMS.register("manual", ManualItem::create);
    public static final RegistryObject<Item> PIRATE_KEY = ITEMS.register("keys/pirate", KeyItem::create);
    public static final RegistryObject<Item> GALLIFREYAN_KEY = ITEMS.register("keys/gallifreyan", KeyItem::create);
    public static final RegistryObject<Item> TEA = ITEMS.register("tea", TeaItem::create);

    //Crafting material
    public static final RegistryObject<Item> CRYSTAL_VIALS = ITEMS.register("crystal_vials", () -> new Item(BasicProps.Item.BASE.get()));
    public static final RegistryObject<Item> CRYSTAL_VIALS_MERCURY = ITEMS.register("crystal_vials_mercury", () -> new Item(BasicProps.Item.BASE.get()));
    public static final RegistryObject<Item> XION_LENS = ITEMS.register("xion_lens", () -> new Item(BasicProps.Item.BASE.get()));
    public static final RegistryObject<Item> EXO_CIRCUITS = ITEMS.register("exo_circuits", () -> new Item(BasicProps.Item.BASE.get()));
    public static final RegistryObject<Item> CIRCUIT_PLATE = ITEMS.register("circuit_plate", () -> new Item(BasicProps.Item.BASE.get()));
    public static final RegistryObject<Item> MERCURY_BOTTLE = ITEMS.register("mercury_bottle", () -> new Item(BasicProps.Item.BASE.get()));
    public static final RegistryObject<Item> CINNABAR = ITEMS.register("cinnabar", () -> new Item(BasicProps.Item.BASE.get()));
    public static final RegistryObject<Item> DATA_CRYSTAL = ITEMS.register("data_crystal", DataCrystalItem::create);
    public static final RegistryObject<Item> BURNT_EXOCIRCUITS = ITEMS.register("burnt_circuits", () -> new SimpleToolTipItem(
            BasicProps.Item.BASE.get(),
            Component.translatable(Tardis.MODID + ".tooltips.burnt_circuit")
    ));

    //Components
    public static final RegistryObject<Item> ARTRON_CAP_LEAKY = ITEMS.register("artron_capacitors/leaky", () -> new ArtronCapacitorItem(BasicProps.Item.ONE.get(), 32));
    public static final RegistryObject<Item> ARTRON_CAP_BASIC = ITEMS.register("artron_capacitors/basic", () -> new ArtronCapacitorItem(BasicProps.Item.ONE.get(), 64));
    public static final RegistryObject<Item> ARTRON_CAP_MID = ITEMS.register("artron_capacitors/mid", () -> new ArtronCapacitorItem(BasicProps.Item.ONE.get(), 128));
    public static final RegistryObject<Item> ARTRON_CAP_HIGH = ITEMS.register("artron_capacitors/high", () -> new ArtronCapacitorItem(BasicProps.Item.ONE.get(), 256));
    public static final RegistryObject<Item> DEMAT_CIRCUIT = ITEMS.register("components/demat_circuit", () -> new SubsystemItem(BasicProps.Item.ONE.get().durability(7200), SubsystemRegistry.FLIGHT_TYPE));
    public static final RegistryObject<Item> FLUID_LINKS = ITEMS.register("components/fluid_links", () -> new SubsystemItem(BasicProps.Item.ONE.get().durability(2500), SubsystemRegistry.FLUID_LINK_TYPE));
    public static final RegistryObject<Item> CHAMELEON_CIRCUIT = ITEMS.register("components/chameleon_circuit", () -> new SubsystemItem(BasicProps.Item.ONE.get().durability(1000), SubsystemRegistry.CHAMELEON_TYPE));
    public static final RegistryObject<Item> INTERSTITAL_ANTENNA = ITEMS.register("components/interstitial_antenna", SubsystemItem.create(SubsystemRegistry.ANTENNA));
    public static final RegistryObject<Item> NAV_COM = ITEMS.register("components/nav_com", SubsystemItem.create(SubsystemRegistry.NAV_COM));
    public static final RegistryObject<Item> SHIELD_GENERATOR = ITEMS.register("components/shield_generator", SubsystemItem.create(SubsystemRegistry.SHIELD));
    public static final RegistryObject<Item> STABILIZER = ITEMS.register("components/stabilizer", SubsystemItem.create(SubsystemRegistry.STABILIZERS));
    public static final RegistryObject<Item> TEMPORAL_GRACE = ITEMS.register("components/temporal_grace", SubsystemItem.create(SubsystemRegistry.TEMPORAL_GRACE));

    //Upgrades
    public static final RegistryObject<Item> UPGRADE_BASE = ITEMS.register("upgrages/base", () -> new Item(new Item.Properties()));
    public static final RegistryObject<UpgradeItem<TardisUpgradeType<NanoGeneTardisUpgrade>, NanoGeneTardisUpgrade>> UPGRADE_NANO_GENE = ITEMS.register("upgrades/nano_gene", () -> UpgradeItem.create(UpgradeRegistry.TARDIS_NANOGENE));
    public static final RegistryObject<UpgradeItem<TardisUpgradeType<LaserMinerTardisUpgrade>, LaserMinerTardisUpgrade>> UPGRADE_LASER_MINER = ITEMS.register("upgrades/laser_miner", () ->
                UpgradeItem.create(UpgradeRegistry.TARDIS_LASER_MINER)
            );
    public static final RegistryObject<UpgradeItem<TardisUpgradeType<EnergySiphonTardisUpgrade>, EnergySiphonTardisUpgrade>> UPGRADE_ENERGY_SYPHON = ITEMS.register("upgrades/energy_syphon", () ->
                UpgradeItem.create(UpgradeRegistry.TARDIS_ENERGY_SYPHON_UPGRADE)
            );
    public static final RegistryObject<UpgradeItem<TardisUpgradeType<EmptyTardisUpgrade>, EmptyTardisUpgrade>> UPGRADE_REMOTE = ITEMS.register("upgrades/remote_circuit", () -> UpgradeItem.create(UpgradeRegistry.TARDIS_REMOTE));
    public static final RegistryObject<UpgradeItem<TardisUpgradeType<EmptyTardisUpgrade>, EmptyTardisUpgrade>> UPGRADE_ATRIUM = ITEMS.register("upgrades/atrium", () -> UpgradeItem.create(UpgradeRegistry.ATRIUM));

    public static final RegistryObject<SonicUpgradeItem<SonicUpgradeType<EmptySonicUpgrade>>> UPGRADE_SONIC_DEMAT = ITEMS.register("upgrades/sonic/demat", () -> new SonicUpgradeItem<>(
            BasicProps.Item.ONE.get(),
            UpgradeRegistry.SONIC_DEMAT_TARDIS
    ));

    public static final RegistryObject<RecordItem> FIRST_ENTER_RECORD = ITEMS.register("records/first_enter", () ->
            new RecordItem(
                    15, SoundRegistry.FIRST_ENTRANCE_MUSIC, BasicProps.Item.ONE.get(), 840
            ));
    public static final RegistryObject<Item> PLATE_COPPER = ITEMS.register("resources/copper_plate", () ->
                new Item(BasicProps.Item.BASE.get())
            );

    //BlockItems

    public static RegistryObject<BlockItemCraftingCallback> TUBE_ITEM = ITEMS.register(BlockRegistry.TELEPORT_TUBE.getId().getPath(), () ->
            new BlockItemCraftingCallback(BlockRegistry.TELEPORT_TUBE.get(), BasicProps.Item.BASE.get())
                    .withCallback((stack, level, player) -> {
                        CompoundTag itemTag = new CompoundTag();
                        itemTag.putString("teleport_id", UUID.randomUUID().toString());
                        stack.getOrCreateTag().put("BlockEntityTag", itemTag);
                    })
                    .setSpecial()
    );

    public static RegistryObject<BlockItem> registerBlockItem(RegistryObject<?extends Block> block, Function<Item.Properties, Item.Properties> prop){
        return ITEMS.register(block.getId().getPath(), () -> {
            if(block.get().defaultBlockState().hasProperty(BlockStateProperties.HORIZONTAL_FACING)){
                return new BlockItemHorizontal(block.get(), prop.apply(new Item.Properties()));
            }
            return new BlockItem(block.get(), prop.apply(new Item.Properties()));
        });
    }

    public static RegistryObject<BlockItem> registerBlockItem(RegistryObject<? extends Block> block){
        return registerBlockItem(block, prop -> prop);
    }

}
