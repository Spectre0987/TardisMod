package net.tardis.mod.registry;

import net.minecraft.core.registries.Registries;
import net.minecraft.network.FriendlyByteBuf;
import net.minecraft.network.syncher.EntityDataSerializer;
import net.minecraft.network.syncher.EntityDataSerializers;
import net.minecraft.resources.ResourceKey;
import net.minecraft.world.level.Level;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.ForgeRegistries;
import net.minecraftforge.registries.RegistryObject;
import net.tardis.mod.Tardis;
import net.tardis.mod.helpers.Helper;

import java.util.Optional;

public class EntityDataRegistry {

    public static final DeferredRegister<EntityDataSerializer<?>> SERIALIZERS = DeferredRegister.create(ForgeRegistries.Keys.ENTITY_DATA_SERIALIZERS.location(), Tardis.MODID);

    public static final RegistryObject<EntityDataSerializer<Optional<ResourceKey<Level>>>> TARDIS_SERIALIZER = SERIALIZERS.register("tardis", () -> EntityDataSerializer.<Optional<ResourceKey<Level>>>simple(
            (buf, val) -> {
                buf.writeBoolean(val.isPresent());
                val.ifPresent(v -> buf.writeResourceKey(v));
            },
            buf -> {
                boolean has = buf.readBoolean();
                if(has)
                    return Optional.of(buf.readResourceKey(Registries.DIMENSION));
                return Optional.empty();
            }
    ));

}
