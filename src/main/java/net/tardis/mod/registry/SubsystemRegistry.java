package net.tardis.mod.registry;

import net.minecraftforge.eventbus.api.IEventBus;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.IForgeRegistry;
import net.minecraftforge.registries.RegistryBuilder;
import net.minecraftforge.registries.RegistryObject;
import net.tardis.mod.Constants;
import net.tardis.mod.Tardis;
import net.tardis.mod.helpers.Helper;
import net.tardis.mod.subsystem.*;

import java.util.function.Supplier;

public class SubsystemRegistry {

    public static final DeferredRegister<SubsystemType<?>> TYPE = DeferredRegister.create(Helper.createRL("subsystem_type"), Tardis.MODID);

    public static final Supplier<IForgeRegistry<SubsystemType<?>>> TYPE_REGISTRY = TYPE.makeRegistry(RegistryBuilder::new);


    //Subsystem Types
    public static final RegistryObject<SubsystemType<FlightDamageSubsystem>> FLIGHT_TYPE = TYPE.register("flight", SubsystemType::createRequired);
    public static final RegistryObject<SubsystemType<BasicSubsystem>> FLUID_LINK_TYPE = TYPE.register("fluid_links", SubsystemType::createRequired);
    public static final RegistryObject<SubsystemType<BasicSubsystem>> CHAMELEON_TYPE = TYPE.register("chameleon_circuit", SubsystemType::createOptional);
    public static final RegistryObject<SubsystemType<BasicSubsystem>> ANTENNA = TYPE.register("antenna", SubsystemType::createOptional);
    public static final RegistryObject<SubsystemType<BasicSubsystem>> NAV_COM = TYPE.register("nav_com", SubsystemType::createOptional);
    public static final RegistryObject<SubsystemType<ShieldSubsystem>> SHIELD = TYPE.register("shield", SubsystemType::createOptional);
    public static final RegistryObject<SubsystemType<BasicSubsystem>> STABILIZERS = TYPE.register("stabilizers", SubsystemType::createOptional);
    public static final RegistryObject<SubsystemType<TemporalGraceSystem>> TEMPORAL_GRACE = TYPE.register("temporal_grace", SubsystemType::createOptional);



    public static void register(IEventBus bus){
        TYPE.register(bus);
    }

    /**
     * Call from {@link net.minecraftforge.fml.event.lifecycle.FMLCommonSetupEvent}
     */
    public static void registerSubsystems(){

        FLIGHT_TYPE.get().registerSubsystem(stack -> stack.getItem() == ItemRegistry.DEMAT_CIRCUIT.get(), (type, pred, tardis) ->
                    new FlightDamageSubsystem(type, pred, tardis, 5 * 20)//Damage every 5 seconds in flight
                );
        FLUID_LINK_TYPE.get().registerSubsystem(Constants.Predicates.isItem(ItemRegistry.FLUID_LINKS.get()), BasicSubsystem::new);
        CHAMELEON_TYPE.get().registerSubsystem(Constants.Predicates.isItem(ItemRegistry.CHAMELEON_CIRCUIT.get()), BasicSubsystem::new);
        ANTENNA.get().registerSubsystem(Constants.Predicates.isItem(ItemRegistry.INTERSTITAL_ANTENNA.get()), BasicSubsystem::new);
        NAV_COM.get().registerSubsystem(Constants.Predicates.isItem(ItemRegistry.NAV_COM.get()), BasicSubsystem::new);
        SHIELD.get().registerSubsystem(Constants.Predicates.isItem(ItemRegistry.SHIELD_GENERATOR.get()), (type, test, level) -> new ShieldSubsystem(
                type, test, level, 0.5F
        ));
        STABILIZERS.get().registerSubsystem(Constants.Predicates.isItem(ItemRegistry.STABILIZER.get()), BasicSubsystem::new);
        TEMPORAL_GRACE.get().registerSubsystem(Constants.Predicates.isItem(ItemRegistry.TEMPORAL_GRACE.get()), TemporalGraceSystem::new);

    }

}
