package net.tardis.mod.registry;

import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.IForgeRegistry;
import net.minecraftforge.registries.RegistryBuilder;
import net.minecraftforge.registries.RegistryObject;
import net.tardis.mod.Tardis;
import net.tardis.mod.helpers.Helper;
import net.tardis.mod.sonic_screwdriver.SonicPartSlot;

import java.util.EnumMap;
import java.util.function.Supplier;

public class SonicPartRegistry {

    public static final DeferredRegister<SonicPartType> PARTS = DeferredRegister.create(Helper.createRL("sonic_parts"), Tardis.MODID);
    public static final Supplier<IForgeRegistry<SonicPartType>> REGISTRY = PARTS.makeRegistry(RegistryBuilder::new);

    public static final EnumMap<SonicPartSlot, RegistryObject<SonicPartType>> MK1 = registerAll("mk1");
    public static final EnumMap<SonicPartSlot, RegistryObject<SonicPartType>> MK2 = registerAll("mk2");
    public static final EnumMap<SonicPartSlot, RegistryObject<SonicPartType>> MK3 = registerAll("mk3");
    public static final EnumMap<SonicPartSlot, RegistryObject<SonicPartType>> MK4 = registerAll("mk4");
    public static final EnumMap<SonicPartSlot, RegistryObject<SonicPartType>> MK5 = registerAll("mk5");
    public static final EnumMap<SonicPartSlot, RegistryObject<SonicPartType>> MK6 = registerAll("mk6");
    public static final EnumMap<SonicPartSlot, RegistryObject<SonicPartType>> MK7 = registerAll("mk7");


    public static EnumMap<SonicPartSlot, RegistryObject<SonicPartType>> registerAll(String base){
        final EnumMap<SonicPartSlot, RegistryObject<SonicPartType>> list = new EnumMap<>(SonicPartSlot.class);
        for(SonicPartSlot slot : SonicPartSlot.values()){
            list.put(slot, PARTS.register(base + slot.getSuffix(), () -> new SonicPartType(slot)));
        }
        return list;
    }



    public static class SonicPartType{
        final SonicPartSlot slot;

        public SonicPartType(SonicPartSlot slot){
            this.slot = slot;
        }
        public SonicPartSlot getSlot(){
            return this.slot;
        }
    }


}
