package net.tardis.mod.registry;

import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.IForgeRegistry;
import net.minecraftforge.registries.RegistryBuilder;
import net.minecraftforge.registries.RegistryObject;
import net.tardis.mod.Tardis;
import net.tardis.mod.helpers.Helper;
import net.tardis.mod.emotional.loyalty_functions.CloseDoorLoyaltyFunction;
import net.tardis.mod.emotional.loyalty_functions.LoyaltyFunction;

import java.util.function.Supplier;

public class LoyaltyFunctionRegistry {

    public static final DeferredRegister<LoyaltyFunction> LOYALTY = DeferredRegister.create(Helper.createRL("loyalty_function"), Tardis.MODID);
    public static final Supplier<IForgeRegistry<LoyaltyFunction>> REGISTRY = LOYALTY.makeRegistry(RegistryBuilder::new);

    public static final RegistryObject<CloseDoorLoyaltyFunction> CLOSE_DOOR = LOYALTY.register("close_door", CloseDoorLoyaltyFunction::new);

}
