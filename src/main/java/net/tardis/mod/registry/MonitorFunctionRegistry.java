package net.tardis.mod.registry;

import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.IForgeRegistry;
import net.minecraftforge.registries.RegistryBuilder;
import net.minecraftforge.registries.RegistryObject;
import net.tardis.mod.Tardis;
import net.tardis.mod.client.gui.datas.MonitorWaypointData;
import net.tardis.mod.client.gui.monitor.MonitorAtriumScreen;
import net.tardis.mod.helpers.Helper;
import net.tardis.mod.misc.tardis.montior.*;
import net.tardis.mod.misc.tardis.montior.interior.ChangeSoundSchemeMonitorFunction;
import net.tardis.mod.misc.tardis.montior.interior.IntSetMainDoorMonitorFunction;
import net.tardis.mod.misc.tardis.montior.security.ScanForLifeFunction;
import net.tardis.mod.misc.tardis.montior.upgrades.AtriumMonitorFunction;

import java.util.function.Supplier;

public class MonitorFunctionRegistry {

    public static final DeferredRegister<MonitorFunction> FUNCTIONS = DeferredRegister.create(Helper.createRL("monitor_functions"), Tardis.MODID);
    public static final Supplier<IForgeRegistry<MonitorFunction>> REGISTRY = FUNCTIONS.makeRegistry(RegistryBuilder::new);

    public static final RegistryObject<ScanForLifeFunction> SCAN_FOR_LIFE = FUNCTIONS.register("security/scan_for_life", ScanForLifeFunction::new);
    public static final RegistryObject<ChangeExteriorMonitorFunction> CHANGE_EXTERIOR = FUNCTIONS.register("change_exterior", ChangeExteriorMonitorFunction::new);
    public static final RegistryObject<IntSetMainDoorMonitorFunction> SET_MAIN_DOOR = FUNCTIONS.register("interior/set_door", IntSetMainDoorMonitorFunction::new);
    public static final RegistryObject<DematAnimationMonitorFunction> DEMAT_ANIMATION = FUNCTIONS.register("exterior/demat_anim", DematAnimationMonitorFunction::new);
    public static final RegistryObject<TardisManualFunction> TARDIS_MANUAL = FUNCTIONS.register("tardis_index", TardisManualFunction::new);
    public static final RegistryObject<FlightCourseFunction> FLIGHT_COURSE = FUNCTIONS.register("flight_course", FlightCourseFunction::new);
    public static final RegistryObject<ChangeSoundSchemeMonitorFunction> SOUND_SCHEME = FUNCTIONS.register("interior/sound_scheme", ChangeSoundSchemeMonitorFunction::new);
    public static final RegistryObject<WaypointMonitorFunction> WAYPOINTS = FUNCTIONS.register("waypoints", WaypointMonitorFunction::new);
    public static final RegistryObject<BasicToggleMonitorFunction> SET_CAN_LAND_WATER =
            FUNCTIONS.register("exterior/land_water", () -> new BasicToggleMonitorFunction(
                    (tardis, b) -> tardis.getExteriorExtraData().setCanLandUnderwater(b),
                    tardis -> tardis.getExteriorExtraData().canLandInWater()
            ));

    public static final RegistryObject<BasicToggleMonitorFunction> SET_LAND_AIR = FUNCTIONS.register("exterior/land_air", () -> new BasicToggleMonitorFunction(
            (tardis, val) -> tardis.getExteriorExtraData().setCanLandInAir(val),
            tardis -> tardis.getExteriorExtraData().canLandMidAir()
    ));

    public static final RegistryObject<AtriumMonitorFunction> ATRIUM = FUNCTIONS.register("upgrades/atrium", AtriumMonitorFunction::new);


}
