package net.tardis.mod.registry;

import com.google.common.collect.Lists;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.item.BlockItem;
import net.minecraft.world.item.Item;
import net.minecraft.world.level.block.*;
import net.minecraft.world.level.block.state.BlockBehaviour;
import net.minecraft.world.level.block.state.properties.BlockSetType;
import net.minecraft.world.level.material.Material;
import net.minecraftforge.common.capabilities.ForgeCapabilities;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.ForgeRegistries;
import net.minecraftforge.registries.RegistryObject;
import net.tardis.mod.Tardis;
import net.tardis.mod.block.*;
import net.tardis.mod.block.dev.TardisStructureBlock;
import net.tardis.mod.block.dev.TestRendererBlock;
import net.tardis.mod.block.machines.*;
import net.tardis.mod.block.monitors.*;
import net.tardis.mod.blockentities.*;
import net.tardis.mod.blockentities.crafting.AlembicBlockEntity;
import net.tardis.mod.blockentities.crafting.AlembicStillBlockEntity;
import net.tardis.mod.blockentities.exteriors.ChameleonExteriorTile;
import net.tardis.mod.blockentities.machines.TardisLandingPadTile;
import net.tardis.mod.fluids.FluidRegistry;
import net.tardis.mod.item.SpecialBlockItem;

import java.util.EnumMap;
import java.util.function.Function;
import java.util.function.Supplier;

public class BlockRegistry {

    public static final DeferredRegister<Block> BLOCKS = DeferredRegister.create(ForgeRegistries.BLOCKS, Tardis.MODID);

    public static final RegistryObject<TardisStructureBlock> CORRIDOR_STRUCTURE_SAVER = BLOCKS.register("structure_block", () -> new TardisStructureBlock(BasicProps.Block.METAL.get()));

    public static final RegistryObject<ConsoleBlock> G8_CONSOLE = registerWithItemSpecial("g8_console", () -> new ConsoleBlock(TileRegistry.G8_CONSOLE::get));
    public static final RegistryObject<ConsoleBlock> STEAM_CONSOLE = registerWithItemSpecial("steam_console", () -> new ConsoleBlock(TileRegistry.STEAM_CONSOLE::get));
    public static final RegistryObject<ConsoleBlock> NEUVEAU_CONSOLE = registerWithItemSpecial("neuveau_console", () -> new ConsoleBlock(TileRegistry.NOUVEAU_CONSOLE::get));
    public static final RegistryObject<ConsoleBlock> GALVANIC_CONSOLE = registerWithItemSpecial("galvanic_console", () -> new ConsoleBlock(TileRegistry.GALVANIC_CONSOLE::get));
    public static final RegistryObject<ConsoleBlock> NEMO_CONSOLE = registerWithItemSpecial("nemo_console", () -> new ConsoleBlock(TileRegistry.NEMO_CONSOLE));

    public static final RegistryObject<InteriorDoorBlock<InteriorDoorTile>> INTERIOR_DOOR = registerWithItem("interior_door", InteriorDoorBlock::create);
    public static final RegistryObject<ExteriorBlock> STEAM_EXTERIOR = BLOCKS.register("steam_exterior",  () -> new ExteriorBlock(TileRegistry.STEAM_EXTERIOR));
    public static final RegistryObject<ExteriorBlock> TT_CAPSULE = BLOCKS.register("tt_capsule", () -> new ExteriorBlock(TileRegistry.TT_CAPSULE));
    public static final RegistryObject<ExteriorBlock> POLICE_BOX_EXTERIOR = BLOCKS.register("police_box", () -> new ExteriorBlock(TileRegistry.POLICE_BOX_EXTERIOR));
    public static final RegistryObject<ChameleonExteriorBlock<ChameleonExteriorTile>> CHAMELEON_EXTERIOR = BLOCKS.register("chameleon_exterior", () -> new ChameleonExteriorBlock<>(BasicProps.Block.INVINCIBLE.get().noOcclusion(), TileRegistry.CHAMELEON_EXTERIOR));
    public static final RegistryObject<ExteriorBlock> COFFIN_EXTERIOR = BLOCKS.register("coffin_exterior", () -> new ExteriorBlock(TileRegistry.COFFIN));
    public static final RegistryObject<ExteriorBlock> OCTA_EXTERIOR = BLOCKS.register("octa_exterior", () -> new ExteriorBlock(TileRegistry.OCTA_EXTERIOR));
    public static final RegistryObject<ExteriorBlock> TRUNK_EXTERIOR = BLOCKS.register("trunk_exterior", () -> new ExteriorBlock(TileRegistry.TRUNK_EXTERIOR));
    public static final RegistryObject<ExteriorBlock> SPRUCE_EXTERIOR = BLOCKS.register("spruce_exterior", () -> new ExteriorBlock(TileRegistry.SPRUCE_EXTERIOR));
    public static final RegistryObject<ChameleonExteriorAuxillaryBlock> CHAMELEON_AUXILLARY = BLOCKS.register("chameleon_exterior_auxillary", ChameleonExteriorAuxillaryBlock::create);
    public static final RegistryObject<BrokenExteriorBlock> STEAM_BROKEN_EXTERIOR = registerWithItem("steam_broken_exterior", () -> new BrokenExteriorBlock(ExteriorRegistry.STEAM));
    public static final RegistryObject<BrokenExteriorBlock> TT_CAPSULE_BROKEN_EXTERIOR = registerWithItem("tt_cap_broken", () -> new BrokenExteriorBlock(ExteriorRegistry.TT_CAPSULE));
    public static final RegistryObject<DraftingTableBlock> DRAFTING_TABLE = registerWithItem("drafting_table", DraftingTableBlock::create);
    public static final RegistryObject<MultiblockBlock> MULTIBLOCK = BLOCKS.register("mutiblock", MultiblockBlock::new);
    public static final RegistryObject<ExteriorAuxillaryBlock> EXTERIOR_COLLISION = BLOCKS.register("exterior_axillary", ExteriorAuxillaryBlock::new);
    public static final RegistryObject<SteamMonitorBlock> STEAM_MONITOR = registerWithItem("steam_monitor", SteamMonitorBlock::create);
    public static final RegistryObject<MonitorBlock<?>> RCA_MONITOR = registerWithItem("rca_monitor", () -> new MonitorBlock<BaseMonitorTile>(
            BasicProps.Block.METAL.get(),
            MonitorData.RCA,
            TileRegistry.BASIC_MONITOR
    ));
    public static final RegistryObject<EyeMonitorBlock> EYE_MONITOR = registerWithItem("eye_monitor", () -> new EyeMonitorBlock(
            BasicProps.Block.METAL.get(),
            MonitorData.EYE,
            TileRegistry.BASIC_MONITOR
    ));
    public static final RegistryObject<VariableMonitorBlock> VARIABLE_MONITOR = registerWithItem("variable_monitor", VariableMonitorBlock::create);
    public static final RegistryObject<EngineBlock> ENGINE_STEAM = registerWithItemSpecial("steam_engine", () -> new EngineBlock(BlockBehaviour.Properties.of(Material.METAL), TileRegistry.ENGINE));
    public static final RegistryObject<EngineBlock> ENGINE_ROOF = registerWithItemSpecial("roof_engine", () -> new EngineBlock(
            BasicProps.Block.METAL.get().noOcclusion(),
            TileRegistry.ROOF_ENGINE
    ));
    public static final RegistryObject<XionCrystalBlock> XION = registerWithItem("xion", () -> new XionCrystalBlock(BasicProps.Block.CRYSTAL.get()));
    public static final RegistryObject<Block> DATA_MATRIX = registerWithItem("data_matrix", () -> new Block(
            BasicProps.Block.CRYSTAL.get().noOcclusion()
    ));
    public static final RegistryObject<Block> XION_BLOCK = registerWithItem("xion_block", () -> new Block(BlockBehaviour.Properties.of(Material.AMETHYST).sound(SoundType.AMETHYST).emissiveRendering((s, l, p) -> true).strength(4F)));
    public static final RegistryObject<AtriumBlock> ATRIUM_PLATFORM_BLOCK = registerWithItem("atrium", () -> new AtriumBlock(BasicProps.Block.CRYSTAL.get()));
    public static final RegistryObject<AtriumMasterBlock> ATRIUM_MASTER = registerWithItem("atrium_master", AtriumMasterBlock::create);
    public static final RegistryObject<AlembicBlock<AlembicBlockEntity>> ALEMBIC = registerWithItem("alembic", AlembicBlock::<AlembicBlockEntity>create);
    public static final RegistryObject<AlembicStillBlock<AlembicStillBlockEntity>> ALEMBIC_STILL = registerWithItem("alembic_still", AlembicStillBlock::create);
    public static final RegistryObject<ARSPanelBlock> ARS_PANEL = registerWithItem("ars_panel", ARSPanelBlock::create);
    public static final RegistryObject<ARSEggBlock> ARS_EGG = registerWithItem("ars_egg", ARSEggBlock::create);
    public static final RegistryObject<QuantiscopeBlock> QUANTISCOPE = registerWithItem("quantiscope", QuantiscopeBlock::create);
    public static final RegistryObject<RiftPylonBlock> RIFT_PYLON = registerWithItem("rift_pylon", RiftPylonBlock::create);
    public static final RegistryObject<RiftCollectorBlock> RIFT_COLLECTOR = registerWithItem("rift_collector", RiftCollectorBlock::create);
    public static final RegistryObject<MatterBufferBlock> MATTER_BUFFER = registerWithItem("matter_buffer", MatterBufferBlock::create);
    public static final RegistryObject<ScoopVaultBlock> SCOOP_VAULT = registerWithItem("scoop_vault", ScoopVaultBlock::create);
    public static final RegistryObject<WaypointBlock> WAYPOINT_BANKS = registerWithItem("waypoint_banks", WaypointBlock::create);
    public static final RegistryObject<DisguisedBlock<DisguisedBlockTile>> DISGUISED_BLOCK = BLOCKS.register("disguised_block", DisguisedBlock::create);
    public static final RegistryObject<RoundelTapBlock<RoundelTapTile>> ROUNDEL_TAP = registerWithItem("roundel_tap", RoundelTapBlock::create);
    public static final RegistryObject<BaseTileBlock<TardisLandingPadTile>> LANDING_PAD = registerWithItem("landing_pad", () -> new BaseTileBlock<>(
            BasicProps.Block.METAL.get(),
            TileRegistry.LANDING_PAD, true
    ));
    public static final RegistryObject<MultiChildCapRedirectBlock> MULTIBLOCK_REDIR_FLUID = registerWithItem("multi_redir_fluid", () ->
                new MultiChildCapRedirectBlock(() -> Lists.newArrayList(
                        ForgeCapabilities.FLUID_HANDLER
                ))
            );
    public static final RegistryObject<FabricatorBlock> FABRICATOR_MACHINE = registerWithItemSpecial("machines/fabricator", FabricatorBlock::create);
    public static final RegistryObject<TubeBlock> TELEPORT_TUBE = BLOCKS.register("teleport_tube", TubeBlock::create);

    //Fluids
    public static final RegistryObject<LiquidBlock> MERCURY_FLUID = BLOCKS.register("mercury_still", () -> new LiquidBlock(FluidRegistry.MERCURY.get(), BlockBehaviour.Properties.of(Material.WATER)));
    public static final RegistryObject<LiquidBlock> BIOMASS_FLUID = BLOCKS.register("biomass", () -> new LiquidBlock(FluidRegistry.BIOMASS.get(), BlockBehaviour.Properties.of(Material.WATER)));

    //Decoration
    public static final RegistryObject<SimpleHorizonalBlock> FOAM_PIPE = registerWithItem("decoration/foam_pipes", () -> new SimpleHorizonalBlock(BasicProps.Block.METAL.get()));
    public static final RegistryObject<Block> METAL_GRATE = registerWithItem("decoration/metal_grate", () -> new Block(BasicProps.Block.METAL.get().noOcclusion()));
    public static final RegistryObject<SlabBlock> METAL_GRATE_SLAB = registerWithItem("decoration/metal_grate_slab", () -> new SlabBlock(BasicProps.Block.METAL.get().noOcclusion()));
    public static final RegistryObject<Block> METAL_GRATE_SOLID = registerWithItem("decoration/metal_grate_solid", () -> new Block(BasicProps.Block.METAL.get()));
    public static final RegistryObject<TrapDoorBlock> METAL_GRATE_TRAPDOOR = registerWithItem("decoration/metal_grate_trapdoor", () -> new TrapDoorBlock(BasicProps.Block.METAL.get().noOcclusion(), BlockSetType.POLISHED_BLACKSTONE));
    public static final RegistryObject<Block> TUNGSTEN = registerWithItem("decoration/tungsten/tungsten", () -> new Block(BasicProps.Block.METAL.get()));
    public static final RegistryObject<Block> TUNGSTEN_RUNNING_LIGHTS = registerWithItem("decoration/tungsten/tungsten_runner_lights", () -> new Block(BasicProps.Block.METAL.get()));
    public static final RegistryObject<Block> TUNGSTEN_PATTERN = registerWithItem("decoration/tungsten/tungsten_pattern", () -> new Block(BasicProps.Block.METAL.get()));
    public static final RegistryObject<Block> TUNGSTEN_PIPES = registerWithItem("decoration/tungsten/tungsten_pipes", () -> new Block(BasicProps.Block.METAL.get()));
    public static final RegistryObject<Block> TUNGSTEN_PLATE = registerWithItem("decoration/tungsten/tungsten_plate", () -> new Block(BasicProps.Block.METAL.get()));
    public static final RegistryObject<SlabBlock> TUNGSTEN_PLATE_SLAB = registerWithItem("decoration/tungsten/tungsten_plate_slab", () -> new SlabBlock(BasicProps.Block.METAL.get()));
    public static final RegistryObject<Block> TUNGSTEN_PLATE_SMALL = registerWithItem("decoration/tungsten/tungsten_plate_small", () -> new Block(BasicProps.Block.METAL.get()));
    public static final RegistryObject<Block> TUNGSTEN_SCREEN = registerWithItem("decoration/tungsten/tungsten_screen", () -> new Block(BasicProps.Block.METAL.get()));
    public static final RegistryObject<Block> WIRE_HANG = registerWithItem("decoration/wire_hang", () -> new Block(BasicProps.Block.METAL.get().noOcclusion().noCollission()));
    public static final RegistryObject<Block> ALABASTER = registerWithItem("decoration/alabaster", () -> new Block(BasicProps.Block.METAL.get()));
    public static final RegistryObject<Block> ALABASTER_TILES_LARGE = registerWithItem("decoration/alabaster_tiles_large", () -> new Block(BasicProps.Block.METAL.get()));
    public static final RegistryObject<Block> ALABASTER_PILLAR = registerWithItem("decoration/alabaster_pillar", () -> new Block(BasicProps.Block.METAL.get()));
    public static final RegistryObject<Block> ALABASTER_TILES_SMALL = registerWithItem("decoration/alabaster_tiles_small", () -> new RotatedPillarBlock(BasicProps.Block.METAL.get()));
    public static final RegistryObject<StairBlock> ALABASTER_STAIRS = registerWithItem("decoration/alabaster/stairs", () -> new StairBlock(() -> BlockRegistry.ALABASTER.get().defaultBlockState(), BasicProps.Block.METAL.get()));
    public static final RegistryObject<WallBlock> ALABASTER_WALL = registerWithItem("decoration/alabaster/wall", () -> new WallBlock(BasicProps.Block.METAL.get()));
    public static final RegistryObject<WallBlock> ALABASTER_PILLAR_WALL = registerWithItem("decoration/alabaster/pillar_wall", () -> new WallBlock(BasicProps.Block.METAL.get()));
    public static final RegistryObject<SlabBlock> ALABASTER_SLAB = registerWithItem("decoration/alabaster/slab", () -> new SlabBlock(BasicProps.Block.METAL.get()));
    public static final RegistryObject<SimpleHorizonalBlock> ALABASTER_TILES_BIG = registerWithItem("decoration/alabaster/big_tiles", () -> new SimpleHorizonalBlock(BasicProps.Block.METAL.get()));
    public static final RegistryObject<Block> ALABASTER_RUNNER_LIGHTS = registerWithItem("decoration/alabaster/runner_lights", () -> new Block(BasicProps.Block.METAL.get()));
    public static final RegistryObject<Block> ALABASTER_BOOKSHELF = registerWithItem("decoration/alabaster/bookshelf", () -> new Block(BasicProps.Block.METAL.get()));
    public static final RegistryObject<Block> ALABASTER_PIPES = registerWithItem("decoration/alabaster/pipes", () -> new Block(BasicProps.Block.METAL.get()));
    public static final RegistryObject<Block> ALABASTER_SCREEN = registerWithItem("decoration/alabaster/screen", () -> new Block(BasicProps.Block.METAL.get()));
    public static final RegistryObject<Block> TECH_STRUT = registerWithItem("decoration/tech_struts", TechStrutBlock::create);
    public static final RegistryObject<StairBlock> TUNGSTEN_STAIRS = registerWithItem("decoration/tungsten/stair", () -> new StairBlock(() -> TUNGSTEN.get().defaultBlockState(), BasicProps.Block.METAL.get()));
    public static final RegistryObject<StairBlock> TUNGSTEN_STAIRS_SMALL_PLATES = registerWithItem("decoration/tungsten/stair_small_plates", () -> new StairBlock(() -> TUNGSTEN.get().defaultBlockState(), BasicProps.Block.METAL.get()));
    public static final RegistryObject<Block> BLINKING_LIGHTS = registerWithItem("decoration/blinking_lights", () -> new Block(BasicProps.Block.METAL.get()));
    public static final RegistryObject<Block> HOLO_LADDER = registerWithItem("holo_ladder", HoloLadderBlock::create);
    public static final RegistryObject<Block> STEAM_GRATE = registerWithItem("steam_grate", SteamGrateBlock::create);
    public static final RegistryObject<Block> EBONY_BOOKSHELF = registerWithItem("decoration/ebony_bookshelf",  () -> new Block(BasicProps.Block.WOOL.get()));
    public static final RegistryObject<Block> STEEL_HEX_OFFSET = registerWithItem("decoration/steel_hex_offset", () -> new Block(BasicProps.Block.METAL.get().noOcclusion()));
    public static final RegistryObject<Block> STEEL_HEX = registerWithItem("decoration/steel_hex", () -> new Block(BasicProps.Block.METAL.get().noOcclusion()));
    public static final RegistryObject<Block> MOON_BASE_GLASS = registerWithItem("decoration/moon_base_glass", () -> new GlassBlock(BasicProps.Block.GLASS.get()));
    public static final RegistryObject<Block> DEDICATION_PLAQUE = registerWithItem("decoration/dedication_plaque", DedicationPlaqueBlock::create);
    public static final RegistryObject<ChairBlock> COMFY_CHAIR_RED = registerWithItem("comfy_chair_red", ChairBlock::createComfy);
    public static final RegistryObject<ChairBlock> COMFY_CHAIR_GREEN = registerWithItem("comfy_chair_green", ChairBlock::createComfy);
    public static final RegistryObject<ItemPlaqueBlock> ITEM_PLAQUE = registerWithItem("item_plaque", ItemPlaqueBlock::create);
    public static final RegistryObject<Block> ALABASTER_GRATE = registerWithItem("alabaster_grate", () -> new Block(BasicProps.Block.STONE.get().noOcclusion()));
    public static final RegistryObject<Block> TUNGSTEN_GRATE = registerWithItem("decoration/tungsten_grate", () -> new Block(BasicProps.Block.STONE.get().noOcclusion()));
    public static final RegistryObject<BigDoorBlock<BigDoorTile>> BIG_DOOR = registerWithItem("decoration/big_door", BigDoorBlock::create);
    public static final RegistryObject<SlabBlock> TUNGSTEN_SMALL_PLATE_SLAB = registerWithItem("decoration/tungsten_small_plate_slab", () -> new SlabBlock(BasicProps.Block.METAL.get()));
    public static final RegistryObject<Block> CORAL_BLANK = registerWithItem("decoration/coral", () -> new Block(BasicProps.Block.STONE.get()));
    public static final RegistryObject<SlabBlock> CORAL_SLAB = registerWithItem("decoration/coral_slab", () -> new SlabBlock(BasicProps.Block.STONE.get()));
    public static final RegistryObject<StairBlock> CORAL_STAIRS = registerWithItem("decoration/coral_stairs", () -> new StairBlock(
            () -> CORAL_BLANK.get().defaultBlockState(), BasicProps.Block.STONE.get()
    ));
    public static final RegistryObject<WallBlock> CORAL_WALL = registerWithItem("decoration/coral_wall", () -> new WallBlock(BasicProps.Block.STONE.get()));
    public static final RegistryObject<WallBlock> METAL_GRATE_WALL = registerWithItem("decoration/metal_grate_wall", () -> new WallBlock(BasicProps.Block.METAL.get()));
    public static final RegistryObject<RailingBlock> RAILING_STEAM = registerWithItem("decoration/railing_steam", RailingBlock::create);
    public static final RegistryObject<RailingBlock> RAILING_TUNGSTEN = registerWithItem("decoration/railing_tungsten", RailingBlock::create);
    public static final RegistryObject<RailingBlock> RAILING_ALABASTER = registerWithItem("decoration/railing_alabaster", RailingBlock::create);





    public static final RegistryObject<TestRendererBlock> RENDERER_TEST = registerWithItem("renderer_test", TestRendererBlock::new);


    public static <T extends Block> RegistryObject<T> registerWithItem(String name, final Supplier<T> block, Function<Item.Properties, Item.Properties> properties){
        final RegistryObject<T> reg = BLOCKS.register(name, block);
        ItemRegistry.ITEMS.register(name, () -> new BlockItem(reg.get(), properties.apply(new Item.Properties())));
        return reg;
    }

    public static <T extends Block> RegistryObject<T> registerWithItem(String name, final Supplier<T> block){
        return registerWithItem(name, block, p -> p);
    }

    public static <T extends Block> RegistryObject<T> registerWithItemSpecial(String name, final Supplier<T> block){

        final RegistryObject<T> reg = BLOCKS.register(name, block);

        ItemRegistry.ITEMS.register(name, () -> new SpecialBlockItem(reg.get(), new Item.Properties()));

        return reg;
    }

    public enum BlockSetPart{
        NORMAL(Block::new),
        SLAB(SlabBlock::new, "_slab"),
        STAIRS(Block::new, "_stairs");

        private final Function<BlockBehaviour.Properties, ? extends Block> defaultFactory;
        private final String suffix;

        BlockSetPart(Function<BlockBehaviour.Properties, ? extends Block> factory, String suffix){
            this.defaultFactory = factory;
            this.suffix = suffix;
        }

        BlockSetPart(Function<BlockBehaviour.Properties, ? extends Block> factory){
            this(factory, "");
        }
    }

    public static class BlockSetBuilder{

        final ResourceLocation baseRL;
        final BlockBehaviour.Properties properties;
        final EnumMap<BlockSetPart, RegistryObject<Block>> map = new EnumMap<>(BlockSetPart.class);

        public BlockSetBuilder(ResourceLocation baseRL, BlockBehaviour.Properties properties){
            this.baseRL = baseRL;
            this.properties = properties;
        }
    }

}
