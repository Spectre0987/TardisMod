package net.tardis.mod.sound;

import net.minecraft.client.resources.sounds.AbstractTickableSoundInstance;
import net.minecraft.sounds.SoundEvent;
import net.minecraft.sounds.SoundSource;
import net.minecraft.util.RandomSource;
import net.minecraft.world.entity.Entity;

public class MovingSound extends AbstractTickableSoundInstance {

    final Entity entity;

    public MovingSound(Entity entity, SoundEvent event, SoundSource source, RandomSource random, float vol, boolean loop) {
        super(event, source, random);
        this.entity = entity;
        this.volume = vol;
        this.looping = loop;
        this.attenuation = Attenuation.LINEAR;
    }

    @Override
    public void tick() {
        if(this.entity == null || this.entity.isRemoved()){
            stop();
            return;
        }

        this.x = this.entity.getX();
        this.y = this.entity.getY();
        this.z = this.entity.getZ();
    }
}
