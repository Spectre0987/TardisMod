package net.tardis.mod.sound;

import com.mojang.serialization.Codec;
import com.mojang.serialization.codecs.RecordCodecBuilder;
import net.minecraft.network.chat.Component;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.sounds.SoundEvent;
import net.tardis.mod.Tardis;

public class SoundScheme {

    public static final Codec<SoundScheme> CODEC = RecordCodecBuilder.create(instance ->
            instance.group(
                    SoundData.CODEC.fieldOf("takeoff").forGetter(SoundScheme::getTakeoff),
                    SoundData.CODEC.fieldOf("land").forGetter(SoundScheme::getLand),
                    SoundData.CODEC.fieldOf("fly").forGetter(SoundScheme::getFly)
            ).apply(instance, SoundScheme::new));

    private final SoundData takeoff;
    private final SoundData land;
    private final SoundData fly;


    public SoundScheme(SoundEvent takeoff, int takeoffDuration, SoundEvent flyloop, int flyloopDuration, SoundEvent land, int landDuration){
        this(new SoundData(takeoff, takeoffDuration), new SoundData(land, landDuration), new SoundData(flyloop, flyloopDuration));
    }

    public SoundScheme(SoundData takeoff, SoundData land, SoundData fly){
        this.takeoff = takeoff;
        this.land = land;
        this.fly = fly;
    }

    public SoundData getTakeoff() {
        return takeoff;
    }

    public SoundData getFly() {
        return fly;
    }

    public SoundData getLand() {
        return land;
    }

    public record SoundData(SoundEvent event, int time){
        public static final Codec<SoundData> CODEC = RecordCodecBuilder.create(instance -> {
            return instance.group(
                SoundEvent.DIRECT_CODEC.fieldOf("sound").forGetter(SoundData::event),
                Codec.INT.fieldOf("time").forGetter(SoundData::time)
            ).apply(instance, SoundData::new);
        });
    }

}
