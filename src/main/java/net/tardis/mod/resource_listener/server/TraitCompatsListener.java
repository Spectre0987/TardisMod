package net.tardis.mod.resource_listener.server;

import com.google.gson.JsonParser;
import com.mojang.serialization.JsonOps;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.server.packs.resources.Resource;
import net.minecraft.server.packs.resources.ResourceManager;
import net.minecraft.server.packs.resources.SimplePreparableReloadListener;
import net.minecraft.util.profiling.ProfilerFiller;
import net.tardis.mod.Tardis;
import net.tardis.mod.emotional.TraitCompat;
import net.tardis.mod.helpers.Helper;
import org.apache.logging.log4j.Level;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TraitCompatsListener extends SimplePreparableReloadListener<HashMap<ResourceLocation, List<ResourceLocation>>> {

    public static final HashMap<ResourceLocation, List<ResourceLocation>> INCOMPATIBILITY = new HashMap<>();
    public static final String FOLDER = Tardis.MODID + "/traits";
    public static final TraitCompatsListener INSTANCE = new TraitCompatsListener();

    @Override
    protected HashMap<ResourceLocation, List<ResourceLocation>> prepare(ResourceManager pResourceManager, ProfilerFiller pProfiler) {
        INCOMPATIBILITY.clear();

        try{

            Map<ResourceLocation, Resource> compatResources = pResourceManager.listResources(FOLDER, Helper::isRLJson);

            for(Map.Entry<ResourceLocation, Resource> entry : compatResources.entrySet()){

                    TraitCompat compat = TraitCompat.CODEC.decode(JsonOps.INSTANCE, JsonParser.parseReader(entry.getValue().openAsReader())).getOrThrow(false, Tardis.LOGGER::warn).getFirst();

                    INCOMPATIBILITY.computeIfAbsent(compat.traitId(), rl -> new ArrayList<>())
                            .addAll(compat.incompatibleTraits());

            }

        }
        catch(Exception e){
            Tardis.LOGGER.log(Level.WARN, "Error Loading TARDIS Trait Compats!");
            Tardis.LOGGER.log(Level.WARN, e);
        }

        return INCOMPATIBILITY;
    }

    @Override
    protected void apply(HashMap<ResourceLocation, List<ResourceLocation>> pObject, ResourceManager pResourceManager, ProfilerFiller pProfiler) {

    }

}
