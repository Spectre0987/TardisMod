package net.tardis.mod.emotional.loyalty_functions;

import net.minecraft.world.entity.player.Player;
import net.tardis.mod.cap.level.ITardisLevel;

import java.util.function.BiPredicate;

public abstract class LoyaltyFunction {

    final BiPredicate<ITardisLevel, Player> canApply;

    public LoyaltyFunction(BiPredicate<ITardisLevel, Player> canApply){
        this.canApply = canApply;
    }


    public boolean applies(ITardisLevel tardis, Player player){
        return this.canApply.test(tardis, player);
    }



    public static BiPredicate<ITardisLevel, Player> lessThan(int loyalty){
        return (tardis, player) -> tardis.getEmotionalHandler().getLoyalty(player.getUUID()).orElse(0) < loyalty;
    }

    public static BiPredicate<ITardisLevel, Player> moreThan(int loyalty){
        return (tardis, player) -> tardis.getEmotionalHandler().getLoyalty(player.getUUID()).orElse(0) > loyalty;
    }

    public BiPredicate<ITardisLevel, Player> inRange(int min, int max){
        return (tardis, player) -> {
            int l = tardis.getEmotionalHandler().getLoyalty(player.getUUID()).orElse(0);
            return l > min && l < max;
        };
    }

}
