package net.tardis.mod.emotional.traits;

import com.mojang.datafixers.util.Pair;
import com.mojang.serialization.Codec;
import com.mojang.serialization.codecs.RecordCodecBuilder;
import net.minecraft.core.BlockPos;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.Item;
import net.minecraft.world.level.biome.Biome;
import net.tardis.mod.cap.level.ITardisLevel;
import net.tardis.mod.emotional.Trait;
import net.tardis.mod.misc.ObjectOrTagCodec;
import net.tardis.mod.registry.TraitRegistry;

import java.util.List;

public class EmptyTrait extends Trait {

    public EmptyTrait(Codec<? extends Trait> type, List<Pair<ObjectOrTagCodec<Item>, Integer>> likedItems) {
        super(type, likedItems);
    }

    public static final Codec<EmptyTrait> CODEC = RecordCodecBuilder.create(i ->
                i.group(
                        TraitRegistry.REGISTRY.get().getCodec().fieldOf("type").forGetter(Trait::getType),
                        ITEM_CODEC.forGetter(Trait::getLikedItems)
                ).apply(i, EmptyTrait::new)
            );

    @Override
    public void affectLanding(ITardisLevel tardis) {

    }

    @Override
    public void onCrewSecond(ITardisLevel tardis, List<Player> crew) {

    }

    @Override
    public void onLandedHour(ITardisLevel tardis, ServerLevel landedLevel, Biome currentBiome, BlockPos location) {

    }
}
