package net.tardis.mod.commands;

import com.google.common.collect.Lists;
import com.mojang.brigadier.builder.LiteralArgumentBuilder;
import com.mojang.brigadier.builder.RequiredArgumentBuilder;
import com.mojang.brigadier.context.CommandContext;
import com.mojang.brigadier.exceptions.CommandSyntaxException;
import com.mojang.brigadier.suggestion.SuggestionProvider;
import net.minecraft.commands.CommandSourceStack;
import net.minecraft.commands.Commands;
import net.minecraft.commands.SharedSuggestionProvider;
import net.minecraft.commands.arguments.ResourceLocationArgument;
import net.minecraft.core.HolderLookup;
import net.minecraft.core.registries.Registries;
import net.minecraft.resources.ResourceKey;
import net.minecraft.resources.ResourceLocation;
import net.minecraftforge.registries.ForgeRegistry;
import net.minecraftforge.registries.RegistryManager;
import net.tardis.mod.registry.ExteriorRegistry;
import net.tardis.mod.registry.JsonRegistries;

import java.util.List;
import java.util.Optional;

public class UnlockCommand implements ITardisSubCommand {

    public static final SuggestionProvider<CommandSourceStack> REG_FILTER = (context, builder) -> {
        List<ResourceLocation> sugestions = Lists.newArrayList(
                ExteriorRegistry.REGISTRY.get().getRegistryName(),
                JsonRegistries.ARS_ROOM_REGISTRY.location(),
                Registries.DIMENSION_TYPE.location()
        );
        return SharedSuggestionProvider.suggestResource(sugestions, builder);
    };

    public static final SuggestionProvider<CommandSourceStack> ITEM_FILTER = (context, builder) -> {
        ResourceLocation regLoc = context.getArgument("registry", ResourceLocation.class);

        Optional<HolderLookup.RegistryLookup<Object>> levelRegistry = context.getSource().getServer().registryAccess().lookup(ResourceKey.createRegistryKey(regLoc));
        if(levelRegistry.isPresent())
            return SharedSuggestionProvider.suggestResource(levelRegistry.get().listElementIds().map(ResourceKey::location).toList(), builder);

        final ForgeRegistry<?> forgeReg = RegistryManager.ACTIVE.getRegistry(regLoc);
        return SharedSuggestionProvider.suggestResource(forgeReg.getKeys(), builder);
    };

    public static final RequiredArgumentBuilder<CommandSourceStack, ResourceLocation> REGISTRY_ARG = Commands.argument("registry", ResourceLocationArgument.id()).suggests(REG_FILTER);
    public static final RequiredArgumentBuilder<CommandSourceStack, ResourceLocation> ITEM_ARG = Commands.argument("item", ResourceLocationArgument.id()).suggests(ITEM_FILTER);

    @Override
    public LiteralArgumentBuilder<CommandSourceStack> build(LiteralArgumentBuilder<CommandSourceStack> builder) {
        return builder.then(
                Commands.literal("unlock")
                        .then(CommandRegistry.TARDIS_ARG.get().then(
                                REGISTRY_ARG.then(ITEM_ARG.executes(UnlockCommand::unlock)
                                        .then(Commands.literal("relock").executes(UnlockCommand::relock)))
                                )
                        )
        );
    }

    public static ResourceKey<?> getUnlock(CommandContext<CommandSourceStack> source){
        return ResourceKey.create(
                ResourceKey.createRegistryKey(source.getArgument("registry", ResourceLocation.class)),
                source.getArgument("item", ResourceLocation.class)
        );
    }

    public static int unlock(CommandContext<CommandSourceStack> context){
        CommandRegistry.getTardis(context).ifPresent(tardis -> {
            ResourceKey<?> key = getUnlock(context);
            tardis.getUnlockHandler().unlock(key);
        });
        return 1;
    }

    public static int relock(CommandContext<CommandSourceStack> context){
        CommandRegistry.getTardis(context).ifPresent(tardis -> {
            tardis.getUnlockHandler().removeUnlock(getUnlock(context));
        });
        return 1;
    }
}
