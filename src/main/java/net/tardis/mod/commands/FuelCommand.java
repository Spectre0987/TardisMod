package net.tardis.mod.commands;

import com.mojang.brigadier.arguments.FloatArgumentType;
import com.mojang.brigadier.builder.LiteralArgumentBuilder;
import com.mojang.brigadier.context.CommandContext;
import net.minecraft.commands.CommandSourceStack;
import net.minecraft.commands.Commands;

public class FuelCommand implements ITardisSubCommand{
    @Override
    public LiteralArgumentBuilder<CommandSourceStack> build(LiteralArgumentBuilder<CommandSourceStack> builder) {
        return builder.then(Commands.literal("artron")
                        .then(CommandRegistry.TARDIS_ARG.get()
                            .then(Commands.argument("add", FloatArgumentType.floatArg(0, Float.MAX_VALUE))
                                    .executes(FuelCommand::addFuel)))
        );
    }

    public static int setFuel(CommandContext<CommandSourceStack> context){
        CommandRegistry.getTardis(context).ifPresent(tardis -> {

        });
        return 1;
    }

    public static int addFuel(CommandContext<CommandSourceStack> context){
        CommandRegistry.getTardis(context).ifPresent(tardis -> {
            tardis.getFuelHandler().fillArtron(context.getArgument("artron", Float.class), false);
        });
        return 1;
    }
}
