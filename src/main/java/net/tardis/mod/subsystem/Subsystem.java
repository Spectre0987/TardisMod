package net.tardis.mod.subsystem;

import net.minecraft.nbt.CompoundTag;
import net.minecraft.world.item.ItemStack;
import net.minecraftforge.common.util.INBTSerializable;
import net.tardis.mod.cap.level.ITardisLevel;
import net.tardis.mod.helpers.Helper;
import net.tardis.mod.item.components.SubsystemItem;
import net.tardis.mod.misc.tardis.TardisEngine;
import net.tardis.mod.registry.SubsystemRegistry;

import java.util.Optional;
import java.util.function.Predicate;

public abstract class Subsystem implements INBTSerializable<CompoundTag>{

    private final ITardisLevel tardis;
    private final SubsystemType<? extends Subsystem> type;
    private Predicate<ItemStack> stackTest;

    boolean isActive = true;

    public Subsystem(SubsystemType<? extends Subsystem> type, Predicate<ItemStack> stackTest, ITardisLevel level) {
        this.type = type;
        this.stackTest = stackTest;
        this.tardis = level;
    }

    public SubsystemType<? extends Subsystem> getType(){
        return this.type;
    }

    public ITardisLevel getTardis(){
        return this.tardis;
    }

    public boolean canFly(){
        if(!this.getType().isRequiredForFlight){
            return true; //If this is not required for flight, we don't give a damn about it's opinion
        }

        return !this.isBrokenOrOff();

    }

    public boolean isBrokenOrOff(){

        if(this.isBroken()){
            return true;
        }
        return !this.isActive;
    }

    public boolean isActivated(){
        return this.isActive;
    }

    public void setActive(boolean isActive){
        this.isActive = isActive;
    }

    public void onFlightTick(){}
    public void onNonFlightTick(){}
    public void onLand(){}
    public void onTakeoff(){}

    public ItemStack getItem(){
        for(int i = 0; i < tardis.getEngine().getInventoryFor(TardisEngine.EngineSide.SUBSYSTEMS).getSlots(); ++i){
            final ItemStack stack = tardis.getEngine().getInventoryFor(TardisEngine.EngineSide.SUBSYSTEMS).getStackInSlot(i);
            if(stack.getItem() instanceof SubsystemItem item){
                if(item.getSubsytemType() == this.getType()){
                    return stack;
                }
            }
        }
        return ItemStack.EMPTY;
    }

    /**
     *
     * @return - true on broken
     */
    public boolean damage(int damage){
        return this.getItem().hurt(1, this.tardis.getLevel().random, null);
    }

    public float getSubsytemHealth(){
        ItemStack stack = this.getItem();
        return this.getItem().getMaxDamage() == 0 ? 1.0F :
                (stack.getMaxDamage() - stack.getDamageValue()) / (float)stack.getMaxDamage();
    }

    @Override
    public CompoundTag serializeNBT() {
        CompoundTag tag = new CompoundTag();
        Helper.writeRegistryToNBT(tag, SubsystemRegistry.TYPE_REGISTRY.get(), this.type, "type");
        tag.putBoolean("is_active", this.isActive);
        return tag;
    }

    @Override
    public void deserializeNBT(CompoundTag tag) {
        this.isActive = tag.getBoolean("is_active");
    }

    public boolean canBeUsed(){
        return this.getType().canBeUsed(this.tardis);
    }

    public boolean isBroken() {
        ItemStack stack = this.getItem();

        if(stack.isEmpty() || !this.stackTest.test(stack)){
            return true;
        }
        return stack.getDamageValue() >= stack.getMaxDamage();
    }
}
