package net.tardis.mod.subsystem;

import com.mojang.datafixers.util.Pair;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.item.ItemStack;
import net.tardis.mod.cap.level.ITardisLevel;
import net.tardis.mod.item.components.SubsystemItem;
import net.tardis.mod.misc.tardis.TardisEngine;
import net.tardis.mod.registry.SubsystemRegistry;
import org.apache.commons.lang3.function.TriFunction;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;

public class SubsystemType<T extends Subsystem> {

    public final boolean isRequiredForFlight;
    public final List<Pair<Predicate<ItemStack>, TriFunction<SubsystemType<T>, Predicate<ItemStack>, ITardisLevel, T>>> systems = new ArrayList<>();
    private String translationKey = null;

    public SubsystemType(boolean requiredForFlight){
        this.isRequiredForFlight = requiredForFlight;
    }

    public static <T extends Subsystem> SubsystemType<T> createRequired(){
        return  new SubsystemType<T>(true);
    }

    public static <T extends Subsystem> SubsystemType<T> createOptional(){
        return new SubsystemType<>(false);
    }

    public Optional<T> createSubsystem(ITardisLevel level){
        //search engine for valid subsystem item
        for(int i = 0; i < level.getEngine().getInventoryFor(TardisEngine.EngineSide.SUBSYSTEMS).getSlots(); ++i){
            final ItemStack stack = level.getEngine().getInventoryFor(TardisEngine.EngineSide.SUBSYSTEMS).getStackInSlot(i);
            if(stack.getItem() instanceof SubsystemItem item){
                if(item.getSubsytemType() == this){ //If the engine contains an item that has this
                    //Get all subsystems to find a matching one
                    for(Pair<Predicate<ItemStack>, TriFunction<SubsystemType<T>, Predicate<ItemStack>, ITardisLevel, T>> system : this.systems){
                        if(system.getFirst().test(stack)){
                            return Optional.of(system.getSecond().apply(this, system.getFirst(), level));
                        }
                    }
                }
            }
        }
        return Optional.empty();
    }

    public boolean canBeUsed(ITardisLevel tardis){
        Optional<T> sys = tardis.getSubsystem(this);
        return sys.isPresent() && !sys.get().isBrokenOrOff();
    }

    public String getTranslationKey(){
        if(this.translationKey == null){
            final ResourceLocation key = SubsystemRegistry.TYPE_REGISTRY.get().getKey(this);
            this.translationKey = "subsystem_type.%s.%s".formatted(key.getNamespace(), key.getPath().replace('/', '.'));
        }
        return this.translationKey;
    }

    public void registerSubsystem(Predicate<ItemStack> stackTest, TriFunction<SubsystemType<T>, Predicate<ItemStack>, ITardisLevel, T> func){
        systems.add(new Pair<>(stackTest, func));
    }

}
