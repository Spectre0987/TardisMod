package net.tardis.mod.subsystem;

import net.minecraft.world.item.ItemStack;
import net.tardis.mod.cap.level.ITardisLevel;

import java.util.function.Predicate;

//Subsystem that just exists for things that track damage
public class BasicSubsystem extends Subsystem {

    public BasicSubsystem(SubsystemType type, Predicate<ItemStack> stackTest, ITardisLevel level) {
        super(type, stackTest, level);
    }
}
