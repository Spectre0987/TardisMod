package net.tardis.mod.subsystem;

import net.minecraft.core.Registry;
import net.minecraft.core.RegistryAccess;
import net.minecraft.core.registries.Registries;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.server.level.ServerPlayer;
import net.minecraft.world.damagesource.DamageType;
import net.minecraft.world.effect.MobEffectInstance;
import net.minecraft.world.effect.MobEffects;
import net.minecraft.world.item.ItemStack;
import net.tardis.mod.cap.level.ITardisLevel;
import net.tardis.mod.misc.ObjectHolder;
import net.tardis.mod.tags.DamageSourceTags;

import java.util.function.Predicate;

public class TemporalGraceSystem extends BasicSubsystem{
    public TemporalGraceSystem(SubsystemType type, Predicate<ItemStack> stackTest, ITardisLevel level) {
        super(type, stackTest, level);
    }

    @Override
    public void onNonFlightTick() {
        super.onNonFlightTick();
        giveGraceEffect();
    }

    @Override
    public void onFlightTick() {
        super.onFlightTick();
        giveGraceEffect();
    }

    public void giveGraceEffect(){
        if(getTardis().getLevel() instanceof ServerLevel server && server.getGameTime() % 40 == 0){
            for(ServerPlayer player : server.players()){
                player.addEffect(new MobEffectInstance(MobEffects.DAMAGE_RESISTANCE, 60, 1, true, false, false));
            }
        }
    }

    public boolean isBlockedCompletely(DamageType type){
        final RegistryAccess access = getTardis().getLevel().registryAccess();
        final Registry<DamageType> damReg = access.registryOrThrow(Registries.DAMAGE_TYPE);

        final ObjectHolder<Boolean> result = new ObjectHolder<>(false);

        damReg.getTag(DamageSourceTags.temporalGraceBlocks).ifPresent(tag -> {
            damReg.getResourceKey(type).ifPresent(key -> {
                if(tag.contains(damReg.getHolderOrThrow(key))){
                    result.set(true);
                }
            });
        });

        return result.get();
    }
}
