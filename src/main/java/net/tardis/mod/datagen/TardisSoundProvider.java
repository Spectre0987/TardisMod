package net.tardis.mod.datagen;

import net.minecraft.data.PackOutput;
import net.minecraft.sounds.SoundEvent;
import net.minecraftforge.common.data.ExistingFileHelper;
import net.minecraftforge.common.data.SoundDefinition;
import net.minecraftforge.common.data.SoundDefinitionsProvider;
import net.minecraftforge.registries.ForgeRegistries;
import net.tardis.mod.Constants;
import net.tardis.mod.Tardis;
import net.tardis.mod.helpers.Helper;
import net.tardis.mod.sound.SoundRegistry;

import java.util.function.Supplier;

public class TardisSoundProvider extends SoundDefinitionsProvider {
    /**
     * Creates a new instance of this data provider.
     *
     * @param output The {@linkplain PackOutput} instance provided by the data generator.
     * @param helper The existing file helper provided by the event you are initializing this provider in.
     */
    public TardisSoundProvider(PackOutput output, ExistingFileHelper helper) {
        super(output, Tardis.MODID, helper);
    }

    @Override
    public void registerSounds() {

        this.add(SoundRegistry.AXIS_CONTROL_SOUND, addMultiSoundDef(
                "tardis/controls/direction_one",
                "tardis/controls/direction_two",
                "tardis/controls/direction_three"
        ));
        this.add(SoundRegistry.CONTROL_GENERIC, addMultiSoundDef("tardis/controls/generic_one", "tardis/controls/generic_three"));
        this.add(SoundRegistry.RANDOMIZER_CONTROL, addMultiSoundDef("tardis/controls/randomizer"));
        this.add(SoundRegistry.DEFAULT_TARDIS_TAKEOFF, addMultiSoundDef("tardis/sound_scheme/tardis_takeoff"));
        this.add(SoundRegistry.DEFAULT_TARDIS_LOOP, addMultiSoundDef("tardis/sound_scheme/tardis_fly_loop"));
        this.add(SoundRegistry.DEFAULT_TARDIS_LAND, addMultiSoundDef("tardis/sound_scheme/tardis_land"));
        this.add(SoundRegistry.TAKEOFF_FAIL, addMultiSoundDef("tardis/tardis_cant_start"));
        this.add(SoundRegistry.DOOR_CLOSE, "tardis/door_close");
        this.add(SoundRegistry.DOOR_OPEN, "tardis/door_open");
        this.add(SoundRegistry.DOOR_LOCK, "tardis/door_lock");
        this.add(SoundRegistry.DOOR_UNLOCK, "tardis/door_unlock");
        this.add(SoundRegistry.SUBSYSTEM_ON, "tardis/subsystems_on");
        this.add(SoundRegistry.SUBSYSTEM_OFF, "tardis/subsystems_off");
        this.add(SoundRegistry.REFUEL_ON, "tardis/controls/refuel_start");
        this.add(SoundRegistry.REFUEL_OFF, "tardis/controls/refuel_stop");
        this.add(SoundRegistry.SONIC_FAIL, "tardis/sonic/sonic_fail");
        this.add(SoundRegistry.SONIC_INTERACT, "tardis/sonic/sonic_generic");
        this.add(SoundRegistry.SONIC_MODE_CHANGE, "tardis/sonic/sonic_mode_change");
        this.add(SoundRegistry.TARDIS_POWER_ON, "tardis/tardis_power_up");
        this.add(SoundRegistry.TARDIS_POWER_OFF, "tardis/tardis_shut_down");
        this.add(SoundRegistry.SIMPLE_BEEP, "tardis/controls/generic_one");
        this.add(SoundRegistry.TARDIS_LAND_COMPLETE, "tardis/reached_destination");
        this.add(SoundRegistry.STEAM_HISS,
                "tardis/machines/steam_hiss_001",
                "tardis/machines/steam_hiss_002",
                "tardis/machines/steam_hiss_003",
                "tardis/machines/steam_hiss_004"
        );
        this.add(SoundRegistry.ELECTRIC_SPARK,
                "tardis/machines/electric_spark_a",
                    "tardis/machines/electric_spark_b",
                "tardis/machines/electric_spark_c"
                );
        this.add(SoundRegistry.CLOISTER_BELL, "tardis/cloister_bell");
        this.add(SoundRegistry.MASTER_TARDIS_TAKEOFF, "tardis/sound_scheme/master_takeoff");
        this.add(SoundRegistry.MASTER_TARDIS_LAND, "tardis/sound_scheme/master_land");
        this.add(SoundRegistry.TARDIS_IMPACT, "tardis/impact/impact_1", "tardis/impact/impact_2", "tardis/impact/impact_3");
        this.add(SoundRegistry.TV_TARDIS_LAND, "tardis/sound_scheme/tardis_land_tv");
        this.add(SoundRegistry.TV_TARDIS_TAKEOFF, "tardis/sound_scheme/tardis_takeoff_tv");
        this.add(SoundRegistry.JUNK_TARDIS_LAND, "tardis/sound_scheme/junk_land");
        this.add(SoundRegistry.JUNK_TARDIS_TAKEOFF, "tardis/sound_scheme/junk_takeoff");
        this.add(SoundRegistry.WELDING, "tardis/tools/welding");
        this.add(SoundRegistry.DOOR_FAILED_LOCKED, "tardis/door_failed_locked");
        this.add(SoundRegistry.SLIDING_DOOR, "tardis/slidingdoor");
        this.add(SoundRegistry.COMMUNICATOR_BELL, "tardis/controls/com_bell");
        this.add(SoundRegistry.FIRST_ENTRANCE_MUSIC, 3.0F, true, "tardis/music/tardis_first_entrance");
        this.add(SoundRegistry.TARDIS_CREAK, 1.0F, true, "tardis/creaks_loop");
        this.add(SoundRegistry.TUBE,
                "machines/tube/tube_01",
                "machines/tube/tube_02",
                "machines/tube/tube_03"
        );
    }

    public void add(Supplier<SoundEvent> sound, String... path){
        this.add(sound, 1.0F, false, path);
    }
    public void add(Supplier<SoundEvent> sound, float vol, boolean isLong, String... path){
        this.add(sound, addMultiSoundDef(isLong, vol, path).subtitle(Constants.Translation.makeGenericTranslation(ForgeRegistries.SOUND_EVENTS, sound.get())));
    }

    public static SoundDefinition addMultiSoundDef(SoundDefinition.SoundType type, float vol, boolean isLong, SoundDefinition def, String... paths){
        SoundDefinition.Sound[] sounds = new SoundDefinition.Sound[paths.length];
        for(int i = 0; i < paths.length; ++i){
            sounds[i] = SoundDefinition.Sound.sound(Helper.createRL(paths[i]), type);
            if(isLong)
                sounds[i] = sounds[i].stream();
        }
        return def.with(sounds);
    }

    public static SoundDefinition addMultiSoundDef(String... paths){
        return addMultiSoundDef(false, 1.0F, paths);
    }
    public static SoundDefinition addMultiSoundDef(boolean isLong, float vol,  String... paths){
        return addMultiSoundDef(SoundDefinition.SoundType.SOUND, vol, isLong, SoundDefinition.definition(), paths);
    }
}
