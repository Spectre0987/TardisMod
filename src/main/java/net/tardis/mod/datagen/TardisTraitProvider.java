package net.tardis.mod.datagen;

import com.google.common.collect.Lists;
import com.mojang.datafixers.util.Pair;
import com.mojang.serialization.Codec;
import com.mojang.serialization.JsonOps;
import net.minecraft.data.CachedOutput;
import net.minecraft.data.DataGenerator;
import net.minecraft.data.DataProvider;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.tags.BiomeTags;
import net.minecraft.tags.ItemTags;
import net.minecraft.tags.TagKey;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.Items;
import net.minecraft.world.level.ItemLike;
import net.minecraft.world.level.biome.Biome;
import net.minecraft.world.level.block.Blocks;
import net.minecraftforge.common.Tags;
import net.minecraftforge.registries.ForgeRegistries;
import net.tardis.mod.Tardis;
import net.tardis.mod.emotional.Trait;
import net.tardis.mod.emotional.traits.EmptyTrait;
import net.tardis.mod.emotional.traits.LikeBiomeTrait;
import net.tardis.mod.emotional.traits.LikeCreatureTrait;
import net.tardis.mod.emotional.traits.LikeUseItemTrait;
import net.tardis.mod.misc.ObjectOrTagCodec;
import net.tardis.mod.registry.BlockRegistry;
import net.tardis.mod.registry.TraitRegistry;
import net.tardis.mod.resource_listener.server.TraitListener;

import java.nio.file.Path;
import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.function.Consumer;

public class TardisTraitProvider implements DataProvider {

    final Map<ResourceLocation, Trait> traits = new HashMap<>();
    final DataGenerator generator;
    final String modid;

    public TardisTraitProvider(DataGenerator generator, String modid){
        this.generator = generator;
        this.modid = modid;
    }

    public TardisTraitProvider(DataGenerator gen){
        this(gen, Tardis.MODID);
    }

    public void addAllTraits(){
        this.addTrait("likes_cats", new LikeCreatureTrait(TraitRegistry.LIKE_CREATURE.get(),
                new ItemBuilder()
                        .add(ItemTags.FISHES, 10)
                        .build(),
                new ObjectOrTagCodec<>(ForgeRegistries.ENTITY_TYPES, EntityType.CAT)));
        this.addTrait("likes_dogs", new LikeCreatureTrait(TraitRegistry.LIKE_CREATURE.get(),
                new ItemBuilder()
                        .add(Tags.Items.BONES, 10)
                        .build(),
                new ObjectOrTagCodec<>(ForgeRegistries.ENTITY_TYPES, EntityType.WOLF)));

        this.addTrait("likes_heat", new LikeBiomeTrait(
                TraitRegistry.LIKE_BIOME.get(),
                new ItemBuilder()
                        .add(Items.LAVA_BUCKET, 20)
                        .add(Blocks.MAGMA_BLOCK, 10)
                        .add(Blocks.CAMPFIRE, 5)
                        .build(),
                Lists.newArrayList(
                        new ObjectOrTagCodec<>(ForgeRegistries.BIOMES, BiomeTags.IS_NETHER),
                        new ObjectOrTagCodec<>(ForgeRegistries.BIOMES, Tags.Biomes.IS_HOT)
                )
        ));

        this.addUseItemTrait("gourmet", b ->
                b.add(Items.BAKED_POTATO, 10)
                        .add(Items.COOKED_BEEF, 10)
                        .add(Items.COOKED_CHICKEN, 10)
                        .add(Items.CAKE, 25)
                        .add(Items.COCOA_BEANS, 5),
                new LikeUseItemTrait.LikedData(
                        new ObjectOrTagCodec<>(ForgeRegistries.ITEMS, Items.COOKIE),
                        10, 1
                )
        );
        this.addTrait("likes_spoopy", new LikeBiomeTrait(TraitRegistry.LIKE_BIOME.get(),
                    new ItemBuilder()
                            .add(Tags.Items.BONES, 10)
                            .add(Items.SKELETON_SKULL, 50)
                            .add(Items.WITHER_SKELETON_SKULL, 120)
                            .add(Items.ROTTEN_FLESH, 5)
                            .add(Items.CANDLE, 10)
                            .build(),
                    Lists.newArrayList(
                            new ObjectOrTagCodec<>(ForgeRegistries.BIOMES, BiomeTags.IS_NETHER)
                    )
                ));
        this.addTrait("bibliophile", new EmptyTrait(TraitRegistry.EMPTY.get(),
            new ItemBuilder()
                    .add(ItemTags.LECTERN_BOOKS, 10)
                    .add(ItemTags.BOOKSHELF_BOOKS, 10)
                    .add(Items.INK_SAC, 5)
                    .add(Items.GLOW_INK_SAC, 10)
                    .add(Blocks.LECTERN, 40)
                    .add(Items.LANTERN, 30)
                    .build()
        ));
        this.addTrait("explorer", new LikeUseItemTrait(
                TraitRegistry.LIKE_USE_ITEM.get(),
                new ItemBuilder()
                        .add(Items.COMPASS, 10)
                        .add(Items.FILLED_MAP, 30)
                        .add(Items.SPYGLASS, 40)
                        .build(),
                Lists.newArrayList(
                    new LikeUseItemTrait.LikedData(
                            new ObjectOrTagCodec<>(ForgeRegistries.ITEMS, Items.SPYGLASS),
                            5, 1
                    ), new LikeUseItemTrait.LikedData(
                            new ObjectOrTagCodec<>(ForgeRegistries.ITEMS, Items.FILLED_MAP),
                                5, 1
                        )
                )
        ));
        this.addTrait("like_cold", new LikeBiomeTrait(
                TraitRegistry.LIKE_BIOME.get(),
                new ItemBuilder()
                        .add(Blocks.ICE, 10)
                        .add(Blocks.BLUE_ICE, 20)
                        .add(Blocks.PACKED_ICE, 20)
                        .add(Items.SNOWBALL, 10)
                        .build(),
                Lists.newArrayList(
                        new ObjectOrTagCodec<>(ForgeRegistries.BIOMES, Tags.Biomes.IS_COLD)
                )
        ));
    }

    public void addTrait(String name, Trait trait){
        this.traits.put(new ResourceLocation(this.modid, name), trait);
    }

    public void addUseItemTrait(String name, Consumer<ItemBuilder> unlockItems, LikeUseItemTrait.LikedData... items){
        final ItemBuilder builder = new ItemBuilder();
        unlockItems.accept(builder);
        this.addTrait(name, new LikeUseItemTrait(TraitRegistry.LIKE_USE_ITEM.get(), builder.build(), Lists.newArrayList(items)));
    }

    @Override
    public CompletableFuture<?> run(CachedOutput output) {
        this.addAllTraits();
        CompletableFuture<?>[] tasks = new CompletableFuture[this.traits.size()];

        int i = 0;
        for(Map.Entry<ResourceLocation, ? extends Trait> trait : this.traits.entrySet()){
            tasks[i] = DataProvider.saveStable(output,
                    ((Codec<Trait>)trait.getValue().getType()).encodeStart(JsonOps.INSTANCE, trait.getValue()).getOrThrow(false, Tardis.LOGGER::error),
                    getPath(trait.getKey().getPath()));
            ++i;
        }

        return CompletableFuture.allOf(tasks);
    }

    public Path getPath(String path){
        return this.generator.getPackOutput().getOutputFolder()
                .resolve("data")
                .resolve(modid)
                .resolve(TraitListener.PATH)
                .resolve(path + ".json");
    }

    @Override
    public String getName() {
        return "TARDIS Trait Data";
    }

    public static class ItemBuilder{
        final List<Pair<ObjectOrTagCodec<Item>, Integer>> items = new ArrayList<>();


        public ItemBuilder add(ItemLike item, int amount){
            this.items.add(new Pair<>(new ObjectOrTagCodec<>(ForgeRegistries.ITEMS, item.asItem()), amount));
            return this;
        }

        public ItemBuilder add(TagKey<Item> tag, int amount){
            this.items.add(new Pair<>(new ObjectOrTagCodec<>(ForgeRegistries.ITEMS, tag), amount));
            return this;
        }

        public List<Pair<ObjectOrTagCodec<Item>, Integer>> build(){
            return this.items;
        }

    }
}
