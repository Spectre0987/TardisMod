package net.tardis.mod.datagen;

import net.minecraft.core.HolderLookup;
import net.minecraft.data.PackOutput;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.StairBlock;
import net.minecraftforge.common.Tags;
import net.minecraftforge.common.data.BlockTagsProvider;
import net.minecraftforge.common.data.ExistingFileHelper;
import net.minecraftforge.registries.ForgeRegistries;
import net.tardis.mod.Tardis;
import net.tardis.mod.block.RoundelBlock;
import net.tardis.mod.registry.BlockRegistry;
import net.tardis.mod.block.BrokenExteriorBlock;
import net.tardis.mod.block.ConsoleBlock;
import net.tardis.mod.block.ExteriorBlock;
import net.tardis.mod.tags.BlockTags;
import org.jetbrains.annotations.Nullable;

import java.util.concurrent.CompletableFuture;
import java.util.function.Predicate;

public class TardisBlockTagsProvider extends BlockTagsProvider {

    public TardisBlockTagsProvider(PackOutput output, CompletableFuture<HolderLookup.Provider> lookupProvider, @Nullable ExistingFileHelper existingFileHelper) {
        super(output, lookupProvider, Tardis.MODID, existingFileHelper);
    }

    @Override
    protected void addTags(HolderLookup.Provider lookup) {
        this.tag(BlockTags.TEMPORAL_DISTORTION).addTags(BlockTags.BROKEN_EXTERIORS);

        this.tag(net.minecraft.tags.BlockTags.WALLS)
                .add(BlockRegistry.ALABASTER_WALL.get())
                .add(BlockRegistry.ALABASTER_PILLAR_WALL.get())
                .add(BlockRegistry.CORAL_WALL.get())
                .add(BlockRegistry.METAL_GRATE_WALL.get());

        this.addAllToTag(tag(BlockTags.BROKEN_EXTERIORS), b -> b instanceof BrokenExteriorBlock);
        this.addAllToTag(tag(BlockTags.CONSOLES), b -> b instanceof ConsoleBlock);

        this.tag(Tags.Blocks.GLASS)
                .add(BlockRegistry.MOON_BASE_GLASS.get());

        this.tag(BlockTags.UNBREAKABLES)
                .add(BlockRegistry.EXTERIOR_COLLISION.get())
                .add(BlockRegistry.CHAMELEON_AUXILLARY.get())
                .add(BlockRegistry.CHAMELEON_EXTERIOR.get());
        this.addAllToTag(tag(BlockTags.UNBREAKABLES), block -> block instanceof ExteriorBlock);

        this.addAllToTag(this.tag(net.minecraft.tags.BlockTags.MINEABLE_WITH_PICKAXE), s -> true);

        this.addAllToTag(this.tag(BlockTags.FULL_ROUNDEL),
                b -> {
                    final ResourceLocation key = ForgeRegistries.BLOCKS.getKey(b);
                    return b instanceof RoundelBlock && key.getNamespace().equals(Tardis.MODID) && key.getPath().endsWith("_full");
                }
        );
        this.addAllToTag(this.tag(net.minecraft.tags.BlockTags.STAIRS), block -> block instanceof StairBlock);

    }

    public void addAllToTag(IntrinsicTagAppender<Block> tag, Predicate<Block> test){
        final Predicate<Block> modidTest = b -> ForgeRegistries.BLOCKS.getKey(b).getNamespace().equals(this.modId);
        ForgeRegistries.BLOCKS.getValues().stream().filter(test).filter(modidTest).forEach(tag::add);
    }
}
