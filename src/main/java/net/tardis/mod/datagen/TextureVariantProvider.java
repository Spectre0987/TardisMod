package net.tardis.mod.datagen;

import com.mojang.datafixers.util.Pair;
import com.mojang.serialization.JsonOps;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.texture.AbstractTexture;
import net.minecraft.data.CachedOutput;
import net.minecraft.data.DataGenerator;
import net.minecraft.data.DataProvider;
import net.minecraft.resources.ResourceKey;
import net.minecraft.resources.ResourceLocation;
import net.tardis.mod.Tardis;
import net.tardis.mod.blockentities.TileRegistry;
import net.tardis.mod.misc.TextureVariant;
import net.tardis.mod.registry.JsonRegistries;

import java.nio.file.Path;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;

public class TextureVariantProvider implements DataProvider {

    public final List<Builder<?>> builders = new ArrayList<>();
    final String modid;
    final DataGenerator gen;

    public TextureVariantProvider(DataGenerator gen, String modid){
        this.gen = gen;
        this.modid = modid;
    }

    public TextureVariantProvider(DataGenerator gen){
        this(gen, Tardis.MODID);
    }

    @Override
    public CompletableFuture<?> run(CachedOutput cachedOutput) {
        this.addTextureVariants();
        final List<Pair<ResourceLocation, TextureVariant>> allVariants = new ArrayList<>();
        for(Builder<?> build : this.builders){
            for(Pair<ResourceLocation, TextureVariant> i : build.build()){
                allVariants.add(i);
            }
        }
        final CompletableFuture[] tasks = new CompletableFuture[allVariants.size()];

        int i = 0;
        for(Pair<ResourceLocation, TextureVariant> var : allVariants){
            tasks[i] = DataProvider.saveStable(cachedOutput,
                        TextureVariant.CODEC.encodeStart(JsonOps.INSTANCE, var.getSecond())
                                .getOrThrow(false, Tardis.LOGGER::warn),
                        getPath(var.getFirst())
                    );
            ++i;
        }

        return CompletableFuture.allOf(tasks);
    }

    public Path getPath(ResourceLocation id){
        return this.gen.getPackOutput().getOutputFolder()
                .resolve("data")
                .resolve(this.modid)
                .resolve(JsonRegistries.TEXTURE_VARIANTS.location().getNamespace())
                .resolve(JsonRegistries.TEXTURE_VARIANTS.location().getPath())
                .resolve(id.getPath() + ".json");
    }

    public void addTextureVariants(){
        this.variant(TileRegistry.TRUNK_EXTERIOR.getKey())
                .addTexture("trunk/alchemist", "textures/tiles/exteriors/trunk/trunk_exterior_alchemist.png")
                .addTexture("trunk/edgelord", "textures/tiles/exteriors/trunk/trunk_exterior_edgelord.png")
                .addTexture("trunk/tropical", "textures/tiles/exteriors/trunk/trunk_exterior_tropical.png");

        this.variant(TileRegistry.TT_CAPSULE.getKey())
                .addTexture("tt_capsule/basic", "textures/tiles/exteriors/tt_capsule/basic_shell.png")
                .addTexture("tt_capsule/rusty", "textures/tiles/exteriors/tt_capsule/rusty_shell.png")
                .addTexture("tt_capsule/stealth", "textures/tiles/exteriors/tt_capsule/stealth_shell.png");

        this.variant(TileRegistry.GALVANIC_CONSOLE.getKey())
                .addTexture("galvanic/classic", "textures/tiles/consoles/galvanic/galvanic_classic.png")
                .addTexture("galvanic/crimson", "textures/tiles/consoles/galvanic/galvanic_crimson.png")
                .addTexture("galvanic/rosewood", "textures/tiles/consoles/galvanic/galvanic_rosewood.png")
                .addTexture("galvanic/shadow", "textures/tiles/consoles/galvanic/galvanic_shadow.png");
        this.variant(TileRegistry.STEAM_CONSOLE.getKey())
                .addTexture("steam/ironclad", "textures/tiles/exteriors/steam/ironclad.png")
                .addTexture("steam/rosewood", "textures/tiles/exteriors/steam/rosewood.png")
                .addTexture("steam_brass", "textures/tiles/exteriors/steam/steam_brass.png");
        this.variant(TileRegistry.NEMO_CONSOLE.getKey())
                .addTexture("nemo/classic", "textures/tiles/consoles/nemo/nemo_console_classic.png")
                .addTexture("nemo/craftsman", "textures/tiles/consoles/nemo/nemo_console_craftsman.png")
                .addTexture("nemo/ivory", "textures/tiles/consoles/nemo/nemo_console_ivory.png")
                .addTexture("nemo/riverboat", "textures/tiles/consoles/nemo/nemo_console_riverboat.png")
                .addTexture("nemo/rouge", "textures/tiles/consoles/nemo/nemo_console_rouge.png");
        this.variant(TileRegistry.OCTA_EXTERIOR.getKey())
                .addTexture("octa/base", "textures/tiles/exteriors/octacapsule.png")
                .addTexture("octa/blue", "textures/tiles/exteriors/octa/octacapsule_blueish.png")
                .addTexture("octa/brassy", "textures/tiles/exteriors/octa/octacapsule_brassy.png")
                .addTexture("octa/mossy", "textures/tiles/exteriors/octa/octacapsule_mossy.png")
                .addTexture("octa/shadow", "textures/tiles/exteriors/octa/octacapsule_shadow.png")
                .addTexture("octa/shiny", "textures/tiles/exteriors/octa/octacapsule_shiny.png");
        this.variant(TileRegistry.COFFIN.getKey())
                .addTexture("coffin/dark", "textures/tiles/exteriors/coffin/coffin_darkoak.png")
                .addTexture("coffin/polished", "textures/tiles/exteriors/coffin/coffin_polished.png")
                .addTexture("coffin/sunbleached", "textures/tiles/exteriors/coffin/coffin_sunbleached.png")
                .addTexture("coffin/rustic", "textures/tiles/exteriors/coffin_rustic.png");
        this.variant(TileRegistry.G8_CONSOLE.getKey())
                .addTexture("g8/basic", "textures/tiles/consoles/g8.png")
                .addTexture("g8/master", "textures/tiles/consoles/g8/g8_masterful.png")
                .addTexture("g8/murphy", "textures/tiles/consoles/g8/g8_murphy.png")
                .addTexture("g8/reseda", "textures/tiles/consoles/g8/g8_reseda.png")
                .addTexture("g8/thornwood", "textures/tiles/consoles/g8/g8_thornwood.png");
    }

    public <T> Builder<T> variant(ResourceKey<T> key, String modid){
        final Builder<T> build = Builder.create(key, modid);
        this.builders.add(build);
        return build;
    }

    public <T> Builder<T> variant(ResourceKey<T> key){
        return variant(key, this.modid);
    }

    @Override
    public String getName() {
        return "TARDIS Texture Variants";
    }

    public static class Builder<T>{

        final ResourceKey<T> item;
        final String modid;
        final HashMap<ResourceLocation, ResourceLocation> textures = new HashMap<>();


        public Builder(ResourceKey<T> key, String modid){
            this.item = key;
            this.modid = modid;
        }

        public static <T> Builder<T> create(ResourceKey<T> itemToTexture, String modid){
            return new Builder<>(itemToTexture, modid);
        }

        public Builder<T> addTexture(String id, ResourceLocation texturePath){
            this.textures.put(new ResourceLocation(this.modid, id), texturePath);
            return this;
        }

        public Builder<T> addTexture(String id, String texturePath){
            return this.addTexture(id, new ResourceLocation(this.modid, texturePath));
        }

        public List<Pair<ResourceLocation, TextureVariant>> build(){
            final List<Pair<ResourceLocation, TextureVariant>> list = new ArrayList<>();
            for(Map.Entry<ResourceLocation, ResourceLocation> entry : textures.entrySet()){

                list.add(new Pair<>(entry.getKey(), new TextureVariant(this.item, entry.getValue())));
            }
            return list;
        }

    }
}
