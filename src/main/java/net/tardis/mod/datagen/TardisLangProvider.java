package net.tardis.mod.datagen;

import net.minecraft.core.Registry;
import net.minecraft.core.registries.Registries;
import net.minecraft.data.DataGenerator;
import net.minecraft.network.chat.Component;
import net.minecraft.network.chat.ComponentContents;
import net.minecraft.network.chat.contents.TranslatableContents;
import net.minecraft.resources.ResourceKey;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.sounds.SoundEvent;
import net.minecraft.world.item.CreativeModeTab;
import net.minecraft.world.item.Item;
import net.minecraftforge.common.data.LanguageProvider;
import net.minecraftforge.registries.ForgeRegistries;
import net.minecraftforge.registries.IForgeRegistry;
import net.minecraftforge.registries.RegistryObject;
import net.tardis.mod.Constants;
import net.tardis.mod.Tardis;
import net.tardis.mod.registry.BlockRegistry;
import net.tardis.mod.block.RoundelBlock;
import net.tardis.mod.blockentities.ARSPanelTile;
import net.tardis.mod.blockentities.machines.quantiscope_settings.QuantiscopeSetting;
import net.tardis.mod.cap.items.StattenhiemRemoteCapability;
import net.tardis.mod.cap.items.functions.ItemFunctionRegistry;
import net.tardis.mod.cap.items.functions.cfl.CFLDistortionDetector;
import net.tardis.mod.cap.items.functions.cfl.CFLInternalDiagnosticFunction;
import net.tardis.mod.cap.items.functions.cfl.CFLTrackTardis;
import net.tardis.mod.cap.items.functions.sonic.ARSSonicMode;
import net.tardis.mod.client.ClientRegistry;
import net.tardis.mod.client.gui.sonic.SonicNamingGui;
import net.tardis.mod.client.gui.containers.TardisEngineScreen;
import net.tardis.mod.client.gui.diagnostic_tool.DiagTardisInfoScreen;
import net.tardis.mod.client.gui.minigame.ChameleonGame;
import net.tardis.mod.client.gui.monitor.MonitorAtriumScreen;
import net.tardis.mod.client.gui.monitor.MonitorScreen;
import net.tardis.mod.client.gui.monitor.MonitorSelectExteriorScreen;
import net.tardis.mod.client.gui.monitor.MonitorWaypointSaveScreen;
import net.tardis.mod.client.monitor_world_text.*;
import net.tardis.mod.commands.AttuneItemCommand;
import net.tardis.mod.commands.EmotionalCommands;
import net.tardis.mod.compat.jei.*;
import net.tardis.mod.control.*;
import net.tardis.mod.creative_tabs.Tabs;
import net.tardis.mod.entity.EntityRegistry;
import net.tardis.mod.exterior.ExteriorType;
import net.tardis.mod.fluids.FluidRegistry;
import net.tardis.mod.helpers.Helper;
import net.tardis.mod.registry.ItemRegistry;
import net.tardis.mod.item.components.ArtronCapacitorItem;
import net.tardis.mod.misc.DoorHandler;
import net.tardis.mod.misc.tardis.TardisUnlockManager;
import net.tardis.mod.misc.tardis.montior.interior.IntSetMainDoorMonitorFunction;
import net.tardis.mod.network.packets.ARSSpawnItemMessage;
import net.tardis.mod.network.packets.LoadWaypointMessage;
import net.tardis.mod.network.packets.SaveWaypointMessage;
import net.tardis.mod.network.packets.TelepathicMessage;
import net.tardis.mod.registry.*;

import java.util.function.Supplier;

public class TardisLangProvider extends LanguageProvider {

    public TardisLangProvider(DataGenerator gen) {
        super(gen.getPackOutput(), Tardis.MODID, "en_us");
    }

    @Override
    protected void addTranslations() {
        this.add(Tabs.MAIN, "TARDIS Main Tab");
        this.add(Tabs.ROUNDELS, "TARDIS Mod Roundel Tab");

        //Items
        this.add(ItemRegistry.WELDING_TORCH.get(), "Welding Torch");
        this.add(ItemRegistry.CHRONOFAULT_LOCATOR.get(), "Chronofault Locator");
        this.add(ItemRegistry.PISTON_HAMMER.get(), "Piston Hammer");
        this.add(ItemRegistry.EXO_CIRCUITS.get(), "Exo-Circuits");
        this.add(ItemRegistry.CIRCUIT_PLATE.get(), "Blank Circuit Plate");
        this.add(ItemRegistry.CRYSTAL_VIALS.get(), "Crystal Vial");
        this.add(ItemRegistry.CRYSTAL_VIALS_MERCURY.get(), "Vial Of Mercury");
        this.add(ItemRegistry.XION_LENS.get(), "Xion Lens");
        this.add(ItemRegistry.ARTRON_CAP_LEAKY.get(), "Leaky Artron Capacitor");
        this.add(ItemRegistry.MERCURY_BOTTLE.get(), "Bottled Mercury");
        this.add(ItemRegistry.MANUAL.get(), "TARDIS Type 40 Manual");
        this.add(ItemRegistry.DEMAT_CIRCUIT.get(), "Dematerialization Circuit");
        this.add(ItemRegistry.CINNABAR.get(), "Cinnabar Chunk");
        this.add(ItemRegistry.GALLIFREYAN_KEY.get(), "TARDIS Key");
        this.add(ItemRegistry.PIRATE_KEY.get(), "TARDIS Key");
        this.add(ItemRegistry.FLUID_LINKS.get(), "Fluid Links");
        this.add(ItemRegistry.CHAMELEON_CIRCUIT.get(), "Chameleon Circuit");
        this.add(ItemRegistry.INTERSTITAL_ANTENNA.get(), "Interstital Antenna");
        this.add(ItemRegistry.NAV_COM.get(), "Nav-Com Circuit");
        this.add(ItemRegistry.SHIELD_GENERATOR.get(), "Shield Extrapolator Circuit");
        this.add(ItemRegistry.STABILIZER.get(), "Flight Stabilizer Circuit");
        this.add(ItemRegistry.TEMPORAL_GRACE.get(), "Temporal Grace Circuits");
        this.add(ItemRegistry.DATA_CRYSTAL.get(), "Data Crystal");
        this.add(ItemRegistry.ARTRON_CAP_BASIC.get(), "Artron Capacitor");
        this.add(ItemRegistry.ARTRON_CAP_MID.get(), "Medium Capacity Artron Capacitor");
        this.add(ItemRegistry.ARTRON_CAP_HIGH.get(), "High Capacity Artron Capacitor");
        this.add(ItemRegistry.TAPE_MEASURE.get(), "Tape Measure");
        this.add(ItemRegistry.TEA.get(), "Cup of Tea");
        this.add(ItemRegistry.SONIC.get(), "Sonic Screwdriver");
        this.add(ItemRegistry.UPGRADE_NANO_GENE.get(), "Nano-gene Carrier Upgrade Circuit");
        this.add(ItemRegistry.UPGRADE_LASER_MINER.get(), "TARDIS Laser Mining Upgrade Circuit");
        this.add(ItemRegistry.UPGRADE_BASE.get(), "Blank Upgrade Circuit");
        this.add(ItemRegistry.UPGRADE_ENERGY_SYPHON.get(), "Energy Syphon Upgrade Circuit");
        this.add(ItemRegistry.STATTENHEIM_REMOTE.get(), "Stattenheim Remote");
        this.add(ItemRegistry.UPGRADE_REMOTE.get(), "Remote Operations Upgrade Circuit");
        this.add(ItemRegistry.BURNT_EXOCIRCUITS.get(), "Burnt Exo-circuits");
        this.add(ItemRegistry.PERSONAL_SHIELD.get(), "Shield Generator Enhanced Personal Shield");
        this.add(ItemRegistry.UPGRADE_ATRIUM.get(), "TARDIS Atrium Upgrade");
        this.add(ItemRegistry.FIRST_ENTER_RECORD.get(), "The Abandoned Ship Music Disc");
        this.addMusicDiscDescr(ItemRegistry.FIRST_ENTER_RECORD.get(), "The Abandoned Ship");
        this.add(ItemRegistry.PLATE_COPPER.get(), "Copper Plate");
        this.add(ItemRegistry.UPGRADE_SONIC_DEMAT.get(), "Temporal Displacement Upgrade");


        //Tooltips
        this.add(Constants.Translation.makeTardisTranslation(ResourceKey.create(Registries.DIMENSION, Helper.createRL(""))), "Bound to %s");
        this.add(ArtronCapacitorItem.TOOLTIP, "Max Artron Capacity: %d");
        this.add(Tardis.MODID + ".tooltips.burnt_circuit", "This can be repaired with some sonic T.L.C.");
        this.add(Constants.Translation.ARTRON_STORED, "Stored Artron %d / %d");

        //Blocks
        this.add(BlockRegistry.DRAFTING_TABLE.get(), "Temporal Recipe Design System");
        this.add(BlockRegistry.STEAM_BROKEN_EXTERIOR.get(), "Derelict Timeship");
        this.add(BlockRegistry.G8_CONSOLE.get(), "G-8 Emergency Navigation Console");
        this.add(BlockRegistry.STEAM_CONSOLE.get(), "Steam-Powered Console");
        this.add(BlockRegistry.INTERIOR_DOOR.get(), "Chameleon-Linked Inner Door");
        this.add(BlockRegistry.STEAM_MONITOR.get(), "Steam-Powered Monitor");
        this.add(BlockRegistry.XION.get(), "Xion Crystal");
        this.add(BlockRegistry.ENGINE_STEAM.get(), "TARDIS Steam-powered Engine");
        this.add(BlockRegistry.XION_BLOCK.get(), "Xion Block");
        this.add(BlockRegistry.ATRIUM_PLATFORM_BLOCK.get(), "Atrium Platform");
        this.add(BlockRegistry.NEUVEAU_CONSOLE.get(), "Nouveau Console");
        this.add(BlockRegistry.ALEMBIC.get(), "Alembic");
        this.add(BlockRegistry.TT_CAPSULE.get(), "TT Capsule Exterior");
        this.add(BlockRegistry.TT_CAPSULE_BROKEN_EXTERIOR.get(), "Derelict Timeship");
        this.add(BlockRegistry.METAL_GRATE_SLAB.get(), "Metal Grate Slab");
        this.add(BlockRegistry.METAL_GRATE.get(), "Metal Grate");
        this.add(BlockRegistry.METAL_GRATE_SOLID.get(), "Metal Grate");
        this.add(BlockRegistry.FOAM_PIPE.get(), "Black Piping");
        this.add(BlockRegistry.WIRE_HANG.get(), "Hanging Wires");
        this.add(BlockRegistry.TUNGSTEN.get(), "Tungsten Block");
        this.add(BlockRegistry.TUNGSTEN_PLATE.get(), "Tungsten Plate");
        this.add(BlockRegistry.TUNGSTEN_SCREEN.get(), "Tungsten Screen");
        this.add(BlockRegistry.TUNGSTEN_RUNNING_LIGHTS.get(), "Tungsten Running Lights");
        this.add(BlockRegistry.TUNGSTEN_PATTERN.get(), "Tungsten Pattern");
        this.add(BlockRegistry.TUNGSTEN_PLATE_SLAB.get(), "Tungsten Plate Slab");
        this.add(BlockRegistry.TUNGSTEN_PLATE_SMALL.get(), "Tungsten Small Plates");
        this.add(BlockRegistry.TUNGSTEN_PIPES.get(), "Tungsten Piping");
        this.add(BlockRegistry.TUNGSTEN_STAIRS_SMALL_PLATES.get(), "Tungsten Small Plate Stairs");
        this.add(BlockRegistry.BIOMASS_FLUID.get(), "Biomass");
        this.add(BlockRegistry.DATA_MATRIX.get(), "Data Matrix");
        this.add(BlockRegistry.ALABASTER.get(), "Alabaster");
        this.add(BlockRegistry.ALABASTER_TILES_LARGE.get(), "Alabaster Large Tiles");
        this.add(BlockRegistry.ALABASTER_TILES_SMALL.get(), "Alabaster Small Tiles");
        this.add(BlockRegistry.ALABASTER_PILLAR.get(), "Alabaster Pillars");
        this.add(BlockRegistry.ARS_PANEL.get(), "ARS Panel");
        this.add(BlockRegistry.GALVANIC_CONSOLE.get(), "Galvanic Console");
        this.add(BlockRegistry.TECH_STRUT.get(), "Industrial Struts");
        this.add(BlockRegistry.TUNGSTEN_STAIRS.get(), "Tungsten Stairs");
        this.add(BlockRegistry.RCA_MONITOR.get(), "RCA Monitor");
        this.add(BlockRegistry.EYE_MONITOR.get(), "Eye Monitor");
        this.add(BlockRegistry.ALABASTER_STAIRS.get(), "Alabaster Stairs");
        this.add(BlockRegistry.BLINKING_LIGHTS.get(), "Blinking Light Bank");
        this.add(BlockRegistry.HOLO_LADDER.get(), "Holo-Projected Ladder");
        this.add(BlockRegistry.STEAM_GRATE.get(), "Steam Grate");
        this.add(BlockRegistry.ALABASTER_WALL.get(), "Alabaster Wall");
        this.add(BlockRegistry.ALABASTER_SLAB.get(), "Alabaster Slab");
        this.add(BlockRegistry.METAL_GRATE_TRAPDOOR.get(), "Metal Grate Trapdoor");
        this.add(BlockRegistry.COMFY_CHAIR_RED.get(), "Red Comfy Chair");
        this.add(BlockRegistry.COMFY_CHAIR_GREEN.get(), "Green Comfy Chair");
        this.add(BlockRegistry.ALABASTER_PILLAR_WALL.get(), "Alabaster Pillar Wall");
        this.add(BlockRegistry.ALABASTER_BOOKSHELF.get(), "Alabaster Bookshelf");
        this.add(BlockRegistry.ALABASTER_SCREEN.get(), "Alabaster Screen");
        this.add(BlockRegistry.ALABASTER_TILES_BIG.get(), "Alabaster Big Tiles");
        this.add(BlockRegistry.ALABASTER_RUNNER_LIGHTS.get(), "Alabaster Blue Runner Lights");
        this.add(BlockRegistry.ALABASTER_PIPES.get(), "Alabaster Pipes");
        this.add(BlockRegistry.STEEL_HEX_OFFSET.get(), "Steel Hexagon Offset");
        this.add(BlockRegistry.STEEL_HEX.get(), "Steel Hexagon");
        this.add(BlockRegistry.EBONY_BOOKSHELF.get(), "Ebony Bookshelf");
        this.add(BlockRegistry.MOON_BASE_GLASS.get(), "Moon Base Glass");
        this.add(BlockRegistry.ARS_EGG.get(), "ARS Egg");
        this.add(BlockRegistry.ENGINE_ROOF.get(), "Roof-Mounted TARDIS Engine");
        this.add(BlockRegistry.POLICE_BOX_EXTERIOR.get(), "Police Box Exterior");
        this.add(BlockRegistry.DEDICATION_PLAQUE.get(), "TARDIS Dedication Plaque");
        this.add(BlockRegistry.QUANTISCOPE.get(), "Multi-Quantiscope");
        this.add(BlockRegistry.RIFT_PYLON.get(), "Rift Collection Pylon");
        this.add(BlockRegistry.RIFT_COLLECTOR.get(),"Rift Collection Controller");
        this.add(BlockRegistry.RIFT_COLLECTOR.get().getSonicTranslation(true), "Rift Safeties Re-Engaged");
        this.add(BlockRegistry.RIFT_COLLECTOR.get().getSonicTranslation(false), "WARNING: Rift Safeties Disengaged!");
        this.add(BlockRegistry.MATTER_BUFFER.get(), "Real-Matter Processor");
        this.add(BlockRegistry.SCOOP_VAULT.get(), "Temporal Scoop Vault");
        this.add(BlockRegistry.NEMO_CONSOLE.get(), "Nemo Console");
        this.add(BlockRegistry.MULTIBLOCK.get(), "Multiblock");
        this.add(BlockRegistry.WAYPOINT_BANKS.get(), "Waypoint Banks");
        this.add(BlockRegistry.DISGUISED_BLOCK.get(), "Disguised Block (Sssh...)");
        this.add(BlockRegistry.ATRIUM_MASTER.get(), "Atrium Exterior Marker");
        this.add(BlockRegistry.CHAMELEON_EXTERIOR.get(), "I Dunno, but it's not a TARDIS");
        this.add(BlockRegistry.CHAMELEON_AUXILLARY.get(), "I Dunno, but it's not a TARDIS");
        this.add(BlockRegistry.ITEM_PLAQUE.get(), "Trophy Plaque");
        this.add(BlockRegistry.TUNGSTEN_GRATE.get(), "Tungsten Grate");
        this.add(BlockRegistry.ALABASTER_GRATE.get(), "Alabaster Grate");
        this.add(BlockRegistry.BIG_DOOR.get(), "Sliding Door");
        this.add(BlockRegistry.ROUNDEL_TAP.get(), "Roundel Tap");
        this.add(BlockRegistry.LANDING_PAD.get(), "TARDIS Landing Pad");
        this.add(BlockRegistry.TUNGSTEN_SMALL_PLATE_SLAB.get(), "Tungsten Small Plate Slab");
        this.add(BlockRegistry.VARIABLE_MONITOR.get(), "Dynamically-Sized Monitor");
        this.add(BlockRegistry.CORAL_BLANK.get(), "TARDIS Coral Block");
        this.add(BlockRegistry.CORAL_SLAB.get(), "TARDIS Coral Slab");
        this.add(BlockRegistry.CORAL_STAIRS.get(), "TARDIS Coral Stairs");
        this.add(BlockRegistry.ALEMBIC_STILL.get(), "Industrial Still");
        this.add(BlockRegistry.MULTIBLOCK_REDIR_FLUID.get(), "Multiblock");
        this.add(BlockRegistry.CORAL_WALL.get(), "TARDIS Coral Wall");
        this.add(BlockRegistry.METAL_GRATE_WALL.get(), "Metal Grate Wall");
        this.add(BlockRegistry.FABRICATOR_MACHINE.get(), "Industrial Fabricator");
        this.add(BlockRegistry.TELEPORT_TUBE.get(), "Hypertube");
        this.add(BlockRegistry.COFFIN_EXTERIOR.get(), "A Very Normal Coffin");
        this.add(BlockRegistry.OCTA_EXTERIOR.get(), "Octa Exterior");
        this.add(BlockRegistry.TRUNK_EXTERIOR.get(), "Steamer Trunk");
        this.add(BlockRegistry.SPRUCE_EXTERIOR.get(), "Spruce Door");
        this.add(BlockRegistry.RAILING_STEAM.get(), "Brass Railings");
        this.add(BlockRegistry.RAILING_ALABASTER.get(), "Alabaster Railings");
        this.add(BlockRegistry.RAILING_TUNGSTEN.get(), "Tungsten Railings");

        this.add(RoundelBlock.TRANSLATION_KEY, "TARDIS Roundel");

        //Entities
        this.add(EntityRegistry.CONTROL.get(), "TARDIS Control");
        this.add(EntityRegistry.CHAIR.get(), "Chair");

        //Guis
        this.add(ChameleonGame.TITLE.getContents(), "Chameleon Circuit");
        this.add("jei." + Tardis.MODID + ".category.drafting", "Temporal Recipe Design System Table");
        this.add(Jei.DRAFTING_TABLE_INFO.getContents(), "Created by right-clicking a smithing table with a xion crystal in the world");
        this.add(TardisEngineScreen.SUBSYSTEM_FILTER_TRANS, "Valid Component Type: %s");
        this.add(SonicNamingGui.SAVE_TRANS, "Set Name");
        this.add(SonicNamingGui.TITLE, "Configure Name");

        //Monitor

        //Monitor Entries (World-text)
        this.add(LocationalMonitorEntry.LOCATION_KEY, "Location: %s, %s, %s");
        this.add(LocationalMonitorEntry.LOCATION_DIM_KEY, "Dimension: %s");
        this.add(LocationalMonitorEntry.DEST_KEY, "Destination: %s, %s, %s");
        this.add(LocationalMonitorEntry.DEST_DIM_KEY, "Dest Dim: %s");
        this.add(FuelMonitorEntry.TRANS_KEY, "Artron: %dAu");
        this.add(FlightEventMonitorEntry.TRANSLATION, "WARNING: %s!");
        this.add(LocationalMonitorEntry.NAV_COM_MISSING, "Unable to get a Space-time fix!");
        this.add(FlightCourseProgressMonitorEntry.TRANS, "Course Progress: %d%%");
        this.add(ExteriorExtraMonitorEntry.IS_IN_RIFT, "Rift Detected!");
        this.add(FirstEnterTARDISMonitorEntry.TRANS, "WARNING! Corruption Detected in TARDIS Banks! Please perform a calibration flight!");

        //Monitor functions
        this.add(MonitorFunctionRegistry.SCAN_FOR_LIFE.get().makeDefault(), "Detect Internal Lifesigns");
        this.add(MonitorFunctionRegistry.SCAN_FOR_LIFE.get().makeDefaultFeedback(), "Found %s humanoids, and %s others!");
        this.add(MonitorFunctionRegistry.CHANGE_EXTERIOR.get().makeDefault(), "Change Exterior");
        this.add(MonitorFunctionRegistry.SET_MAIN_DOOR.get().makeDefault(), "Set Main Door");
        this.add(IntSetMainDoorMonitorFunction.SUCCESS.get(), "Set interior door at %s, %s, %s door as main!");
        this.add(IntSetMainDoorMonitorFunction.FAIL.get(), "WARNING: Could not locate door!");
        this.add(MonitorFunctionRegistry.DEMAT_ANIMATION.get().makeDefault(), "Change Dematerialization Type");
        this.add(MonitorFunctionRegistry.FLIGHT_COURSE.get().makeDefault(), "Flight Course");
        this.add(MonitorFunctionRegistry.TARDIS_MANUAL.get().makeDefault(), "TARDIS Index");
        this.add(MonitorFunctionRegistry.SET_CAN_LAND_WATER.get().getConditionalText(true), "Can Land Underwater: True");
        this.add(MonitorFunctionRegistry.SET_CAN_LAND_WATER.get().getConditionalText(false), "Can Land Underwater: False");
        this.add(MonitorFunctionRegistry.SET_LAND_AIR.get().getConditionalText(true), "Can Land Mid-Air: True");
        this.add(MonitorFunctionRegistry.SET_LAND_AIR.get().getConditionalText(false), "Can Land Mid-Air: False");
        this.add(MonitorFunctionRegistry.SOUND_SCHEME.get().makeDefault(), "Change Flight Sound Scheme");
        this.add(MonitorFunctionRegistry.WAYPOINTS.get().makeDefault(), "Waypoint Banks");
        this.add(MonitorFunctionRegistry.ATRIUM.get().makeDefault(), "Select Active Atrium");
        this.add(MonitorAtriumScreen.SELECT_TRANS, "Select an Active Atrium");
        this.add(MonitorScreen.NEXT_BUTTON, "Next");
        this.add(MonitorScreen.PREV_BUTTON, "Previous");
        //Monitor folders
        this.add(MonitorScreen.createFolderTrans("security"), "Security Settings");
        this.add(MonitorScreen.createFolderTrans("interior"), "Interior Settings");
        this.add(MonitorScreen.createFolderTrans("exterior"), "Exterior Settings");
        this.add(MonitorScreen.createFolderTrans("upgrades"), "Upgrade Configuration");

        new JsonRegistryBuilder(this, JsonRegistries.SOUND_SCHEMES_REGISTRY)
                .add(Helper.createRL("default"), "Basic TT Capsule")
                .add(Helper.createRL("master"), "Sinister TT Capsule")
                .add(Helper.createRL("tv"), "TV TT Capsule")
                .add(Helper.createRL("junk"), "Damaged TT Capsule");

        //Control Feedback
        this.add(DimensionControl.TRANS, "Dimension Target: %s");
        this.add(IncrementControl.MESSAGE_KEY, "Increment Modifier %d");
        this.add(AxisControl.NOTIF_TRANS, "TARDIS Destination %s, %s, %s");
        this.add(RefuelerControl.NO_CAP, "No Artron Capacitors detected!");
        this.add(TelepathicMessage.FAIL, "WARNING: Could not find biome in destination dimension!");

        //Controls
        this.addControl(ControlRegistry.THROTTLE, "Space-Time Throttle");
        this.addControl(ControlRegistry.REFUELER, "Refuling Switch");
        this.addControl(ControlRegistry.LANDING_TYPE, "Landing Type Selector");
        this.addControl(ControlRegistry.HANDBRAKE, "Handbrake");
        this.addControl(ControlRegistry.INCREMENT, "Coordinate Increment Modifier");
        this.addControl(ControlRegistry.RANDOMIZER, "Space-Time Coorinate Randomizer");
        this.addControl(ControlRegistry.COMMUNICATOR, "Communications Panel");
        this.addControl(ControlRegistry.FACING, "Cardinal Direction Selector");
        this.addControl(ControlRegistry.FAST_RETURN, "Fast Return Switch");
        this.addControl(ControlRegistry.TELEPATHICS, "Telepathic Link");
        this.addControl(ControlRegistry.DIMENSIONS, "Dimensional Selector");
        this.addControl(ControlRegistry.STABILIZERS, "Stabilizers");
        this.addControl(ControlRegistry.X, "X Selector");
        this.addControl(ControlRegistry.Y, "Y Selector");
        this.addControl(ControlRegistry.Z, "Z Selector");
        this.addControl(ControlRegistry.SONIC_PORT, "Sonic Port");
        this.addControl(ControlRegistry.MONITOR, "TARDIS Monitor");

        this.add(RefuelerControl.NO_RIFT, "No rift energy detected!");

        this.add(DoorHandler.LOCKED_TRANS_KEY, "Door is locked!");
        this.add(DoorHandler.UNLOCKED_TRANS_KEY, "Door is unlocked!");

        //Traits
        //this.add(TraitRegistry.LIKE_HOT.get().getHintTranslation(), "The Ship sends you dreams of fire and warm beaches, you awake feeling content");

        //Subsystems
        this.add(SubsystemRegistry.FLIGHT_TYPE.get().getTranslationKey(), "Dematerialization Circuit");
        this.add(SubsystemRegistry.FLUID_LINK_TYPE.get().getTranslationKey(), "Fluid Links");
        this.add(SubsystemRegistry.NAV_COM.get().getTranslationKey(), "Nav-Com Unit");
        this.add(SubsystemRegistry.CHAMELEON_TYPE.get().getTranslationKey(), "Chameleon Circuit");
        this.add(SubsystemRegistry.SHIELD.get().getTranslationKey(), "Isomorphic Shield Generators");
        this.add(SubsystemRegistry.TEMPORAL_GRACE.get().getTranslationKey(), "Temporal Grace Circuits");
        this.add(SubsystemRegistry.ANTENNA.get().getTranslationKey(), "Interstital Antenna");
        this.add(SubsystemRegistry.STABILIZERS.get().getTranslationKey(), "Stabilizing Circuit");

        //Flight Events
        this.add(FlightEventRegistry.ADD_ARTRON.get().makeTrans(), "Artron Pocket Detected!");
        this.add(FlightEventRegistry.TIME_WINDS.get().makeTrans(), "Excessive Flight Winds!");
        this.add(FlightEventRegistry.X_SHIFT.get().makeTrans(), "Course correction! X Axis!");
        this.add(FlightEventRegistry.Y_SHIFT.get().makeTrans(), "Course correction! Y Axis!");
        this.add(FlightEventRegistry.Z_SHIFT.get().makeTrans(), "Course correction! Z Axis!");
        this.add(FlightEventRegistry.SPACE_EVENT_DODGE.get().makeTrans(), "Dodge Incoming Fire!");
        this.add(FlightEventRegistry.SPACE_EVENT_SEARCH.get().makeTrans(), "Unknown data detected, initialize scan?");
        this.add(FlightEventRegistry.SPACE_EVENT_ESCAPE.get().makeTrans(), "Pursing vehicle Detected!");
        this.add("notif.tardis.cant_land", "Failed to find valid landing position!");


        this.add(Control.MISSING_REQUIRED_SYSTEM, "Missing Required Subsystem for this Control!");

        //Exterior Types
        Builder.create(this, ExteriorRegistry.REGISTRY.get())
                .add(ExteriorRegistry.POLICE_BOX.get(), "Strange Police Box")
                .add(ExteriorRegistry.STEAM.get(), "Steam-powered Capsule")
                .add(ExteriorRegistry.TT_CAPSULE.get(), "T.T. Capsule")
                .add(ExteriorRegistry.IMPALA.get(), "Impala")
                .add(ExteriorRegistry.CHAMELEON.get(), "Phased Optic Shell")
                .add(ExteriorRegistry.COFFIN.get(), "Coffin")
                .add(ExteriorRegistry.OCTA.get(), "Octa")
                .add(ExteriorRegistry.TRUNK.get(), "Trunk")
                .add(ExteriorRegistry.SPRUCE.get(), "Spruce Door");

        //Item Functions
        this.add(CFLInternalDiagnosticFunction.name, "Internal Timeship Diagnostics");
        this.add(CFLDistortionDetector.NAME, "Temporal Distortion Detector");
        this.add(CFLDistortionDetector.FEEDBACK, "Located Temporal Distortion!");
        this.add(CFLDistortionDetector.TRACKING_TITLE, "Tracking Temporal Distortion");
        this.add(CFLTrackTardis.TRACKING_TITLE, "Tracking TARDIS");
        this.add(ItemFunctionRegistry.CFL_TRACK_TARDIS.get().getDefaultTranslation(), "Track TARDIS Location");
        this.add(ItemFunctionRegistry.CFL_SUBSYSTEM_INFO.get().getDefaultTranslation(), "Subsytem Info");
        this.add(ARSSonicMode.NOT_IN_ROOM, "WARNING: No ARS Room Detected");
        this.add(ARSSonicMode.NEEDS_REPAIR_TRANS, "WARNING: Must Repair ARS Room first!");

        //Advancements
        this.add(TardisAdvancementProvider.translate(TardisAdvancementProvider.GATHER_XION, true), "Beyond the Stars");
        this.add(TardisAdvancementProvider.translate(TardisAdvancementProvider.GATHER_XION, false), "This crystal seems to vibrate with a strange energy");
        this.add(TardisAdvancementProvider.translate(TardisAdvancementProvider.CRYSTAL_DREAM, true), "Crystalline Dreams");
        this.add(TardisAdvancementProvider.translate(TardisAdvancementProvider.CRYSTAL_DREAM, false), "You dream of the strange crystal you found. Lost in its cascading facets, until you glimpse infinity, When you wake your mind is flooded with ideas and the sense that something is calling out to you");
        this.add(TardisAdvancementProvider.translate(TardisAdvancementProvider.FIRST_TAKEOFF, true), "Temporal Flight");
        this.add(TardisAdvancementProvider.translate(TardisAdvancementProvider.FIRST_TAKEOFF, false), "Fly the TARDIS for the first time");
        this.add(TardisAdvancementProvider.translate(Helper.createRL("make_tea"), true), "It keeps things civilized...");
        this.add(TardisAdvancementProvider.translate(Helper.createRL("make_tea"), false), "Make tea");

        //guis
        this.add(DiagTardisInfoScreen.IN_FLIGHT_TRANS, "Timeship in Vortex: %s");
        this.add(MonitorWaypointSaveScreen.SAVE, "Save");
        this.add(MonitorWaypointSaveScreen.EXIT, "Exit");
        this.add(LoadWaypointMessage.SUCCESS_TRANS, "Waypoint Coordinates Loaded!");
        this.add(SaveWaypointMessage.FAILED, "WARNING: No Room Detected in Waypoint Banks!");
        this.add(SaveWaypointMessage.SUCCESS, "Saved Coordinates to Databank!");
        this.add(MonitorSelectExteriorScreen.SELECT_TRANS, "Select Exterior");

        Builder.create(this, DematAnimationRegistry.REGISTRY.get())
                .add(DematAnimationRegistry.CLASSIC.get(), "Fade Out")
                .add(DematAnimationRegistry.NEW_WHO.get(), "Pulse")
                .add(DematAnimationRegistry.PARTICLE_FALL.get(), "Wyld Stallyns");

        //Fluid Types
        this.add(FluidRegistry.MERCURY_TYPE.get().getDescription(), "Mercury");
        this.add(FluidRegistry.BIOMASS_TYPE.get().getDescription(), "Biomass");

        //Compat
        this.add(AlembicJeiCategory.TITLE, "Alembic Brewing");
        this.add(AlembicBottlingRecipeCategory.TITLE, "Alembic Bottling");
        this.add(CraftingQuantiscopeJEIRecipeCategory.TITLE, "Quantiscope Crafting");
        this.add(ARSEggRecipeCategory.TITLE, "ARS Egg");
        this.add(Jei.makeInfoTrans("cinnabar"), "Random drop from mining redstone");

        //Misc
        this.add("dedication_plaque." + Tardis.MODID + ".line_1", "TT Capsule 40 Mark III");
        this.add("dedication_plaque." + Tardis.MODID + ".line_2", "Blackhole shipyards of Kasterborous");
        this.add(ARSSpawnItemMessage.LOW_BUFFER, "Not enough matter in buffer! %d available");
        this.add(TardisUnlockManager.TRANS_KEY, "Research Complete! %s");
        this.add(TardisUnlockManager.HAS_UNLOCK_KEY, "Already Researched %s!");
        this.add(QuantiscopeSetting.createTitle(Helper.createRL("craft")), "Micro-Welding");
        this.add(QuantiscopeSetting.createTitle(Helper.createRL("sonic")), "Sonic Casing");
        this.add(QuantiscopeSetting.createTitle(Helper.createRL("sonic_upgrade")), "Sonic Upgrade");
        this.add(Constants.Translation.MUST_BE_USED_IN_TARDIS, "Error: Must be used inside TARDIS");
        this.add(StattenhiemRemoteCapability.ERROR_NO_REMOTE_CIRCUIT, "Error: Timeship must have a Remote Control Circuit!");
        this.add(StattenhiemRemoteCapability.ERROR_NO_STABILIZERS, "Error: Timeship must have a working Stabilization System!");
        this.add(StattenhiemRemoteCapability.ERROR_NO_POWER, "Not Enough Power stored!");
        this.add(AttuneItemCommand.ATTUNED_ITEM_TRANS, "Attuned item %s");
        this.add(EmotionalCommands.LOYALTY_SET_TRANS, "Set %s loyalty to %d");
        this.add(Constants.Translation.POWER_STORED, "FE: %d / %d");
        this.add(ARSPanelTile.NEEDS_WELD_TRANS, "INFO: Repair Requires Welding");
        this.add(ClientRegistry.KEY_BINDS_CATEGORY, "TARDIS Mod Keybinds");
        this.add(ClientRegistry.MANUAL_KEY.getTranslatedKeyMessage(), "Manual Info Key");
        this.add(Constants.Translation.NOT_ENOUGH_POWER, "Not enough power in tool, requires %d");
        this.add(Constants.Translation.MUST_BE_ATTUNED, "Item requires attunement!");

        //Vortex Phenomenas
        Builder.create(this, VortexPhenomenaRegistry.REGISTRY)
                .add(VortexPhenomenaRegistry.SPACE_BATTLE.get(), "Massive Space Battle")
                .add(VortexPhenomenaRegistry.WORMHOLE.get(), "Wormhole")
                .add(VortexPhenomenaRegistry.ANCIENT_DEBRIS.get(), "Ancient Debris")
                .add(VortexPhenomenaRegistry.ASTROID.get(), "Asteroid Field");

        /*
        Builder.create(this, TraitRegistry.REGISTRY)
                .add(TraitRegistry.LIKE_HOT.get(), "Enjoys Heat")
                .add(TraitRegistry.LIKES_CATS.get(), "Cat Lover")
                .add(TraitRegistry.LIKES_COLD.get(), "Enjoys Cold")
                .add(TraitRegistry.LIKES_DOGS.get(), "Dog Lover");

         */

        this.add(TardisManualProvider.MAIN_CHAPTER.makeTranslation(), "The TARDIS");
        this.add(TardisManualProvider.COMPONENTS.makeTranslation(), "Subsystems");
        this.add(TardisManualProvider.CFL.makeTranslation(), "Chronofault Locator");
        this.add(TardisManualProvider.CFL_FUNCTIONS_CHAPTER.makeTranslation(), "Functions");
        this.add(TardisManualProvider.TOOLS_CHAPTER.makeTranslation(), "Useful Gadgets");
        this.add(TardisManualProvider.LANDING.makeTranslation(), "Exiting Flight");
        this.add(TardisManualProvider.REFULING.makeTranslation(), "Fueling the TARDIS");
        this.add(TardisManualProvider.SONIC.makeTranslation(), "Sonic Screwdriver");
        this.add(TardisManualProvider.SONIC_FUNCTIONS.makeTranslation(), "Functions");
        this.add(TardisManualProvider.GENERAL_FLIGHT.makeTranslation(), "Vortex Flight");
        this.add(TardisManualProvider.TAKEOFF.makeTranslation(), "Entering the Vortex");
        this.add(TardisManualProvider.VP_CHAPTER.makeTranslation(), "Vortex Phenomena");
        this.add(TardisManualProvider.FLIGHT.makeTranslation(), "Flying the Ship");
        this.add(TardisManualProvider.UPGRADES.makeTranslation(), "Retrofits");
        this.add(TardisManualProvider.FLIGHT_EVENTS.makeTranslation(), "Events In Flight");
        this.add(TardisManualProvider.MACHINES.makeTranslation(), "Machines");
        this.add(TardisManualProvider.BROKEN_EXTERIORS.makeTranslation(), "Derelict Time Ships");

        //this.printControlRegistry();

    }

    //Enabled to post the resource locations of all controls in a way that works for the control plugin
    public void printControlRegistry(){
        for(ResourceLocation loc : ControlRegistry.REGISTRY.get().getKeys()){
            System.out.println("\"%s\" : \"%s\"".formatted(loc, ""));
        }
    }

    public <T> void addDefault(IForgeRegistry<T> reg, T val, String trans){
        this.add(Constants.Translation.makeGenericTranslation(reg, val), trans);
    }

    public void addMusicDiscDescr(Item item, String trans){
        final ResourceLocation id = ForgeRegistries.ITEMS.getKey(item);
        add("item." + id.getNamespace() + "." + id.getPath().replace('/', '.') + ".desc", trans);
    }

    public void add(Component component, String trans){
        if(component.getContents() instanceof TranslatableContents t){
            add(t.getKey(), trans);
        }
        else add(component.getString(), trans);
    }

    public void add(CreativeModeTab tab, String name){
        this.add(tab.getDisplayName().getContents(), name);
    }

    public void add(ComponentContents contents, String name){
        if(contents instanceof TranslatableContents){
            add(((TranslatableContents)contents).getKey(), name);
        }
        else add(contents.toString(), name);
    }

    public void addControl(RegistryObject<? extends ControlType<?>> control, String trans){
        add(control.get().getTranslationKey(), trans);
    }

    public void add(ExteriorType type, String s){
        add(type.getTranslation(), s);
    }

    public void add(SoundEvent event, String trans){
        this.add(Constants.Translation.makeGenericTranslation(ForgeRegistries.SOUND_EVENTS, event), trans);
    }

    public static class Builder<T>{

        final IForgeRegistry<T> reg;
        final TardisLangProvider provider;

        private Builder(TardisLangProvider provider, IForgeRegistry<T> reg){
            this.provider = provider;
            this.reg = reg;
        }

        public static <V> Builder<V> create(TardisLangProvider provider, IForgeRegistry<V> reg){
            return new Builder<>(provider, reg);
        }

        public static <V> Builder<V> create(TardisLangProvider provider, Supplier<IForgeRegistry<V>> reg){
            return create(provider, reg.get());
        }

        public Builder<T> add(T val, String trans){
            provider.add(Constants.Translation.makeGenericTranslation(this.reg, val), trans);
            return this;
        }

    }

    public static class JsonRegistryBuilder{

        final ResourceKey<? extends Registry<?>> reg;
        final TardisLangProvider provider;

        public JsonRegistryBuilder(TardisLangProvider provider, ResourceKey<? extends Registry<?>> loc){
            this.provider = provider;
            this.reg = loc;
        }

        public JsonRegistryBuilder add(ResourceLocation loc, String key){
            provider.add(Constants.Translation.makeGenericTranslation(this.reg, loc), key);
            return this;
        }

    }

    public void checkRegistry(Registry<?> reg, String modid){

    }
}
