package net.tardis.mod.world.placements;

import com.mojang.serialization.Codec;
import com.mojang.serialization.codecs.RecordCodecBuilder;
import net.minecraft.world.level.levelgen.blockpredicates.BlockPredicate;
import net.minecraft.world.level.levelgen.placement.PlacementModifier;
import net.minecraft.world.level.levelgen.placement.PlacementModifierType;

public class OnBlockPlacementModiferType<T extends PlacementModifier> implements PlacementModifierType<T> {

    public static final Codec<OnBlockPlacement> CODEC = RecordCodecBuilder.create(instance -> {
        return instance.group(
                Codec.INT.fieldOf("max_range").forGetter(place -> place.searchRange)
        ).apply(instance, OnBlockPlacement::new);
    });



    @Override
    public Codec codec() {
        return CODEC;
    }
}
