package net.tardis.mod.block;

import net.minecraft.core.BlockPos;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.block.state.StateDefinition;
import net.minecraft.world.level.block.state.properties.BooleanProperty;
import net.tardis.mod.cap.Capabilities;
import net.tardis.mod.registry.BlockRegistry;
import net.tardis.mod.registry.TardisWorldStructureRegistry;

public class WaypointBlock extends SimpleHorizonalBlock {

    public static final BooleanProperty IS_TOP = BooleanProperty.create("top");

    public WaypointBlock(Properties pProperties) {
        super(pProperties);
        this.registerDefaultState(this.defaultBlockState().setValue(IS_TOP, false));
    }

    public static WaypointBlock create(){
        return new WaypointBlock(
                BasicProps.Block.METAL.get().noOcclusion()
        );
    }

    @Override
    public void onPlace(BlockState pState, Level pLevel, BlockPos pPos, BlockState pOldState, boolean pMovedByPiston) {
        super.onPlace(pState, pLevel, pPos, pOldState, pMovedByPiston);
        if(!pState.getValue(IS_TOP)){
            Capabilities.getCap(Capabilities.TARDIS, pLevel).ifPresent(tardis -> {
                tardis.getInteriorManager().addWorldStructre(pPos, TardisWorldStructureRegistry.WAYPOINT.get().create(tardis, pPos));
            });
            pLevel.setBlock(pPos.above(), BlockRegistry.WAYPOINT_BANKS.get().defaultBlockState().setValue(IS_TOP, true), 3);
        }

    }

    @Override
    public void onRemove(BlockState pState, Level pLevel, BlockPos pPos, BlockState pNewState, boolean pMovedByPiston) {
        if(!pState.getValue(IS_TOP)){
            Capabilities.getCap(Capabilities.TARDIS, pLevel).ifPresent(tardis -> {
                tardis.getInteriorManager().removeWorldStructure(pPos);
            });
            pLevel.setBlock(pPos.above(), Blocks.AIR.defaultBlockState(), 3);
        }
        else{
            pLevel.destroyBlock(pPos.below(), true);
        }

        super.onRemove(pState, pLevel, pPos, pNewState, pMovedByPiston);
    }

    @Override
    protected void createBlockStateDefinition(StateDefinition.Builder<Block, BlockState> pBuilder) {
        super.createBlockStateDefinition(pBuilder);
        pBuilder.add(IS_TOP);
    }
}
