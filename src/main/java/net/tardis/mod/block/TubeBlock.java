package net.tardis.mod.block;

import net.minecraft.core.BlockPos;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResult;
import net.minecraft.world.entity.item.ItemEntity;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.context.BlockPlaceContext;
import net.minecraft.world.level.BlockGetter;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.block.entity.BlockEntityTicker;
import net.minecraft.world.level.block.entity.BlockEntityType;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.block.state.StateDefinition;
import net.minecraft.world.level.block.state.properties.BlockStateProperties;
import net.minecraft.world.phys.BlockHitResult;
import net.minecraft.world.phys.Vec3;
import net.minecraft.world.phys.shapes.BooleanOp;
import net.minecraft.world.phys.shapes.CollisionContext;
import net.minecraft.world.phys.shapes.Shapes;
import net.minecraft.world.phys.shapes.VoxelShape;
import net.tardis.mod.block.misc.DirectionalVoxelShape;
import net.tardis.mod.blockentities.TileRegistry;
import net.tardis.mod.blockentities.machines.TeleportTubeTile;
import net.tardis.mod.cap.Capabilities;
import net.tardis.mod.helpers.Helper;
import net.tardis.mod.registry.ItemRegistry;
import org.jetbrains.annotations.Nullable;

import java.util.function.Supplier;

public class TubeBlock extends BaseTileBlock<TeleportTubeTile>{

    public static final DirectionalVoxelShape SHAPE = new DirectionalVoxelShape(makeShape());

    public TubeBlock(Properties pProperties, Supplier<? extends BlockEntityType<TeleportTubeTile>> type) {
        super(pProperties, type);
    }

    public static TubeBlock create(){
        return new TubeBlock(
                BasicProps.Block.STONE.get().noOcclusion(),
                TileRegistry.TELEPORT_TUBE
        );
    }

    @Override
    protected void createBlockStateDefinition(StateDefinition.Builder<Block, BlockState> pBuilder) {
        super.createBlockStateDefinition(pBuilder);
        pBuilder.add(BlockStateProperties.HORIZONTAL_FACING);
    }

    @Nullable
    @Override
    public BlockState getStateForPlacement(BlockPlaceContext pContext) {
        return super.getStateForPlacement(pContext)
                .setValue(BlockStateProperties.HORIZONTAL_FACING, pContext.getHorizontalDirection().getOpposite());
    }

    @Override
    public VoxelShape getShape(BlockState pState, BlockGetter pLevel, BlockPos pPos, CollisionContext pContext) {
        return SHAPE.getShapeFor(pState.getValue(BlockStateProperties.HORIZONTAL_FACING));
    }

    @Override
    public InteractionResult use(BlockState pState, Level level, BlockPos pos, Player pPlayer, InteractionHand pHand, BlockHitResult pHit) {
        return InteractionResult.sidedSuccess(level.isClientSide());
    }

    @Override
    public void onRemove(BlockState pState, Level pLevel, BlockPos pPos, BlockState pNewState, boolean pMovedByPiston) {
        if(pLevel.getBlockEntity(pPos) instanceof TeleportTubeTile tube){
            tube.getTeleportId().ifPresent(id -> {
                Capabilities.getCap(Capabilities.TARDIS, pLevel).ifPresent(tardis -> {
                    tardis.getInteriorManager().removeTubeEntry(id, pPos);
                });
                if(!pLevel.isClientSide()) {
                    final ItemStack stack = new ItemStack(ItemRegistry.TUBE_ITEM.get());
                    final Vec3 pos = Helper.blockPosToVec3(pPos, true);

                    stack.getOrCreateTag().put("BlockEntityTag",  tube.serializeNBT());
                    pLevel.addFreshEntity(new ItemEntity(pLevel, pos.x(), pos.y(), pos.z(), stack));
                }
            });
        }
        super.onRemove(pState, pLevel, pPos, pNewState, pMovedByPiston);
    }

    @Override
    public void onPlace(BlockState pState, Level pLevel, BlockPos pPos, BlockState pOldState, boolean pMovedByPiston) {
        super.onPlace(pState, pLevel, pPos, pOldState, pMovedByPiston);
        if(pLevel.getBlockEntity(pPos) instanceof TeleportTubeTile tube){
            tube.getTeleportId().ifPresent(id -> {
                Capabilities.getCap(Capabilities.TARDIS, pLevel).ifPresent(tardis -> {
                    tardis.getInteriorManager().addTubeEndpoint(id, pPos);
                });
            });
        }
    }

    @Nullable
    @Override
    public <T extends BlockEntity> BlockEntityTicker<T> getTicker(Level pLevel, BlockState pState, BlockEntityType<T> pBlockEntityType) {
        return createTickerHelper(pBlockEntityType, TileRegistry.TELEPORT_TUBE.get(), !pLevel.isClientSide() ?
                createSimpleTileTicker(TeleportTubeTile::serverTick):
                createSimpleTileTicker(TeleportTubeTile::clientTick));
    }

    public static VoxelShape makeShape(){
        VoxelShape shape = Shapes.empty();
        shape = Shapes.join(shape, Shapes.box(1.03125, 0, -0.125, 1.15625, 3.1875, 1), BooleanOp.OR);
        shape = Shapes.join(shape, Shapes.box(-0.14375000000000004, 0, -0.125, -0.018750000000000044, 3.1875, 1), BooleanOp.OR);
        shape = Shapes.join(shape, Shapes.box(-0.14375000000000004, 3.0625, -0.125, 1.16875, 3.1875, 1), BooleanOp.OR);
        shape = Shapes.join(shape, Shapes.box(-0.14375000000000004, 2, -0.25, 1.10625, 3.1875, -0.125), BooleanOp.OR);
        shape = Shapes.join(shape, Shapes.box(-0.14375000000000004, 0, -0.125, 1.16875, 0.125, 1), BooleanOp.OR);
        shape = Shapes.join(shape, Shapes.box(-0.14375000000000004, 0, 1, 1.16875, 2, 1.0625), BooleanOp.OR);

        return shape;
    }

}
