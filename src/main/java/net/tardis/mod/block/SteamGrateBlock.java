package net.tardis.mod.block;

import net.minecraft.core.BlockPos;
import net.minecraft.core.particles.ParticleOptions;
import net.minecraft.core.particles.ParticleTypes;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.sounds.SoundSource;
import net.minecraft.util.Mth;
import net.minecraft.util.RandomSource;
import net.minecraft.world.item.context.BlockPlaceContext;
import net.minecraft.world.level.BlockGetter;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.block.state.StateDefinition;
import net.minecraft.world.level.block.state.properties.BlockStateProperties;
import net.minecraft.world.level.block.state.properties.DirectionProperty;
import net.minecraft.world.phys.Vec3;
import net.minecraft.world.phys.shapes.CollisionContext;
import net.minecraft.world.phys.shapes.VoxelShape;
import net.tardis.mod.Tardis;
import net.tardis.mod.block.misc.DirectionalVoxelShape;
import net.tardis.mod.helpers.Helper;
import net.tardis.mod.helpers.WorldHelper;
import net.tardis.mod.sound.SoundRegistry;
import org.jetbrains.annotations.Nullable;

public class SteamGrateBlock extends SimpleHorizonalBlock {

    public SteamGrateBlock(Properties pProperties) {
        super(pProperties);
    }

    public static SteamGrateBlock create(){
        return ((SteamGrateBlock)new SteamGrateBlock(BasicProps.Block.METAL.get().noOcclusion().randomTicks())
                .withShape(Helper.blockShapePixels(
                        0, 0, 3,
                        16, 1, 13)));
    }

    @Override
    public void animateTick(BlockState pState, Level pLevel, BlockPos pPos, RandomSource pRandom) {
        super.animateTick(pState, pLevel, pPos, pRandom);

        //Only try to release steam every 30 seconds
        if(!(pLevel.getGameTime() % (30 * 20) == 0)){
            return;
        }
        //50 / 50 chance so all grates don't vent together
        if(pLevel.random.nextFloat() < 0.5F){
            return;
        }

        //Get Client side player, and play it the sound
        Tardis.SIDE.getClientPlayer().ifPresent(player -> {
            pLevel.playSound(player, pPos, SoundRegistry.STEAM_HISS.get(), SoundSource.BLOCKS, 1.0F, 1.0F);
        });

        final int particles = 30; //amount of particles to play
        final double angle = 360.0 / (double)particles;
        final Vec3 pos = WorldHelper.centerOfBlockPos(pPos, false).add(0, 0.1, 0);
        final double speed = 0.2;

        for(int i = 0; i < particles; ++i){
            pLevel.addParticle(ParticleTypes.SMOKE,
                    pos.x, pos.y, pos.z,
                    Math.sin(Math.toRadians(angle * i)) * speed, 0.2, -Math.cos(Math.toRadians(angle * i)) * speed
            );
        }

    }
}
