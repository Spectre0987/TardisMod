package net.tardis.mod.block.dev;

import net.minecraft.core.BlockPos;
import net.minecraft.server.level.ServerPlayer;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResult;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.RenderShape;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.phys.BlockHitResult;
import net.tardis.mod.block.BaseTileBlock;
import net.tardis.mod.blockentities.TileRegistry;
import net.tardis.mod.blockentities.dev.TardisStructureTile;
import net.tardis.mod.client.gui.datas.GuiDatas;
import net.tardis.mod.network.Network;
import net.tardis.mod.network.packets.OpenGuiDataMessage;

public class TardisStructureBlock extends BaseTileBlock {
    public TardisStructureBlock(Properties pProperties) {
        super(pProperties, TileRegistry.STRUCTURE);
    }

    @Override
    public InteractionResult use(BlockState pState, Level pLevel, BlockPos pPos, Player pPlayer, InteractionHand pHand, BlockHitResult pHit) {

        if(!pLevel.isClientSide()){
            if(pLevel.getBlockEntity(pPos) instanceof TardisStructureTile t){
                Network.sendTo((ServerPlayer) pPlayer, new OpenGuiDataMessage(GuiDatas.STRUCTURE.create().from(t)));
            }
        }

        return InteractionResult.sidedSuccess(pLevel.isClientSide());
    }

    @Override
    public RenderShape getRenderShape(BlockState pState) {
        return RenderShape.MODEL;
    }
}
