package net.tardis.mod.block;

import net.minecraft.core.BlockPos;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResult;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.RenderShape;
import net.minecraft.world.level.block.entity.BlockEntityType;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.phys.BlockHitResult;
import net.minecraft.world.phys.shapes.Shapes;
import net.tardis.mod.block.misc.DirectionalVoxelShape;
import net.tardis.mod.blockentities.PlaqueTile;
import net.tardis.mod.blockentities.TileRegistry;
import net.tardis.mod.client.renderers.tiles.ItemPlaqueRenderer;

import java.util.function.Supplier;

public class ItemPlaqueBlock extends SimpleHorizonalTileBlock<PlaqueTile> {

    public ItemPlaqueBlock(Properties pProperties, Supplier<? extends BlockEntityType<PlaqueTile>> type) {
        super(pProperties, type);
    }

    public static ItemPlaqueBlock create(){
        ItemPlaqueBlock block = new ItemPlaqueBlock(
                BasicProps.Block.METAL.get().noOcclusion(),
                TileRegistry.ITEM_PLAQUE
        );
        block.withShape(Shapes.box(
                0, 0.1875, 0.9375,
                1, 0.8125, 1
        ));
        return block;
    }

    @Override
    public InteractionResult use(BlockState pState, Level pLevel, BlockPos pPos, Player pPlayer, InteractionHand pHand, BlockHitResult pHit) {
        if(pLevel.getBlockEntity(pPos) instanceof PlaqueTile p){
            pPlayer.setItemInHand(pHand, p.setItem(pPlayer.getItemInHand(pHand)));
        }
        return InteractionResult.sidedSuccess(pLevel.isClientSide());
    }



    @Override
    public RenderShape getRenderShape(BlockState pState) {
        return RenderShape.MODEL;
    }
}
