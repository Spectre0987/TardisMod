package net.tardis.mod.block.misc;

import net.minecraft.core.Direction;
import net.minecraft.world.phys.AABB;
import net.minecraft.world.phys.Vec3;
import net.minecraft.world.phys.shapes.BooleanOp;
import net.minecraft.world.phys.shapes.Shapes;
import net.minecraft.world.phys.shapes.VoxelShape;

public class DirectionalVoxelShape {

    public final VoxelShape north;
    public final VoxelShape east;
    public final VoxelShape south;
    public final VoxelShape west;

    public DirectionalVoxelShape(VoxelShape shape){
        this.north = shape;
        this.east = rotateVoxelShape(shape, Direction.EAST);
        this.south = rotateVoxelShape(shape, Direction.SOUTH);
        this.west = rotateVoxelShape(shape, Direction.WEST);
    }

    public VoxelShape getShapeFor(Direction dir){
        switch(dir){
            default: return this.north;
            case EAST: return this.east;
            case SOUTH: return this.south;
            case WEST: return this.west;
        }
    }

    public static VoxelShape rotateVoxelShape(VoxelShape shape, Direction dir){
        VoxelShape newShape = Shapes.empty();

        for(AABB bb : shape.toAabbs()){
            newShape = Shapes.join(newShape, Shapes.create(rotateAABB(bb.move(-0.5, 0, -0.5), dir).move(0.5, 0, 0.5)), BooleanOp.OR);
        }

        return newShape;
    }

    public static AABB rotateAABB(AABB aabb, Direction dir){
        return new AABB(rotatePoint(new Vec3(aabb.minX, aabb.minY, aabb.minZ), dir), rotatePoint(new Vec3(aabb.maxX, aabb.maxY, aabb.maxZ), dir));
    }

    public static Vec3 rotatePoint(Vec3 point, Direction dir){
        switch(dir){
            default: return point;
            case EAST: return new Vec3(-point.z, point.y, point.x);
            case SOUTH: return new Vec3(-point.x, point.y, -point.z);
            case WEST: return new Vec3(point.z, point.y, -point.x);
        }
    }

}
