package net.tardis.mod.block;

import net.minecraft.core.BlockPos;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResult;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.level.BlockGetter;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.entity.BlockEntityType;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.phys.BlockHitResult;
import net.minecraft.world.phys.shapes.CollisionContext;
import net.minecraft.world.phys.shapes.VoxelShape;
import net.minecraftforge.event.entity.player.PlayerEvent;
import net.tardis.mod.blockentities.DisguisedBlockTile;
import net.tardis.mod.blockentities.TileRegistry;
import net.tardis.mod.blockentities.exteriors.ChameleonExteriorTile;
import net.tardis.mod.blockentities.exteriors.ExteriorTile;

import java.util.function.Supplier;

public class ChameleonExteriorAuxillaryBlock extends DisguisedBlock<DisguisedBlockTile>{
    public ChameleonExteriorAuxillaryBlock(Properties pProperties, Supplier<? extends BlockEntityType<DisguisedBlockTile>> type) {
        super(pProperties, type);
    }

    public static ChameleonExteriorAuxillaryBlock create(){
        return new ChameleonExteriorAuxillaryBlock(
                BasicProps.Block.INVINCIBLE.get().noOcclusion(),
                TileRegistry.DISGUISED_BLOCK
        );
    }

    @Override
    public VoxelShape getShape(BlockState pState, BlockGetter pLevel, BlockPos pPos, CollisionContext pContext) {
        if(pLevel.getBlockEntity(pPos.below()) instanceof ChameleonExteriorTile cham && cham.getDoorHandler().getDoorState().isOpen()){
            return pLevel.getBlockState(pPos.below()).getShape(pLevel, pPos.below(), pContext).move(0, -1, 0);
        }
        return super.getShape(pState, pLevel, pPos, pContext);
    }

    @Override
    public VoxelShape getCollisionShape(BlockState pState, BlockGetter pLevel, BlockPos pPos, CollisionContext pContext) {
        if(pLevel.getBlockEntity(pPos.below()) instanceof ChameleonExteriorTile ext && ext.getDoorHandler().getDoorState().isOpen()){
            return pLevel.getBlockState(pPos.below()).getCollisionShape(pLevel, pPos.below(), pContext).move(0, -1, 0);
        }
        return super.getCollisionShape(pState, pLevel, pPos, pContext);
    }

    @Override
    public InteractionResult use(BlockState pState, Level pLevel, BlockPos pPos, Player pPlayer, InteractionHand pHand, BlockHitResult pHit) {
        return pLevel.getBlockState(pPos.below()).use(pLevel, pPlayer, pHand, pHit.withPosition(pPos.below()));
    }
}
