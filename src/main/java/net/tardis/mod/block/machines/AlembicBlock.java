package net.tardis.mod.block.machines;

import net.minecraft.core.BlockPos;
import net.minecraft.server.level.ServerPlayer;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResult;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.context.BlockPlaceContext;
import net.minecraft.world.level.BlockGetter;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.*;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.block.entity.BlockEntityTicker;
import net.minecraft.world.level.block.entity.BlockEntityType;
import net.minecraft.world.level.block.state.BlockBehaviour;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.block.state.StateDefinition;
import net.minecraft.world.level.block.state.properties.BlockStateProperties;
import net.minecraft.world.level.block.state.properties.BooleanProperty;
import net.minecraft.world.level.block.state.properties.DirectionProperty;
import net.minecraft.world.level.material.Material;
import net.minecraft.world.phys.BlockHitResult;
import net.minecraft.world.phys.shapes.BooleanOp;
import net.minecraft.world.phys.shapes.CollisionContext;
import net.minecraft.world.phys.shapes.Shapes;
import net.minecraft.world.phys.shapes.VoxelShape;
import net.minecraftforge.common.capabilities.ForgeCapabilities;
import net.minecraftforge.common.util.LazyOptional;
import net.minecraftforge.fluids.capability.IFluidHandler;
import net.minecraftforge.fluids.capability.IFluidHandlerItem;
import net.minecraftforge.network.NetworkHooks;
import net.tardis.mod.block.BaseTileBlock;
import net.tardis.mod.block.BasicProps;
import net.tardis.mod.block.misc.DirectionalVoxelShape;
import net.tardis.mod.blockentities.TileRegistry;
import net.tardis.mod.blockentities.crafting.AlembicBlockEntity;
import net.tardis.mod.menu.AlembicMenu;
import net.tardis.mod.menu.DefaultMenuProvider;
import org.jetbrains.annotations.Nullable;

import java.util.function.Supplier;

public class AlembicBlock<T extends AlembicBlockEntity> extends BaseTileBlock<T> {

    public static final BooleanProperty ON = BooleanProperty.create("on");
    public static final DirectionProperty FACING = BlockStateProperties.HORIZONTAL_FACING;
    public static final DirectionalVoxelShape SHAPE = new DirectionalVoxelShape(makeShape());


    public AlembicBlock(Properties pProperties, Supplier<? extends BlockEntityType<T>> type) {
        super(pProperties, type);
    }

    public static AlembicBlock create(){
        return new AlembicBlock(
                BasicProps.Block.STONE.get().noOcclusion(),
                TileRegistry.ALEMBIC
        );
    }

    @Override
    public InteractionResult use(BlockState pState, Level pLevel, BlockPos pPos, Player pPlayer, InteractionHand pHand, BlockHitResult pHit) {

        if(pLevel.getBlockEntity(pPos) instanceof AlembicBlockEntity alembic){
            final ItemStack stack = pPlayer.getItemInHand(pHand);
            final LazyOptional<IFluidHandlerItem> tankHolder = stack.getCapability(ForgeCapabilities.FLUID_HANDLER_ITEM);

            if(tankHolder.isPresent()){
                int accepted = alembic.getCraftingTank().fill(tankHolder.orElseThrow(NullPointerException::new).drain(1000, IFluidHandler.FluidAction.EXECUTE), IFluidHandler.FluidAction.EXECUTE);
                if(accepted > 0 && stack.hasCraftingRemainingItem()){
                    pPlayer.setItemInHand(pHand, stack.getCraftingRemainingItem());
                }
                return InteractionResult.sidedSuccess(pLevel.isClientSide());
            }
        }

        if(pHand == InteractionHand.MAIN_HAND){

            if(!pLevel.isClientSide){
                if(pLevel.getBlockEntity(pPos) instanceof AlembicBlockEntity alembic){
                    NetworkHooks.openScreen((ServerPlayer) pPlayer, new DefaultMenuProvider((id, inv) -> new AlembicMenu(id, inv, alembic)), buf -> {
                        buf.writeBlockPos(pPos);
                        buf.writeNbt(alembic.serializeNBT());
                    });
                }
            }

            return InteractionResult.sidedSuccess(pLevel.isClientSide);
        }
        return super.use(pState, pLevel, pPos, pPlayer, pHand, pHit);
    }

    @Override
    public VoxelShape getShape(BlockState pState, BlockGetter pLevel, BlockPos pPos, CollisionContext pContext) {
        return SHAPE.getShapeFor(pState.getValue(BlockStateProperties.HORIZONTAL_FACING));
    }

    @Nullable
    @Override
    public BlockState getStateForPlacement(BlockPlaceContext pContext) {
        return super.getStateForPlacement(pContext).setValue(BlockStateProperties.HORIZONTAL_FACING, pContext.getHorizontalDirection().getOpposite());
    }

    @Nullable
    @Override
    public <T extends BlockEntity> BlockEntityTicker<T> getTicker(Level pLevel, BlockState pState, BlockEntityType<T> pBlockEntityType) {
        return createTickerHelper(pBlockEntityType, TileRegistry.ALEMBIC.get(), AlembicBlockEntity::tick);
    }

    @Override
    public RenderShape getRenderShape(BlockState pState) {
        return RenderShape.MODEL;
    }

    @Override
    protected void createBlockStateDefinition(StateDefinition.Builder<Block, BlockState> pBuilder) {
        super.createBlockStateDefinition(pBuilder);
        pBuilder.add(ON, FACING);
    }

    public static VoxelShape makeShape(){
        VoxelShape shape = Shapes.empty();
        shape = Shapes.join(shape, Shapes.box(0, 0, 0.25, 1, 0.265625, 0.75), BooleanOp.OR);
        shape = Shapes.join(shape, Shapes.box(0.546875, 0.265625, 0.3125, 0.921875, 0.984375, 0.6875), BooleanOp.OR);
        shape = Shapes.join(shape, Shapes.box(0.03125, 0.265625, 0.359375, 0.40625, 0.625, 0.609375), BooleanOp.OR);

        return shape;
    }


    @Override
    public BlockState rotate(BlockState pState, Rotation pRotation) {
        return pState.setValue(BlockStateProperties.HORIZONTAL_FACING, pRotation.rotate(pState.getValue(BlockStateProperties.HORIZONTAL_FACING)));
    }

    @Override
    public BlockState mirror(BlockState state, Mirror mirror) {
        return state.setValue(BlockStateProperties.HORIZONTAL_FACING, mirror.mirror(state.getValue(BlockStateProperties.HORIZONTAL_FACING)));
    }
}
