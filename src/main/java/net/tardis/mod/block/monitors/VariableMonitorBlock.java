package net.tardis.mod.block.monitors;

import net.minecraft.core.BlockPos;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.level.BlockGetter;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.LevelReader;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.entity.BlockEntityType;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.block.state.properties.BlockStateProperties;
import net.minecraft.world.phys.shapes.CollisionContext;
import net.minecraft.world.phys.shapes.Shapes;
import net.minecraft.world.phys.shapes.VoxelShape;
import net.tardis.mod.block.BasicProps;
import net.tardis.mod.block.misc.DirectionalVoxelShape;
import net.tardis.mod.blockentities.MonitorVariableTile;
import net.tardis.mod.blockentities.TileRegistry;

import java.util.ArrayList;
import java.util.function.Supplier;

public class VariableMonitorBlock extends MonitorBlock<MonitorVariableTile>{

    public static final DirectionalVoxelShape SHAPE = new DirectionalVoxelShape(Shapes.box(
            0, 0, 13 / 16.0,
            1.0, 1.0, 1.0
    ));

    public VariableMonitorBlock(Properties properties, ResourceLocation dataId, Supplier<BlockEntityType<MonitorVariableTile>> type) {
        super(properties, dataId, type);
    }

    public static VariableMonitorBlock create(){
        return new VariableMonitorBlock(
                BasicProps.Block.METAL.get(),
                MonitorData.GALVANIC,
                TileRegistry.MONITOR_VARIABLE
        );
    }

    @Override
    public void neighborChanged(BlockState pState, Level level, BlockPos pPos, Block pNeighborBlock, BlockPos pNeighborPos, boolean pMovedByPiston) {
        super.neighborChanged(pState, level, pPos, pNeighborBlock, pNeighborPos, pMovedByPiston);
        if(level.getBlockEntity(pPos) instanceof MonitorVariableTile mon){
            mon.refreshBounds();
            for(BlockPos pos : mon.gatherConnectedMonitorPos(new ArrayList<>(), pPos, mon.buildSearchList())){
                if(level.getBlockEntity(pos) instanceof MonitorVariableTile mon2){
                    mon2.refreshBounds();
                }
            }
        }
    }

    @Override
    public void onNeighborChange(BlockState state, LevelReader level, BlockPos pPos, BlockPos neighbor) {
        super.onNeighborChange(state, level, pPos, neighbor);

    }

    @Override
    public VoxelShape getShape(BlockState pState, BlockGetter pLevel, BlockPos pPos, CollisionContext pContext) {
        return SHAPE.getShapeFor(pState.getValue(BlockStateProperties.HORIZONTAL_FACING));
    }
}
