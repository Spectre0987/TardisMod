package net.tardis.mod.block.monitors;

import net.minecraft.core.BlockPos;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.level.BlockGetter;
import net.minecraft.world.level.block.entity.BlockEntityType;
import net.minecraft.world.level.block.state.BlockBehaviour;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.block.state.properties.BlockStateProperties;
import net.minecraft.world.phys.shapes.CollisionContext;
import net.minecraft.world.phys.shapes.Shapes;
import net.minecraft.world.phys.shapes.VoxelShape;
import net.minecraftforge.registries.RegistryObject;
import net.tardis.mod.block.misc.DirectionalVoxelShape;
import net.tardis.mod.blockentities.BaseMonitorTile;

public class EyeMonitorBlock extends MonitorBlock<BaseMonitorTile> {

    public static final DirectionalVoxelShape SHAPE = new DirectionalVoxelShape(Shapes.box(-0.375, 0, 0.9, 1.384375, 0.9375, 1));

    public EyeMonitorBlock(BlockBehaviour.Properties properties, ResourceLocation eye, RegistryObject<BlockEntityType<BaseMonitorTile>> steamMonitor) {
        super(properties, eye, steamMonitor);

    }

    @Override
    public VoxelShape getShape(BlockState pState, BlockGetter pLevel, BlockPos pPos, CollisionContext pContext) {
        return SHAPE.getShapeFor(pState.getValue(BlockStateProperties.HORIZONTAL_FACING));
    }
}
