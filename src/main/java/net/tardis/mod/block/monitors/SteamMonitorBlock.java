package net.tardis.mod.block.monitors;

import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.item.context.BlockPlaceContext;
import net.minecraft.world.level.BlockGetter;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.entity.BlockEntityType;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.block.state.StateDefinition;
import net.minecraft.world.level.block.state.properties.BlockStateProperties;
import net.minecraft.world.level.block.state.properties.BooleanProperty;
import net.minecraft.world.phys.shapes.BooleanOp;
import net.minecraft.world.phys.shapes.CollisionContext;
import net.minecraft.world.phys.shapes.Shapes;
import net.minecraft.world.phys.shapes.VoxelShape;
import net.tardis.mod.block.BasicProps;
import net.tardis.mod.block.misc.DirectionalVoxelShape;
import net.tardis.mod.blockentities.BaseMonitorTile;
import net.tardis.mod.blockentities.TileRegistry;
import org.jetbrains.annotations.Nullable;

import java.util.function.Supplier;

public class SteamMonitorBlock extends MonitorBlock<BaseMonitorTile>{

    public static final BooleanProperty IS_HANGING = BlockStateProperties.HANGING;

    public static final DirectionalVoxelShape SHAPE = new DirectionalVoxelShape(makeShape());
    public static final DirectionalVoxelShape HANG_SHAPE = new DirectionalVoxelShape(makeHangShape());

    public SteamMonitorBlock(Properties properties, ResourceLocation data, Supplier<BlockEntityType<BaseMonitorTile>> type) {
        super(properties, data, type);
    }

    public static SteamMonitorBlock create(){
        return new SteamMonitorBlock(BasicProps.Block.METAL.get(), MonitorData.STEAM, TileRegistry.BASIC_MONITOR);
    }

    @Override
    protected void createBlockStateDefinition(StateDefinition.Builder<Block, BlockState> pBuilder) {
        super.createBlockStateDefinition(pBuilder);
        pBuilder.add(IS_HANGING);
    }

    @Override
    public @Nullable BlockState getStateForPlacement(BlockPlaceContext pContext) {
        return super.getStateForPlacement(pContext)
                .setValue(IS_HANGING, pContext.getClickedFace() == Direction.DOWN);
    }

    @Override
    public VoxelShape getShape(BlockState pState, BlockGetter pLevel, BlockPos pPos, CollisionContext pContext) {
        DirectionalVoxelShape shape = pState.getValue(IS_HANGING) ? HANG_SHAPE : SHAPE;
        return shape.getShapeFor(pState.getValue(BlockStateProperties.HORIZONTAL_FACING));
    }

    public static VoxelShape makeHangShape(){
        VoxelShape shape = Shapes.empty();
        shape = Shapes.join(shape, Shapes.box(0.03125, 0, 0.046875, 0.96875, 0.734375, 0.890625), BooleanOp.OR);
        shape = Shapes.join(shape, Shapes.box(0.31875, 0.734375, 0.3125, 0.69375, 1, 0.703125), BooleanOp.OR);

        return shape;
    }

    public static VoxelShape makeShape(){
        VoxelShape shape = Shapes.empty();
        shape = Shapes.join(shape, Shapes.box(0.078125, 0.15000000000000002, 0.15625, 0.953125, 0.93125, 0.90625), BooleanOp.OR);
        shape = Shapes.join(shape, Shapes.box(0.078125, -0.006249999999999978, 0.3125, 0.953125, 0.15000000000000002, 0.703125), BooleanOp.OR);

        return shape;
    }

}
