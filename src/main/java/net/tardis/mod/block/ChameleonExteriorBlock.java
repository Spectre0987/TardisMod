package net.tardis.mod.block;

import net.minecraft.core.BlockPos;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResult;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.level.BlockGetter;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.Mirror;
import net.minecraft.world.level.block.Rotation;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.block.entity.BlockEntityTicker;
import net.minecraft.world.level.block.entity.BlockEntityType;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.block.state.StateDefinition;
import net.minecraft.world.level.block.state.properties.BlockStateProperties;
import net.minecraft.world.phys.BlockHitResult;
import net.minecraft.world.phys.shapes.CollisionContext;
import net.minecraft.world.phys.shapes.VoxelShape;
import net.tardis.mod.blockentities.TileRegistry;
import net.tardis.mod.blockentities.exteriors.ChameleonExteriorTile;
import net.tardis.mod.blockentities.exteriors.ExteriorTile;
import org.jetbrains.annotations.Nullable;
import org.spongepowered.asm.mixin.Pseudo;

import java.util.function.Supplier;

public class ChameleonExteriorBlock<T extends ChameleonExteriorTile> extends DisguisedBlock<T>{
    public ChameleonExteriorBlock(Properties pProperties, Supplier<? extends BlockEntityType<T>> type) {
        super(pProperties, type);
    }

    @Override
    protected void createBlockStateDefinition(StateDefinition.Builder<Block, BlockState> pBuilder) {
        super.createBlockStateDefinition(pBuilder);
        pBuilder.add(BlockStateProperties.HORIZONTAL_FACING);
    }

    @Override
    public VoxelShape getShape(BlockState pState, BlockGetter pLevel, BlockPos pPos, CollisionContext pContext) {
        if(pLevel.getBlockEntity(pPos) instanceof ChameleonExteriorTile cham && cham.getDoorHandler().getDoorState().isOpen()){
            return ExteriorBlock.SHAPE.getShapeFor(pState.getValue(ExteriorBlock.FACING));
        }
        return super.getShape(pState, pLevel, pPos, pContext);
    }

    @Override
    public VoxelShape getCollisionShape(BlockState pState, BlockGetter pLevel, BlockPos pPos, CollisionContext pContext) {
        if(pLevel.getBlockEntity(pPos) instanceof ChameleonExteriorTile ext && ext.getDoorHandler().getDoorState().isOpen()){
            return ExteriorBlock.SHAPE.getShapeFor(pState.getValue(ExteriorBlock.FACING));
        }
        return super.getCollisionShape(pState, pLevel, pPos, pContext);
    }



    @Nullable
    @Override
    public <T extends BlockEntity> BlockEntityTicker<T> getTicker(Level pLevel, BlockState pState, BlockEntityType<T> pBlockEntityType) {
        return createTickerHelper(pBlockEntityType, TileRegistry.CHAMELEON_EXTERIOR.get(), ChameleonExteriorTile::tick);
    }

    @Override
    public BlockState rotate(BlockState pState, Rotation pRotation) {
        return pState.setValue(BlockStateProperties.HORIZONTAL_FACING, pRotation.rotate(pState.getValue(BlockStateProperties.HORIZONTAL_FACING)));
    }

    @Override
    public BlockState mirror(BlockState state, Mirror mirror) {
        return state.setValue(BlockStateProperties.HORIZONTAL_FACING, mirror.mirror(state.getValue(BlockStateProperties.HORIZONTAL_FACING)));
    }

    @Override
    public InteractionResult use(BlockState pState, Level pLevel, BlockPos pPos, Player pPlayer, InteractionHand pHand, BlockHitResult pHit) {

        if(pLevel.getBlockEntity(pPos) instanceof ExteriorTile tile){
            if(!pLevel.isClientSide)
                tile.getDoorHandler().onInteract(pPlayer, pPlayer.getItemInHand(pHand), pHand);
            return InteractionResult.sidedSuccess(pLevel.isClientSide);
        }
        return super.use(pState, pLevel, pPos, pPlayer, pHand, pHit);
    }
}
