package net.tardis.mod.block;

import net.minecraft.client.renderer.chunk.RenderChunkRegion;
import net.minecraft.core.BlockPos;
import net.minecraft.util.Mth;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.BlockGetter;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.block.state.StateDefinition;
import net.minecraft.world.level.block.state.properties.BooleanProperty;
import net.minecraftforge.common.util.LazyOptional;
import net.tardis.mod.Tardis;
import net.tardis.mod.cap.Capabilities;
import net.tardis.mod.cap.items.functions.sonic.SonicBlockInteractions;
import net.tardis.mod.cap.level.ITardisLevel;

public class RoundelBlock extends Block implements SonicBlockInteractions.ISonicBlockAction {

    public static final String TRANSLATION_KEY = "block." + Tardis.MODID + ".roundel";
    public static final BooleanProperty IS_LIT = BooleanProperty.create("is_lit");

    public RoundelBlock(Properties pProperties) {
        super(pProperties.lightLevel(state -> state.getValue(IS_LIT) ? 15 : 0));
        this.registerDefaultState(defaultBlockState().setValue(IS_LIT, false));
    }

    @Override
    public String getDescriptionId() {
        return TRANSLATION_KEY;
    }

    @Override
    protected void createBlockStateDefinition(StateDefinition.Builder<Block, BlockState> pBuilder) {
        super.createBlockStateDefinition(pBuilder);
        pBuilder.add(IS_LIT);
    }

    @Override
    public boolean useOnBlock(BlockState target, BlockPos pos, ItemStack sonic, Level level, Player player, InteractionHand hand) {
        if(!level.isClientSide()){
            level.setBlock(pos, target.cycle(IS_LIT), 3);
        }
        return true;
    }

    @Override
    public int getLightBlock(BlockState pState, BlockGetter level, BlockPos pPos) {
        return Tardis.SIDE.getTARDISLightLevel().orElse(super.getLightBlock(pState, level, pPos));
    }

    @Override
    public int getLightEmission(BlockState state, BlockGetter level, BlockPos pos) {

        return super.getLightEmission(state, level, pos);
    }
}
