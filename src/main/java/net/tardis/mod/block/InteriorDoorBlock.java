package net.tardis.mod.block;

import net.minecraft.core.BlockPos;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResult;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.context.BlockPlaceContext;
import net.minecraft.world.level.BlockGetter;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.Mirror;
import net.minecraft.world.level.block.Rotation;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.block.entity.BlockEntityTicker;
import net.minecraft.world.level.block.entity.BlockEntityType;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.block.state.StateDefinition;
import net.minecraft.world.level.block.state.properties.BlockStateProperties;
import net.minecraft.world.level.block.state.properties.DirectionProperty;
import net.minecraft.world.phys.BlockHitResult;
import net.minecraft.world.phys.shapes.CollisionContext;
import net.minecraft.world.phys.shapes.Shapes;
import net.minecraft.world.phys.shapes.VoxelShape;
import net.tardis.mod.block.misc.DirectionalVoxelShape;
import net.tardis.mod.blockentities.IMultiblockMaster;
import net.tardis.mod.blockentities.InteriorDoorTile;
import net.tardis.mod.blockentities.TileRegistry;
import net.tardis.mod.blockentities.multiblock.MultiblockData;
import net.tardis.mod.blockentities.multiblock.MultiblockDatas;
import net.tardis.mod.cap.Capabilities;
import net.tardis.mod.helpers.WorldHelper;
import net.tardis.mod.misc.DoorHandler;
import org.jetbrains.annotations.Nullable;

import java.util.UUID;
import java.util.function.Supplier;

public class InteriorDoorBlock<T extends InteriorDoorTile> extends AbstractMultiblockMasterBlock<T> {


    public static final DirectionProperty FACING = BlockStateProperties.HORIZONTAL_FACING;

    public static final DirectionalVoxelShape SHAPE = new DirectionalVoxelShape(Shapes.box(0, 0, 10 / 16.0, 1, 2, 1));

    public InteriorDoorBlock(Properties prop, Supplier<? extends BlockEntityType<T>> type, MultiblockData multiblockData) {
        super(prop, type, multiblockData);
    }

    public static <T extends BlockEntity & IMultiblockMaster> InteriorDoorBlock create(){
        return new InteriorDoorBlock(
                BasicProps.Block.WOOD.get().noOcclusion().noCollission().isSuffocating((state, pos, level) -> false),
                TileRegistry.INTERIOR_DOOR,
                MultiblockDatas.TWO_BLOCK
        );
    }


    @Nullable
    @Override
    public <T extends BlockEntity> BlockEntityTicker<T> getTicker(Level p_153212_, BlockState p_153213_, BlockEntityType<T> type) {
        return createTickerHelper(type, TileRegistry.INTERIOR_DOOR.get(), InteriorDoorTile::tick);
    }

    @Nullable
    @Override
    public BlockState getStateForPlacement(BlockPlaceContext context) {
        return super.getStateForPlacement(context)
                .setValue(FACING, context.getHorizontalDirection().getOpposite());
    }

    @Override
    protected void createBlockStateDefinition(StateDefinition.Builder<Block, BlockState> pBuilder) {
        super.createBlockStateDefinition(pBuilder);
        pBuilder.add(FACING);
    }

    @Override
    public VoxelShape getShape(BlockState pState, BlockGetter pLevel, BlockPos pPos, CollisionContext pContext) {
        return SHAPE.getShapeFor(WorldHelper.getHorizontalFacing(pState));
    }

    @Override
    public void onRemove(BlockState pState, Level pLevel, BlockPos pPos, BlockState pNewState, boolean pIsMoving) {
        if(pLevel.getBlockEntity(pPos) instanceof InteriorDoorTile door){
            pLevel.getCapability(Capabilities.TARDIS).ifPresent(tardis -> {
                tardis.getInteriorManager().removeDoor(door.getDoorID());
            });
        }
        super.onRemove(pState, pLevel, pPos, pNewState, pIsMoving);
    }

    @Override
    public void onPlace(BlockState pState, Level pLevel, BlockPos pPos, BlockState pOldState, boolean pIsMoving) {
        super.onPlace(pState, pLevel, pPos, pOldState, pIsMoving);
        if(pLevel.getBlockEntity(pPos) instanceof InteriorDoorTile door){
            door.setDoorID(UUID.randomUUID());
        }
    }

    @Override
    public InteractionResult use(BlockState pState, Level pLevel, BlockPos pPos, Player pPlayer, InteractionHand pHand, BlockHitResult pHit) {
        if(pHand == InteractionHand.OFF_HAND)
            return InteractionResult.sidedSuccess(pLevel.isClientSide);

        if(!pLevel.isClientSide){
            if(pLevel.getBlockEntity(pPos) instanceof InteriorDoorTile door){
                final DoorHandler handler = door.getDoorHandler();
                if(handler != null){
                    handler.onInteract(pPlayer, pPlayer.getItemInHand(pHand), pHand);
                }
            }
        }

        return InteractionResult.sidedSuccess(pLevel.isClientSide());
    }

    @Override
    public BlockState rotate(BlockState pState, Rotation pRotation) {
        return pState.setValue(BlockStateProperties.HORIZONTAL_FACING, pRotation.rotate(pState.getValue(BlockStateProperties.HORIZONTAL_FACING)));
    }

    @Override
    public BlockState mirror(BlockState state, Mirror mirror) {
        return state.setValue(BlockStateProperties.HORIZONTAL_FACING, mirror.mirror(state.getValue(BlockStateProperties.HORIZONTAL_FACING)));
    }
}
