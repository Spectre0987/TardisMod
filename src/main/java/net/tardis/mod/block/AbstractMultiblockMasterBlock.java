package net.tardis.mod.block;

import net.minecraft.core.BlockPos;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.block.entity.BlockEntityType;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.block.state.properties.BlockStateProperties;
import net.tardis.mod.blockentities.IMultiblockMaster;
import net.tardis.mod.blockentities.multiblock.MultiblockData;
import net.tardis.mod.helpers.Helper;
import net.tardis.mod.misc.DelayedServerTask;
import org.jetbrains.annotations.Nullable;

import java.util.function.Supplier;

public class AbstractMultiblockMasterBlock<T extends BlockEntity & IMultiblockMaster> extends BaseTileBlock<T>{

    final MultiblockData data;

    public AbstractMultiblockMasterBlock(Properties pProperties, Supplier<? extends BlockEntityType<T>> type, MultiblockData data) {
        super(pProperties, type);
        this.data = data;
    }

    @Override
    public void onPlace(BlockState pState, Level pLevel, BlockPos pPos, BlockState pOldState, boolean pMovedByPiston) {




        super.onPlace(pState, pLevel, pPos, pOldState, pMovedByPiston);

        if(!pLevel.isClientSide()){
            DelayedServerTask.schedule(pLevel.getServer(), 2, () -> {
                if(pState.getBlock() != pOldState.getBlock()){
                    if(pState.hasProperty(BlockStateProperties.HORIZONTAL_FACING)){
                        data.placeBlocks(pLevel, pPos, Helper.getRotationFromDirection(pState.getValue(BlockStateProperties.HORIZONTAL_FACING)));
                    }
                    else data.placeBlocks(pLevel, pPos);
                }
            });
        }
    }

    @Override
    public void onRemove(BlockState pState, Level pLevel, BlockPos pPos, BlockState pNewState, boolean pMovedByPiston) {
        super.onRemove(pState, pLevel, pPos, pNewState, pMovedByPiston);
        if(pState.getBlock() != pNewState.getBlock()){
            if(pState.hasProperty(BlockStateProperties.HORIZONTAL_FACING)){
                data.destroy(pLevel, pPos, Helper.getRotationFromDirection(pState.getValue(BlockStateProperties.HORIZONTAL_FACING)));
            }
            else data.destroy(pLevel, pPos);
        }
    }

    @Override
    public @Nullable BlockEntity newBlockEntity(BlockPos blockPos, BlockState blockState) {
        T tile = this.blockEntityType.get().create(blockPos, blockState);
        tile.setMasterData(this.data);
        return tile;
    }
}
