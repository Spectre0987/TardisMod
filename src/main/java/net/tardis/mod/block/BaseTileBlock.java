package net.tardis.mod.block;

import com.mojang.datafixers.types.Func;
import net.minecraft.core.BlockPos;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.BaseEntityBlock;
import net.minecraft.world.level.block.RenderShape;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.block.entity.BlockEntityTicker;
import net.minecraft.world.level.block.entity.BlockEntityType;
import net.minecraft.world.level.block.state.BlockState;
import net.tardis.mod.blockentities.IBreakLogic;
import net.tardis.mod.blockentities.crafting.AlembicBlockEntity;
import net.tardis.mod.blockentities.machines.FabricatorTile;
import net.tardis.mod.blockentities.machines.TeleportTubeTile;
import org.jetbrains.annotations.Nullable;

import java.util.function.Consumer;
import java.util.function.Supplier;

public class BaseTileBlock<T extends BlockEntity> extends BaseEntityBlock {

    protected final Supplier<? extends BlockEntityType<T>> blockEntityType;
    final boolean useBakedModel;

    public BaseTileBlock(Properties pProperties, Supplier<? extends BlockEntityType<T>> type) {
        this(pProperties, type, false);
    }

    public BaseTileBlock(Properties pProperties, Supplier<? extends BlockEntityType<T>> type, boolean useBlockModel) {
        super(pProperties);
        this.blockEntityType = type;
        this.useBakedModel = useBlockModel;

    }

    @Nullable
    @Override
    public BlockEntity newBlockEntity(BlockPos blockPos, BlockState blockState) {
        return this.blockEntityType.get().create(blockPos, blockState);
    }

    @Override
    public void onRemove(BlockState pState, Level pLevel, BlockPos pPos, BlockState pNewState, boolean pMovedByPiston) {
        if(!pLevel.isClientSide() && pLevel.getBlockEntity(pPos) instanceof IBreakLogic logic){
            logic.onBroken();
        }
        super.onRemove(pState, pLevel, pPos, pNewState, pMovedByPiston);
    }

    @Override
    public RenderShape getRenderShape(BlockState pState) {
        return this.useBakedModel ? RenderShape.MODEL : super.getRenderShape(pState);
    }

    public static <T extends BlockEntity> BlockEntityTicker<T> createSimpleTileTicker(Consumer<T> tick){
        return (level, pos, state, tile) -> tick.accept(tile);
    }
}
