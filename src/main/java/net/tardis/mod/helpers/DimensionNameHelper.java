package net.tardis.mod.helpers;

import net.minecraft.core.RegistryAccess;
import net.minecraft.resources.ResourceKey;
import net.minecraft.server.MinecraftServer;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.dimension.DimensionType;
import net.tardis.mod.misc.TardisDimensionInfo;
import net.tardis.mod.network.Network;
import net.tardis.mod.network.packets.DimenionNameMessage;
import net.tardis.mod.registry.JsonRegistries;

import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.Locale;
import java.util.Optional;

public class DimensionNameHelper {

    public static final HashMap<ResourceKey<Level>, Optional<String>> CLIENT_NAMES = new HashMap<>();


    public static String getFriendlyName(Level level, ResourceKey<Level> dimKey){
        if(level.isClientSide()){
            //If on client and we don't have a record, ask the server
            if(!CLIENT_NAMES.containsKey(dimKey)){
                Network.sendToServer(new DimenionNameMessage(dimKey, null));
                return formatKey(dimKey);
            }
            return CLIENT_NAMES.get(dimKey).orElse(formatKey(dimKey));
        }
        return getServerCustomName(level.getServer(), dimKey).orElse(formatKey(dimKey));
    }

    public static Optional<String> getServerCustomName(MinecraftServer server, ResourceKey<Level> level){
        ServerLevel l = server.getLevel(level);
        if(l != null){
            final ResourceKey<DimensionType> type = l.dimensionTypeId();
            for(TardisDimensionInfo info : server.registryAccess().registryOrThrow(JsonRegistries.TARDIS_DIM_INFO)){
                if(info.AppliesTo(type) && info.customName() != null && !info.customName().isEmpty()){
                    return Optional.of(info.customName());
                }
            }
        }
        return Optional.empty();
    }

    public static String formatKey(ResourceKey<Level> key){
        final int lastSlash = key.location().getPath().lastIndexOf('/');
        if(lastSlash < 0)
            return capitalizeWords(key.location().getPath().replace('_', ' '));
        String word = key.location().getPath().substring(lastSlash + 1);
        return capitalizeWords(word.replace('_', ' '));
    }

    public static String capitalizeWords(String s){
        final String[] words = s.split(" ");
        if(words.length == 0){
            StringBuilder build = new StringBuilder();
            for(int i = 0; i < s.length(); ++i){
                if(i == 0){
                    build.append((s.charAt(i) + "").toUpperCase(Locale.ROOT));
                }
                else build.append(s.charAt(i));
            }
            return build.toString();
        }
        //If there is more than one word
        StringBuilder builder = new StringBuilder();
        for(String word : words){
            for(int i = 0; i < word.length(); ++i){
                if(i == 0){
                    builder.append((word.charAt(i) + "").toUpperCase(Locale.ROOT));
                }
                else builder.append(word.charAt(i));
            }
            builder.append(" ");//Add space back from the split
        }
        return builder.toString().trim();
    }

}
