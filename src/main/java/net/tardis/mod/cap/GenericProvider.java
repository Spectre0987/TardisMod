package net.tardis.mod.cap;

import net.minecraft.core.Direction;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.nbt.Tag;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.capabilities.ICapabilitySerializable;
import net.minecraftforge.common.util.INBTSerializable;
import net.minecraftforge.common.util.LazyOptional;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class GenericProvider<T extends INBTSerializable<C>, C extends Tag, I extends T> implements ICapabilitySerializable<C> {

    private final T object;
    private final LazyOptional<T> holder;
    private final Capability<I> instance;

    public GenericProvider(Capability<I> instance, T object){
        this.instance = instance;
        this.object = object;
        this.holder = LazyOptional.of(() -> this.object);
    }

    @Override
    public @NotNull <T> LazyOptional<T> getCapability(@NotNull Capability<T> cap, @Nullable Direction side) {
        return cap == this.instance ? this.holder.cast() : LazyOptional.empty();
    }

    @Override
    public C serializeNBT() {
        return this.object.serializeNBT();
    }

    @Override
    public void deserializeNBT(C nbt) {
        this.object.deserializeNBT(nbt);
    }
}
