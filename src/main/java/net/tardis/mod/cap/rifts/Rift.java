package net.tardis.mod.cap.rifts;

import net.minecraft.core.BlockPos;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.resources.ResourceKey;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.util.Mth;
import net.minecraft.world.level.ChunkPos;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.chunk.LevelChunk;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.common.util.INBTSerializable;
import net.minecraftforge.common.util.LazyOptional;
import net.tardis.api.artron.IArtronStorage;
import net.tardis.api.events.RiftEvent;
import net.tardis.mod.Tardis;
import net.tardis.mod.cap.Capabilities;
import net.tardis.mod.cap.chunks.IChunkCap;
import net.tardis.mod.datagen.TardisBlockAsLootProvider;
import net.tardis.mod.helpers.Helper;
import net.tardis.mod.loot.BlockLootTable;
import net.tardis.mod.network.Network;
import net.tardis.mod.network.packets.RiftArtronMessage;
import net.tardis.mod.registry.JsonRegistries;
import org.jetbrains.annotations.Nullable;

import java.util.Optional;

public class Rift implements IArtronStorage, INBTSerializable<CompoundTag> {

    public static final ResourceLocation RIFT_BLOCK_LOOT = Helper.createRL("rift");
    public static final float OVERFILLED_PERCENT = 0.75F;
    public static final float SPAWN_ARTRON_BASE = 1000,
        SPAWN_ARTRON_EXTRA = 3000;
    public static final int MC_HOUR = 1000;
    public static final int RADIUS_PER_THOUSAND = 7;

    final LevelChunk chunk;
    final BlockPos position; //Localized to chunk
    int radius;
    float storedArtron;
    float artronMaxCap = 10000;
    boolean isClosed = false;

    public Rift(LevelChunk chunk, BlockPos pos){
        this.chunk = chunk;
        this.position = pos.immutable();
    }

    public Rift(LevelChunk chunk, CompoundTag tag){
        this(chunk, BlockPos.of(tag.getLong("position")));
        this.deserializeNBT(tag);
    }

    public static Optional<Rift> getFromChunk(@Nullable LevelChunk chunk) {
        if(chunk == null)
            return Optional.empty();

        LazyOptional<IChunkCap> chunkHolder = chunk.getCapability(Capabilities.CHUNK);
        if(chunkHolder.isPresent()){
            IChunkCap c = chunkHolder.orElseThrow(NullPointerException::new);
            return c.getRift().isPresent() && !c.getRift().get().isClosed() ?
                    c.getRift() : Optional.empty();
        }

        return Optional.empty();
    }


    public boolean isIn(BlockPos pos){
        return this.getWorldPos().closerThan(pos, this.getRadius());
    }

    public int getRadius(){
        return Mth.floor(this.storedArtron * RADIUS_PER_THOUSAND);
    }

    public ChunkPos getChunkPos(){
        return this.chunk.getPos();
    }

    public BlockPos getWorldPos(){
        return this.chunk.getPos().getWorldPosition().offset(this.position);
    }

    public boolean isNaturallyFull(){
        return this.storedArtron >= (this.artronMaxCap * OVERFILLED_PERCENT);
    }

    public boolean isCompletelyFull(){
        return this.storedArtron >= this.artronMaxCap;
    }


    public void tick(Level level){
        if(level.isClientSide())
            return;
        if(level.getGameTime() % MC_HOUR == 0){
            if(!this.isNaturallyFull()){
                this.fillArtron(10.0F, false);
            }
        }

        if(this.isCompletelyFull()){
            this.explode();
        }
    }

    /**
     * Called when the rift if completely filled, causing something to spawn
     */
    public void explode(){
        if(!this.chunk.getLevel().isClientSide) {
            MinecraftForge.EVENT_BUS.post(new RiftEvent.Closed(this.chunk.getLevel(), this.chunk.getPos(), this));
            BlockLootTable blockTable = this.chunk.getLevel().registryAccess().registry(JsonRegistries.BLOCK_AS_LOOT).get().get(RIFT_BLOCK_LOOT);
            BlockState state = blockTable.getRandom(chunk.getLevel().random).defaultBlockState();
            chunk.getLevel().setBlock(this.getWorldPos().above(), state, 3);
            this.onClosed(false);
        }
    }

    public void onAdded(){
        this.storedArtron = SPAWN_ARTRON_BASE + chunk.getLevel().random.nextFloat() * SPAWN_ARTRON_EXTRA;
    }

    public void onClosed(boolean dueToDrain){
        this.isClosed = true;
    }

    @Override
    public CompoundTag serializeNBT() {
        final CompoundTag tag = new CompoundTag();
        tag.putFloat("artron", this.storedArtron);
        tag.putFloat("max_artron", this.artronMaxCap);
        tag.putLong("position", this.position.asLong());
        tag.putBoolean("is_closed", this.isClosed);
        return tag;
    }

    @Override
    public void deserializeNBT(CompoundTag nbt) {
        this.storedArtron = nbt.getFloat("artron");
        if(nbt.contains("max_artron"))
            this.artronMaxCap = nbt.getFloat("max_artron");
        this.isClosed = nbt.getBoolean("is_closed");
    }

    public boolean isClosed(){
        return this.isClosed;
    }

    @Override
    public float fillArtron(float artronToFill, boolean simulate) {
        final float fill = Math.min(artronToFill, this.artronMaxCap - this.storedArtron);
        if(!simulate) {
            this.storedArtron += fill;
            this.update();
        }
        Tardis.LOGGER.debug("Rift added {} {}", this.storedArtron, this.artronMaxCap);
        return fill;
    }

    @Override
    public float getStoredArtron() {
        return this.storedArtron;
    }

    @Override
    public float getMaxArtron() {
        return this.artronMaxCap;
    }

    public void update(){
        if(!chunk.getLevel().isClientSide()){
            Network.sendToTracking(this.chunk, new RiftArtronMessage(this.getWorldPos(), this.storedArtron));
        }
    }

    public void setArtronDirect(float artron){
        this.storedArtron = Mth.clamp(artron, 0, this.artronMaxCap);
    }

    @Override
    public float takeArtron(float artronToRecieve, boolean simulate) {

        final float pulled = Math.min(artronToRecieve, this.storedArtron);

        if(!simulate && pulled > 0){
            this.storedArtron -= artronToRecieve;
            this.update();
        }

        return pulled;
    }


    /**
     *
     * @param level
     * @param pos
     * @return an {@link Optional} of the rift this block position is in (empty if not in a rift)
     */
    public static Optional<Rift> getClosestRiftIn(Level level, BlockPos pos){

        for(ChunkPos p : ChunkPos.rangeClosed(new ChunkPos(pos), 10).toList()){
            LazyOptional<IChunkCap> chunkHolder = Capabilities.getCap(Capabilities.CHUNK, level.getChunk(p.x, p.z));
            if(chunkHolder.isPresent()){
                final IChunkCap cap = chunkHolder.orElseThrow(NullPointerException::new);
                if(cap.getRift().isPresent()){
                    return cap.getRift();
                }
            }
        }
        return Optional.empty();
    }
}
