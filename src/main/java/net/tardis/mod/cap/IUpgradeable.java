package net.tardis.mod.cap;

import net.tardis.mod.upgrade.Upgrade;
import net.tardis.mod.upgrade.types.UpgradeType;

import java.util.List;
import java.util.Optional;

public interface IUpgradeable<T> {

    List<? extends Upgrade<T>> getUpgrades();
    <U extends Upgrade<T>> Optional<U> getUpgrade(UpgradeType<T, U> type);
}
