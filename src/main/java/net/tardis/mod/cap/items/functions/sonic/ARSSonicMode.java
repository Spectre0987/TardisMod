package net.tardis.mod.cap.items.functions.sonic;

import net.minecraft.nbt.CompoundTag;
import net.minecraft.network.chat.Component;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.server.level.ServerPlayer;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.ItemStack;
import net.minecraftforge.common.util.LazyOptional;
import net.tardis.mod.Tardis;
import net.tardis.mod.cap.Capabilities;
import net.tardis.mod.cap.items.ISonicCapability;
import net.tardis.mod.cap.items.functions.ItemFunctionRegistry;
import net.tardis.mod.cap.items.functions.ItemFunctionType;
import net.tardis.mod.cap.level.ITardisLevel;
import net.tardis.mod.client.gui.datas.GuiDatas;
import net.tardis.mod.network.Network;
import net.tardis.mod.network.packets.OpenGuiDataMessage;
import net.tardis.mod.world.data.ARSRoomLevelData;

public class ARSSonicMode extends AbstractSonicFunction{


    public static final Component NOT_IN_ROOM = Component.translatable("item_function.feedback." + Tardis.MODID + "sonic.ars.not_in_room");
    public static final Component NEEDS_REPAIR_TRANS = Component.translatable(ItemFunctionRegistry.makeDefaultTrans(ItemFunctionRegistry.SONIC_ARS_MOD.get()) + ".feedback.need_repair");

    public ARSSonicMode(ItemFunctionType<ISonicCapability> type, ISonicCapability parent) {
        super(type, parent);
    }

    @Override
    int getPowerCost() {
        return 10;
    }


    @Override
    public void onUse(Player player, ItemStack stack, InteractionHand hand) {
        super.onUse(player, stack, hand);

        if(!validateAndUsePower(player))
            return;

        if(!player.getLevel().isClientSide()){
            LazyOptional<ITardisLevel> tardisHolder = Capabilities.getCap(Capabilities.TARDIS, player.level);
            if(tardisHolder.isPresent()){
                ARSRoomLevelData data = ARSRoomLevelData.getData((ServerLevel) player.level);
                data.getRoomFor(player.getOnPos()).ifPresentOrElse(room -> {
                    room.getRoom(player.level.registryAccess()).ifPresent(r -> {
                        if(r.getParent().isPresent()){
                            player.sendSystemMessage(NEEDS_REPAIR_TRANS);
                        }
                        else{
                            Network.sendTo((ServerPlayer) player, new OpenGuiDataMessage(GuiDatas.SONIC_ARS.create().fromTardis(tardisHolder.orElseThrow(NullPointerException::new))));
                        }
                    });
                }, () -> player.sendSystemMessage(NOT_IN_ROOM));
            }
            else player.sendSystemMessage(NOT_IN_ROOM);
        }
    }

    @Override
    public int timeToRun() {
        return 0;
    }

    @Override
    public boolean shouldDisplay(ISonicCapability parent) {
        return true;
    }

    @Override
    public boolean isInstant() {
        return true;
    }

    @Override
    public void deserializeNBT(CompoundTag compoundTag) {

    }
}
