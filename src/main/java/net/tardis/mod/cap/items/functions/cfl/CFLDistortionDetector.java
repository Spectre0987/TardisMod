package net.tardis.mod.cap.items.functions.cfl;

import net.minecraft.core.BlockPos;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.network.chat.Component;
import net.minecraft.network.chat.Style;
import net.minecraft.server.level.ServerPlayer;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.level.ChunkPos;
import net.minecraft.world.phys.Vec3;
import net.tardis.mod.Tardis;
import net.tardis.mod.cap.Capabilities;
import net.tardis.mod.cap.items.ICFLTool;
import net.tardis.mod.cap.items.functions.ItemFunction;
import net.tardis.mod.cap.items.functions.ItemFunctionRegistry;
import net.tardis.mod.cap.items.functions.ItemFunctionType;
import net.tardis.mod.helpers.Helper;
import net.tardis.mod.helpers.WorldHelper;
import net.tardis.mod.misc.SpaceTimeCoord;
import net.tardis.mod.network.Network;
import net.tardis.mod.network.packets.ItemTrackingCoordMessage;
import net.tardis.mod.tags.BlockTags;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class CFLDistortionDetector extends ItemFunction<ICFLTool> implements ITrackingFunction{

    public static final Component NAME = Component.translatable(ItemFunctionRegistry.makeDefaultTrans(ItemFunctionRegistry.CFL_DETECT_DISTORTION.get()));
    public static final Component FEEDBACK = Component.translatable("item_function.feedback." + Tardis.MODID + ".cfl_distortion");
    public static final Component TRACKING_TITLE = Component.translatable(ItemFunctionRegistry.makeDefaultTrans(ItemFunctionRegistry.CFL_DETECT_DISTORTION.get()) + ".tracking_title")
            .withStyle(Style.EMPTY.withColor(Color.darkGray.getRGB()));
    public static final int DETECTION_RANGE = 32;

    private List<DetectionEntry> detectedPos = new ArrayList<>();
    private boolean hasChanged = false;
    private Optional<SpaceTimeCoord> trackingCoord = Optional.empty();

    public CFLDistortionDetector(ItemFunctionType<ICFLTool> type, ICFLTool parent) {
        super(type, parent);
    }

    @Override
    public void doTickAction(Player player) {

        //Only search for new distortions if there are none left in range for performance
        if(this.detectedPos.isEmpty()){
            //Find blocks that cause temporal distortions
            if(player.tickCount % (20 * 15) == 0){

                this.detectedPos.addAll(BlockPos.betweenClosedStream(WorldHelper.getCenteredAABB(player.getOnPos(), DETECTION_RANGE))
                        .filter(p -> player.getLevel().getBlockState(p).is(BlockTags.TEMPORAL_DISTORTION))
                        .map(p -> new DetectionEntry(p, DETECTION_RANGE))
                        .toList());
            }

            //Find Rifts
            if(player.tickCount % (20 * 20) == 0){
                ChunkPos.rangeClosed(new ChunkPos(player.getOnPos()), 8).forEach(p -> {
                    Capabilities.getCap(Capabilities.CHUNK, player.level.getChunk(p.x, p.z)).ifPresent(chunk -> {
                        if(chunk.getRift().isPresent()){
                            this.detectedPos.add(new DetectionEntry(chunk.getRift().get().getWorldPos(), 8 * 16));
                        }
                    });
                });
            }
        }

        //Remove distortions that are out of range
        if(this.detectedPos.removeIf(pos -> pos.outOfRange(player.getOnPos()))){
            this.hasChanged = true;
            //Remove tracking if this detection entry has been removed
            if(this.trackingCoord.isPresent() && !this.detectedPos.contains(this.trackingCoord.get().getPos())){
                this.setTrackingPos(null);
            }
        }

        if(this.trackingCoord.isEmpty() && !this.detectedPos.isEmpty()){
            this.setTrackingPos(new SpaceTimeCoord(player.level.dimension(), this.detectedPos.get(0).pos()));
        }

        //Display message
        if(!player.level.isClientSide() && !this.detectedPos.isEmpty() && player.tickCount % (5 * 20) == 0){
            player.sendSystemMessage(FEEDBACK);
        }

        if(this.hasChanged){
            this.hasChanged = false;
            this.updateClient(player);
        }

    }

    public void updateClient(Player player){
        InteractionHand hand = player.getMainHandItem() == this.getParent().getItem() ?
                InteractionHand.MAIN_HAND :
                    (player.getOffhandItem() == this.getParent().getItem() ? InteractionHand.OFF_HAND : null);

        if(hand == null){
            return;
        }

        if(player != null && !player.level.isClientSide()){
            Network.sendTo((ServerPlayer) player, new ItemTrackingCoordMessage(hand, this.trackingCoord));
        }
    }

    @Override
    public int timeToRun() {
        return -1;
    }

    @Override
    public boolean shouldDisplay(ICFLTool parent) {
        return true;
    }

    @Override
    public boolean isInstant() {
        return false;
    }

    @Override
    public Component getDisplayName() {
        return NAME;
    }

    @Override
    public void deserializeNBT(CompoundTag nbt) {

    }

    @Override
    public Component getTrackingTypeTitle() {
        return TRACKING_TITLE;
    }

    @Override
    public Optional<SpaceTimeCoord> getTrackingPos() {
        return this.trackingCoord;
    }

    @Override
    public void setTrackingPos(SpaceTimeCoord coord) {
        if(coord == null){
            this.trackingCoord = Optional.empty();
        }
        else this.trackingCoord = Optional.of(coord);

    }

    public record DetectionEntry(BlockPos pos, int dist){
        public boolean outOfRange(BlockPos pos) {
            return !this.pos().closerThan(pos, dist());
        }
    }
}
