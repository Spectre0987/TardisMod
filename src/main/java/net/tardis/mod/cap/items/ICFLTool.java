package net.tardis.mod.cap.items;

import net.minecraft.nbt.CompoundTag;
import net.minecraft.resources.ResourceKey;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResult;
import net.minecraft.world.InteractionResultHolder;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.context.UseOnContext;
import net.minecraft.world.level.Level;
import net.minecraftforge.common.util.INBTSerializable;
import net.tardis.mod.cap.IHaveItemFunctions;
import net.tardis.mod.cap.items.functions.cfl.CFLFunctionType;

import java.util.Optional;

public interface ICFLTool extends INBTSerializable<CompoundTag>, IHaveItemFunctions<ICFLTool, CFLFunctionType> {

    Optional<ResourceKey<Level>> getBoundTARDISKey();

    void tick(Player player);

    InteractionResult useOn(UseOnContext context);

    void attuneTo(ResourceKey<Level> dimension);
    InteractionResultHolder<ItemStack> use(Level world, Player p, InteractionHand hand);

    boolean isPointedAtTarget();

    ItemStack getItem();
}
