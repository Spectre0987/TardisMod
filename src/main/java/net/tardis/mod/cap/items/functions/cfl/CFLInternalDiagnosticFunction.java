package net.tardis.mod.cap.items.functions.cfl;

import net.minecraft.nbt.CompoundTag;
import net.minecraft.network.chat.Component;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.server.level.ServerPlayer;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.entity.player.Player;
import net.tardis.mod.cap.Capabilities;
import net.tardis.mod.cap.items.ICFLTool;
import net.tardis.mod.cap.items.functions.ItemFunction;
import net.tardis.mod.cap.items.functions.ItemFunctionType;
import net.tardis.mod.client.gui.datas.GuiDatas;
import net.tardis.mod.network.Network;
import net.tardis.mod.network.packets.OpenGuiDataMessage;

public class CFLInternalDiagnosticFunction extends ItemFunction<ICFLTool> {

    public static final Component name = Component.translatable("item_functions.tardis.cfl.internal_diagnostic");

    public CFLInternalDiagnosticFunction(ItemFunctionType<ICFLTool> type, ICFLTool tool) {
        super(type, tool);
    }

    @Override
    public boolean isInstant() {
        return true;
    }

    @Override
    public Component getDisplayName() {
        return name;
    }

    @Override
    public int timeToRun() {
        return 0;
    }

    @Override
    public void onSelected(Player player, InteractionHand hand) {

        if(!player.level.isClientSide()){
            this.getParent().getBoundTARDISKey().ifPresent(tardisKey -> {
                ServerLevel tardisLevel = player.level.getServer().getLevel(tardisKey);
                tardisLevel.getCapability(Capabilities.TARDIS).ifPresent(tardis -> {
                    Network.sendTo((ServerPlayer) player, new OpenGuiDataMessage(
                            GuiDatas.DIAG_TARDIS_INFO.create()
                                .set(tardis)
                    ));
                });
            });
        }

    }

    @Override
    public boolean shouldDisplay(ICFLTool parent) {
        return parent.getBoundTARDISKey().isPresent();
    }


    @Override
    public boolean onStart() {
        return true;
    }

    @Override
    public void onEnd(boolean wasCanceled) {

    }

    @Override
    public void deserializeNBT(CompoundTag nbt) {

    }
}
