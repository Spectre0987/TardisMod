package net.tardis.mod.cap.items;

import net.minecraft.nbt.CompoundTag;
import net.minecraft.resources.ResourceKey;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.level.Level;
import net.minecraftforge.common.util.INBTSerializable;
import net.tardis.mod.misc.SpaceTimeCoord;
import org.jetbrains.annotations.Nullable;

import java.util.Optional;

public interface IRemoteItemCapability extends INBTSerializable<CompoundTag> {


    void flyTo(Player player, InteractionHand hand, SpaceTimeCoord target);

    void attuneTo(@Nullable ResourceKey<Level> tardis);

    Optional<ResourceKey<Level>> getBoundTardis();
}
