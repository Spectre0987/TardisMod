package net.tardis.mod.cap.items;

import net.minecraft.core.Direction;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.nbt.Tag;
import net.minecraft.world.item.ItemStack;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.capabilities.ForgeCapabilities;
import net.minecraftforge.common.capabilities.ICapabilitySerializable;
import net.minecraftforge.common.util.LazyOptional;
import net.minecraftforge.energy.EnergyStorage;
import net.minecraftforge.energy.IEnergyStorage;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

/**
 * Used for TM Items that only need to keep track of their forge energy
 */
public class BasicEnergyCapProvider implements ICapabilitySerializable<CompoundTag> {

    private final EnergyStorage instance;
    private final LazyOptional<IEnergyStorage> holder;
    private final ItemStack stack;

    public BasicEnergyCapProvider(ItemStack stack, EnergyStorage battery){
        this.stack = stack;
        this.instance = battery;
        this.holder = LazyOptional.of(() -> this.instance);
    }

    @Override
    public @NotNull <T> LazyOptional<T> getCapability(@NotNull Capability<T> cap, @Nullable Direction side) {
        return cap == ForgeCapabilities.ENERGY ? this.holder.cast() : LazyOptional.empty();
    }

    @Override
    public CompoundTag serializeNBT() {
        this.stack.getOrCreateTag().put("energy", this.instance.serializeNBT());
        return this.stack.getTag();
    }

    @Override
    public void deserializeNBT(CompoundTag tag) {
        if(tag.contains("energy"))
            this.instance.deserializeNBT(tag.get("energy"));
    }
}
