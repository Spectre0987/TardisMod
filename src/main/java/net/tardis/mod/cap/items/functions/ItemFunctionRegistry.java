package net.tardis.mod.cap.items.functions;

import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.Items;
import net.minecraft.world.level.block.Blocks;
import net.minecraftforge.common.Tags;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.IForgeRegistry;
import net.minecraftforge.registries.RegistryBuilder;
import net.minecraftforge.registries.RegistryObject;
import net.tardis.mod.Constants;
import net.tardis.mod.Tardis;
import net.tardis.mod.cap.items.functions.sonic.*;
import net.tardis.mod.registry.BlockRegistry;
import net.tardis.mod.cap.Capabilities;
import net.tardis.mod.cap.items.functions.cfl.*;
import net.tardis.mod.helpers.Helper;
import net.tardis.mod.registry.ItemRegistry;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Supplier;

public class ItemFunctionRegistry {

    public static final DeferredRegister<ItemFunctionType<?>> TYPES = DeferredRegister.create(Helper.createRL("item_function"), Tardis.MODID);
    public static final Supplier<IForgeRegistry<ItemFunctionType<?>>> REGISTRY = TYPES.makeRegistry(RegistryBuilder::new);


    //CFL
    public static final RegistryObject<CFLFunctionType> CFL_INTERNAL_DIAG = TYPES.register("cfl_internal", () -> new CFLFunctionType(
            stack -> stack.getCapability(Capabilities.CFL).isPresent(),
            CFLInternalDiagnosticFunction::new
    ));

    public static final RegistryObject<CFLFunctionType> CFL_DETECT_DISTORTION = TYPES.register("cfl_detect_distortion", () -> new CFLFunctionType(
            stack -> stack.getCapability(Capabilities.CFL).isPresent(),
            CFLDistortionDetector::new
    ));

    public static final RegistryObject<CFLFunctionType> CFL_TRACK_TARDIS = TYPES.register("cfl_track_tardis", () ->
                new CFLFunctionType(
                        stack -> stack.getCapability(Capabilities.CFL).isPresent(),
                        CFLTrackTardis::new
                )
            );
    public static final RegistryObject<CFLFunctionType> CFL_SUBSYSTEM_INFO = TYPES.register("cfl_subsystem_info", () ->
            new CFLFunctionType(
                    stack -> stack.getCapability(Capabilities.CFL).isPresent(),
                    CFLSubsystemInfoFunc::new
            )
        );

    //Sonics
    public static final RegistryObject<SonicFunctionType> SONIC_BLOCK_MODE = TYPES.register("sonic_block", () ->
                new SonicFunctionType(stack -> stack.getCapability(Capabilities.SONIC).isPresent(),
                        BlockSonicFunction::new,
                        new ItemStack(Blocks.GRASS_BLOCK)
                )
            );

    public static final RegistryObject<SonicFunctionType> SONIC_ARS_MOD = TYPES.register("sonic_ars", () ->
                new SonicFunctionType(stack -> stack.getItem() == ItemRegistry.SONIC.get(),
                        ARSSonicMode::new,
                        new ItemStack(BlockRegistry.ARS_PANEL.get())
                )
            );
    public static final RegistryObject<SonicFunctionType> SONIC_TARDIS_DEMAT = TYPES.register("sonic_demat", () ->
                new SonicFunctionType(stack -> stack.getItem() == ItemRegistry.UPGRADE_SONIC_DEMAT.get(),
                        SonicTardisDematFunction::new,
                        new ItemStack(BlockRegistry.STEAM_BROKEN_EXTERIOR.get())
                )
            );
    public static final RegistryObject<SonicFunctionType> SONIC_TEX_VAR = TYPES.register("sonic_tex_var", () ->
                new SonicFunctionType(stack -> stack.getItem() == ItemRegistry.SONIC.get(),
                        TexVariantSonicMode::new,
                        new ItemStack(Items.BLUE_DYE)
                )
            );

    public static <T extends ItemFunctionType<?>> List<T> getAllFor(Class<T> typeClass){
        List<T> list = new ArrayList<>();
        for(ItemFunctionType<?> type : REGISTRY.get()){
            if(typeClass.isInstance(type)){
                list.add((T)type);
            }
        }
        return list;
    }

    public static <T extends ItemFunctionType<?>> String makeDefaultTrans(T val){
        return Constants.Translation.makeGenericTranslation(REGISTRY.get(), val);
    }


}
