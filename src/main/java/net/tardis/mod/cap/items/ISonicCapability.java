package net.tardis.mod.cap.items;

import net.minecraft.nbt.CompoundTag;
import net.minecraft.resources.ResourceKey;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResultHolder;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.context.UseOnContext;
import net.minecraft.world.level.Level;
import net.minecraftforge.common.util.INBTSerializable;
import net.minecraftforge.energy.IEnergyStorage;
import net.minecraftforge.items.ItemStackHandler;
import net.tardis.mod.cap.IHaveItemFunctions;
import net.tardis.mod.cap.IUpgradeable;
import net.tardis.mod.cap.items.functions.sonic.SonicFunctionType;
import net.tardis.mod.registry.SonicPartRegistry;
import net.tardis.mod.sonic_screwdriver.SonicPartSlot;

import javax.annotation.Nullable;
import java.util.Optional;

public interface ISonicCapability extends INBTSerializable<CompoundTag>, IHaveItemFunctions<ISonicCapability, SonicFunctionType>, IUpgradeable<ISonicCapability>, IEnergyStorage {


    SonicPartRegistry.SonicPartType getSonicPart(SonicPartSlot slot);
    void setSonicPart(SonicPartSlot slot, SonicPartRegistry.SonicPartType point);

    InteractionResultHolder<ItemStack> onUse(Level level, Player player, InteractionHand hand);

    void useOn(UseOnContext pContext);

    ItemStack getItem();

    ItemStackHandler getUpgradeInv();

    Optional<ResourceKey<Level>> getBoundTARDIS();
    void setBoundTARDIS(@Nullable ResourceKey<Level> tardis);
}
