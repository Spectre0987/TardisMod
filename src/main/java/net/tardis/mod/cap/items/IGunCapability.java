package net.tardis.mod.cap.items;

import net.minecraft.nbt.CompoundTag;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResult;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.ItemStack;
import net.minecraftforge.common.util.INBTSerializable;
import net.minecraftforge.energy.IEnergyStorage;

public interface IGunCapability extends IEnergyStorage, INBTSerializable<CompoundTag> {

    ItemStack getItem();
    int shotsBeforeCooldown();
    int cooldownTime();

    /**
     *
     * @param shooter
     * @param hand
     * @return True if this gun has fired
     */
    boolean shoot(Player shooter, InteractionHand hand);

}
