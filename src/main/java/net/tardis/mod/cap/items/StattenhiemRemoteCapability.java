package net.tardis.mod.cap.items;

import net.minecraft.core.registries.Registries;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.network.chat.Component;
import net.minecraft.resources.ResourceKey;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.Level;
import net.minecraftforge.energy.IEnergyStorage;
import net.tardis.mod.Constants;
import net.tardis.mod.Tardis;
import net.tardis.mod.cap.Capabilities;
import net.tardis.mod.cap.level.ITardisLevel;
import net.tardis.mod.helpers.Helper;
import net.tardis.mod.misc.SpaceTimeCoord;
import net.tardis.mod.misc.tardis.FlightCourse;
import net.tardis.mod.registry.ControlRegistry;
import net.tardis.mod.registry.SubsystemRegistry;
import net.tardis.mod.registry.UpgradeRegistry;

import java.util.Optional;

public class StattenhiemRemoteCapability implements IRemoteItemCapability, IEnergyStorage{

    public static final Component ERROR_NO_STABILIZERS = makeError("no_stabilizers");
    public static final Component ERROR_NO_REMOTE_CIRCUIT = makeError("no_remote_upgrade");
    public static final Component ERROR_NO_POWER = makeError("no_power");
    public static final int powerRequired = 100;
    public Optional<ResourceKey<Level>> boundTardis = Optional.empty();

    final ItemStack item;

    private int energy = 0;

    public StattenhiemRemoteCapability(ItemStack item){
        this.item = item;
    }

    public ItemStack getItem(){
        return this.item;
    }
    @Override
    public CompoundTag serializeNBT() {
        final CompoundTag tag = new CompoundTag();
        this.boundTardis.ifPresent(key -> tag.putString("tardis", key.location().toString()));
        tag.putInt("energy", this.energy);
        return tag;
    }

    @Override
    public void deserializeNBT(CompoundTag tag) {
        if(tag.contains("tardis")){
            this.boundTardis = Optional.of(ResourceKey.create(Registries.DIMENSION, new ResourceLocation(tag.getString("tardis"))));
        }
        this.energy = tag.getInt("energy");
    }

    @Override
    public void attuneTo(ResourceKey<Level> tardisKey){
        this.boundTardis = Helper.nullableToOptional(tardisKey);
    }

    @Override
    public Optional<ResourceKey<Level>> getBoundTardis() {
        return this.boundTardis;
    }

    @Override
    public void flyTo(Player player, InteractionHand hand, SpaceTimeCoord target){
        this.boundTardis.ifPresentOrElse(tardisKey -> {
            if(player.getLevel() instanceof ServerLevel serverLevel){

                //Check Power and use power
                if(this.energy < powerRequired) {
                    player.sendSystemMessage(ERROR_NO_POWER);
                    return;
                }

                ServerLevel tardisLevel = serverLevel.getLevel().getServer().getLevel(tardisKey);
                Capabilities.getCap(Capabilities.TARDIS, tardisLevel).ifPresent(tardis -> {
                    Component errorMessage = flyTardis(tardis, target);
                    if(errorMessage != null){
                        player.sendSystemMessage(errorMessage);
                    }
                    else {
                        this.energy -= powerRequired;
                        player.setItemInHand(hand, item);
                    }
                });
            }
        }, () -> player.sendSystemMessage(Constants.Translation.MUST_BE_ATTUNED));
    }

    public static Component flyTardis(ITardisLevel tardis, SpaceTimeCoord target){
        if(!SubsystemRegistry.STABILIZERS.get().canBeUsed(tardis)){
            return ERROR_NO_STABILIZERS;
        }
        if(!UpgradeRegistry.TARDIS_REMOTE.get().canBeUsed(tardis)){
            return ERROR_NO_REMOTE_CIRCUIT;
        }

        tardis.setDestination(target);
        tardis.setFlightCourse(new FlightCourse().addPoint(tardis, target));
        tardis.getControlDataOrCreate(ControlRegistry.STABILIZERS.get()).set(true);
        tardis.getControlDataOrCreate(ControlRegistry.FACING.get()).set(target.getDirection());
        tardis.getControlDataOrCreate(ControlRegistry.THROTTLE.get()).set(1.0F);
        tardis.getControlDataOrCreate(ControlRegistry.HANDBRAKE.get()).set(false);
        if(!tardis.isInVortex())
            tardis.takeoff();
        return null;
    }

    public static Component makeError(String error){
        return Component.translatable(Tardis.MODID + ".remote.errors" + error);
    }

    @Override
    public int receiveEnergy(int maxReceive, boolean simulate) {
        final int room = this.getMaxEnergyStored() - this.energy;

        final int amountToTake = Math.min(room, maxReceive);
        if(!simulate)
            this.energy += amountToTake;
        return amountToTake;

    }

    @Override
    public int extractEnergy(int maxExtract, boolean simulate) {

        final int taken = Math.min(maxExtract, this.getEnergyStored());
        if(!simulate)
            this.energy -= taken;

        return taken;
    }

    @Override
    public int getEnergyStored() {
        return this.energy;
    }

    @Override
    public int getMaxEnergyStored() {
        return 1000;
    }

    @Override
    public boolean canExtract() {
        return false;
    }

    @Override
    public boolean canReceive() {
        return true;
    }
}
