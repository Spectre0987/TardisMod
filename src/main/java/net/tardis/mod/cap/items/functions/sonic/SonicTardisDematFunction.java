package net.tardis.mod.cap.items.functions.sonic;

import net.minecraft.nbt.CompoundTag;
import net.minecraft.resources.ResourceKey;
import net.minecraft.sounds.SoundSource;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.Level;
import net.tardis.mod.Constants;
import net.tardis.mod.cap.Capabilities;
import net.tardis.mod.cap.items.ISonicCapability;
import net.tardis.mod.cap.items.functions.ItemFunctionType;
import net.tardis.mod.network.Network;
import net.tardis.mod.network.packets.PlayMovingSoundMessage;
import net.tardis.mod.registry.ControlRegistry;
import net.tardis.mod.registry.UpgradeRegistry;
import net.tardis.mod.sound.SoundRegistry;

public class SonicTardisDematFunction extends AbstractSonicFunction {

    public SonicTardisDematFunction(ItemFunctionType<ISonicCapability> type, ISonicCapability parent) {
        super(type, parent);
    }

    @Override
    int getPowerCost() {
        return 40;
    }

    @Override
    public int timeToRun() {
        return -1;
    }

    @Override
    public boolean shouldDisplay(ISonicCapability parent) {
        return UpgradeRegistry.SONIC_DEMAT_TARDIS.get().canBeUsed(parent);
    }

    @Override
    public boolean isInstant() {
        return true;
    }

    @Override
    public void onUse(Player player, ItemStack stack, InteractionHand hand) {

        if(!validateAndUsePower(player))
            return;

        if(!this.getParent().getBoundTARDIS().isPresent()){
            player.sendSystemMessage(Constants.Translation.MUST_BE_ATTUNED);
            return;
        }
        ResourceKey<Level> tardisKey = this.getParent().getBoundTARDIS().get();
        if(!player.getLevel().isClientSide()){
            Capabilities.getCap(Capabilities.TARDIS, player.getServer().getLevel(tardisKey)).ifPresent(tardis -> {
                if(tardis.isInVortex())
                    tardis.initLanding(false);
                else {
                    tardis.getControlDataOrCreate(ControlRegistry.THROTTLE.get()).set(1.0F);
                    tardis.getControlDataOrCreate(ControlRegistry.HANDBRAKE.get()).set(false);
                    tardis.getControlDataOrCreate(ControlRegistry.STABILIZERS.get()).set(false);
                    tardis.setDestination(tardis.getLocation());
                    tardis.takeoff();
                }
                Network.sendToTracking(player, new PlayMovingSoundMessage(SoundRegistry.SONIC_INTERACT.get(), SoundSource.AMBIENT, player.getId(), 1.0F, false));
            });
        }

    }

    @Override
    public void deserializeNBT(CompoundTag nbt) {

    }
}
