package net.tardis.mod.cap.chunks;

import net.minecraft.core.BlockPos;
import net.minecraft.core.SectionPos;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.nbt.ListTag;
import net.minecraft.nbt.LongTag;
import net.minecraft.nbt.Tag;
import net.minecraft.world.level.ChunkPos;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.LightLayer;
import net.minecraft.world.level.chunk.LevelChunk;

import java.util.ArrayList;
import java.util.List;

public class TardisChunkCap implements ITardisChunkCap{

    private final LevelChunk chunk;
    private final List<BlockPos> lightingPos = new ArrayList<>();

    public TardisChunkCap(LevelChunk chunk){
        this.chunk = chunk;
    }

    @Override
    public void addTardisLightBlock(BlockPos pos) {
        this.lightingPos.add(pos);
    }

    @Override
    public void updateLighting() {

        this.chunk.getLights().forEach(chunk.getLevel().getLightEngine()::checkBlock);

    }

    @Override
    public CompoundTag serializeNBT() {
        final CompoundTag tag = new CompoundTag();

        final ListTag positionList = new ListTag();
        for(BlockPos pos : this.lightingPos){
            positionList.add(LongTag.valueOf(pos.asLong()));
        }
        tag.put("light_poses", positionList);



        return tag;
    }

    @Override
    public void deserializeNBT(CompoundTag tag) {
        this.lightingPos.clear();

        final ListTag positionList = tag.getList("light_poses", Tag.TAG_LONG);
        for(int i = 0; i < positionList.size(); ++i){
            this.lightingPos.add(BlockPos.of(((LongTag)positionList.get(i)).getAsLong()));
        }
    }
}
