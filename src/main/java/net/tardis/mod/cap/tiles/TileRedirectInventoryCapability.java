package net.tardis.mod.cap.tiles;

import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraftforge.common.util.LazyOptional;
import net.minecraftforge.items.IItemHandler;
import net.minecraftforge.items.ItemStackHandler;
import net.tardis.mod.cap.Capabilities;
import net.tardis.mod.cap.level.ITardisLevel;
import org.jetbrains.annotations.NotNull;

import java.util.function.Function;

public class TileRedirectInventoryCapability<T extends BlockEntity> implements IItemHandler {

    final T tile;
    final Function<T, ItemStackHandler> invGetter;

    public TileRedirectInventoryCapability(T entity, Function<T, ItemStackHandler> invGetter){
        this.tile = entity;
        this.invGetter = invGetter;
    }

    @Override
    public int getSlots() {
        final ItemStackHandler inv = getInv();
        if(inv == null)
            return 0;
        return inv.getSlots();
    }

    @Override
    public @NotNull ItemStack getStackInSlot(int slot) {
        final ItemStackHandler inv = getInv();
        if(inv == null)
            return ItemStack.EMPTY;
        return inv.getStackInSlot(slot);
    }

    @Override
    public @NotNull ItemStack insertItem(int slot, @NotNull ItemStack stack, boolean simulate) {
        final ItemStackHandler inv = getInv();
        if(inv == null)
            return ItemStack.EMPTY;
        return inv.insertItem(slot, stack, simulate);
    }

    @Override
    public @NotNull ItemStack extractItem(int slot, int amount, boolean simulate) {
        final ItemStackHandler inv = getInv();
        if(inv == null)
            return ItemStack.EMPTY;
        return inv.extractItem(slot, amount, simulate);
    }

    @Override
    public int getSlotLimit(int slot) {
        return 64;
    }

    @Override
    public boolean isItemValid(int slot, @NotNull ItemStack stack) {
        final ItemStackHandler inv = getInv();
        if(inv == null)
            return false;
        return inv.isItemValid(slot, stack);
    }

    public ItemStackHandler getInv(){
        return this.invGetter.apply(this.tile);
    }
}
