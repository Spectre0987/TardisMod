package net.tardis.mod.cap.level;

import net.minecraft.nbt.CompoundTag;
import net.minecraft.resources.ResourceKey;
import net.minecraft.world.level.Level;
import net.minecraftforge.common.MinecraftForge;
import net.tardis.api.events.TardisEvent;
import net.tardis.mod.misc.SpaceTimeCoord;
import net.tardis.mod.misc.tardis.ArtronTrail;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

public class GeneralWorldCap implements IGeneralLevel{

    public static final int ARTRON_TRAIL_EXPIRE = 12000; // 10 minutes

    private Map<ResourceKey<Level>, ArtronTrail> trails = new HashMap<>();
    private final Level level;

    public GeneralWorldCap(Level level){
        this.level = level;
    }

    @Override
    public void addArtronTrail(ITardisLevel tardis) {
        if(MinecraftForge.EVENT_BUS.post(new TardisEvent.TardisAddArtronTrailEvent(tardis))){
            return; //If event was canceled
        }
        final ResourceKey<Level> tardisKey = tardis.getLevel().dimension();
        this.trails.put(tardisKey, new ArtronTrail(tardisKey, level.getGameTime(), tardis.getDestination()));
    }

    @Override
    public Optional<SpaceTimeCoord> getArtronTrail(ResourceKey<Level> tardis) {
        if(this.trails.containsKey(tardis)){

            //If trail is expired, remove it
            if(this.trails.get(tardis).timeCreated() + ARTRON_TRAIL_EXPIRE > this.level.getGameTime()){
                this.trails.remove(tardis);
                return Optional.empty();
            }

            return Optional.of(this.trails.get(tardis).destination());
        }
        return Optional.empty();
    }

    @Override
    public void tick() {

    }

    @Override
    public CompoundTag serializeNBT() {
        final CompoundTag tag = new CompoundTag();

        return tag;
    }

    @Override
    public void deserializeNBT(CompoundTag tag) {

    }
}
