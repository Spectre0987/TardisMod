package net.tardis.mod.compat.jei;

import com.mojang.blaze3d.vertex.PoseStack;
import mezz.jei.api.constants.VanillaTypes;
import mezz.jei.api.gui.builder.IRecipeLayoutBuilder;
import mezz.jei.api.gui.drawable.IDrawable;
import mezz.jei.api.gui.ingredient.IRecipeSlotsView;
import mezz.jei.api.helpers.IGuiHelper;
import mezz.jei.api.recipe.IFocusGroup;
import mezz.jei.api.recipe.RecipeIngredientRole;
import mezz.jei.api.recipe.RecipeType;
import mezz.jei.api.recipe.category.IRecipeCategory;
import net.minecraft.network.chat.Component;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.item.ItemStack;
import net.tardis.mod.Tardis;
import net.tardis.mod.registry.BlockRegistry;
import net.tardis.mod.helpers.Helper;
import net.tardis.mod.recipes.AlembicRecipe;

public class AlembicJeiCategory implements IRecipeCategory<AlembicRecipe> {

    public static final RecipeType TYPE = RecipeType.create(Tardis.MODID, "alembic", AlembicRecipe.class);
    public static final Component TITLE = Jei.makeRecipeCategoryTitle(Helper.createRL("alembic"));
    public static final ResourceLocation TEXTURE = Helper.createRL("textures/screens/containers/alembic.png");

    final IGuiHelper gui;


    public AlembicJeiCategory(IGuiHelper gui){
        this.gui = gui;
    }

    @Override
    public RecipeType<AlembicRecipe> getRecipeType() {
        return TYPE;
    }

    @Override
    public Component getTitle() {
        return TITLE;
    }

    @Override
    public IDrawable getBackground() {
        return this.gui.createDrawable(TEXTURE, 43, 5, 89, 63);
    }

    @Override
    public int getWidth() {
        return IRecipeCategory.super.getWidth();
    }

    @Override
    public int getHeight() {
        return IRecipeCategory.super.getHeight();
    }

    @Override
    public IDrawable getIcon() {
        return this.gui.createDrawableIngredient(VanillaTypes.ITEM_STACK, new ItemStack(BlockRegistry.ALEMBIC.get()));
    }

    @Override
    public void setRecipe(IRecipeLayoutBuilder layout, AlembicRecipe recipe, IFocusGroup group) {
        layout.addSlot(RecipeIngredientRole.INPUT, 2, 4).addFluidStack(recipe.getCraftingFluid().getFluid(), recipe.getCraftingFluid().getAmount())
                .setFluidRenderer(1000, true, 11, 54)
                .setOverlay(this.gui.createDrawable(TEXTURE, 193, 18, 11, 54), 0, 0);
        layout.addSlot(RecipeIngredientRole.INPUT, 19, 6).addIngredients(recipe.getIngredient());
        layout.addSlot(RecipeIngredientRole.OUTPUT, 75, 4).addFluidStack(recipe.getResult().getFluid(), recipe.getResult().getAmount())
                .setFluidRenderer(1000, true, 11, 54)
                .setOverlay(this.gui.createDrawable(TEXTURE, 180, 18, 11, 55), 0, 0);
    }

    @Override
    public void draw(AlembicRecipe recipe, IRecipeSlotsView recipeSlotsView, PoseStack stack, double mouseX, double mouseY) {
        IRecipeCategory.super.draw(recipe, recipeSlotsView, stack, mouseX, mouseY);
    }
}
