package net.tardis.mod.compat.jei;

import com.mojang.blaze3d.vertex.PoseStack;
import mezz.jei.api.constants.VanillaTypes;
import mezz.jei.api.gui.builder.IRecipeLayoutBuilder;
import mezz.jei.api.gui.builder.IRecipeSlotBuilder;
import mezz.jei.api.gui.drawable.IDrawable;
import mezz.jei.api.gui.ingredient.IRecipeSlotsView;
import mezz.jei.api.helpers.IGuiHelper;
import mezz.jei.api.recipe.IFocusGroup;
import mezz.jei.api.recipe.RecipeIngredientRole;
import mezz.jei.api.recipe.RecipeType;
import mezz.jei.api.recipe.category.IRecipeCategory;
import net.minecraft.client.Minecraft;
import net.minecraft.network.chat.Component;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.Items;
import net.minecraft.world.item.crafting.Ingredient;
import net.tardis.mod.Tardis;
import net.tardis.mod.registry.BlockRegistry;
import net.tardis.mod.helpers.Helper;
import net.tardis.mod.misc.ToolType;
import net.tardis.mod.recipes.DraftingTableRecipe;
import net.tardis.mod.registry.JsonRegistries;

import java.util.Map;

public class DraftingTableCategory implements IRecipeCategory<DraftingTableRecipe> {

    public static final Component TITLE = Component.translatable("jei." + Tardis.MODID + ".category.drafting");
    public static final RecipeType<DraftingTableRecipe> TYPE = RecipeType.create(Tardis.MODID, "drafting", DraftingTableRecipe.class);

    public final IGuiHelper guiHelper;

    public DraftingTableCategory(IGuiHelper helper){
        this.guiHelper = helper;
    }

    @Override
    public RecipeType getRecipeType() {
        return TYPE;
    }

    @Override
    public Component getTitle() {
        return TITLE;
    }

    @Override
    public IDrawable getBackground() {
        return guiHelper.createDrawable(Helper.createRL("textures/screens/containers/trds_table.png"), 12, 0, 141, 70);
    }

    @Override
    public IDrawable getIcon() {
        return this.guiHelper.createDrawableIngredient(VanillaTypes.ITEM_STACK, new ItemStack(BlockRegistry.DRAFTING_TABLE.get()));
    }

    @Override
    public void setRecipe(IRecipeLayoutBuilder builder, DraftingTableRecipe recipe, IFocusGroup focuses) {
        final int x = 18, y = 17;
        for(int i = 0; i < 3; ++i){
            for(int j = 0; j < 3; ++j){

                Ingredient ing = Ingredient.of(Items.ACACIA_LOG);
                if(j < recipe.pattern.size()){ //If we have this row
                    char[] line = recipe.pattern.get(j).toCharArray();
                    if(i < line.length){
                        Ingredient localIng = recipe.key.getOrDefault(line[i], Ingredient.EMPTY);
                        ing = localIng;
                    }
                }

                builder.addSlot(RecipeIngredientRole.INPUT, x + (i * 18), y + (j * 18))
                        .addIngredients(ing);
            }
        }
        builder.addSlot(RecipeIngredientRole.OUTPUT, 117, 23)
                .addItemStack(recipe.getResultItem(Minecraft.getInstance().level.registryAccess()));

        final int toolX = 90, toolY = 49;

        int index = 0;
        for(Map.Entry<ToolType, Integer> entry : recipe.tools.entrySet()){
            IRecipeSlotBuilder slot = builder.addSlot(RecipeIngredientRole.RENDER_ONLY, toolX, toolY + (index * 18));

            slot.addItemStacks(Minecraft.getInstance().level.registryAccess().registryOrThrow(JsonRegistries.TOOLS)
                    .stream()
                    .filter(t -> t.type() == entry.getKey())
                    .map(t -> new ItemStack(t.item()))
                    .toList()
            );
            ++index;
        }
    }

    @Override
    public void draw(DraftingTableRecipe recipe, IRecipeSlotsView recipeSlotsView, PoseStack stack, double mouseX, double mouseY) {
        IRecipeCategory.super.draw(recipe, recipeSlotsView, stack, mouseX, mouseY);
        final int x = 90, y = 55, color = 0x000000;
        int index = 0;
        for(Map.Entry<ToolType, Integer> types : recipe.tools.entrySet()){
            Minecraft.getInstance().font.draw(stack, Component.literal((types.getValue() / 20) + "s"), x + 18, (y - 4) + (index++ * 18), color);
        }
    }

}
