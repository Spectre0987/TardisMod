package net.tardis.mod.compat.jei;

import mezz.jei.api.IModPlugin;
import mezz.jei.api.JeiPlugin;
import mezz.jei.api.registration.IRecipeCategoryRegistration;
import mezz.jei.api.registration.IRecipeRegistration;
import mezz.jei.api.registration.IRecipeTransferRegistration;
import mezz.jei.api.registration.IRuntimeRegistration;
import net.minecraft.client.Minecraft;
import net.minecraft.network.chat.Component;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.item.ItemStack;
import net.minecraftforge.registries.ForgeRegistries;
import net.tardis.mod.Tardis;
import net.tardis.mod.registry.BlockRegistry;
import net.tardis.mod.helpers.Helper;
import net.tardis.mod.registry.ItemRegistry;
import net.tardis.mod.menu.DraftingTableMenu;
import net.tardis.mod.menu.MenuRegistry;
import net.tardis.mod.recipes.RecipeRegistry;
import net.tardis.mod.tags.ItemTags;

@JeiPlugin
public class Jei implements IModPlugin{

    public static final ResourceLocation MAIN_ID = Helper.createRL("main");

    public static final Component DRAFTING_TABLE_INFO = Component.translatable("jei." + Tardis.MODID + "info.drafting_table");

    @Override
    public ResourceLocation getPluginUid() {
        return MAIN_ID;
    }

    @Override
    public void registerCategories(IRecipeCategoryRegistration reg) {
        IModPlugin.super.registerCategories(reg);
        reg.addRecipeCategories(
                new DraftingTableCategory(reg.getJeiHelpers().getGuiHelper()),
                new AlembicJeiCategory(reg.getJeiHelpers().getGuiHelper()),
                new AlembicBottlingRecipeCategory(reg.getJeiHelpers().getGuiHelper()),
                new CraftingQuantiscopeJEIRecipeCategory((reg.getJeiHelpers().getGuiHelper())),
                new ARSEggRecipeCategory(reg.getJeiHelpers().getGuiHelper())
        );
    }

    @Override
    public void registerRecipes(IRecipeRegistration reg) {
        IModPlugin.super.registerRecipes(reg);
        reg.addItemStackInfo(new ItemStack(BlockRegistry.DRAFTING_TABLE.get()), DRAFTING_TABLE_INFO);
        reg.addItemStackInfo(new ItemStack(ItemRegistry.CINNABAR.get()), makeInfoTrans("cinnabar"));

        reg.addRecipes(DraftingTableCategory.TYPE, Minecraft.getInstance().level.getRecipeManager().getAllRecipesFor(RecipeRegistry.DRAFTING_TABLE_TYPE));
        reg.addRecipes(AlembicJeiCategory.TYPE, Minecraft.getInstance().level.getRecipeManager().getAllRecipesFor(RecipeRegistry.ALEMBIC_TYPE));
        reg.addRecipes(AlembicBottlingRecipeCategory.TYPE, Minecraft.getInstance().level.getRecipeManager().getAllRecipesFor(RecipeRegistry.ALEMBIC_BOTTLING_TYPE));
        reg.addRecipes(CraftingQuantiscopeJEIRecipeCategory.TYPE, Minecraft.getInstance().level.getRecipeManager().getAllRecipesFor(RecipeRegistry.QUANTISCOPE_CRAFTING_TYPE));
        reg.addRecipes(ARSEggRecipeCategory.TYPE, ForgeRegistries.ITEMS.tags().getTag(ItemTags.ARS_EGG_ITEMS).stream().map(ARSEggRecipeCategory.DummyARSRecipe::fromItem).toList());

    }

    @Override
    public void registerRecipeTransferHandlers(IRecipeTransferRegistration registration) {
        IModPlugin.super.registerRecipeTransferHandlers(registration);
        registration.addRecipeTransferHandler(DraftingTableMenu.class, MenuRegistry.DRAFTING_TABLE.get(), DraftingTableCategory.TYPE, 0, 9, 9, 36);
    }

    @Override
    public void registerRuntime(IRuntimeRegistration registration) {
        IModPlugin.super.registerRuntime(registration);
    }

    public static Component makeRecipeCategoryTitle(ResourceLocation rl){
        return Component.translatable("jei.category." + rl.getNamespace() + "." + rl.getPath().replaceAll("/", "."));
    }

    public static Component makeInfoTrans(String string){
        return Component.translatable("jei." + Tardis.MODID + ".info." + string);
    }
}
