package net.tardis.mod.blockentities.machines;

import net.minecraft.core.BlockPos;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.ChunkPos;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.block.entity.BlockEntityType;
import net.minecraft.world.level.block.state.BlockState;
import net.tardis.mod.registry.BlockRegistry;
import net.tardis.mod.blockentities.TileRegistry;
import net.tardis.mod.cap.rifts.Rift;
import net.tardis.mod.helpers.WorldHelper;
import net.tardis.mod.resource_listener.server.ItemArtronValueReloader;

import java.util.List;
import java.util.Optional;

/**
 * Absorbs Rift energy and sends it to the collector
 */
public class RiftPylonTile extends BlockEntity{

    public static final int CRYSTAL_RANGE = 16;
    public static final int CRYSTAL_GROW_TIME = 200;

    public Optional<RiftCollectorTile> collector = Optional.empty();


    public RiftPylonTile(BlockEntityType<?> pType, BlockPos pPos, BlockState pBlockState) {
        super(pType, pPos, pBlockState);
    }

    public RiftPylonTile(BlockPos pos, BlockState state){
        this(TileRegistry.RIFT_PYLON.get(), pos, state);
    }

    private Optional<RiftCollectorTile> findCollector(){

        return WorldHelper.getClosest(this.level, this.getBlockPos(), 32, TileRegistry.RIFT_COLLECTOR.get());
    }

    public Optional<RiftCollectorTile> getCollector(boolean search){
        if(this.collector.isPresent()){
            if(this.collector.get().isRemoved()){
                this.collector = this.findCollector();
            }
            return this.collector;
        }
        return search ? this.collector = findCollector() : Optional.empty();
    }

    public void tick() {

        //Do nothing without a valid collector near us
        final Optional<RiftCollectorTile> collectorHolder = this.getCollector(this.getLevel().getGameTime() % 40 == 0);
        //Get the rift we're in
        final ChunkPos cPos = new ChunkPos(this.getBlockPos());
        final Optional<Rift> rift = Rift.getFromChunk(getLevel().getChunk(cPos.x, cPos.z));
        if(rift.isPresent()){
            //Absorb energy and transmit it to a collector
            collectorHolder.ifPresent(collector -> {

                //If we have a rift, pull from it
                if(rift.isPresent()){
                    //Push or pull from rifts, depending
                    if(collector.shouldAcceptArtron()){
                        // If there is enough artron to pull from the rift without closing it, or we don't have safeties engaged, pull artron
                        if(!collector.areSafetiesEngaged() || rift.get().getStoredArtron() > 1.0){

                            //Fill collector from rift
                            float amountCanFill = collector.fillArtron(1.0F, true);
                            collector.fillArtron(rift.get().takeArtron(amountCanFill, false), false);
                        }
                    }
                    else{
                        float collectorArtron = collector.takeArtron(1.0F, true);
                        if(collectorArtron > 0){
                            collector.takeArtron(rift.get().fillArtron(collectorArtron, false), false);
                        }
                    }
                }
            });

            //If no collector grow Xion clusters
            final float artronRequired = ItemArtronValueReloader.ArtronValue.getArtronValue(new ItemStack(BlockRegistry.XION.get())) * 1.25F;
            if(collectorHolder.isEmpty() && this.getLevel().getGameTime() % CRYSTAL_GROW_TIME == 0){
                List<BlockPos> crystalList = WorldHelper.blockInRadius(getBlockPos(), CRYSTAL_RANGE).stream()
                        .filter(p -> getLevel().getBlockState(p).getBlock() == BlockRegistry.XION.get())
                        .toList();

                if(crystalList.isEmpty())
                    return;
                //Get random crystal in range
                BlockPos crystalPos = crystalList.get(getLevel().getRandom().nextInt(crystalList.size()));

                if(rift.get().getStoredArtron() > artronRequired + 1){ //If there is enough artron in the rift to grow a crystal

                    List<BlockPos> crystalNearPoses = WorldHelper.blockInRadius(crystalPos, 2);
                    final BlockPos posToTry = crystalNearPoses.get(getLevel().getRandom().nextInt(crystalNearPoses.size()));

                    if(getLevel().getBlockState(posToTry).canBeReplaced() && BlockRegistry.XION.get().canSurvive(BlockRegistry.XION.get().defaultBlockState(), getLevel(), posToTry)){
                        if(rift.get().takeArtron(artronRequired, false) >= artronRequired){
                            getLevel().setBlock(posToTry, BlockRegistry.XION.get().defaultBlockState(), 3);
                        }
                        return;
                    }

                }
            }
        }

    }
}
