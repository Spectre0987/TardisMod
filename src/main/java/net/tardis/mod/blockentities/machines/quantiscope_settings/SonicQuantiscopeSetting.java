package net.tardis.mod.blockentities.machines.quantiscope_settings;

import net.minecraft.nbt.CompoundTag;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.inventory.MenuConstructor;
import net.minecraftforge.items.ItemStackHandler;
import net.tardis.mod.blockentities.machines.QuantiscopeTile;
import net.tardis.mod.menu.quantiscope.SonicQuantiscopeMenu;
import net.tardis.mod.misc.ItemStackHandlerWithListener;

import java.util.Optional;

public class SonicQuantiscopeSetting extends QuantiscopeSetting{

    final ItemStackHandlerWithListener inventory = new ItemStackHandlerWithListener(1);

    public SonicQuantiscopeSetting(ResourceLocation id, QuantiscopeTile tile) {
        super(id, tile);
        this.inventory.onChange(s -> tile.setChanged());
    }

    @Override
    public void tick() {

    }

    @Override
    public Optional<MenuConstructor> getMenu() {
        return Optional.of((winId, inv, player) -> new SonicQuantiscopeMenu(winId, inv, this));
    }

    @Override
    public Optional<ItemStackHandler> getInventory() {
        return Optional.of(inventory);
    }

    @Override
    public CompoundTag serializeNBT() {
        final CompoundTag tag = new CompoundTag();
        tag.put("inventory", this.inventory.serializeNBT());
        return tag;
    }

    @Override
    public void deserializeNBT(CompoundTag tag) {
        this.inventory.deserializeNBT(tag.getCompound("inventory"));
    }
}
