package net.tardis.mod.blockentities.machines;

import net.minecraft.core.BlockPos;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.nbt.ListTag;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.block.entity.BlockEntityType;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraftforge.items.ItemStackHandler;
import net.tardis.mod.blockentities.IBreakLogic;
import net.tardis.mod.blockentities.TileRegistry;
import net.tardis.mod.blockentities.machines.quantiscope_settings.CraftQuantiscopeSetting;
import net.tardis.mod.blockentities.machines.quantiscope_settings.QuantiscopeSetting;
import net.tardis.mod.blockentities.machines.quantiscope_settings.SonicQuantiscopeSetting;
import net.tardis.mod.helpers.Helper;
import net.tardis.mod.helpers.WorldHelper;
import net.tardis.mod.network.Network;
import net.tardis.mod.network.packets.QuantiscopeModeChangeMessage;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.BiFunction;

public class QuantiscopeTile extends BlockEntity implements IBreakLogic {

    public HashMap<ResourceLocation, QuantiscopeSetting> settings = new HashMap<>();
    public QuantiscopeSetting currentSetting;

    public QuantiscopeTile(BlockEntityType<?> pType, BlockPos pPos, BlockState pBlockState) {
        super(pType, pPos, pBlockState);
        for(ResourceLocation key : QuantiscopeSetting.ALL_SETTINGS.keySet()){
            this.settings.put(key, QuantiscopeSetting.ALL_SETTINGS.get(key).apply(key, this));
        }
    }

    public QuantiscopeTile(BlockPos pos, BlockState state){
        this(TileRegistry.QUANTISCOPE.get(), pos, state);
    }


    public void tick(){
        if(this.getCurrentSetting() != null){
            this.getCurrentSetting().tick();
        }
    }

    public QuantiscopeSetting getCurrentSetting(){
        if(this.currentSetting == null){
            this.currentSetting = this.settings.get(this.settings.keySet().stream().toList().get(0));
        }
        return this.currentSetting;
    }

    public void setSetting(ResourceLocation id){
        if(this.settings.containsKey(id)){
            this.currentSetting = this.settings.get(id);
            this.setChanged();
        }
    }

    @Override
    public void load(CompoundTag tag) {
        super.load(tag);

        ListTag settingList = tag.getList("settings", ListTag.TAG_COMPOUND);
        for(int i = 0; i < settingList.size(); ++i){
            final CompoundTag settingTag = settingList.getCompound(i);
            final ResourceLocation id = new ResourceLocation(settingTag.getString("type"));
            this.settings.get(id).deserializeNBT(settingTag);
        }
    }

    @Override
    protected void saveAdditional(CompoundTag tag) {
        super.saveAdditional(tag);

        final ListTag settingsList = new ListTag();
        for(Map.Entry<ResourceLocation, QuantiscopeSetting> setting : this.settings.entrySet()){
            final CompoundTag settingTag = setting.getValue().serializeNBT();
            settingTag.putString("type", setting.getKey().toString());
            settingsList.add(settingTag);
        }
        tag.put("settings", settingsList);
    }

    @Override
    public void onBroken() {
        for(QuantiscopeSetting setting : this.settings.values()){
            setting.getInventory().ifPresent(inv -> {
                WorldHelper.dropItems(this.level, this.getBlockPos(), inv);
            });
        }
    }

    public void changeMode(int mod) {
        final List<QuantiscopeSetting> settings = new ArrayList<>();
        settings.addAll(this.settings.values());

        int index = getCurrentModeIndex() + mod;
        if(index < 0){
            index = settings.size() - 1;
        }
        else if(index >= settings.size()){
            index = 0;
        }
        this.currentSetting = settings.get(index);

        if(getLevel().isClientSide){
            //Send mode change packet to server
            Network.sendToServer(new QuantiscopeModeChangeMessage(this.getBlockPos(), this.currentSetting.getId()));
        }

    }

    public int getCurrentModeIndex(){
        final QuantiscopeSetting currentSetting = this.getCurrentSetting();
        int i = 0;
        for(QuantiscopeSetting set : this.settings.values()){
            if(currentSetting == set){
                return i;
            }
            ++i;
        }
        return 0;
    }
}
