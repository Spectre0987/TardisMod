package net.tardis.mod.blockentities.dev;

import net.minecraft.core.BlockPos;
import net.minecraft.core.Vec3i;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.network.protocol.Packet;
import net.minecraft.network.protocol.game.ClientGamePacketListener;
import net.minecraft.network.protocol.game.ClientboundBlockEntityDataPacket;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.block.entity.BlockEntityType;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.phys.AABB;
import net.tardis.mod.blockentities.TileRegistry;
import org.jetbrains.annotations.Nullable;

import java.util.Optional;

public class TardisStructureTile extends BlockEntity {

    Vec3i size;

    public TardisStructureTile(BlockPos pPos, BlockState pBlockState) {
        super(TileRegistry.STRUCTURE.get(), pPos, pBlockState);
    }

    public void setSize(Vec3i size){
        this.size = size;
        this.setChanged();
    }

    public Optional<Vec3i> getSize(){
        return this.size == null ? Optional.empty() : Optional.of(this.size);
    }

    @Override
    public void load(CompoundTag tag) {
        super.load(tag);
        if(tag.contains("sizeX")){
            this.size = new Vec3i(tag.getInt("sizeX"), tag.getInt("sizeY"), tag.getInt("sizeZ"));
        }
    }

    @Override
    protected void saveAdditional(CompoundTag tag) {
        super.saveAdditional(tag);
        if(size != null){
            tag.putInt("sizeX", size.getX());
            tag.putInt("sizeY", size.getY());
            tag.putInt("sizeZ", size.getZ());
        }
    }

    @Nullable
    @Override
    public Packet<ClientGamePacketListener> getUpdatePacket() {
        return ClientboundBlockEntityDataPacket.create(this);
    }

    @Override
    public CompoundTag getUpdateTag() {
        return serializeNBT();
    }

    @Override
    public void handleUpdateTag(CompoundTag tag) {
        this.deserializeNBT(tag);
    }

    @Override
    public AABB getRenderBoundingBox() {
        return super.getRenderBoundingBox();
    }
}
