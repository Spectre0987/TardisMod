package net.tardis.mod.blockentities;

import net.minecraft.core.BlockPos;
import net.minecraft.world.level.block.entity.BlockEntityType;
import net.minecraft.world.level.block.state.BlockState;

public class DedicationPlaqueTile extends BaseRendererTile{
    public DedicationPlaqueTile(BlockPos pPos, BlockState pBlockState) {
        super(TileRegistry.DEDICATION_PLAQUE.get(), pPos, pBlockState);
    }
}
