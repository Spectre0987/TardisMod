package net.tardis.mod.blockentities.crafting;

import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.core.particles.ParticleTypes;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.network.protocol.Packet;
import net.minecraft.network.protocol.game.ClientGamePacketListener;
import net.minecraft.network.protocol.game.ClientboundBlockEntityDataPacket;
import net.minecraft.sounds.SoundEvents;
import net.minecraft.sounds.SoundSource;
import net.minecraft.world.inventory.ContainerData;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.crafting.RecipeType;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.block.entity.BlockEntityType;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.phys.Vec3;
import net.minecraftforge.common.ForgeHooks;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.capabilities.ForgeCapabilities;
import net.minecraftforge.common.util.LazyOptional;
import net.minecraftforge.fluids.capability.IFluidHandler;
import net.minecraftforge.fluids.capability.templates.FluidTank;
import net.minecraftforge.items.ItemStackHandler;
import net.tardis.mod.blockentities.IBreakLogic;
import net.tardis.mod.blockentities.TileRegistry;
import net.tardis.mod.fluids.FluidTankWithCallback;
import net.tardis.mod.helpers.Helper;
import net.tardis.mod.helpers.WorldHelper;
import net.tardis.mod.misc.ItemStackHandlerWithListener;
import net.tardis.mod.network.Network;
import net.tardis.mod.network.packets.AlembicFluidChangeMessage;
import net.tardis.mod.recipes.AlembicBottlingRecipe;
import net.tardis.mod.recipes.AlembicRecipe;
import net.tardis.mod.recipes.ItemStackHandlerContinerWrapper;
import net.tardis.mod.recipes.RecipeRegistry;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Optional;

public class AlembicBlockEntity extends BlockEntity implements IBreakLogic {

    protected int craftingTime = 0;
    protected int timeToFinish = 0;
    protected int fuelLeft = 0;
    protected int maxFuelBurnTime = 0;

    protected FluidTankWithCallback craftingTank = new FluidTankWithCallback(1000)
            .withCallback(tank -> {
                if(!getLevel().isClientSide()){
                    Network.sendNear(getLevel().dimension(),getBlockPos(), 10, new AlembicFluidChangeMessage(getBlockPos(), false, tank.getFluid()));
                }
            });
    protected FluidTankWithCallback resultTank = new FluidTankWithCallback(1000)
            .withCallback(tank -> {
                if(!getLevel().isClientSide()){
                    Network.sendNear(getLevel().dimension(),getBlockPos(), 10, new AlembicFluidChangeMessage(getBlockPos(), true, tank.getFluid()));
                }
            });
    protected Optional<AlembicRecipe> currentlyCraftingRecipe = Optional.empty();

    public ItemStackHandlerWithListener inventory = new ItemStackHandlerWithListener(6);

    public final ContainerData containerData = new ContainerData() {
        @Override
        public int get(int pIndex) {
            switch(pIndex){
                case 0 : return AlembicBlockEntity.this.fuelLeft;
                case 1 : return AlembicBlockEntity.this.maxFuelBurnTime;
                case 2 : return AlembicBlockEntity.this.craftingTime;
                case 3 : return AlembicBlockEntity.this.timeToFinish;
            }
            return 0;
        }

        @Override
        public void set(int pIndex, int pValue) {
            switch(pIndex){
                case 0:
                    AlembicBlockEntity.this.fuelLeft = pValue;
                    break;
                case 1:
                    AlembicBlockEntity.this.maxFuelBurnTime = pValue;
                    break;
                case 2:
                    AlembicBlockEntity.this.craftingTime = pValue;
                    break;
                case 3:
                    AlembicBlockEntity.this.timeToFinish = pValue;
                    break;
            }
        }

        @Override
        public int getCount() {
            return 4;
        }
    };

    public AlembicBlockEntity(BlockEntityType<?> pType, BlockPos pPos, BlockState pBlockState) {
        super(pType, pPos, pBlockState);
        this.inventory.onChange(slot -> {
            findNewRecipe();
        });
    }

    public static AlembicBlockEntity create(BlockPos pos, BlockState state){
        return new AlembicBlockEntity(TileRegistry.ALEMBIC.get(), pos, state);
    }

    public boolean isCrafting(){
        return this.craftingTime > 0;
    }

    /**
     * Called when crafing is successfully completed
     * @param recipe - currently working recipe
     */
    public void finishCrafting(AlembicRecipe recipe){
        this.stopCrafting();
        recipe.assemble(createWrapper(), level.registryAccess());
        if(!getLevel().isClientSide){
            getLevel().playSound(null, this.getBlockPos(), SoundEvents.BREWING_STAND_BREW, SoundSource.BLOCKS, 1.0F, 1.0F);
        }
        this.setChanged();
    }

    /**
     * Stops working on this recipe without any changes to inventory
     */
    public void stopCrafting(){
        this.timeToFinish = this.craftingTime = 0;
        this.currentlyCraftingRecipe = Optional.empty();
        this.setChanged();
    }

    public boolean findNewRecipe(){
        ItemStackHandlerContinerWrapper<AlembicBlockEntity> wrapper = createWrapper();

        if(this.currentlyCraftingRecipe.isPresent() && this.currentlyCraftingRecipe.get().matches(wrapper, getLevel())){
            return false;
        }

        for(AlembicRecipe recipe : level.getRecipeManager().getAllRecipesFor(RecipeRegistry.ALEMBIC_TYPE)){
            if(recipe.matches(wrapper, level)){
                this.currentlyCraftingRecipe = Optional.of(recipe);
                this.craftingTime = 0;
                this.timeToFinish = recipe.timeToCraft();
                return true;
            }
        }
        return false;
    }

    public float getProgress(){
        if(this.craftingTime <= 0){
            return 0.0F;
        }

        return this.craftingTime / (float)this.timeToFinish;
    }

    public float getFuelPercentage(){
        if(this.maxFuelBurnTime <= 0)
            return 0;
        return (this.fuelLeft / (float)this.maxFuelBurnTime);
    }

    public ItemStackHandlerContinerWrapper<AlembicBlockEntity> createWrapper(){
        return new ItemStackHandlerContinerWrapper<>(this.inventory, this, AlembicBlockEntity::setChanged);
    }

    @Override
    public @NotNull <T> LazyOptional<T> getCapability(@NotNull Capability<T> cap, @Nullable Direction side) {
        return super.getCapability(cap, side);
    }

    public static void tick(Level level, BlockPos pos, BlockState state, AlembicBlockEntity alembic){
        //Fill Fluid Containers

        if(level.isClientSide)
            return;

        //Fill the crafting tank
        alembic.fillTankFromItem(alembic.craftingTank, 0,1);

        //Bottle Results
        if(!alembic.resultTank.isEmpty()){
            alembic.fillItemFromTank(alembic.resultTank, 4, 5);
        }

        //Cancel crafting if this no longer matches the recipe
        if(!alembic.currentlyCraftingRecipe.isPresent()){
            alembic.craftingTime = alembic.timeToFinish = 0;
        }
        else {
            alembic.currentlyCraftingRecipe.ifPresent(recipe -> {
                if(recipe.matches(alembic.createWrapper(), level)){
                    //Play particles

                    Vec3 partPos = Helper.blockPosToVec3(alembic.getBlockPos(), true)
                            .add(-0.3, 1, 0)
                            .yRot(WorldHelper.getDegreeFromRotation(WorldHelper.getHorizontalFacing(alembic.getBlockState())));

                    level.addParticle(ParticleTypes.SMOKE, partPos.x(), partPos.y(), partPos.z(), 0, 0.1, 0);

                    if(alembic.fuelLeft > 0) {
                        alembic.craftingTime++;
                    }
                    if (alembic.craftingTime >= alembic.timeToFinish) {
                        alembic.finishCrafting(recipe);
                    }
                }
                else alembic.stopCrafting();
            });
        }

        //Decrement fuel and add some if there is a fuel item present
        if(alembic.fuelLeft > 0){
            --alembic.fuelLeft;
        }
        final int fuelSlotBurnTime = ForgeHooks.getBurnTime(alembic.inventory.getStackInSlot(3), RecipeType.SMELTING);
        //If we are working on a recipe and the fuel slot has a fuel item, consume it
        if(alembic.currentlyCraftingRecipe.isPresent() && fuelSlotBurnTime > 0 && alembic.fuelLeft <= 0){
            ItemStack fuelStack = alembic.inventory.extractItem(3, 1, false);
            if(fuelStack != ItemStack.EMPTY){
                alembic.fuelLeft = alembic.maxFuelBurnTime = fuelSlotBurnTime;
                if(!fuelStack.getCraftingRemainingItem().isEmpty()){
                    alembic.inventory.insertItem(3, fuelStack.getCraftingRemainingItem(), false);
                }
            }

        }
    }

    public void fillTankFromItem(FluidTank tank, int inputSlot, int outputSlot){
        this.inventory.getStackInSlot(inputSlot).getCapability(ForgeCapabilities.FLUID_HANDLER_ITEM).ifPresent(cap -> {
            if(this.inventory.insertItem(outputSlot, cap.getContainer(), true) == ItemStack.EMPTY){
                // If we can put this item in the "drained" slot for this tank
                if(tank.fill(cap.drain(1000, IFluidHandler.FluidAction.EXECUTE), IFluidHandler.FluidAction.EXECUTE) > 0){
                    this.inventory.setStackInSlot(inputSlot, this.inventory.insertItem(outputSlot, cap.getContainer(), false));
                    this.setChanged();
                }
            }
        });
    }

    public void fillItemFromTank(FluidTank tank, int inputSlot, int outputSlot){
        final ItemStack inputStack = this.inventory.getStackInSlot(inputSlot);
        final ItemStack outputStack = this.inventory.getStackInSlot(outputSlot);

        if(!resultTank.isEmpty()){

            //TODO: Fill capability containers

            //Alembic Recipes
            ItemStackHandlerContinerWrapper<AlembicBlockEntity> wrapper = createWrapper();
            for(AlembicBottlingRecipe recipe : level.getRecipeManager().getAllRecipesFor(RecipeRegistry.ALEMBIC_BOTTLING_TYPE)){
                if(recipe.matches(wrapper, level)){
                    this.inventory.insertItem(outputSlot, recipe.assemble(wrapper, level.registryAccess()), false);
                    this.setChanged();
                    return;
                }
            }
        }

    }

    public FluidTank getCraftingTank(){
        return this.craftingTank;
    }

    public FluidTank getResultTank(){
        return this.resultTank;
    }

    public ItemStackHandler getInventory(){
        return this.inventory;
    }

    @Override
    public void load(CompoundTag tag) {
        super.load(tag);
        this.craftingTank.readFromNBT(tag.getCompound("crafting_tank"));
        this.resultTank.readFromNBT(tag.getCompound("result_tank"));
        this.inventory.deserializeNBT(tag.getCompound("inventory"));
        this.fuelLeft = tag.getInt("fuel");
        this.maxFuelBurnTime = tag.getInt("max_fuel");
        this.timeToFinish = tag.getInt("time_to_finish");
        this.craftingTime = tag.getInt("crafting_time");
    }

    @Override
    protected void saveAdditional(CompoundTag tag) {
        super.saveAdditional(tag);
        tag.put("crafting_tank", this.craftingTank.writeToNBT(new CompoundTag()));
        tag.put("result_tank", this.resultTank.writeToNBT(new CompoundTag()));
        tag.put("inventory", this.inventory.serializeNBT());
        tag.putInt("fuel", this.fuelLeft);
        tag.putInt("max_fuel", this.maxFuelBurnTime);
        tag.putInt("time_to_finish", this.timeToFinish);
        tag.putInt("crafting_time", this.craftingTime);
    }

    @Nullable
    @Override
    public Packet<ClientGamePacketListener> getUpdatePacket() {
        return ClientboundBlockEntityDataPacket.create(this);
    }

    @Override
    public CompoundTag getUpdateTag() {
        return this.serializeNBT();
    }


    @Override
    public void onBroken() {
        WorldHelper.dropItems(this.getLevel(), this.getBlockPos(), this.inventory);
    }

    @Override
    public void onLoad() {
        super.onLoad();
        findNewRecipe();
    }
}
