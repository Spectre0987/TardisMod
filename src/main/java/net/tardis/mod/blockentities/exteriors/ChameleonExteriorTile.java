package net.tardis.mod.blockentities.exteriors;

import net.minecraft.core.BlockPos;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.nbt.NbtOps;
import net.minecraft.network.protocol.Packet;
import net.minecraft.network.protocol.game.ClientGamePacketListener;
import net.minecraft.network.protocol.game.ClientboundBlockEntityDataPacket;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.block.entity.BlockEntityType;
import net.minecraft.world.level.block.state.BlockState;
import net.tardis.mod.Tardis;
import net.tardis.mod.registry.BlockRegistry;
import net.tardis.mod.blockentities.IDisguisedBlock;
import net.tardis.mod.blockentities.TileRegistry;
import net.tardis.mod.misc.IHaveMatterState;
import net.tardis.mod.network.Network;
import net.tardis.mod.network.packets.UpdateChameleonDisguseMessage;
import org.jetbrains.annotations.Nullable;

public class ChameleonExteriorTile extends ExteriorTile implements IDisguisedBlock {

    public BlockState disguisedState;

    public ChameleonExteriorTile(BlockEntityType<?> type, BlockPos pos, BlockState state) {
        super(type, pos, state);
        this.disguisedState = Blocks.STONE.defaultBlockState();
    }

    public ChameleonExteriorTile(BlockPos pos, BlockState state) {
        this(TileRegistry.CHAMELEON_EXTERIOR.get(), pos, state);
    }

    @Override
    public void setDisguisedState(BlockState state) {
        this.disguisedState = state;
        this.setChanged();
        if(getLevel() instanceof ServerLevel server){
            server.getLightEngine().checkBlock(this.getBlockPos());
            Network.sendToTracking(this, new UpdateChameleonDisguseMessage(getBlockPos(), state));
        }
    }

    @Override
    public void setDisguisedTile(BlockEntity entity) {

    }

    @Override
    public BlockState getDisguisedState() {
        return this.disguisedState;
    }

    @Override
    public BlockEntity getDisguisedTile() {
        return null;
    }

    @Override
    public void load(CompoundTag tag) {
        super.load(tag);
        if(tag.contains("disguised_state"))
            this.disguisedState = BlockState.CODEC.parse(NbtOps.INSTANCE, tag.get("disguised_state")).getOrThrow(true, Tardis.LOGGER::warn);
    }

    @Override
    protected void saveAdditional(CompoundTag tag) {
        super.saveAdditional(tag);
        if(this.disguisedState != null){
            tag.put("disguised_state", BlockState.CODEC.encodeStart(NbtOps.INSTANCE, this.disguisedState).getOrThrow(true, Tardis.LOGGER::warn));
        }
    }

    @Override
    public void delete() {
        super.delete();
    }

    @Override
    public void onLanded() {
        getLevel().setBlock(this.getBlockPos().above(), BlockRegistry.CHAMELEON_AUXILLARY.get().defaultBlockState(), 3);
        final BlockEntity aboveTile = getLevel().getBlockEntity(this.getBlockPos().above());
        if(aboveTile instanceof IDisguisedBlock dis){
            dis.setDisguisedState(this.disguisedState);
        }
        if(aboveTile instanceof IHaveMatterState matter){
                matter.getMatterStateHandler().copyFrom(this.getMatterStateHandler());
        }
    }

    @Override
    public @Nullable Packet<ClientGamePacketListener> getUpdatePacket() {
        return ClientboundBlockEntityDataPacket.create(this);
    }

    @Override
    public CompoundTag getUpdateTag() {
        return super.getUpdateTag();
    }

    @Override
    public void handleUpdateTag(CompoundTag tag) {
        super.handleUpdateTag(tag);
    }
}
