package net.tardis.mod.blockentities.exteriors;

import net.minecraft.core.BlockPos;
import net.minecraft.world.level.block.state.BlockState;
import net.tardis.mod.blockentities.TileRegistry;

public class TTCapsuleExteriorTile extends ExteriorTile{
    public TTCapsuleExteriorTile(BlockPos pos, BlockState state) {
        super(TileRegistry.TT_CAPSULE.get(), pos, state);
    }
}
