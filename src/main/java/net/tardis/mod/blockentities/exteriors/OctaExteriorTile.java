package net.tardis.mod.blockentities.exteriors;

import net.minecraft.core.BlockPos;
import net.minecraft.world.level.block.entity.BlockEntityType;
import net.minecraft.world.level.block.state.BlockState;
import net.tardis.mod.blockentities.TileRegistry;

public class OctaExteriorTile extends ExteriorTile{
    public OctaExteriorTile(BlockEntityType<?> type, BlockPos pos, BlockState state) {
        super(type, pos, state);
    }

    public OctaExteriorTile(BlockPos pos, BlockState state) {
        this(TileRegistry.OCTA_EXTERIOR.get(), pos, state);
    }
}
