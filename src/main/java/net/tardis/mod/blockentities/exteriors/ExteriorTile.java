package net.tardis.mod.blockentities.exteriors;

import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.core.registries.Registries;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.nbt.LongTag;
import net.minecraft.network.protocol.Packet;
import net.minecraft.network.protocol.game.ClientGamePacketListener;
import net.minecraft.network.protocol.game.ClientboundBlockEntityDataPacket;
import net.minecraft.resources.ResourceKey;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.block.entity.BlockEntityType;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.phys.AABB;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.capabilities.ForgeCapabilities;
import net.minecraftforge.common.util.LazyOptional;
import net.minecraftforge.fluids.IFluidTank;
import net.minecraftforge.fluids.capability.IFluidHandler;
import net.minecraftforge.registries.ForgeRegistries;
import net.tardis.api.events.TardisEvent;
import net.tardis.mod.registry.BlockRegistry;
import net.tardis.mod.blockentities.IDisguisedBlock;
import net.tardis.mod.cap.Capabilities;
import net.tardis.mod.cap.level.ITardisLevel;
import net.tardis.mod.exterior.ExteriorType;
import net.tardis.mod.helpers.Helper;
import net.tardis.mod.helpers.WorldHelper;
import net.tardis.mod.item.KeyItem;
import net.tardis.mod.misc.*;
import net.tardis.mod.misc.enums.DoorState;
import net.tardis.mod.misc.enums.MatterState;
import net.tardis.mod.network.Network;
import net.tardis.mod.network.packets.UpdateShellMessage;
import net.tardis.mod.network.packets.ExteriorMatterStateMessage;
import net.tardis.mod.network.packets.UpdateDoorStateMessage;
import net.tardis.mod.registry.ExteriorRegistry;
import net.tardis.mod.registry.JsonRegistries;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.*;

public class ExteriorTile extends BlockEntity implements IDoor, IHaveMatterState, ITeleportEntities<ExteriorTile>, ITextureVariantBlockEntity {

    private ResourceKey<Level> interior;
    private DoorHandler doorHandler = new DoorHandler(stack -> {
        Optional<ResourceKey<Level>> tardis = KeyItem.getKeyData(stack);
        if(tardis.isPresent() && tardis.get().equals(this.interior)){
            return true;
        }
        return false;
    }, DoorState.values());
    private ExteriorType exteriorType;
    public final AABB renderBox;
    public final MatterStateHandler matterStateHandler = new MatterStateHandler(() -> WorldHelper.centerOfBlockPos(this.getBlockPos(), false))
            .setChangedAction(() -> {
                this.setChanged();
                if(!getLevel().isClientSide()) {
                    Network.sendToTracking(this, new ExteriorMatterStateMessage(this.getBlockPos(), this.getMatterStateHandler()));
                    for(BlockPos pos : this.shell){
                        if(getLevel().getBlockEntity(this.getBlockPos().offset(pos)) instanceof IHaveMatterState matter && matter != this){
                            matter.getMatterStateHandler().copyFrom(this.getMatterStateHandler());
                        }
                    }
                    //top block
                    if(getLevel().getBlockEntity(this.getBlockPos().above()) instanceof IHaveMatterState matter){
                        matter.getMatterStateHandler().copyFrom(this.getMatterStateHandler());
                    }
                }
            })
            .setRemovedAction(this::delete)
            .setRadius(this::calculateRadiusFromShell);
    public final TeleportHandler<ExteriorTile> teleportHandler = new TeleportHandler<>(this,
                (level) -> level.getServer().getLevel(this.interior),
                () -> new AABB(this.getBlockPos()),
                (level, entity) -> this.getTeleportHandler().interiorDoorPos(level, entity)
            ).setPlaceAtMePosition(entity -> new TeleportEntry.LocationData(
                    WorldHelper.centerOfBlockPos(getBlockPos(), false).add(0, 0.1, 0),
                    WorldHelper.getFacingAngle(this.getBlockState()) + (WorldHelper.getHorizontalFacing(this.getBlockState()).getAxis() == Direction.Axis.X ? -180 : 0)
    )).setTeleportPreEvent(e -> getTardis().isPresent() ? new TardisEvent.EnterEvent.Pre(getTardis().get(), e) : null);
    public List<BlockPos> shell = new ArrayList<>();
    private ITardisLevel cachedTardis;

    private LazyOptional<IFluidTank> tankHolder = LazyOptional.empty();

    private Optional<ResourceLocation> variantId = Optional.empty();

    public int animationTicks;

    public ExteriorTile(BlockEntityType<?> type, BlockPos pos, BlockState state) {
        super(type, pos, state);

        this.getDoorHandler().setPacketSender(() -> {
            //Send door state to interior door
            if(level != null && !level.isClientSide()){
                Network.sendToTracking(this, new UpdateDoorStateMessage(this.getBlockPos(), this.getDoorHandler()));
            }
        }).setDataSave(this::setChanged);

        this.doorHandler.linkedDoor = () -> {
            if(level != null && !level.isClientSide){
                ServerLevel interior = level.getServer().getLevel(this.interior);
                if(interior != null){
                    interior.getCapability(Capabilities.TARDIS).ifPresent(tardis -> {
                        tardis.getInteriorManager().getDoorHandler().update(this.doorHandler, tardis.getLevel());
                    });
                }
            }
        };
        this.renderBox = new AABB(0, 0, 0, 1, 3, 1).move(pos);
    }


    public void setShell(Collection<BlockPos> pos){
        this.shell.clear();
        this.shell.addAll(pos);
        this.setChanged();
        if(!getLevel().isClientSide()){
            Network.sendToTracking(this, new UpdateShellMessage(getBlockPos(), this.shell));
            for(BlockPos shell : pos){
                if(getLevel().getBlockEntity(getBlockPos().offset(shell)) instanceof IHaveMatterState matter){
                    matter.getMatterStateHandler().copyFrom(this.getMatterStateHandler());
                }
            }
        }
    }

    public void setInterior(ResourceKey<Level> interior, ExteriorType type){
        this.interior = interior;
        this.exteriorType = type;
        this.setChanged();
        this.findFluidTank();
    }

    public static void tick(Level level, BlockPos pos, BlockState state, ExteriorTile tile){
       if(!level.isClientSide()){
            tile.getMatterStateHandler().tick(level);
           //Teleport
           tile.getTeleportHandler().tick((ServerLevel) level);
           tile.deleteIfDuplicate();
       }
       else tile.animationTicks++;
    }

    public float calculateRadiusFromShell(){
        final AABB shellPosition = WorldHelper.getBoundsFromBlocks(this.shell);
        final double width = shellPosition.maxX - shellPosition.minX,
            length = shellPosition.maxZ - shellPosition.minZ;
        final float rad = width > length ? (float)width : (float)length;
        return rad > 1.0 ? rad : 1.0F;
    }

    public void deleteIfDuplicate(){
        if(level instanceof ServerLevel level){
            ServerLevel tardisWorld = level.getLevel().getServer().getLevel(this.interior);
            if(tardisWorld != null){
                tardisWorld.getCapability(Capabilities.TARDIS).ifPresent(tardis -> {
                    //If in a different location than this
                    if(tardis.isInVortex() && !tardis.isTakingOffOrLanding() && this.getMatterStateHandler().getMatterState() == MatterState.SOLID){
                        this.getMatterStateHandler().startMatterState(level, MatterState.DEMAT, 100, this.getMatterStateHandler().getDematType());
                    }
                });
            }
        }
    }

    public Optional<ITardisLevel> getTardis(){
        if(cachedTardis != null){
            return Optional.of(this.cachedTardis);
        }
        if(this.level instanceof ServerLevel ser){
            LazyOptional<ITardisLevel> tardisHolder = Capabilities.getCap(Capabilities.TARDIS, ser.getServer().getLevel(this.interior));
            if(tardisHolder.isPresent()){
                this.cachedTardis = tardisHolder.orElseThrow(NullPointerException::new);
                return Optional.of(cachedTardis);
            }
        }
        return Optional.empty();
    }

    public void onLanded(){
        getLevel().setBlock(this.getBlockPos().above(), BlockRegistry.EXTERIOR_COLLISION.get().defaultBlockState(), 3);
    }

    public void findFluidTank(){
        this.tankHolder.invalidate();
        getLinkedTardis().ifPresent(tardis -> {
            this.tankHolder = tardis.getInteriorManager().getFluidBuffer();
        });
    }

    public void delete(){
        if(!level.isClientSide()){
            this.tankHolder.invalidate();
            level.setBlock(this.getBlockPos(), Blocks.AIR.defaultBlockState(), 3);
            level.setBlock(this.getBlockPos().above(), Blocks.AIR.defaultBlockState(), 3);

            //Destroy shell
            for(BlockPos pos : this.shell){
                if(level.getBlockEntity(this.getBlockPos().offset(pos)) instanceof IDisguisedBlock)
                    level.setBlock(this.getBlockPos().offset(pos), Blocks.AIR.defaultBlockState(), 3);
            }
        }
    }

    @Override
    public AABB getRenderBoundingBox() {
        return super.getRenderBoundingBox();
    }

    public DoorHandler getDoorHandler(){
        return this.doorHandler;
    }

    @Override
    public void load(CompoundTag tag) {
        super.load(tag);
        this.shell.clear();

        if(tag.contains("interior"))
            this.interior = ResourceKey.create(Registries.DIMENSION, new ResourceLocation(tag.getString("interior")));
        Helper.readRegistryFromString(tag, ExteriorRegistry.REGISTRY.get(), "exterior").ifPresent(ext -> {
            this.exteriorType = ext;
            this.doorHandler.setValidDoorStates(this.exteriorType.getDoorStates());
        });

        this.doorHandler.deserializeNBT(tag.getCompound("door"));
        this.matterStateHandler.deserializeNBT(tag.getCompound("matter_handler"));
        this.shell.addAll(Helper.<BlockPos, LongTag>readListNBT(tag, "outer_shell", CompoundTag.TAG_LONG, t -> {
            BlockPos p = BlockPos.of(t.getAsLong());
            return p;
        }));
        this.variantId = Helper.readOptionalNBT(tag, "tex_variant", (t, k) -> new ResourceLocation(t.getString(k)));
    }

    @Override
    protected void saveAdditional(CompoundTag tag) {
        super.saveAdditional(tag);
        if(this.interior != null){
            tag.putString("interior", this.interior.location().toString());
        }
        Helper.writeRegistryToNBT(tag, ExteriorRegistry.REGISTRY.get(), this.exteriorType, "exterior");
        tag.put("door", this.doorHandler.serializeNBT());
        tag.put("matter_handler", this.matterStateHandler.serializeNBT());
        Helper.writeListNBT(tag, "outer_shell", this.shell, (pos, list) -> list.add(LongTag.valueOf(pos.asLong())));
        this.variantId.ifPresent(v -> tag.putString("tex_variant", v.toString()));
    }

    @Nullable
    @Override
    public Packet<ClientGamePacketListener> getUpdatePacket() {
        return ClientboundBlockEntityDataPacket.create(this);
    }

    @Override
    public CompoundTag getUpdateTag() {
        return this.serializeNBT();
    }

    @Override
    public void handleUpdateTag(CompoundTag tag) {
        this.deserializeNBT(tag);
    }

    /**
     *
     * @return always empty on the client
     */
    public LazyOptional<ITardisLevel> getLinkedTardis(){
        if(level.isClientSide() || this.interior == null){
            return LazyOptional.empty();
        }

        ServerLevel tardisLevel = level.getServer().getLevel(this.interior);
        return Capabilities.getCap(Capabilities.TARDIS, tardisLevel);
    }

    @Override
    public void onChunkUnloaded() {
        if(!this.level.isClientSide){
            if(this.getMatterStateHandler().getMatterState() == MatterState.DEMAT){
                this.delete();
            }
        }
        super.onChunkUnloaded();
    }

    @Override
    public MatterStateHandler getMatterStateHandler() {
        return this.matterStateHandler;
    }

    @Override
    public TeleportHandler<ExteriorTile> getTeleportHandler() {
        return this.teleportHandler;
    }

    @Override
    public @NotNull <T> LazyOptional<T> getCapability(@NotNull Capability<T> cap, @Nullable Direction side) {
        if(cap == ForgeCapabilities.FLUID_HANDLER)
            return this.tankHolder.cast();
        return super.getCapability(cap, side);
    }

    @Override
    public void onLoad() {
        super.onLoad();
        this.findFluidTank();
        this.updateTextureVariant();
    }

    @Override
    public Optional<TextureVariant> getTextureVariant() {
        return this.variantId.map(id -> getLevel().registryAccess().registryOrThrow(JsonRegistries.TEXTURE_VARIANTS).get(id));
    }

    @Override
    public void setTextureVariant(@Nullable TextureVariant variant) {

        final ResourceKey<BlockEntityType<?>> exteriorId = ForgeRegistries.BLOCK_ENTITY_TYPES.getResourceKey(this.getType()).get();
        if(variant.key().equals(exteriorId)){
            this.variantId = variant == null ? Optional.empty() : getLevel().registryAccess().registryOrThrow(JsonRegistries.TEXTURE_VARIANTS).getResourceKey(variant)
                    .map(ResourceKey::location);
        }
        else this.variantId = Optional.empty();

        this.updateTextureVariant();
        this.getLinkedTardis().ifPresent(tardis -> {
            tardis.getExteriorExtraData().setExteriorTexVariant(this.variantId.orElse(null));
        });
    }
}
