package net.tardis.mod.blockentities;

import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.block.state.BlockState;

public interface IDisguisedBlock {

    void setDisguisedState(BlockState state);
    void setDisguisedTile(BlockEntity entity);

    BlockState getDisguisedState();
    BlockEntity getDisguisedTile();
}
