package net.tardis.mod.blockentities;

import net.minecraft.core.BlockPos;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.level.ClipContext;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.block.state.properties.BlockStateProperties;
import net.minecraft.world.phys.AABB;
import net.minecraft.world.phys.BlockHitResult;
import net.minecraft.world.phys.HitResult;
import net.minecraft.world.phys.Vec3;
import net.minecraftforge.common.util.LazyOptional;
import net.tardis.api.events.TardisEvent;
import net.tardis.mod.blockentities.multiblock.MultiblockData;
import net.tardis.mod.cap.Capabilities;
import net.tardis.mod.cap.level.ITardisLevel;
import net.tardis.mod.helpers.Helper;
import net.tardis.mod.helpers.WorldHelper;
import net.tardis.mod.misc.*;
import net.tardis.mod.misc.enums.DoorState;
import net.tardis.mod.misc.tardis.InteriorDoorData;
import net.tardis.mod.registry.ControlRegistry;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class InteriorDoorTile extends BlockEntity implements IDoor, IMultiblockMaster, ITeleportEntities<InteriorDoorTile> {

    public static final int BIG_SUCC_RANGE = 20;

    private UUID doorID;
    private List<BlockPos> children = new ArrayList<>();
    private MultiblockData multiblockData;
    private TeleportHandler<InteriorDoorTile> teleportHandler;

    public InteriorDoorTile(BlockPos pWorldPosition, BlockState pBlockState) {
        super(TileRegistry.INTERIOR_DOOR.get(), pWorldPosition, pBlockState);
        this.teleportHandler = new TeleportHandler<>(this,
                level -> {
                    LazyOptional<ITardisLevel> tardisHolder = Capabilities.getCap(Capabilities.TARDIS, level);
                    if(tardisHolder.isPresent())
                        return level.getServer().getLevel(tardisHolder.orElseThrow(NullPointerException::new).getLocation().getLevel());
                    return null;
                },
                () -> WorldHelper.moveVoxelShapeTo(this.getBlockState().getShape(getLevel(), getBlockPos()), getBlockPos()).bounds(),
                (target, entity) -> {
                    LazyOptional<ITardisLevel> tardisHolder = Capabilities.getCap(Capabilities.TARDIS, getLevel());
                    if(tardisHolder.isPresent()){
                        return tardisHolder.orElseThrow(NullPointerException::new).getExterior().getEntityTeleportTarget(target, entity);
                    }
                    return new TeleportEntry.LocationData(Vec3.ZERO, 0);
                })
                .setOtherTeleportHandler((level) -> {
                    LazyOptional<ITardisLevel> tardisHolder = Capabilities.getCap(Capabilities.TARDIS, getLevel());
                    if(tardisHolder.isPresent()){
                        return tardisHolder.orElseThrow(NullPointerException::new).getExterior().getTeleportHandler();
                    }
                    return null;
        }).setTeleportPreEvent(e -> Capabilities.getCap(Capabilities.TARDIS, this.getLevel()).isPresent() ? new TardisEvent.ExitEvent.Pre(Capabilities.getCap(Capabilities.TARDIS, this.getLevel()).orElseThrow(NullPointerException::new), e) : null);
    }


    public static void tick(Level level, BlockPos pos, BlockState state, InteriorDoorTile door){
        level.getCapability(Capabilities.TARDIS).ifPresent(tardis -> {
            if(!level.isClientSide){

                if(door.doorID == null){
                    door.setDoorID(UUID.randomUUID());
                }

                door.getTeleportHandler().tick((ServerLevel) level);

                /*
                if(door.getDoorHandler().getDoorState().isOpen()){
                    //Transfer people close to the door outside
                    ServerLevel exteriorLevel = level.getServer().getLevel(tardis.getLocation().getLevel());
                    if(exteriorLevel != null){
                        final List<Entity> entitiesToTeleport = tardis.getLevel().getEntitiesOfClass(Entity.class, state.getShape(level, pos).move(pos.getX(), pos.getY(), pos.getZ()).bounds());
                        if(!entitiesToTeleport.isEmpty()){
                            ChunkLoader.addTicketToExterior(exteriorLevel, tardis.getLocation().getPos());
                            for(Entity e : entitiesToTeleport){
                                tardis.getExterior().placeEntityAt(exteriorLevel, e);
                            }
                        }
                    }
                }

                 */
            }

            if(tardis.getInteriorManager().getDoorHandler().getDoorState().isOpen()){
                //Suck out entities in flight
                if(tardis.isInVortex()){
                    door.suckOffEntities();
                }
            }

        });
    }

    public void suckOffEntities(){
        AABB bb = WorldHelper.getCenteredAABB(this.getBlockPos().relative(this.getBlockState().getValue(BlockStateProperties.HORIZONTAL_FACING), BIG_SUCC_RANGE),
                BIG_SUCC_RANGE);

        for(LivingEntity entity : level.getEntitiesOfClass(LivingEntity.class, bb)){

            final Vec3 blockPos = Helper.blockPosToVec3(getBlockPos(), true);

            //If the entity has blocks in front of it, don't pull
            BlockHitResult result = level.clip(new ClipContext(blockPos, entity.position(), ClipContext.Block.COLLIDER, ClipContext.Fluid.ANY, entity));
            if(result.getType() == HitResult.Type.BLOCK){
                continue;
            }

            Vec3 motion = blockPos.subtract(entity.position())
                    .normalize().scale(0.1D);

            entity.setDeltaMovement(entity.getDeltaMovement().add(motion));

        }

    }

    public UUID getDoorID() {
        if(!this.level.isClientSide && this.doorID == null){
            this.setDoorID(UUID.randomUUID());
        }
        return this.doorID;
    }

    public void setDoorID(UUID id){
        this.doorID = id;
        this.setChanged();
        if(level != null && !level.isClientSide){
            level.getCapability(Capabilities.TARDIS).ifPresent(tardis -> {
                tardis.getInteriorManager().addInteriorDoor(this.doorID, new InteriorDoorData(this.getBlockPos()));
            });
        }
    }

    @Override
    public void load(CompoundTag tag) {
        super.load(tag);
        if(tag.contains("door_id"))
            this.doorID = UUID.fromString(tag.getString("door_id"));
    }

    @Override
    protected void saveAdditional(CompoundTag pTag) {
        super.saveAdditional(pTag);
        if(this.doorID != null)
            pTag.putString("door_id", this.doorID.toString());
    }

    @Override
    public void onLoad() {
        super.onLoad();
        if(this.level != null && !this.level.isClientSide()){
            if(this.doorID == null)
                this.setDoorID(UUID.randomUUID());
            level.getCapability(Capabilities.TARDIS).ifPresent(tardis -> {
                tardis.getInteriorManager().addInteriorDoor(this.doorID, new InteriorDoorData(this.getBlockPos()));
            });
        }

    }

    @Override
    public DoorHandler getDoorHandler() {
        //Get door handler from tardis world
        if(this.level != null) {
            LazyOptional<ITardisLevel> tardis = this.level.getCapability(Capabilities.TARDIS);
            if (tardis.isPresent()) {
                return tardis.orElseThrow(NullPointerException::new).getInteriorManager().getDoorHandler();
            }
        }
        return new DoorHandler(true, DoorState.values());
    }

    @Override
    public void setMasterData(MultiblockData data) {
        this.multiblockData = data;
    }

    @Override
    public List<BlockPos> getChildren() {
        return this.children;
    }

    @Override
    public MultiblockData getMultiblockData() {
        return this.multiblockData;
    }

    @Override
    public TeleportHandler<InteriorDoorTile> getTeleportHandler() {
        return this.teleportHandler;
    }
}
