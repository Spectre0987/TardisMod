package net.tardis.mod.blockentities;

import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.network.Connection;
import net.minecraft.network.protocol.Packet;
import net.minecraft.network.protocol.game.ClientGamePacketListener;
import net.minecraft.network.protocol.game.ClientboundBlockEntityDataPacket;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.block.entity.BlockEntityType;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.util.LazyOptional;
import net.tardis.mod.block.MultiChildCapRedirectBlock;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class MultiblockChildTile extends BlockEntity implements IMultiblock {

    private BlockPos masterPos;
    private List<Capability<?>> redirectCaps = new ArrayList<>();

    public MultiblockChildTile(BlockEntityType<?> pType, BlockPos pPos, BlockState pBlockState) {
        super(pType, pPos, pBlockState);
    }

    public MultiblockChildTile(BlockPos pPos, BlockState pBlockState) {
        super(TileRegistry.MULTIBLOCK.get(), pPos, pBlockState);
    }

    @Override
    public void load(CompoundTag pTag) {
        super.load(pTag);
        this.masterPos = pTag.contains("master_pos") ? BlockPos.of(pTag.getLong("master_pos")) : null;
    }

    @Override
    protected void saveAdditional(CompoundTag pTag) {
        super.saveAdditional(pTag);
        if(this.masterPos != null)
            pTag.putLong("master_pos", this.masterPos.asLong());
    }

    @Override
    public BlockPos getMasterPos() {
        return this.masterPos;
    }

    @Override
    public void setMasterPos(BlockPos masterPos) {
        this.masterPos = masterPos;
        this.setChanged();
    }

    @Override
    public @NotNull <T> LazyOptional<T> getCapability(@NotNull Capability<T> cap, @Nullable Direction side) {
        if(this.getBlockState().getBlock() instanceof MultiChildCapRedirectBlock r && r.isCapRedirected(cap) && this.getMasterPos() != null){
            if(this.getLevel().getBlockEntity(this.getBlockPos().offset(masterPos)) instanceof IMultiblockMaster master){
                return master.getRedirectedCap(cap, side);
            }
        }
        return super.getCapability(cap, side);
    }

    @Nullable
    @Override
    public Packet<ClientGamePacketListener> getUpdatePacket() {
        return ClientboundBlockEntityDataPacket.create(this);
    }

    @Override
    public CompoundTag getUpdateTag() {
        return this.serializeNBT();
    }

    @Override
    public void onDataPacket(Connection net, ClientboundBlockEntityDataPacket pkt) {
        super.onDataPacket(net, pkt);
        this.deserializeNBT(pkt.getTag());
    }

    @Override
    public void handleUpdateTag(CompoundTag tag) {
        super.handleUpdateTag(tag);
        this.deserializeNBT(tag);
    }
}
