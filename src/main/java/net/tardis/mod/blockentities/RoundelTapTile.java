package net.tardis.mod.blockentities;

import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.block.entity.BlockEntityType;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.capabilities.ForgeCapabilities;
import net.minecraftforge.common.util.LazyOptional;
import net.minecraftforge.energy.EnergyStorage;
import net.minecraftforge.energy.IEnergyStorage;
import net.minecraftforge.fluids.IFluidTank;
import net.tardis.mod.cap.Capabilities;
import net.tardis.mod.cap.level.ITardisLevel;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class RoundelTapTile extends BlockEntity {

    public RoundelTapTile(BlockEntityType<?> pType, BlockPos pPos, BlockState pBlockState) {
        super(pType, pPos, pBlockState);
    }

    public RoundelTapTile(BlockPos pos, BlockState state){
        this(TileRegistry.ROUNDEL_TAP.get(), pos, state);
    }

    public void tick(){
        //Push power
        final LazyOptional<EnergyStorage> tardisPowerHolder = getEnergyBufferForWorld(this.level);
        for(Direction dir : Direction.values()){
            BlockEntity otherEnntity = this.level.getBlockEntity(this.getBlockPos().relative(dir));
            if(otherEnntity != null){
                LazyOptional<IEnergyStorage> otherPower = otherEnntity.getCapability(ForgeCapabilities.ENERGY, dir.getOpposite());
                otherPower.ifPresent(p -> {
                    tardisPowerHolder.ifPresent(tardisPower -> {
                        final int powerToGive = tardisPower.extractEnergy(128, true);
                        tardisPower.extractEnergy(p.receiveEnergy(powerToGive, false), false);
                    });
                });
            }
        }
    }

    @Override
    public @NotNull <T> LazyOptional<T> getCapability(@NotNull Capability<T> cap, @Nullable Direction side) {

        if(cap == ForgeCapabilities.ENERGY)
            return getEnergyBufferForWorld(this.level).cast();

        if(cap == ForgeCapabilities.FLUID_HANDLER)
            return getTankFromWorld(this.level).cast();

        return super.getCapability(cap, side);
    }

    public static LazyOptional<EnergyStorage> getEnergyBufferForWorld(@Nullable Level level){
        LazyOptional<ITardisLevel> tardisHolder = Capabilities.getCap(Capabilities.TARDIS, level);
        if(tardisHolder.isPresent())
            return tardisHolder.orElseThrow(NullPointerException::new).getInteriorManager().getFEBuffer();
        return LazyOptional.empty();
    }

    public static LazyOptional<IFluidTank> getTankFromWorld(@Nullable Level level){
        LazyOptional<ITardisLevel> tardisHolder = Capabilities.getCap(Capabilities.TARDIS, level);
        if(tardisHolder.isPresent())
            return tardisHolder.orElseThrow(NullPointerException::new).getInteriorManager().getFluidBuffer();
        return LazyOptional.empty();
    }
}
