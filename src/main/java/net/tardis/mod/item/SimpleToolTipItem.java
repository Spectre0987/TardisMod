package net.tardis.mod.item;

import net.minecraft.network.chat.Component;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.TooltipFlag;
import net.minecraft.world.level.Level;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public class SimpleToolTipItem extends Item {

    public final Component[] tooltip;

    public SimpleToolTipItem(Properties pProperties, Component... tooltips) {
        super(pProperties);
        this.tooltip = tooltips;
    }

    @Override
    public void appendHoverText(ItemStack pStack, @Nullable Level pLevel, List<Component> tooltips, TooltipFlag pIsAdvanced) {
        for(Component text : this.tooltip){
            tooltips.add(text);
        }
        super.appendHoverText(pStack, pLevel, tooltips, pIsAdvanced);
    }
}
