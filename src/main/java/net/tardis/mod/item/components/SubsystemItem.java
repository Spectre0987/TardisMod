package net.tardis.mod.item.components;

import net.tardis.mod.block.BasicProps;
import net.tardis.mod.cap.level.ITardisLevel;
import net.tardis.mod.item.IEngineToggleable;
import net.tardis.mod.network.Network;
import net.tardis.mod.subsystem.Subsystem;
import net.tardis.mod.subsystem.SubsystemType;

import java.util.Optional;
import java.util.function.Supplier;

public class SubsystemItem extends TardisComponentItem implements IEngineToggleable {

    final Supplier<? extends SubsystemType<?>> subsystemType;

    public SubsystemItem(Properties p_41383_, Supplier<? extends SubsystemType<?>> type) {
        super(p_41383_, ComponentType.SUBSYSTEM);
        this.subsystemType = type;
    }

    public static Supplier<SubsystemItem> create(Supplier<? extends SubsystemType<?>> type, int durability){
        return () -> new SubsystemItem(BasicProps.Item.ONE.get().durability(durability), type);
    }

    public static Supplier<SubsystemItem> create(Supplier<? extends SubsystemType<?>> type){
        return create(type, 1000);
    }

    public SubsystemType<?> getSubsytemType(){
        return this.subsystemType.get();
    }


    @Override
    public void onToggle(ITardisLevel tardis, boolean active) {
        tardis.getSubsystem(this.getSubsytemType()).ifPresent(sys -> {
            sys.setActive(active);
        });
    }

    @Override
    public boolean isActive(ITardisLevel tardis) {
        Optional<? extends Subsystem> sysHolder = tardis.getSubsystem(this.getSubsytemType());
        return sysHolder.isPresent() && sysHolder.get().isActivated();
    }
}
