package net.tardis.mod.item.components;

import net.minecraft.world.item.Item;
import net.tardis.mod.block.BasicProps;
import net.tardis.mod.cap.level.ITardisLevel;
import net.tardis.mod.item.IEngineToggleable;
import net.tardis.mod.upgrade.tardis.BaseTardisUpgrade;
import net.tardis.mod.upgrade.types.TardisUpgradeType;

import java.util.Optional;
import java.util.function.Supplier;

public class UpgradeItem<T extends TardisUpgradeType<U>, U extends BaseTardisUpgrade> extends Item implements IEngineToggleable{

    final Supplier<T> type;

    public UpgradeItem(Properties pProperties, Supplier<T> type) {
        super(pProperties);
        this.type = type;
    }

    public static <T extends TardisUpgradeType<U>, U extends BaseTardisUpgrade> UpgradeItem<T, U> create(Supplier<T> type){
        return new UpgradeItem(
                BasicProps.Item.ONE.get(),
                type
        );
    }

    @Override
    public void onToggle(ITardisLevel tardis, boolean active) {
        tardis.getUpgrade(this.type.get()).ifPresent(u -> {
            u.setActive(active);
        });
    }

    @Override
    public boolean isActive(ITardisLevel tardis) {
        Optional<? extends BaseTardisUpgrade> upgrade = tardis.getUpgrade(type.get());
        return upgrade.isPresent() && upgrade.get().isActive();
    }
}
