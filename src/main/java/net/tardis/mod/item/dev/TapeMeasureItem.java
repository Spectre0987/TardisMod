package net.tardis.mod.item.dev;

import net.minecraft.core.BlockPos;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.network.chat.Component;
import net.minecraft.world.InteractionResult;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.TooltipFlag;
import net.minecraft.world.item.context.UseOnContext;
import net.minecraft.world.level.Level;
import net.tardis.mod.block.BasicProps;
import net.tardis.mod.helpers.Helper;
import org.jetbrains.annotations.Nullable;

import java.util.List;
import java.util.Optional;

public class TapeMeasureItem extends Item {
    public TapeMeasureItem(Properties pProperties) {
        super(pProperties);
    }

    public static TapeMeasureItem create(){
        return new TapeMeasureItem(BasicProps.Item.ONE.get());
    }

    @Override
    public InteractionResult useOn(UseOnContext pContext) {
        if(pContext.getPlayer().isShiftKeyDown()){
            clearPoints(pContext.getItemInHand());
        }
        else{

            //Set second point if we already have one
            if(getPoint(pContext.getItemInHand(), true).isPresent()){
                setPoint(pContext.getItemInHand(), pContext.getClickedPos(), false);
            }
            else{
                setPoint(pContext.getItemInHand(), pContext.getClickedPos(), true);
            }

        }
        return super.useOn(pContext);
    }

    @Override
    public void appendHoverText(ItemStack pStack, @Nullable Level pLevel, List<Component> pTooltipComponents, TooltipFlag pIsAdvanced) {
        Optional<BlockPos> first = getPoint(pStack, true);
        Optional<BlockPos> second = getPoint(pStack, false);

        //If we have both, caclulate the difference
        if(first.isPresent() && second.isPresent()){
            BlockPos diff = Helper.max(first.get(), second.get()).subtract(Helper.min(first.get(), second.get()));
            pTooltipComponents.add(Component.literal("Distance: %d, %d, %d".formatted(diff.getX(), diff.getY(), diff.getZ())));
        }

        super.appendHoverText(pStack, pLevel, pTooltipComponents, pIsAdvanced);
    }

    public static void clearPoints(ItemStack stack){
        if(stack.getTag() != null && stack.getTag().contains("measure")){
            stack.getTag().remove("measure");
        }
    }

    public static Optional<BlockPos> getPoint(ItemStack stack, boolean first){

        if(stack.getTag() != null && stack.getTag().contains("measure")){
            final CompoundTag tag = stack.getTag().getCompound("measure");
            if(tag.contains(getKey(first))){
                return Optional.of(BlockPos.of(tag.getLong(getKey(first))));
            }
        }

        return Optional.empty();
    }

    public static String getKey(boolean first){
        return first ? "first" : "second";
    }

    public static void setPoint(ItemStack stack, BlockPos pos, boolean first){

        if(!stack.getOrCreateTag().contains("measure")){
            stack.getOrCreateTag().put("measure", new CompoundTag());
        }

        final CompoundTag tag = stack.getOrCreateTag().getCompound("measure");

        tag.putLong(getKey(first), pos.asLong());

        stack.getOrCreateTag().put("measure", tag);

    }

}
