package net.tardis.mod.item;

import net.minecraft.nbt.CompoundTag;
import net.minecraft.network.chat.Component;
import net.minecraft.resources.ResourceKey;
import net.minecraft.world.InteractionResult;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.TooltipFlag;
import net.minecraft.world.item.context.UseOnContext;
import net.minecraft.world.level.Level;
import net.minecraftforge.common.capabilities.ForgeCapabilities;
import net.minecraftforge.common.capabilities.ICapabilityProvider;
import net.minecraftforge.common.util.LazyOptional;
import net.tardis.mod.Constants;
import net.tardis.mod.block.BasicProps;
import net.tardis.mod.cap.Capabilities;
import net.tardis.mod.cap.items.IRemoteItemCapability;
import net.tardis.mod.cap.items.StattenhiemRemoteCapability;
import net.tardis.mod.cap.level.ITardisLevel;
import net.tardis.mod.misc.IAttunable;
import net.tardis.mod.misc.SpaceTimeCoord;
import org.jetbrains.annotations.Nullable;

import java.util.List;
import java.util.Optional;

public class StattenheimRemoteItem extends Item implements IAttunable {
    public StattenheimRemoteItem(Properties pProperties) {
        super(pProperties);
    }

    public static StattenheimRemoteItem create(){
        return new StattenheimRemoteItem(
                BasicProps.Item.ONE.get()
        );
    }

    @Override
    public InteractionResult useOn(UseOnContext pContext) {
        if(!pContext.getLevel().isClientSide()){
            pContext.getItemInHand().getCapability(Capabilities.REMOTE).ifPresent(remote -> remote.flyTo(
                    pContext.getPlayer(),
                    pContext.getHand(),
                    new SpaceTimeCoord(pContext.getLevel().dimension(), pContext.getClickedPos().above(), pContext.getPlayer().getDirection().getOpposite())
            ));
        }
        return InteractionResult.sidedSuccess(pContext.getLevel().isClientSide());
    }

    @Override
    public @Nullable ICapabilityProvider initCapabilities(ItemStack stack, @Nullable CompoundTag nbt) {
        return new MultipleCapabilityProvider(new StattenhiemRemoteCapability(stack))
                .add(Capabilities.REMOTE)
                .add(ForgeCapabilities.ENERGY);
    }

    @Override
    public ItemStack onAttuned(ITardisLevel tardis, ItemStack stack) {
        stack.getCapability(Capabilities.REMOTE).ifPresent(remote -> remote.attuneTo(tardis.getId()));
        return stack;
    }

    @Override
    public int getTicksToAttune() {
        return 15 * 60 * 20; //15 Minutes
    }

    @Override
    public @Nullable CompoundTag getShareTag(ItemStack stack) {
        return super.getShareTag(stack);
    }

    @Override
    public void readShareTag(ItemStack stack, @Nullable CompoundTag nbt) {
        super.readShareTag(stack, nbt);
    }

    @Override
    public Optional<ResourceKey<Level>> getAttunedTardis(ItemStack stack) {
        LazyOptional<IRemoteItemCapability> capHolder = stack.getCapability(Capabilities.REMOTE);
        if(capHolder.isPresent()){
            return capHolder.orElseThrow(NullPointerException::new).getBoundTardis();
        }
        return Optional.empty();
    }

    @Override
    public void appendHoverText(ItemStack pStack, @Nullable Level pLevel, List<Component> pTooltipComponents, TooltipFlag pIsAdvanced) {
        IAttunable.super.appendHoverText(pStack, pLevel, pTooltipComponents, pIsAdvanced);
        Capabilities.getCap(ForgeCapabilities.ENERGY, pStack).ifPresent(battery -> pTooltipComponents.add(Constants.Translation.translatePower(battery.getEnergyStored(), battery.getMaxEnergyStored())));
    }
}
