package net.tardis.mod.item;

import net.minecraft.world.item.Item;
import net.tardis.mod.upgrade.types.SonicUpgradeType;

import java.util.function.Supplier;

public class SonicUpgradeItem<T extends SonicUpgradeType<?>> extends Item {


    final Supplier<T> upgradeType;


    public SonicUpgradeItem(Properties pProperties, Supplier<T> upgradeType) {
        super(pProperties);
        this.upgradeType = upgradeType;
    }

    public SonicUpgradeType<?> getUpgradeType(){
        return this.upgradeType.get();
    }
}
