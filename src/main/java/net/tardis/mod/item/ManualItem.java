package net.tardis.mod.item;

import net.minecraft.server.level.ServerPlayer;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResultHolder;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.Level;
import net.tardis.mod.block.BasicProps;
import net.tardis.mod.client.gui.datas.GuiDatas;
import net.tardis.mod.network.Network;
import net.tardis.mod.network.packets.OpenGuiDataMessage;

public class ManualItem extends Item {
    public ManualItem(Properties pProperties) {
        super(pProperties);
    }

    public static ManualItem create(){
        return new ManualItem(BasicProps.Item.ONE.get());
    }

    @Override
    public InteractionResultHolder<ItemStack> use(Level pLevel, Player pPlayer, InteractionHand pUsedHand) {
        if(!pLevel.isClientSide()){
            Network.sendTo((ServerPlayer) pPlayer, new OpenGuiDataMessage(GuiDatas.MANUAL.create()));
        }
        return super.use(pLevel, pPlayer, pUsedHand);
    }
}
