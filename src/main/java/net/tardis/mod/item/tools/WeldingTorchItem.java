package net.tardis.mod.item.tools;

import net.minecraft.world.InteractionResult;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.UseAnim;
import net.minecraft.world.item.context.UseOnContext;
import net.tardis.mod.block.BasicProps;

public class WeldingTorchItem extends Item {

    public WeldingTorchItem(Properties prop) {
        super(prop);
    }

    public static WeldingTorchItem create(){
        return new WeldingTorchItem(
                BasicProps.Item.ONE.get()
        );
    }

    @Override
    public boolean onEntitySwing(ItemStack stack, LivingEntity entity) {
        return true;
    }

    @Override
    public UseAnim getUseAnimation(ItemStack pStack) {
        return UseAnim.BOW;
    }

    @Override
    public int getUseDuration(ItemStack pStack) {
        return 72000;
    }

}
