package net.tardis.mod.item.tools;

import net.minecraft.nbt.CompoundTag;
import net.minecraft.network.chat.Component;
import net.minecraft.resources.ResourceKey;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResult;
import net.minecraft.world.InteractionResultHolder;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.TooltipFlag;
import net.minecraft.world.item.context.UseOnContext;
import net.minecraft.world.level.Level;
import net.minecraftforge.common.capabilities.ICapabilityProvider;
import net.minecraftforge.common.util.LazyOptional;
import net.tardis.mod.Constants;
import net.tardis.mod.cap.Capabilities;
import net.tardis.mod.cap.GenericProvider;
import net.tardis.mod.cap.items.DiagnosticItemCapability;
import net.tardis.mod.cap.items.ICFLTool;
import net.tardis.mod.cap.items.functions.cfl.ITrackingFunction;
import net.tardis.mod.cap.level.ITardisLevel;
import net.tardis.mod.misc.IAttunable;
import org.jetbrains.annotations.Nullable;

import java.util.List;
import java.util.Optional;

public class CFLItem extends Item implements IAttunable {

    public CFLItem() {
        super(new Properties().stacksTo(1));
    }

    @Override
    public InteractionResultHolder<ItemStack> use(Level world, Player p, InteractionHand hand) {
        final ItemStack stack = p.getItemInHand(hand);
        if(!world.isClientSide()){
            final LazyOptional<ICFLTool> toolHolder = stack.getCapability(Capabilities.CFL);
            if(toolHolder.isPresent()){

                return toolHolder.orElseThrow(NullPointerException::new).use(world, p, hand);
            }
            return InteractionResultHolder.pass(stack);
        }
        return InteractionResultHolder.success(stack);
    }

    @Override
    public InteractionResult useOn(UseOnContext pContext) {
        final LazyOptional<ICFLTool> toolCap = pContext.getItemInHand().getCapability(Capabilities.CFL);

        if(!toolCap.isPresent()){
            return super.useOn(pContext);
        }

        return toolCap.orElseThrow(NullPointerException::new).useOn(pContext);

    }

    @Override
    public void inventoryTick(ItemStack pStack, Level pLevel, Entity pEntity, int pSlotId, boolean pIsSelected) {
        super.inventoryTick(pStack, pLevel, pEntity, pSlotId, pIsSelected);
        if(pEntity instanceof Player player){
            pStack.getCapability(Capabilities.CFL).ifPresent(tool -> tool.tick(player));
        }
    }

    @Override
    public @Nullable ICapabilityProvider initCapabilities(ItemStack stack, @Nullable CompoundTag nbt) {
        final GenericProvider provider = new GenericProvider<>(Capabilities.CFL, new DiagnosticItemCapability(stack));
        if(nbt != null){
            provider.deserializeNBT(nbt);
        }
        return provider;
    }

    @Override
    public void appendHoverText(ItemStack pStack, @Nullable Level pLevel, List<Component> tooltip, TooltipFlag pIsAdvanced) {
        pStack.getCapability(Capabilities.CFL).ifPresent(tool -> {
            tool.getBoundTARDISKey().ifPresent(key -> {
                tooltip.add(Constants.Translation.makeTardisTranslation(key));
            });

            tool.getCurrentFunction().ifPresent(func -> {
                if(func instanceof ITrackingFunction track){
                    tooltip.add(track.getTrackingTypeTitle());
                }
            });

        });
        super.appendHoverText(pStack, pLevel, tooltip, pIsAdvanced);
    }



    @Override
    public ItemStack onAttuned(ITardisLevel tardis, ItemStack stack) {
        stack.getCapability(Capabilities.CFL).ifPresent(tool -> {
            tool.attuneTo(tardis.getId());
        });
        return stack;
    }

    @Override
    public int getTicksToAttune() {
        return 200;
    }

    @Override
    public Optional<ResourceKey<Level>> getAttunedTardis(ItemStack stack) {
        final LazyOptional<ICFLTool> capHolder = stack.getCapability(Capabilities.CFL);
        if(capHolder.isPresent()){
            return capHolder.orElseThrow(NullPointerException::new).getBoundTARDISKey();
        }
        return Optional.empty();
    }
}
