package net.tardis.mod.item;

import net.minecraft.nbt.CompoundTag;
import net.minecraft.network.chat.Component;
import net.minecraft.util.Mth;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResultHolder;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.ShieldItem;
import net.minecraft.world.item.TooltipFlag;
import net.minecraft.world.level.Level;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.capabilities.ForgeCapabilities;
import net.minecraftforge.common.capabilities.ICapabilityProvider;
import net.minecraftforge.common.util.LazyOptional;
import net.minecraftforge.energy.EnergyStorage;
import net.minecraftforge.energy.IEnergyStorage;
import net.tardis.mod.Constants;
import net.tardis.mod.block.BasicProps;
import net.tardis.mod.cap.Capabilities;
import net.tardis.mod.cap.GenericProvider;
import net.tardis.mod.cap.items.BasicEnergyCapProvider;
import org.jetbrains.annotations.Nullable;

import java.security.KeyStore;
import java.util.List;

public class PersonalShieldItem extends ShieldItem {
    public PersonalShieldItem(Properties pProperties) {
        super(pProperties);
    }

    public static PersonalShieldItem create(){
        return new PersonalShieldItem(
                BasicProps.Item.ONE.get()
        );
    }

    @Override
    public InteractionResultHolder<ItemStack> use(Level pLevel, Player pPlayer, InteractionHand pHand) {
        // If this has no more power, do not let you block with this
        LazyOptional<IEnergyStorage> batteryHolder = Capabilities.getCap(ForgeCapabilities.ENERGY, pPlayer.getItemInHand(pHand));
        if(batteryHolder.isPresent() && batteryHolder.orElseThrow(NullPointerException::new).getEnergyStored() <= 0){
            return InteractionResultHolder.fail(pPlayer.getItemInHand(pHand));
        }

        //Use power
        batteryHolder.orElseThrow(NullPointerException::new).extractEnergy(1, false);

        return super.use(pLevel, pPlayer, pHand);
    }

    @Override
    public boolean isValidRepairItem(ItemStack pToRepair, ItemStack pRepair) {
        return false;
    }

    @Override
    public @Nullable ICapabilityProvider initCapabilities(ItemStack stack, @Nullable CompoundTag nbt) {
        BasicEnergyCapProvider provider = new BasicEnergyCapProvider(stack, new EnergyStorage(1000));
        if(nbt != null)
            provider.deserializeNBT(nbt);
        return provider;
    }

    @Override
    public void appendHoverText(ItemStack pStack, @Nullable Level pLevel, List<Component> pTooltip, TooltipFlag pFlag) {
        super.appendHoverText(pStack, pLevel, pTooltip, pFlag);
        pStack.getCapability(ForgeCapabilities.ENERGY).ifPresent(power -> {
            pTooltip.add(Constants.Translation.translatePower(power));
        });
    }

    @Override
    public boolean isBarVisible(ItemStack pStack) {
        return true;
    }

    @Override
    public int getBarWidth(ItemStack pStack) {
        LazyOptional<IEnergyStorage> batteryHolder = pStack.getCapability(ForgeCapabilities.ENERGY);
        if(batteryHolder.isPresent()){
            IEnergyStorage battery = batteryHolder.orElseThrow(NullPointerException::new);
            return Mth.floor((battery.getEnergyStored() / (float)battery.getMaxEnergyStored()) * 13);
        }
        return 0;
    }
}
