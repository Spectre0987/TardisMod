package net.tardis.mod.exterior.data;

import net.tardis.mod.blockentities.exteriors.IExteriorObject;

public abstract class ExteriorData {

    final ExteriorDataType<?> type;
    final IExteriorObject ext;

    public ExteriorData(ExteriorDataType<?> type, IExteriorObject ext){
        this.type = type;
        this.ext = ext;
    }

    public ExteriorDataType<?> getType(){
        return this.type;
    }

    public IExteriorObject getParent(){
        return this.ext;
    }

}
