package net.tardis.mod.exterior;

import net.minecraft.core.BlockPos;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.level.Level;
import net.minecraft.world.phys.AABB;
import net.minecraft.world.phys.Vec3;
import net.minecraftforge.common.util.INBTSerializable;
import net.tardis.mod.cap.level.ITardisLevel;
import net.tardis.mod.misc.*;

import java.util.Optional;

/** Raw template for ExteriorsTypes.*/
public abstract class Exterior extends TypeHolder<ExteriorType> implements INBTSerializable<CompoundTag> {

    public final ITardisLevel tardis;

    public Exterior(ExteriorType type, ITardisLevel level){
        super(type);
        this.tardis = level;
    }

    public abstract void demat(Level level);
    public abstract  void remat(Level level);
    public abstract void setPosition(Level level, BlockPos pos);
    public abstract void delete(Level level, BlockPos pos);
    public abstract void fly(Vec3 motion);
    public abstract void placeEntityAt(ServerLevel exteriorLevel, Entity e);
    public abstract TeleportEntry.LocationData getEntityTeleportTarget(ServerLevel exteriorLevel, Entity e);
    public abstract void playMoodChangedEffects(Level level, SpaceTimeCoord location, boolean isPositive);

    public abstract void setDoorHandler(DoorHandler other);
    public abstract Optional<DoorHandler> getDoorHandler();
    public abstract AABB getSize();

    public abstract TeleportHandler<?> getTeleportHandler();
}
