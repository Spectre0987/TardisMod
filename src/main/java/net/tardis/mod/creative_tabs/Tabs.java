package net.tardis.mod.creative_tabs;

import net.minecraft.network.chat.Component;
import net.minecraft.world.item.CreativeModeTab;
import net.minecraft.world.item.ItemStack;
import net.minecraftforge.event.CreativeModeTabEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.registries.ForgeRegistries;
import net.tardis.mod.Tardis;
import net.tardis.mod.registry.BlockRegistry;
import net.tardis.mod.helpers.Helper;
import net.tardis.mod.tags.ItemTags;

@Mod.EventBusSubscriber(modid = Tardis.MODID, bus = Mod.EventBusSubscriber.Bus.MOD)
public class Tabs {


    public static CreativeModeTab MAIN;
    public static CreativeModeTab ROUNDELS;



    public static String buildName(String name){
        return "creativetab." + Tardis.MODID + name;
    }

    @SubscribeEvent
    public static void registerTabs(CreativeModeTabEvent.Register event){
        MAIN = event.registerCreativeModeTab(Helper.createRL("main"), builder -> {
            builder.
                    icon(() -> new ItemStack(BlockRegistry.G8_CONSOLE.get()))
                    .title(Component.translatable(buildName("main")))
                    .build();
        });

        ROUNDELS = event.registerCreativeModeTab(Helper.createRL("roundel"), builder -> {
            builder.
                    icon(() -> new ItemStack(BlockRegistry.XION.get()))
                    .title(Component.translatable(buildName("roundels")))
                    .build();
        });
    }

    @SubscribeEvent
    public static void addItemsEvent(CreativeModeTabEvent.BuildContents event){
        if(event.getTab() == MAIN){
            ForgeRegistries.ITEMS.tags().getTag(ItemTags.CREATIVE_TAB_COMPONENTS).forEach(event::accept);
        }
        else if(event.getTab() == ROUNDELS){
            ForgeRegistries.ITEMS.tags().getTag(ItemTags.CREATIVE_TAB_ROUNDEL).forEach(event::accept);
        }
    }

}
