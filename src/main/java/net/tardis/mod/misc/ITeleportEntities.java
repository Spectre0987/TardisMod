package net.tardis.mod.misc;

public interface ITeleportEntities<T> {

    TeleportHandler<T> getTeleportHandler();

}
