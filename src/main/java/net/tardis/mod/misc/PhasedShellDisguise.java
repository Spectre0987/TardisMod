package net.tardis.mod.misc;

import com.mojang.serialization.Codec;
import com.mojang.serialization.codecs.RecordCodecBuilder;
import net.minecraft.core.BlockPos;
import net.minecraft.core.Holder;
import net.minecraft.core.HolderSet;
import net.minecraft.core.Registry;
import net.minecraft.core.registries.Registries;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.tags.BiomeTags;
import net.minecraft.tags.TagKey;
import net.minecraft.world.level.biome.Biome;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.levelgen.structure.Structure;
import net.tardis.mod.helpers.Helper;
import oshi.util.tuples.Pair;

import java.util.*;

public record PhasedShellDisguise(Map<BlockPos, BlockState> disguise, List<TagKey<Biome>> validBioms, Optional<List<TagKey<Structure>>> structures) {

    public static final Codec<Pair<BlockPos, BlockState>> MAP_CODEC = RecordCodecBuilder.create(i ->
            i.group(
                    BlockPos.CODEC.fieldOf("position").forGetter(Pair::getA),
                    BlockState.CODEC.fieldOf("state").forGetter(Pair::getB)
            ).apply(i, (pos, state) -> new Pair<>(pos, state))
    );
    public static final Codec<PhasedShellDisguise> CODEC = RecordCodecBuilder.create(i ->
        i.group(
                MAP_CODEC.listOf().fieldOf("disguise").forGetter(PhasedShellDisguise::getPairsForCodec),
                TagKey.codec(Registries.BIOME).listOf().fieldOf("valid_biomes").forGetter(PhasedShellDisguise::validBioms),
                TagKey.codec(Registries.STRUCTURE).listOf().optionalFieldOf("valid_structures").forGetter(PhasedShellDisguise::structures)
        ).apply(i, PhasedShellDisguise::deserializeFromCodec)
    );

    public List<Pair<BlockPos, BlockState>> getPairsForCodec(){
        List<Pair<BlockPos, BlockState>> list = new ArrayList<>();
        for(Map.Entry<BlockPos, BlockState> pos : this.disguise().entrySet()){
            list.add(new Pair<>(pos.getKey(), pos.getValue()));
        }
        return list;
    }

    public static PhasedShellDisguise deserializeFromCodec(List<Pair<BlockPos, BlockState>> list, List<TagKey<Biome>> biome, Optional<List<TagKey<Structure>>> structure){
        final HashMap<BlockPos, BlockState> map = new HashMap<>();

        for(Pair<BlockPos, BlockState> pos : list){
            map.put(pos.getA(), pos.getB());
        }

        return new PhasedShellDisguise(map, biome, structure);
    }

    public boolean isInValidStructure(ServerLevel level, BlockPos pos, Registry<Structure> structureReg){
        if(!this.structures().isPresent()){
            return true;
        }
        Set<Structure> structuresIn = level.structureManager().getAllStructuresAt(pos).keySet();
        return structuresIn.stream().anyMatch(s -> isStructureInKey(structureReg, s));
    }

    public boolean isInBiome(Registry<Biome> reg, Holder<Biome> b){
        for(TagKey<Biome> key : this.validBioms()){
            final Optional<HolderSet.Named<Biome>> tag = reg.getTag(key);
            if(tag.isPresent() && tag.get().contains(b)){
                return true;
            }
            //TODO: Error logging
        }
        return false;
    }

    private boolean isStructureInKey(Registry<Structure> reg, Structure s){
        for(TagKey<Structure> key : this.structures().get()){
            if(reg.getTag(key).get().contains(reg.wrapAsHolder(s))){
                return true;
            }
        }
        return false;
    }


    public static class Builder{

        final HashMap<BlockPos, BlockState> map = new HashMap<>();
        final List<TagKey<Biome>> biomes = new ArrayList<>();
        List<TagKey<Structure>> structures;

        public Builder add(BlockPos offset, BlockState state){
            map.put(offset, state);
            return this;
        }

        public Builder add(BlockPos offset, Block block){
            return add(offset, block.defaultBlockState());
        }

        public Builder addBiomeTag(TagKey<Biome> biomes){
            this.biomes.add(biomes);
            return this;
        }

        public Builder setStructureTag(TagKey<Structure> structures){
            if(structures == null)
                this.structures = new ArrayList<>();
            this.structures.add(structures);
            return this;
        }

        public PhasedShellDisguise build(){
            return new PhasedShellDisguise(map, biomes, Helper.nullableToOptional(this.structures));
        }

    }

}
