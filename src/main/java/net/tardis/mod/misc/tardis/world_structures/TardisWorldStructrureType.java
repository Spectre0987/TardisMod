package net.tardis.mod.misc.tardis.world_structures;

import net.minecraft.core.BlockPos;
import net.tardis.mod.cap.level.ITardisLevel;
import org.apache.commons.lang3.function.TriFunction;

public record TardisWorldStructrureType<T extends TardisWorldStructure>(TriFunction<TardisWorldStructrureType<T>, ITardisLevel, BlockPos, T> supplier) {

    public T create(ITardisLevel tardis, BlockPos pos){
        return this.supplier().apply(this, tardis, pos);
    }

}
