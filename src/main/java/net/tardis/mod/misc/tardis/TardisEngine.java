package net.tardis.mod.misc.tardis;

import net.minecraft.core.Direction;
import net.minecraft.core.NonNullList;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.server.level.ServerPlayer;
import net.minecraft.util.Mth;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.inventory.MenuType;
import net.minecraft.world.item.ItemStack;
import net.minecraftforge.common.capabilities.ForgeCapabilities;
import net.minecraftforge.common.util.INBTSerializable;
import net.minecraftforge.items.ItemStackHandler;
import net.tardis.mod.cap.Capabilities;
import net.tardis.mod.cap.level.ITardisLevel;
import net.tardis.mod.item.components.IArtronCapacitorItem;
import net.tardis.mod.menu.MenuRegistry;
import net.tardis.mod.misc.IAttunable;
import net.tardis.mod.misc.ItemStackHandlerWithListener;
import net.tardis.mod.network.Network;
import net.tardis.mod.network.packets.AttumentProgressMessage;
import net.tardis.mod.network.packets.UpdateTardisEngineContents;
import net.tardis.mod.resource_listener.server.ItemArtronValueReloader;

import java.util.Optional;

public class TardisEngine implements INBTSerializable<CompoundTag> {

    public static final int ATTUNEMENT_SLOT = 4;


    private ITardisLevel tardis;
    private ItemStackHandlerWithListener subsystems = new ItemStackHandlerWithListener(EngineSide.SUBSYSTEMS.slots).onChange(this::onSubsystemInvChanged);
    private ItemStackHandlerWithListener upgrades = new ItemStackHandlerWithListener(EngineSide.UPGRADES.slots);
    private ItemStackHandlerWithListener charging = new ItemStackHandlerWithListener(EngineSide.CHARGING.slots);
    private ItemStackHandlerWithListener capacitors = new ItemStackHandlerWithListener(EngineSide.CAPACITORS.slots).onChange(this::caclulateArtron);

    public int attunementTicksFlown = 0;

    public ItemStackHandler getInventoryFor(EngineSide side){
        switch(side){
            default: return this.subsystems;
            case UPGRADES: return this.upgrades;
            case CAPACITORS: return this.capacitors;
            case CHARGING: return this.charging;
        }
    }

    public ItemStackHandler getInventoryFor(Direction dir){
        return getInventoryFor(EngineSide.getForDirection(dir).get());
    }

    public TardisEngine(ITardisLevel tardis){
        this.tardis = tardis;
    }

    public void onSubsystemInvChanged(int slot){
        if(!this.tardis.isClient()) {
            /*
            Tardis.LOGGER.log(Level.DEBUG, "Subsystem inv was changed!");
            ItemStack stack = this.subsystems.getStackInSlot(slot);

            //Save old subsystem data
            Map<SubsystemType, CompoundTag> oldSystemsTags = new HashMap<>();

            for (SubsystemType type : SubsystemRegistry.TYPE_REGISTRY.get()) {
                tardis.getSubsystem(type).ifPresent(sys -> {
                    oldSystemsTags.put(type, sys.serializeNBT());
                });
            }

            //Create new subsystem list
            this.tardis.createSubsystems();

            //Restore data to components that are still there
            for (Map.Entry<SubsystemType, CompoundTag> entry : oldSystemsTags.entrySet()) {
                this.tardis.getSubsystem(entry.getKey()).ifPresent(sys -> {
                    sys.deserializeNBT(entry.getValue());
                });
            }

             */
        }
    }

    public void caclulateArtron(int slotChanged){
        int maxArtron = 0;
        for(int i = 0; i < capacitors.getSlots(); ++i){
            final ItemStack stack = capacitors.getStackInSlot(i);
            if(stack.getItem() instanceof IArtronCapacitorItem cap){
                maxArtron += cap.getMaxArtron(stack);
            }
            //Fill tardis with artron from this cap
            stack.getCapability(Capabilities.ARTRON_TANK_ITEM).ifPresent(a -> {
                tardis.getFuelHandler().fillArtron(a.takeArtron(Float.MAX_VALUE, false), false);
            });
        }
        tardis.getFuelHandler().maxArtron = maxArtron;
    }

    public void tick(){

        if(tardis.isClient())
            return;

        //If this has an attunable item
        if(tardis.isInVortex() && !tardis.isClient()){
            final ItemStack stack = this.getInventoryFor(EngineSide.CHARGING).getStackInSlot(ATTUNEMENT_SLOT);
            if(stack.getItem() instanceof IAttunable attune && attune.getAttunedTardis(stack).isEmpty()){
                ++this.attunementTicksFlown;

                if(tardis.getLevel().getGameTime() % 20 == 10 && tardis.getLevel() instanceof ServerLevel server){
                    for(ServerPlayer player : server.players()){
                        if(player.containerMenu != null && player.containerMenu.getType() == MenuRegistry.TARDIS_ENGINE.get()){
                            Network.sendTo(player, new AttumentProgressMessage(this.attunementTicksFlown));
                        }
                    }
                }

                //If complete
                if(this.attunementTicksFlown >= stack.getCount() * attune.getTicksToAttune()){
                    this.getInventoryFor(EngineSide.CHARGING)
                            .setStackInSlot(ATTUNEMENT_SLOT, attune.onAttuned(this.tardis, stack));
                    Network.sendPacketToDimension(tardis.getId(), new UpdateTardisEngineContents(EngineSide.CHARGING, ATTUNEMENT_SLOT, getInventoryFor(EngineSide.CHARGING).getStackInSlot(ATTUNEMENT_SLOT)));
                }

            }
            else this.attunementTicksFlown = 0;
        }
        //Fill artron from crystals
        for(int i = 0; i < EngineSide.CAPACITORS.slots; ++i){
            ItemStack stack = capacitors.getStackInSlot(i);
            final float fuelVal = ItemArtronValueReloader.ArtronValue.getArtronValue(stack);
            if(fuelVal > 0){

                int stackAmountToTake = Mth.ceil(tardis.getFuelHandler().fillArtron(fuelVal * stack.getCount(), false) / fuelVal);
                if(stackAmountToTake > 0){

                    if(stackAmountToTake >= stack.getCount()){
                        capacitors.setStackInSlot(i, ItemStack.EMPTY);
                    }
                    else capacitors.setStackInSlot(i, capacitors.getStackInSlot(i).copyWithCount(stack.getCount() - stackAmountToTake));
                }
            }
        }
        //Charge FE
        for(int i = 0; i < 4; ++i){
            final ItemStack stack = this.charging.getStackInSlot(i);
            final int slotIndex = i;
            stack.getCapability(ForgeCapabilities.ENERGY)
                    .ifPresent(cap -> {
                        if(cap.receiveEnergy(1, false) > 0)
                            this.charging.setStackInSlot(slotIndex, stack);
                    });
        }
    }

    @Override
    public CompoundTag serializeNBT() {
        CompoundTag tag = new CompoundTag();
        tag.put("subsystems", this.subsystems.serializeNBT());
        tag.put("upgrades", this.upgrades.serializeNBT());
        tag.put("charging", this.charging.serializeNBT());
        tag.put("capacitors", this.capacitors.serializeNBT());
        return tag;
    }

    @Override
    public void deserializeNBT(CompoundTag nbt) {
        this.subsystems.deserializeNBT(nbt.getCompound("subsystems"));
        this.upgrades.deserializeNBT(nbt.getCompound("upgrades"));
        this.charging.deserializeNBT(nbt.getCompound("charging"));
        this.capacitors.deserializeNBT(nbt.getCompound("capacitors"));

        fixEngineSizeIfBroken();

    }

    public void fixEngineSizeIfBroken(){
        for(EngineSide side : EngineSide.values()){
            ItemStackHandler inv = getInventoryFor(side);
            if(inv.getSlots() != side.slots){
                NonNullList<ItemStack> old = NonNullList.withSize(inv.getSlots(), ItemStack.EMPTY);
                //Save Old Items
                for(int i = 0; i < old.size(); ++i){
                    old.set(i, inv.getStackInSlot(i));
                }
                //Set new size, which will clear the stored items
                getInventoryFor(side).setSize(side.slots);
                //Restore old items
                for(int i = 0; i < old.size(); ++i){
                    if(i >= side.slots){
                        break; //discard overfill if the new engine side is smaller than the old amount
                    }
                    getInventoryFor(side).setStackInSlot(i, old.get(i));
                }
            }
        }
    }

    public void setAttunmentProgress(int attunment) {
        this.attunementTicksFlown = attunment;
    }

    public enum EngineSide{
        SUBSYSTEMS(Direction.NORTH,9),
        UPGRADES(Direction.EAST, 10),
        CAPACITORS(Direction.SOUTH, 9),
        CHARGING(Direction.WEST, 5);

        final Direction dir;
        final int slots;

        EngineSide(Direction dir, int slots){
            this.dir = dir;
            this.slots = slots;
        }

        public Direction getSide(){
            return this.dir;
        }

        public static Optional<EngineSide> getForDirection(Direction dir){
            for(EngineSide side : EngineSide.values()){
                if(side.getSide() == dir)
                    return Optional.of(side);
            }
            return Optional.empty();
        }
    }

}
