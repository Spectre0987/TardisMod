package net.tardis.mod.misc.tardis.world_structures;

import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.state.BlockState;
import net.tardis.mod.registry.BlockRegistry;
import net.tardis.mod.cap.level.ITardisLevel;
import net.tardis.mod.helpers.Helper;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;

public class AtriumWorldStructure extends TardisWorldStructure{

    public HashMap<BlockPos, BlockState> shell = new HashMap<>();
    private String name;

    public AtriumWorldStructure(TardisWorldStructrureType<?> type, ITardisLevel tardis, BlockPos pos) {
        super(type, tardis, pos);
    }

    @Override
    public boolean isStillValid() {
        return true;
    }

    public List<BlockPos> getAllConnectedBlocks(){
        final List<BlockPos> positions = new ArrayList<>();
        for(Direction dir : Direction.values()){
            if(dir.getAxis().isHorizontal()){
                getConnected(tardis.getLevel(), this.getPosition().below().relative(dir).immutable(), positions);
            }
        }
        return positions;
    }

    private void getConnected(Level level, BlockPos pos, List<BlockPos> positions){
        if(positions.contains(pos))
            return;
        final BlockState state = level.getBlockState(pos);
        if(state.getBlock() == BlockRegistry.ATRIUM_PLATFORM_BLOCK.get()){
            positions.add(pos);
            for(Direction dir : Direction.values()){

                if(dir.getAxis().isHorizontal()){
                    this.getConnected(level, pos.relative(dir), positions);
                }
            }
        }
    }

    public void buildShell(List<BlockPos> platformPositions){
        this.shell.clear();

        for(BlockPos pos : platformPositions){
            for(int y = 1; y < 7; ++y){
                BlockPos newPos = pos.above(y);
                final BlockState state = tardis.getLevel().getBlockState(newPos);
                if(!state.isAir() && state.getBlock() != BlockRegistry.ATRIUM_MASTER.get()){
                    this.shell.put(newPos.subtract(getPosition()), state);
                }
            }
        }

    }

    public void setName(String name){
        this.name = name;
    }

    public Optional<String> getName(){
        return this.name == null ? Optional.empty() : Optional.of(this.name);
    }


    @Override
    public CompoundTag serializeNBT() {
        final CompoundTag tag = new CompoundTag();
        if(name != null)
            tag.putString("name", this.name);
        return tag;
    }

    @Override
    public void deserializeNBT(CompoundTag nbt) {
        Helper.<String>readOptionalNBT(nbt, "name", (t, n) -> t.getString(n)).ifPresent(this::setName);
    }
}
