package net.tardis.mod.misc.tardis;

import it.unimi.dsi.fastutil.ints.Int2ObjectArrayMap;
import net.minecraft.network.FriendlyByteBuf;
import net.minecraft.network.chat.Component;
import net.tardis.api.events.TardisEvent;
import net.tardis.mod.Tardis;
import net.tardis.mod.cap.level.ITardisLevel;
import org.jetbrains.annotations.Nullable;

import java.util.function.Function;

public class TardisNotifications {

    private static final Int2ObjectArrayMap<TardisNotif> NOTIFS = new Int2ObjectArrayMap<>();

    public static int CANT_LAND = createDefault("cant_land", 5);

    /**
     * Add-Ons don't use this, use {@link TardisNotifications#register(Function)}
     * @param notif String to be converted to a translatable component
     * @param time Time in seconds to display
     * @return ID of the notifications
     */
    public static int createDefault(String notif, int time){
        return register(id -> new TardisNotif(id, tardis ->
                    Component.translatable("notif." + Tardis.MODID + "." + notif),
                time * 20
        ));
    }

    public static @Nullable TardisNotif getById(int id){
        if(NOTIFS.containsKey(id))
            return NOTIFS.get(id);
        return null;
    }
    public static int register(Function<Integer, TardisNotif> not){
        final int nextId = NOTIFS.size();
        NOTIFS.put(nextId, not.apply(nextId));
        return nextId;
    }

    /**
     * TARDIS notifications are displayed on the monitor, as well as some bound tools
     * @param notif
     * @param ticksToDisplay
     */
    public record TardisNotif(int id, Function<ITardisLevel, Component> notif, int ticksToDisplay){}

    public record TardisNotifWrapper(TardisNotif notif, long worldTimeToExpire){

    }
}
