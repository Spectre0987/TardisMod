package net.tardis.mod.misc.tardis.montior;

import com.mojang.datafixers.kinds.Const;
import net.minecraft.network.chat.Component;
import net.minecraft.world.entity.player.Player;
import net.tardis.mod.Constants;
import net.tardis.mod.cap.level.ITardisLevel;
import net.tardis.mod.misc.tardis.montior.BasicMonitorFunction;
import net.tardis.mod.registry.MonitorFunctionRegistry;

import java.util.function.BiConsumer;
import java.util.function.Function;

public class BasicToggleMonitorFunction extends BasicMonitorFunction {

    final BiConsumer<ITardisLevel, Boolean> setter;
    final Function<ITardisLevel, Boolean> getter;

    public Component activeName;
    public Component deactiveName;

    public BasicToggleMonitorFunction(BiConsumer<ITardisLevel, Boolean> setter, Function<ITardisLevel, Boolean> getter){
        this.setter = setter;
        this.getter = getter;
    }

    @Override
    public void doServerAction(ITardisLevel tardis, Player player) {
        setter.accept(tardis, !getter.apply(tardis));
    }

    @Override
    public boolean doClientAction(ITardisLevel tardis, Player player) {
        return true;
    }

    @Override
    public Component getText(ITardisLevel level) {
        return this.getConditionalText(this.getter.apply(level));
    }

    public Component getConditionalText(boolean active){

        if(active){
            if(this.activeName == null){
                this.activeName = Component.translatable(
                        Constants.Translation.makeGenericTranslation(MonitorFunctionRegistry.REGISTRY.get(), this) +
                                ".active");
            }
            return this.activeName;
        }

        if(deactiveName == null){
            this.deactiveName = Component.translatable(
                    Constants.Translation.makeGenericTranslation(MonitorFunctionRegistry.REGISTRY.get(), this) +
                            ".inactive");
        }

        return this.deactiveName;

    }
}
