package net.tardis.mod.misc.tardis.montior;

import net.minecraft.world.entity.player.Player;
import net.tardis.mod.cap.level.ITardisLevel;
import net.tardis.mod.client.gui.datas.GuiData;
import net.tardis.mod.client.gui.datas.GuiDatas;

public class WaypointMonitorFunction extends OpenGuiMonitorFunction{
    @Override
    public GuiData getGui(ITardisLevel tardis, Player player) {
        return GuiDatas.MON_WAYPOINTS.create().fromTardis(tardis);
    }
}
