package net.tardis.mod.misc.tardis.montior;

import net.minecraft.network.chat.Component;
import net.minecraft.network.chat.MutableComponent;
import net.minecraft.resources.ResourceLocation;
import net.tardis.mod.Constants;
import net.tardis.mod.Tardis;
import net.tardis.mod.cap.level.ITardisLevel;
import net.tardis.mod.helpers.Helper;
import net.tardis.mod.registry.MonitorFunctionRegistry;

import javax.annotation.Nullable;

public abstract class BasicMonitorFunction implements MonitorFunction {

    public String name;

    @Override
    public Component getText(ITardisLevel level) {
        if(name == null){
            this.name = Constants.Translation.makeGenericTranslation(MonitorFunctionRegistry.REGISTRY.get(), this);
        }
        return Component.translatable(this.name);
    }

    @Override
    public boolean shouldDisplay(ITardisLevel tardis) {
        return true;
    }

    public String makeDefault(){
        return Constants.Translation.makeGenericTranslation(MonitorFunctionRegistry.REGISTRY.get(), this);
    }

    public String makeDefaultFeedback(){
        return Constants.Translation.makeGenericTranslation(MonitorFunctionRegistry.REGISTRY.get(), this) + ".feedback";
    }
}
