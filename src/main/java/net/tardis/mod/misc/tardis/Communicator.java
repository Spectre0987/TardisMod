package net.tardis.mod.misc.tardis;

import net.minecraft.core.Registry;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.world.level.levelgen.WorldGenSettings;
import net.minecraftforge.common.util.INBTSerializable;
import net.tardis.mod.cap.level.ITardisLevel;

public class Communicator implements INBTSerializable<CompoundTag> {

    final ITardisLevel tardis;

    public Communicator(ITardisLevel tardis){
        this.tardis = tardis;
    }

    @Override
    public CompoundTag serializeNBT() {
        final CompoundTag tag = new CompoundTag();

        return tag;
    }

    @Override
    public void deserializeNBT(CompoundTag tag) {

    }
}
