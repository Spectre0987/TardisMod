package net.tardis.mod.misc.tardis;

public class MonitorTransformData {

    private float[] bounds = new float[2];

    private float[] transform = new float[3];

    public MonitorTransformData(float width, float height, float x, float y, float z){
        this.bounds[0] = width;
        this.bounds[1] = height;

        this.transform[0] = x;
        this.transform[1] = y;
        this.transform[2] = z;
    }

    public float[] getDimensions(){
        return this.bounds;
    }

    public float[] getTransform(){
        return this.transform;
    }

}
