package net.tardis.mod.misc;

import com.mojang.serialization.Codec;
import com.mojang.serialization.codecs.RecordCodecBuilder;
import net.minecraft.resources.ResourceKey;
import net.minecraft.resources.ResourceLocation;
import net.tardis.mod.helpers.CodecHelper;

public record TextureVariant(ResourceKey<?> key, ResourceLocation texture) {

    public static final Codec<TextureVariant> CODEC = RecordCodecBuilder.create(i ->
                i.group(
                        CodecHelper.GENERIC_RESOURCE_KEY_CODEC.fieldOf("for").forGetter(TextureVariant::key),
                        ResourceLocation.CODEC.fieldOf("texture").forGetter(TextureVariant::texture)
                ).apply(i, TextureVariant::new)
            );

}
