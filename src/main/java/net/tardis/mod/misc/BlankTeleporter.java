package net.tardis.mod.misc;

import net.minecraft.server.level.ServerLevel;
import net.minecraft.server.level.ServerPlayer;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.level.portal.PortalInfo;
import net.minecraft.world.phys.Vec3;
import net.minecraftforge.common.util.ITeleporter;
import net.tardis.mod.cap.level.ITardisLevel;
import net.tardis.mod.misc.tardis.InteriorDoorData;
import org.jetbrains.annotations.Nullable;

import java.util.function.Function;

public class BlankTeleporter implements ITeleporter {

    private final PortalInfo position;

    public BlankTeleporter(PortalInfo portal){
        this.position = portal;
    }

    public BlankTeleporter(InteriorDoorData data, ITardisLevel tardis, Entity e){
        this(fromTardis(tardis, data, e));

    }

    public static PortalInfo fromTardis(ITardisLevel tardisLevel, InteriorDoorData data, Entity entity){
        return new PortalInfo(data.placeAtMe(entity, tardisLevel), entity.getDeltaMovement(), data.getRotation(tardisLevel), 0);
    }

    @Override
    public Entity placeEntity(Entity entity, ServerLevel currentWorld, ServerLevel destWorld, float yaw, Function<Boolean, Entity> repositionEntity) {
        return repositionEntity.apply(false);
    }

    @Override
    public @Nullable PortalInfo getPortalInfo(Entity entity, ServerLevel destWorld, Function<ServerLevel, PortalInfo> defaultPortalInfo) {
        return this.position;
    }

    @Override
    public boolean isVanilla() {
        return false;
    }

    @Override
    public boolean playTeleportSound(ServerPlayer player, ServerLevel sourceWorld, ServerLevel destWorld) {
        return false;
    }
}
