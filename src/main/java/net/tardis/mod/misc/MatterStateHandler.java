package net.tardis.mod.misc;

import net.minecraft.nbt.CompoundTag;
import net.minecraft.network.FriendlyByteBuf;
import net.minecraft.world.level.Level;
import net.minecraft.world.phys.Vec3;
import net.minecraftforge.common.util.INBTSerializable;
import net.tardis.mod.helpers.Helper;
import net.tardis.mod.misc.enums.MatterState;
import net.tardis.mod.registry.DematAnimationRegistry;

import java.util.function.Supplier;

public class MatterStateHandler implements INBTSerializable<CompoundTag> {

    private MatterState state = MatterState.SOLID;
    private long startTick;
    private long endTick;
    private DematAnimationRegistry.DematAnimationType dematType;
    public Runnable onDematComplete = () -> {};
    public Runnable onChanged = () -> {};
    Supplier<Vec3> position;
    Supplier<Float> radius = () -> 1.0F;

    public MatterStateHandler(Supplier<Vec3> positionSupplier){
        this.position = positionSupplier;
    }


    public MatterStateHandler setRemovedAction(Runnable action){
        this.onDematComplete = action;
        return this;
    }

    public MatterStateHandler setChangedAction(Runnable run){
        this.onChanged = run;
        return this;
    }

    public MatterStateHandler setRadius(Supplier<Float> radius){
        this.radius = radius;
        return this;
    }

    public void startMatterState(Level level, MatterState state, int animTime, DematAnimationRegistry.DematAnimationType dematType){
        this.state = state;
        this.dematType = dematType;
        this.startTick = level.getGameTime();
        this.endTick = level.getGameTime() + animTime;
        this.onChanged.run();
    }

    public void tick(Level level){

        if(level.getGameTime() > this.endTick){
            if(this.state == MatterState.DEMAT){
                this.onDematComplete.run();
                this.onChanged.run();
            }
            if(state == MatterState.REMAT){
                this.state = MatterState.SOLID;
                this.onChanged.run();
            }
        }

    }


    @Override
    public CompoundTag serializeNBT() {
        final CompoundTag tag = new CompoundTag();
        tag.putInt("state", this.state.ordinal());
        tag.putLong("start_tick", this.startTick);
        tag.putLong("end_tick", this.endTick);
        Helper.writeRegistryToNBT(tag, DematAnimationRegistry.REGISTRY.get(), this.dematType, "demat_type");
        return tag;
    }

    @Override
    public void deserializeNBT(CompoundTag tag) {
        this.state = MatterState.values()[tag.getInt("state")];
        this.startTick = tag.getLong("start_tick");
        this.endTick = tag.getLong("end_tick");
        Helper.readRegistryFromString(tag, DematAnimationRegistry.REGISTRY.get(), "demat_type").ifPresent(type -> this.dematType = type);
    }

    public void encode(FriendlyByteBuf buf){
        buf.writeEnum(this.state);
        buf.writeLong(this.startTick);
        buf.writeLong(this.endTick);
        buf.writeRegistryId(DematAnimationRegistry.REGISTRY.get(), this.getDematType());
    }

    public static MatterStateHandler decode(FriendlyByteBuf buf){
        final MatterStateHandler handler = new MatterStateHandler(() -> Vec3.ZERO);
        handler.state = buf.readEnum(MatterState.class);
        handler.startTick = buf.readLong();
        handler.endTick = buf.readLong();
        handler.dematType = buf.readRegistryId();
        return handler;
    }

    public void copyFrom(MatterStateHandler handler){
        this.state = handler.state;
        this.startTick = handler.startTick;
        this.endTick = handler.endTick;
        this.dematType = handler.dematType;
        this.onChanged.run();
    }

    public MatterState getMatterState() {
        return this.state;
    }

    public long getStartTick(){
        return this.startTick;
    }

    public long getEndTick(){
        return this.endTick;
    }

    public DematAnimationRegistry.DematAnimationType getDematType(){
        if(this.dematType == null)
            return DematAnimationRegistry.CLASSIC.get();
        return this.dematType;
    }

    public Vec3 getPosition(){
        return this.position.get();
    }

    /**
     * Mostly used for particles
     * @return
     */
    public float getRadius(){
        return this.radius.get();
    }
}
