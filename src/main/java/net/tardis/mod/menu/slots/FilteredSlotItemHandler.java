package net.tardis.mod.menu.slots;

import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.ItemStack;
import net.minecraftforge.items.IItemHandler;
import net.minecraftforge.items.SlotItemHandler;
import org.jetbrains.annotations.NotNull;

import java.util.function.Predicate;

public class FilteredSlotItemHandler extends SlotItemHandler {

    final Predicate<ItemStack> stackTest;

    public FilteredSlotItemHandler(IItemHandler itemHandler, Predicate<ItemStack> stackTest, int index, int xPosition, int yPosition) {
        super(itemHandler, index, xPosition, yPosition);
        this.stackTest = stackTest;
    }

    @Override
    public boolean mayPlace(@NotNull ItemStack stack) {

        return (stackTest.test(stack) || stack.isEmpty()) ? super.mayPlace(stack) : false;
    }

    @Override
    public boolean mayPickup(Player playerIn) {
        return super.mayPickup(playerIn);
    }
}
