package net.tardis.mod.menu;

import net.minecraft.core.BlockPos;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.network.FriendlyByteBuf;
import net.minecraft.world.entity.player.Inventory;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.inventory.AbstractContainerMenu;
import net.minecraft.world.inventory.Slot;
import net.minecraft.world.item.ItemStack;
import net.minecraftforge.items.ItemStackHandler;
import net.minecraftforge.items.SlotItemHandler;
import net.tardis.mod.blockentities.crafting.DraftingTableTile;
import net.tardis.mod.helpers.GuiHelper;
import net.tardis.mod.menu.slots.FilteredSlotItemHandler;

public class DraftingTableMenu extends AbstractContainerMenu {

    public DraftingTableTile table;

    public DraftingTableMenu(int id, Inventory inventory, DraftingTableTile table){
        super(MenuRegistry.DRAFTING_TABLE.get(), id);
        this.table = table;
        this.setupSlots(inventory);

    }

    public DraftingTableMenu(int id, Inventory inv, FriendlyByteBuf buf){
        this(id, inv, getTile(inv, buf.readBlockPos(), buf.readNbt()));
    }

    public static DraftingTableTile getTile(Inventory inv, BlockPos pos, CompoundTag tag){
        if(inv.player.level.getBlockEntity(pos) instanceof DraftingTableTile tile){
            tile.deserializeNBT(tag);
            return tile;
        }
        return null;
    }

    public void setupSlots(Inventory inv){

        final int startX = 30;
        final int startY = 17;

        for(int i = 0; i < 9; ++i){
            this.addSlot(new Slot(this.table, i,
                    startX + (i % 3) * 18,
                    startY + (i / 3) * 18));
        }

        this.addSlot(new FilteredSlotItemHandler(this.table.inventory, stack -> false, 9, 129, 23));

        for(Slot s : GuiHelper.getPlayerSlots(inv, 8, 84)){
            this.addSlot(s);
        }
    }

    @Override
    public ItemStack quickMoveStack(Player pPlayer, int pIndex) {

        final Slot slot = this.getSlot(pIndex);

        //From Table to player
        if(slot.container == this.table) {
            if (pPlayer.addItem(slot.getItem())) {
                slot.set(ItemStack.EMPTY);
                return ItemStack.EMPTY;
            }
        }
        //From player to table
        else{
            for(Slot s : this.slots){
                if(s.container == table){
                    final ItemStack prevItem = slot.getItem();
                    s.safeInsert(slot.getItem());
                    if(prevItem != slot.getItem()){
                        slot.set(prevItem);
                        return ItemStack.EMPTY;
                    }
                }
            }
        }

        return ItemStack.EMPTY;
    }

    @Override
    public boolean stillValid(Player pPlayer) {
        return true;
    }
}
