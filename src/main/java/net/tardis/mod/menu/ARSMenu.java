package net.tardis.mod.menu;

import net.minecraft.network.FriendlyByteBuf;
import net.minecraft.world.entity.player.Inventory;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.inventory.AbstractContainerMenu;
import net.minecraft.world.inventory.Slot;
import net.minecraft.world.item.ItemStack;
import net.minecraftforge.items.SlotItemHandler;
import net.tardis.mod.blockentities.ARSPanelTile;
import net.tardis.mod.helpers.GuiHelper;
import net.tardis.mod.helpers.Helper;
import net.tardis.mod.registry.ItemRegistry;
import net.tardis.mod.menu.slots.FilteredSlotItemHandler;

import java.util.Optional;

public class ARSMenu extends AbstractContainerMenu {

    private ARSPanelTile panel;

    protected ARSMenu(int pContainerId) {
        super(MenuRegistry.ARS.get(), pContainerId);
    }

    //Client
    public ARSMenu(int windowId, Inventory inv, FriendlyByteBuf buf){
        this(windowId);
        if(inv.player.getLevel().getBlockEntity(buf.readBlockPos()) instanceof ARSPanelTile panel){
            this.setup(inv, panel);
        }
    }

    public ARSMenu(int windowId, Inventory inv, ARSPanelTile panel){
        this(windowId);
        this.setup(inv, panel);
    }

    public void setup(Inventory inv, ARSPanelTile panel){
        this.panel = panel;
        this.addSlot(new FilteredSlotItemHandler(panel.getInventory(), stack -> stack.getItem() == ItemRegistry.DATA_CRYSTAL.get(), 0, 33, 20));
        this.addSlot(new SlotItemHandler(panel.getInventory(), 1, 61, 20));
        this.addSlot(new SlotItemHandler(panel.getInventory(), 2, 47, 47));
        this.addSlot(new SlotItemHandler(panel.getInventory(), 3, 97, 49));

        for(Slot slot : GuiHelper.getPlayerSlots(inv, 8, 84)){
            this.addSlot(slot);
        }

    }

    @Override
    public ItemStack quickMoveStack(Player pPlayer, int pIndex) {
        return ItemStack.EMPTY;
    }

    public Optional<ARSPanelTile> getPanel(){
        return Helper.nullableToOptional(this.panel);
    }

    @Override
    public boolean stillValid(Player pPlayer) {
        return true;
    }
}
