package net.tardis.mod.menu.quantiscope;

import net.minecraft.core.BlockPos;
import net.minecraft.network.FriendlyByteBuf;
import net.minecraft.world.entity.player.Inventory;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.inventory.AbstractContainerMenu;
import net.minecraft.world.inventory.ContainerLevelAccess;
import net.minecraft.world.inventory.MenuType;
import net.minecraft.world.item.ItemStack;
import net.tardis.mod.registry.BlockRegistry;
import net.tardis.mod.blockentities.machines.QuantiscopeTile;
import net.tardis.mod.blockentities.machines.quantiscope_settings.QuantiscopeSetting;

import java.util.Optional;
import java.util.function.Supplier;

public abstract class BaseQuantiscopeMenu<T extends QuantiscopeSetting> extends AbstractContainerMenu {

    public final Supplier<? extends MenuType<? extends BaseQuantiscopeMenu<T>>> menuType;
    public T setting;

    protected BaseQuantiscopeMenu(Supplier<? extends MenuType<? extends BaseQuantiscopeMenu<T>>> pMenuType, int pContainerId) {
        super(pMenuType.get(), pContainerId);
        this.menuType = pMenuType;
    }

    public static Optional<QuantiscopeSetting> getSettingFrom(Inventory inv, FriendlyByteBuf buf){
        final BlockPos pos = buf.readBlockPos();
        if(inv.player.level.getBlockEntity(pos) instanceof QuantiscopeTile tile){
            return Optional.of(tile.currentSetting);
        }
        return Optional.empty();
    }

    public abstract void setup(Inventory player, T setting);

    @Override
    public ItemStack quickMoveStack(Player pPlayer, int pIndex) {
        return ItemStack.EMPTY;
    }

    @Override
    public boolean stillValid(Player pPlayer) {
        if(setting == null)
            return false;
        return stillValid(ContainerLevelAccess.create(pPlayer.level, setting.getParent().getBlockPos()), pPlayer, BlockRegistry.QUANTISCOPE.get());
    }
}
