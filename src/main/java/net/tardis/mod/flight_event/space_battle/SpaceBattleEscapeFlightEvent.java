package net.tardis.mod.flight_event.space_battle;

import net.tardis.mod.cap.level.ITardisLevel;
import net.tardis.mod.control.datas.ControlData;
import net.tardis.mod.flight_event.FlightEvent;
import net.tardis.mod.flight_event.FlightEventType;
import net.tardis.mod.registry.ControlRegistry;

public class SpaceBattleEscapeFlightEvent extends FlightEvent {
    public SpaceBattleEscapeFlightEvent(FlightEventType type, ITardisLevel level) {
        super(type, level);
    }

    @Override
    public boolean onControlUse(ControlData<?> control) {
        if(control.getType() == ControlRegistry.THROTTLE.get()){
            this.complete();
            return true;
        }
        return false;
    }

    @Override
    public void onFail() {
        super.onFail();
        this.tardis.setCurrentFlightEvent(this.getType());
    }

    @Override
    public void onStart() {

    }
}
