package net.tardis.api.artron;

public interface IArtronSink {

    /**
     * Take artron from this item
     * @param artronToRecieve - the max amount to take
     * @param simulate
     * @return how much was actually taken from this item
     */
    float takeArtron(float artronToRecieve, boolean simulate);

}
