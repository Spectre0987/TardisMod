package net.tardis.api.artron;

public interface IArtronStorage extends IArtronReciever, IArtronSink{

    float getStoredArtron();
    float getMaxArtron();

}
