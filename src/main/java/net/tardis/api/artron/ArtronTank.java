package net.tardis.api.artron;

import net.minecraft.nbt.FloatTag;
import net.minecraftforge.common.util.INBTSerializable;

/**
 * Default implementation of {@link IArtronStorage}
 */
public class ArtronTank implements IArtronStorage, INBTSerializable<FloatTag> {

    float artron = 0;
    final float maxArtron;

    Runnable onUpdated = () -> {};

    public ArtronTank(float maxArtron){
        this.maxArtron = maxArtron;
    }

    public ArtronTank withUpdateListener(Runnable update){
        this.onUpdated = update;
        return this;
    }

    @Override
    public float fillArtron(float artronToFill, boolean simulate) {
        final float room = this.maxArtron - this.artron;
        final float filled = room > artronToFill ? artronToFill : room;

        if(!simulate){
            final float oldArtron = this.artron;
            this.artron += filled;
            this.update(oldArtron, this.artron);
        }

        return filled;
    }

    @Override
    public float takeArtron(float artronToRecieve, boolean simulate) {

        final float taken = this.artron > artronToRecieve ? artronToRecieve : artron;

        if(!simulate){
            final float oldArtron = this.artron;
            this.artron -= taken;
            this.update(oldArtron, this.artron);
        }

        return taken;
    }

    public void update(float oldVal, float newVal){
        if(oldVal != newVal){
            this.onUpdated.run();
        }
    }

    @Override
    public float getStoredArtron() {
        return this.artron;
    }

    @Override
    public float getMaxArtron() {
        return this.maxArtron;
    }

    @Override
    public FloatTag serializeNBT() {
        return FloatTag.valueOf(this.artron);
    }

    @Override
    public void deserializeNBT(FloatTag nbt) {
        this.artron = nbt.getAsFloat();
    }
}
